#!/bin/bash
echo "Update the following players:"
for d in players/* ; do
	if [[ "$d" != "players/battlecode-maps" && ($d != *.bak) && ($d != *.bc18) && ($d != *.sh) ]]
	then
		echo "$d"
		cp -r src/tools $d/.
	fi
done
