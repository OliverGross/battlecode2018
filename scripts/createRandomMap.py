import numpy as np
import sys

args = sys.argv
n = int(args[1])
p = float(args[2])

arr = np.random.choice([0, 1], size=(n,n), p=[1.-p, p])

start = None
while start is None:
    x = np.random.randint(5)
    y = np.random.randint(5)
    if arr[x,y] == 0:
        start = (x,y)
end = None
while end is None:
    x = np.random.randint(n-5, n)
    y = np.random.randint(n-5, n)
    if arr[x,y] == 0 and (x,y) != start:
        end = (x,y)

print('%d %d' % (n, n))
for i in range(n):
    for j in range(n):
        if (i,j) == start:
            print('S', end='')
        elif (i,j) == end:
            print('G', end='')
        elif arr[i,j] == 1:
            print('#', end='')
        else:
            print('.', end='')
    print()
