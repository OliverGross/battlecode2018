package tools.infocaching;

import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;

public class KarboniteInformation {

	private long width;
	private long height;
	private long[][] karboniteArray;
	private long[][] startingMap = null;
	
	/**
	 * 
	 * @param planetMap the starting Map for Planet p
	 * @param p Planet for which the KarboniteInformation holds
	 */
	public KarboniteInformation(PlanetMap planetMap, Planet p) {
		width = planetMap.getWidth();
		height = planetMap.getHeight();
		karboniteArray = new long[(int) width][(int) height];
		MapLocation loc = new MapLocation(Planet.Earth, 0, 0);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				loc.setX(x);
				loc.setY(y);
				planetMap.initialKarboniteAt(loc);
			}
		}
		startingMap = deepCopy(karboniteArray, width, height);
	}
	
	/**
	 * 
	 * @param oldInfo the new information takes startingMap, height and width from the old information aswell as a deep copy of the old karboniteArray
	 */
	public KarboniteInformation(KarboniteInformation oldInfo) {
		this.karboniteArray = deepCopy(oldInfo.karboniteArray, oldInfo.width, oldInfo.height);
		this.startingMap = oldInfo.startingMap; //no deep copy necessary bc it doesnt change
		this.height = oldInfo.height;
		this.width = oldInfo.width;
	}
	
	public final long[][] getKarboniteDeposits() {
		return karboniteArray;
	}
	
	/**
	 * 
	 * @param location Location on the map
	 * @param karbonite Karbonite on location
	 */
	public void update(MapLocation location, long karbonite) {
		int x = location.getX();
		int y = location.getY();
		this.karboniteArray[x][y] = karbonite;
	}
	
	/**
	 * 
	 * @param oldInfo KarboniteInformation to which the current state is compared
	 * @return 2-dimen-array. For every value returns: currentstate[x][y] - oldstate[x][y]
	 */
	public long[][] diff(KarboniteInformation oldInfo) {
		long[][] karboniteDiff = new long[(int)width][(int)height];
		for(int x=0; x<width; x++) {
			for(int y=0; y<height;y++) {
				karboniteDiff[x][y] = this.karboniteArray[x][y] - oldInfo.karboniteArray[x][y];
			}
		}
		return karboniteDiff;
	}
	
	/**
	 * 
	 * @return long[x][y] map of the difference between the current state and the starting state
	 * For example: starting state for (0,0) was 10 Karbonite. Now there is 5. The function returns -5 for (0,0)
	 * returns null if there is no startingMap
	 */
	public long[][] diffThisToStart() {
		if(startingMap != null) {
			long[][] diff = new long[(int) width][(int) height];
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					diff[x][y] = karboniteArray[x][y] - startingMap[x][y];
				}
			}
			return diff;
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @return KarboniteNow - KarboniteAtStart OR -1 if startingMap is null (which shouldnt happen)
	 */
	public long diffThisToStartForPoint(int x, int y) {
		if(startingMap != null) {
			return karboniteArray[x][y] - startingMap[x][y];
		} else {
			return -1;
		}
	}
	
	/**
	 * 
	 * @param loc
	 * @return KarboniteNow - KarboniteAtStart OR -1 if startingMap is null (which shouldnt happen)
	 */
	public long diffThisToStartForPoint(MapLocation loc) {
		return diffThisToStartForPoint(loc.getX(), loc.getY());
	}
	
	/**
	 * 
	 * @param obj object to copy
	 * @param width known width for convenience
	 * @param height known height for convenience
	 * @return a deep copy of obj
	 */
	public long[][] deepCopy(long[][] obj, long width, long height) {
		long[][] ret = new long[(int)width][(int)height];
		for(int x = 0; x<width; x++) {
			for (int y=0; y<height; y++) {
				ret[x][y] = obj[x][y];
			}
		}
		return ret;
	}

}