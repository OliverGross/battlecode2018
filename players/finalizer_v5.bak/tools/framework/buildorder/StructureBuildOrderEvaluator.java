package tools.framework.buildorder;

import bc.UnitType;
import tools.framework.WorldState;

public interface StructureBuildOrderEvaluator {
	
	
	public UnitType getNextUnit(WorldState ws);

}
