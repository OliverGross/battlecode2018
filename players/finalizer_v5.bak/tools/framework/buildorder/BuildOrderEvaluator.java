package tools.framework.buildorder;

import bc.UnitType;
import tools.framework.WorldState;

public interface BuildOrderEvaluator {

	public UnitType getNextUnit(WorldState ws);
	
}
