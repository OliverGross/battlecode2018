package tools.framework.behaviours;

import java.util.Iterator;

import bc.Direction;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.MarsProgramPlan;

public class RocketLoadBehaviour extends Behaviour {
	
	MarsProgramPlan parent;
	static int launchCounter = 0;

	public RocketLoadBehaviour(WorldState worldState,
			MarsProgramPlan marsProgramPlan) {
		parent = marsProgramPlan;
	}

	@Override
	public void act(WorldState ws) {
		boolean loaded = false;
		
		for (Iterator<Integer> it = parent.evacuees.iterator(); it.hasNext();) {
			int evacuee = it.next();
			loaded = false;
			for (int rocket_id : parent.loadableRockets) {
				if (ws.gc.canLoad(rocket_id, evacuee)) {
					ws.gc.load(rocket_id, evacuee);
					it.remove();
					loaded = true;
					break;
				}
			}
			if (!loaded) {
				if (ws.gc.isMoveReady(evacuee) && parent.loadableRockets.size() > 1) {
					Direction nextDir = ws.pathfinding.getNextDirection(evacuee,
							parent.loadableRocketsSites);
					if (ws.gc.canMove(evacuee, nextDir)) {
						ws.gc.moveRobot(evacuee, nextDir);
					}
				}
			}
		}
	}
}
