package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;

public class TargetInfo {
	int x;
	int y;
	long health;
	long defense;
	MapLocation location;
	int targetId;
	UnitType type;
	List<Integer> unitsThatTarget;

	public TargetInfo(ArrayList<Integer> unitsThatTarget, Unit target) {
		if (target.location().isOnMap()) {
			type = target.unitType();
			health = target.health();
			location = target.location().mapLocation();
			targetId = target.id();
			this.unitsThatTarget = new ArrayList<Integer>(unitsThatTarget);
		} else {
			location = null;
			throw new RuntimeException("Target location was not on map!");
		}
		if (target.unitType() == UnitType.Knight) {
			this.defense = target.knightDefense();
		} else {
			this.defense = 0;
		}
	}

	public void deleteAttacker(Unit u) {
		// System.out.println("before:" + unitsThatTarget.size());
		// System.out.println("removing:" + u.id());
		// for (int i = 0; i < unitsThatTarget.size(); i++)
		// System.out.println(unitsThatTarget.get(i));
		if (!unitsThatTarget.remove(Integer.valueOf(u.id())))
			throw new RuntimeException("This should not have happend!");
		// System.out.println("after:" + unitsThatTarget.size());

		// for (int i = 0; i < unitsThatTarget.size(); i++)
		// System.out.println(unitsThatTarget.get(i));
	}

	public int leftHealth() {
		return (int) (health - unitsThatTarget.size() * (30 - defense));
	}

	public boolean isDead() {
		long damage = unitsThatTarget.size() * (30 - defense);
		return damage >= health;
	}
}
