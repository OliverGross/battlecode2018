package tools;

import java.util.ArrayList;
import java.util.List;

import bc.Direction;
import bc.GameController;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Unit;
import bc.bc;
import tools.pathfinding.Pathfinding;

public class LowLevelLogic {
	GameController gc;
	Pathfinding pathfinding;

	public LowLevelLogic(GameController gameController,
			Pathfinding pathfinding) {
		gc = gameController;
		this.pathfinding = pathfinding;
	}

	// public boolean buildStructre(int worker_id, UnitType unit_type,
	// MapLocation location) {
	// Unit unitAtLocation = gc.senseUnitAtLocation(location);
	// Unit workerUnit = gc.unit(worker_id);
	// MapLocation workerLocation = workerUnit.location().mapLocation();
	//
	// int unitId = unitAtLocation.id();
	// if (location.isAdjacentTo(workerLocation)) {
	// if (gc.canBuild(worker_id, unitId)) {
	// gc.build(worker_id, unitId);
	// return true;
	// } else {
	// Direction direction = workerLocation.directionTo(location);
	// if (gc.canBlueprint(worker_id, unit_type, direction)) {
	// gc.blueprint(worker_id, unit_type, direction);
	// }
	// }
	// }
	//
	// else {
	// goTo(worker_id, location);
	// }
	// return false;
	// }

	public boolean canReallyMove(int uid, Direction dir) {
		return gc.canMove(uid, dir) && gc.unit(uid).movementHeat() < 10;
	}

	// public void goTo(int unit_id, List<MapLocation> locations) {
	// Direction dir = pathfinding.getNextDirection(unit_id, locations);
	// if (dir == null) {
	// dir = Direction.Center;
	// }
	// if (canReallyMove(unit_id, dir)) {
	// gc.moveRobot(unit_id, dir);
	// } else {
	// // gc.moveRobot(unit_id, direction);
	// // TODO should instead call goTo again, when jps is implemented
	// }
	// }

	// public void goTo(int unit_id, List<MapLocation> locations,
	// LocalPathFinder lpf) {
	// Direction dir = pathfinding.getNextDirection(unit_id, locations);
	// if (dir == null) {
	// dir = Direction.Center;
	// }
	// if (canReallyMove(unit_id, dir)) {
	// gc.moveRobot(unit_id, dir);
	// } else {
	// // gc.moveRobot(unit_id, direction);
	// // TODO should instead call goTo again, when jps is implemented
	// }
	// }

	// public void goTo(int unit_id, MapLocation location) {
	// goTo(unit_id, Arrays.asList(location));
	// }

	public boolean unloadUnits(List<Integer> unit_ids, int structure_id) {
		List<Direction> emptyFields = getEmptyFields(structure_id);
		if (emptyFields.size() == 0) {
			return false;
		}
		for (int i = 0; i < emptyFields.size(); i++) {
			if (gc.canUnload(structure_id, emptyFields.get(i))) {
				gc.unload(structure_id, emptyFields.get(i));
			}

		}
		return true;
	}

	public List<Direction> getEmptyFields(int unit_id) {
		Unit unit = gc.unit(unit_id);
		MapLocation location = unit.location().mapLocation();
		Direction[] dirs = Direction.values();
		List<Direction> returnList = new ArrayList<Direction>();
		for (int i = 0; i < 8; i++) {
			if (!(gc.hasUnitAtLocation(location.add(dirs[i])))) {
				returnList.add(dirs[i]);
			}
		}
		return returnList;
	}

	public List<Direction> getPassableFields(int unit_id) {
		Unit unit = gc.unit(unit_id);
		if (unit.location().isOnMap())
			return _getPassableFields(unit.location().mapLocation());
		else
			return new ArrayList<Direction>();
	}

	private List<Direction> _getPassableFields(MapLocation mapLocation) {
		PlanetMap map = gc.startingMap(mapLocation.getPlanet());
		List<Direction> returnList = new ArrayList<Direction>();
		for (Direction d : Direction.values()) {
			if (map.isPassableTerrainAt(mapLocation) == 1) {
				returnList.add(d);
			}
		}
		return returnList;
	}

	public Direction kite(int unit_id, Direction direction) {
		Direction endDir = bc.bcDirectionOpposite(direction);
		if (gc.canMove(unit_id, endDir)) {
			return endDir;
		} else if (gc.canMove(unit_id, bc.bcDirectionRotateRight(endDir))) {
			// System.out.println("couldnt move " + endDir + "so I moved " +
			// bc.bcDirectionRotateRight(endDir));
			return bc.bcDirectionRotateRight(endDir);
		} else if (gc.canMove(unit_id, bc.bcDirectionRotateLeft(endDir))) {
			return bc.bcDirectionRotateLeft(endDir);
		}
		return Direction.Center;
	}
}
