package tools.framework.behaviours;

import bc.Direction;
import bc.MapLocation;
import bc.UnitType;
import bc.bc;
import tools.DirHelp;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.MarsProgramPlan;

public class RocketBuildBehaviour extends Behaviour {
	MarsProgramPlan parent;

	public RocketBuildBehaviour(WorldState worldState,
			MarsProgramPlan marsProgramPlan) {
		parent = marsProgramPlan;
	}

	@Override
	public void act(WorldState ws) {
		// Worker behavior
		// replicate

		if (parent.rocketScientist.size() <= parent.minScientist) {
			for (int worker_id : parent.rocketScientist) {
				for (Direction d : DirHelp.adjacentDirections) {
					if (ws.gc.canReplicate(worker_id, d)) {
						ws.gc.replicate(worker_id, d);
						ws.resources.assignById(
								ws.gc.senseUnitAtLocation(ws.gc.unit(worker_id)
										.location().mapLocation().add(d)).id(),
								parent.pid);
						break;
					}
				}
			}
			parent.rocketScientist = ws.resources
					.getUnitsByPlanAndType(parent.pid, UnitType.Worker);
		}
		// construction of rockets
		boolean buildOrBluePrinted = false;
		int rocketCost = (int) bc.costOf(UnitType.Rocket,
				ws.gc.researchInfo().getLevel(UnitType.Rocket));

		for (int worker_id : parent.rocketScientist) {
			buildOrBluePrinted = false;
			for (int rocket_id : parent.buildableRockets) {
				if (ws.gc.canBuild(worker_id, rocket_id)) {
					ws.gc.build(worker_id, rocket_id);
					buildOrBluePrinted = true;
					break;
				}
			}
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canBlueprint(worker_id, UnitType.Rocket, d)
						&& ws.resources
								.getAvailableKarbonite(parent.pid) >= rocketCost
						&& parent.loadableRockets
								.size() < parent.maxNumLoadableRockets
						&& parent.buildableRockets
								.size() < parent.maxNumBuildableRockets) {
					System.out.println("Blueprinting a rocket");
					ws.gc.blueprint(worker_id, UnitType.Rocket, d);
					MapLocation newBluePrint = ws.gc.unit(worker_id).location()
							.mapLocation().add(d);
					parent.constructionSites.add(newBluePrint);
					ws.resources.assignById(
							ws.gc.senseUnitAtLocation(newBluePrint).id(),
							parent.pid);
					buildOrBluePrinted = true;
					break;
				}

			}
			// Probably move if nothing was done so far
			if (!buildOrBluePrinted) {
				if (ws.gc.isMoveReady(worker_id)
						&& parent.rocketResearchLevel >= 1
						&& parent.constructionSites.size() > 0) {
					Direction nextDir = ws.pathfinding.getNextDirection(
							worker_id, parent.constructionSites);
					if (nextDir != null) {
						if (ws.gc.canMove(worker_id, nextDir)) {
							ws.gc.moveRobot(worker_id, nextDir);
						}
					}
				}
			}

		}
	}
}
