package tools.framework.plans;

import java.util.ArrayList;

import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.BehaviourPlan;
import tools.framework.WorldState;
import tools.framework.behaviours.HealerHealBehaviour;
import tools.framework.behaviours.HealerMoveBehaviour;
import tools.framework.behaviours.HealerOverchargeBehaviour;
import tools.framework.behaviours.UnloadBehaviour;

public class HealerPlan extends BehaviourPlan {
	ArrayList<Integer> healersOnMap = new ArrayList<>();

	public HealerPlan(WorldState worldState) {
		super(worldState);
		Behaviour healBehaviour = new HealerHealBehaviour(this);
		Behaviour moveBehaviour = new HealerMoveBehaviour(this);
		Behaviour unloadBehaviour = new UnloadBehaviour();
		Behaviour overChargeBehaviour = new HealerOverchargeBehaviour(this);
		
		// unload all Units
		addBehaviour(unloadBehaviour);
		// heal nearby units that are low
		addBehaviour(healBehaviour);
		//tryOvercharge
		addBehaviour(overChargeBehaviour);
		// move towards enemy or escape/kite
		addBehaviour(moveBehaviour);
		// try heal again after move
		addBehaviour(healBehaviour);
		//try overcharge again after moving
		addBehaviour(overChargeBehaviour);
	}

	@Override
	public void tick(WorldState ws) {
		updateHealers(ws);
		super.tick(ws);
	}

	private void updateHealers(WorldState ws) {
		ws.resources.assignAllFreeUnitsByTypeToPlan(pid, UnitType.Healer);
		healersOnMap = ws.resources.getUnitsByPlan(pid);
	}

	public ArrayList<Integer> getHealersOnMap() {
		return healersOnMap;
	}
}
