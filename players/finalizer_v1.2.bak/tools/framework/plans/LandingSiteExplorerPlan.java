package tools.framework.plans;

import java.util.ArrayList;

import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.Veci32;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class LandingSiteExplorerPlan extends Plan {
	boolean[][] map = null;
	int w;
	int h;
	public static final int maxLocations = 99;
	ArrayList<MapLocation> landingSites = new ArrayList<MapLocation>();

	public LandingSiteExplorerPlan(WorldState worldstate) {
		super(worldstate);
		resetTeamArray();
		initializeMap(worldstate);
		findLandingSites();
	}

	@Override
	public void tick(WorldState ws) {
		for (MapLocation loc : landingSites) {
			appendLocToArrayList(ws, loc);
		}
		System.out.println("====LandingSiteExplorerPlan:Debug====");
		System.out.println("Location 1(READ):"
				+ LandingSiteExplorerPlan.getLocFromTeamArray(ws, 1));
		System.out.println("Location 2(READ):"
				+ LandingSiteExplorerPlan.getLocFromTeamArray(ws, 2));
		System.out.println("Location 3(READ):"
				+ LandingSiteExplorerPlan.getLocFromTeamArray(ws, 3));
		System.out.println("Wrote a total of "
				+ LandingSiteExplorerPlan.getTotalLandingSites(ws)
				+ " landing sites.");
		System.out.println("=====================================");
		this.status = Status.FINAL;
	}

	void initializeMap(WorldState ws) {
		PlanetMap pmap = ws.gc.startingMap(ws.gc.planet());
		Planet planet = pmap.getPlanet();
		w = (int) pmap.getWidth();
		h = (int) pmap.getHeight();
		map = new boolean[w][h];
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				map[x][y] = pmap.isPassableTerrainAt(
						new MapLocation(planet, x, y)) == 0;
			}
	}

	public void findLandingSites() {
		boolean safeSite = true;
		for (MapLocation stratPoint : ws.pga.strategicLocations) {
			if (!isPassableTerrain(stratPoint)) {
				continue;
			} else if (landingSites.size() > maxLocations) {
				break;
			} else if (landingSites.size() == 0) {
				landingSites.add(stratPoint);
			} else {
				safeSite = true;
				for (MapLocation landingSite : landingSites) {
					if (stratPoint.isAdjacentTo(landingSite)) {
						safeSite = false;
						break;
					}
				}
				if (safeSite) {
					landingSites.add(stratPoint);
				}
			}
		}
	}

	void resetTeamArray() {
		Veci32 teamArray = ws.gc.getTeamArray(Planet.Mars);
		for (int i = 0; i < teamArray.size(); i++) {
			ws.gc.writeTeamArray(i, 0);
		}
	}

	private boolean isPassableTerrain(MapLocation loc) {
		return !(map[loc.getX()][loc.getY()]);

	}

	void appendLocToArrayList(WorldState ws, MapLocation loc) {
		int i = ws.gc.getTeamArray(Planet.Mars).get(0);
		if (i >= maxLocations) {
			return;
		} else {
			ws.gc.writeTeamArray(0, i + 1);
			ws.gc.writeTeamArray(i + 1,
					(loc.getY() & 0xFFFF) + (loc.getX() << 16));
		}
	}

	/**
	 * Get a location to land on mars.Returns null if i > availablePositions.
	 * \NOTE locations are shuffled on beginning of the game
	 * 
	 * @param ws(WorldState)
	 * @param i(int)
	 * @return MapLocation at position i
	 */
	public static MapLocation getLocFromTeamArray(WorldState ws, int i) {
		Veci32 teamArray = ws.gc.getTeamArray(Planet.Mars);
		int maxLocs = teamArray.get(0);
		if (i > maxLocs)
			return null;
		int xy = teamArray.get(i + 1);
		int x = xy >> 16;
		int y = xy & 0xFFFF;
		return new MapLocation(Planet.Mars, x, y);
	}

	/**
	 * Return total number of available landingSites on Mars
	 * 
	 * @param ws
	 * @return
	 */
	public static long getTotalLandingSites(WorldState ws) {
		return ws.gc.getTeamArray(Planet.Mars).get(0);
	}
}
