package tools.infocaching;

import java.util.ArrayList;

import bc.MapLocation;

public class TurnInformation {

	UnitInformation unitInformation;
	long karbonite;
	KarboniteInformation karboniteInformation;
	ArrayList<MapLocation> seenMapLocations;
	long round;
	
	public TurnInformation(long round, UnitInformation unitInfo, long karbonite, KarboniteInformation karboniteInfo, ArrayList<MapLocation> seenMapLocations) {
		unitInformation = unitInfo;
		this.karbonite = karbonite;
		karboniteInformation = karboniteInfo;
		this.seenMapLocations = seenMapLocations;
		this.round = round;
	}	
}