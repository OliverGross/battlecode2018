package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class RangerBuildPlan extends Plan {

	public int savedKarbonite;

	public RangerBuildPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
		savedKarbonite = 0;
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		int RangerCost = (int) bc.costOf(UnitType.Ranger,
				ws.gc.researchInfo().getLevel(UnitType.Ranger));
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Factory)
				continue;
			if (ws.gc.canProduceRobot(uid, UnitType.Ranger)
					&& ws.availableKarbonite() + savedKarbonite >= RangerCost) {
				ws.gc.produceRobot(uid, UnitType.Ranger);
				System.out.print("Rangerproducing");
				savedKarbonite -= ws
						.spendSavings(Math.max(savedKarbonite, RangerCost));
				this.status = Status.RUNNING;
			} else {
				savedKarbonite += ws.saveKarbonite(RangerCost);
			}
		}
	}

}