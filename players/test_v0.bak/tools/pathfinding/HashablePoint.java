package tools.pathfinding;

import java.util.Arrays;

import bc.MapLocation;

public final class HashablePoint {
	public int[] a = { 0, 0 };

	public HashablePoint(int[] a) {
		this.a[0] = a[0];
		this.a[1] = a[1];
	}

	public HashablePoint(MapLocation a) {
		this.a[0] = a.getX();
		this.a[1] = a.getY();
	}

	@Override
	public int hashCode() {
		return a[0] + 1000 * a[1];
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HashablePoint other = (HashablePoint) obj;
		if (!Arrays.equals(this.a, other.a))
			return false;
		return true;
	}
}
