import bc.Direction;
import bc.GameController;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;

public class Player {
	
	public static void main(String[] args) {
		/*UCGrid testGrid = new UCGrid(11, 11);
		for (int i = 0; i < 11; i++) {
			testGrid.set(5, i, true);
			testGrid.set(7, i, true);
		}
		testGrid.set(7, 0, false);
		testGrid.set(5, 8, false);
		JPS finder = new JPS(testGrid);
		int[] start = { 0, 0 };
		int[] finish = { 10, 0 };
		List<int[]> path = finder.getPath(start, finish);
		System.out.println("The path:");
		String s = "";
		for (int[] a : path) {
			s += ", " + GeoPoint.toString(a);
		}
		System.out.println(s.substring(2));
		*/
		GameController gc = new GameController();
		while(true) {
			System.out.println("Round:" + gc.round());
			VecUnit units = gc.myUnits();
			int workercount = 0;
			//count workers
			for(int i=0; i<units.size(); i++) {
				Unit u = units.get(i);
				if(u.unitType() == UnitType.Worker) {
					workercount++;
				}
			}
			
			// MapLocation buildingPlace = new MapLocation(Planet.Earth, 5, 5);
			for(int i=0; i<units.size(); i++) {
				Unit u = units.get(i);
				if(u.unitType() == UnitType.Worker) {
					if(u.abilityHeat() < 10) {
						if(gc.canReplicate(u.id(), Direction.South) && workercount < 3) {
							gc.replicate(u.id(), Direction.South);
						}
					}
				}
			}

			gc.nextTurn();
		}
	}
}