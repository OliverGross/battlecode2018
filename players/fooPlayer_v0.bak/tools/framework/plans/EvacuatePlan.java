package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import bc.*;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class EvacuatePlan extends Plan {
	// earliest round to launch any rocket
	long launchDay;

	// latest round to launch even
	final long latestLaunch = 745;
	final int maxNumBuildableRockets = 4;
	final int maxNumLoadableRockets = 4;
	final int minScientist = 2;

	ArrayList<Integer> rocketScientist = new ArrayList<Integer>();
	ArrayList<Integer> rocketFactories = new ArrayList<Integer>();
	ArrayList<Integer> buildableRockets = new ArrayList<Integer>();
	// Location of unfinished workSites
	HashSet<MapLocation> constructionSites = new HashSet<MapLocation>();
	ArrayList<Integer> launchableRockets = new ArrayList<Integer>();
	ArrayList<Integer> loadableRockets = new ArrayList<Integer>();
	ArrayList<Integer> evacuees = new ArrayList<Integer>();

	// launch counter
	int launchedRockets = 0;

	// landing sites
	// \TODO find landingSites
	ArrayList<MapLocation> landingSites = new ArrayList<MapLocation>();

	public EvacuatePlan(WorldState worldState, long launchRound) {
		super(worldState);
		initResources(worldState);
		findLandingSites(worldState);
		launchDay = launchRound;
	}

	public void initResources(WorldState ws) {
		VecUnit myUnits = ws.gc.myUnits();
		for (int i = 0; i < myUnits.size(); i++) {
			Unit unit = myUnits.get(i);
			if (unit.unitType().equals(UnitType.Worker)) {
				rocketScientist.add(unit.id());
			} else if (unit.unitType().equals(UnitType.Factory)) {
				rocketFactories.add(unit.id());
			} else if (unit.unitType().equals(UnitType.Rocket)) {
				buildableRockets.add(unit.id());
				constructionSites.add(unit.location().mapLocation());
			} else {
				evacuees.add(unit.id());
			}
		}
	}

	public void findLandingSites(WorldState ws) {
		PlanetMap marsMap = ws.gc.startingMap(Planet.Mars);
		MapLocation loc = new MapLocation(Planet.Mars, 0, 0);
		for (int x = 0; x < marsMap.getHeight(); x++) {
			for (int y = 0; y < marsMap.getWidth(); y++) {
				System.out.println(x + "," + y);
				loc.setX(y);
				loc.setY(x);
				try {
					if (marsMap.isPassableTerrainAt(loc) == 1) {
						landingSites.add(loc);
						x++;
						y++;
					}
				} catch(Exception e) {
					break;
				}
			}
		}
	}

	public void updateResources(WorldState ws) {
		for (Iterator<Integer> it = rocketScientist.iterator(); it.hasNext();) {
			if (!ws.gc.canSenseUnit(it.next())) {
				it.remove();
			}
		}
		for (Iterator<Integer> it = rocketFactories.iterator(); it.hasNext();) {
			if (!ws.gc.canSenseUnit(it.next())) {
				it.remove();
			}
		}
		for (Iterator<Integer> it = loadableRockets.iterator(); it.hasNext();) {
			int id = it.next();
			if (!ws.gc.canSenseUnit(id)) {
				it.remove();
			} else if (ws.gc.unit(id).structureGarrison().size() >= 8
					|| evacuees.size() == 0) {
				launchableRockets.add(id);
				it.remove();
			}
		}
		for (Iterator<Integer> it = buildableRockets.iterator(); it
				.hasNext();) {
			int id = it.next();
			if (!ws.gc.canSenseUnit(id)) {
				it.remove();
			} else if (ws.gc.unit(id).structureIsBuilt() == 1) {
				loadableRockets.add(id);
			}
		}
		for (Iterator<Integer> it = launchableRockets.iterator(); it
				.hasNext();) {
			int id = it.next();
			if (!ws.gc.canSenseUnit(it.next())) {
				it.remove();
			} else if (ws.gc.unit(id).location().isInSpace()) {
				it.remove();
			}
		}
		// update constructionSites \NOTE this has to be done at the end
		constructionSites = new HashSet<MapLocation>();
		for (int rocket_id : buildableRockets) {
			constructionSites
					.add(ws.gc.unit(rocket_id).location().mapLocation());
		}
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		updateResources(ws);
		// check if launchDay has come
		if (ws.gc.round() >= launchDay) {
			// launch every full rocket
			for (int rocket_id : launchableRockets) {
				for (Iterator<MapLocation> it = landingSites.iterator(); it
						.hasNext();) {
					MapLocation loc = it.next();
					if (ws.gc.canLaunchRocket(rocket_id, loc)) {
						ws.gc.launchRocket(rocket_id, loc);
						it.remove();
						break;
					}
				}
			}
		}
		// emergency evacuation of non-full rockets
		if (ws.gc.round() > latestLaunch) {
			for (int rocket_id : loadableRockets) {
				for (Iterator<MapLocation> it = landingSites.iterator(); it
						.hasNext();) {
					MapLocation loc = it.next();
					if (ws.gc.canLaunchRocket(rocket_id, loc)) {
						ws.gc.launchRocket(rocket_id, loc);
						it.remove();
						break;
					}
				}

			}
		}
		// Worker behavior
		// replicate

		if (rocketScientist.size() <= minScientist) {
			ArrayList<Integer> temp = new ArrayList<Integer>();
			for (int worker_id : rocketScientist) {
				for (Direction d : DirHelp.adjacentDirections) {
					if (ws.gc.canReplicate(worker_id, d)) {
						ws.gc.replicate(worker_id, d);
						temp.add(ws.gc.senseUnitAtLocation(ws.gc.unit(worker_id)
								.location().mapLocation().add(d)).id());
						break;
					}
				}
			}
			rocketScientist.addAll(temp);
		}
		// construction of rockets
		boolean buildOrBluePrinted = false;
		int rocketCost = (int) bc.costOf(UnitType.Rocket,
				ws.gc.researchInfo().getLevel(UnitType.Rocket));

		for (int worker_id : rocketScientist) {
			buildOrBluePrinted = false;
			for (int rocket_id : buildableRockets) {
				if (ws.gc.canBuild(worker_id, rocket_id)) {
					ws.gc.build(worker_id, rocket_id);
					buildOrBluePrinted = true;
					break;
				}
			}
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canBlueprint(worker_id, UnitType.Rocket, d)
						&& ws.availableKarbonite() >= rocketCost
						&& loadableRockets.size() < maxNumLoadableRockets
						&& buildableRockets.size() < maxNumBuildableRockets) {
					ws.gc.blueprint(worker_id, UnitType.Factory, d);
					MapLocation newBluePrint = ws.gc.unit(worker_id).location()
							.mapLocation().add(d);
					constructionSites.add(newBluePrint);
					buildableRockets
							.add(ws.gc.senseUnitAtLocation(newBluePrint).id());
					buildOrBluePrinted = true;
					break;
				}

			}
			// Probably move if nothing was done so far
			if (!buildOrBluePrinted) {
				if (ws.gc.isMoveReady(worker_id)) {
					Direction nextDir = Direction.Center;
					if (constructionSites.size() > 0) {
						nextDir = ws.pathfinding.getNextDirection(
								ws.gc.unit(worker_id).location().mapLocation(),
								constructionSites);
					} else {
						for (Direction dir : DirHelp.adjacentDirections) {
							if (ws.gc.canMove(worker_id, dir)) {
								nextDir = dir;
								break;
							}
						}
					}
					if (ws.gc.canMove(worker_id, nextDir)) {
						ws.gc.moveRobot(worker_id, nextDir);
					}
				}
			}

		}
		// Factory add
		for (int factory_id : rocketFactories) {
			if (ws.gc.unit(factory_id).structureGarrison().size() > 0) {
				for (Direction d : Direction.values()) {
					if (ws.gc.canUnload(factory_id, d)) {
						ws.gc.unload(factory_id, d);
						evacuees.add(ws.gc.senseUnitAtLocation(
								ws.gc.unit(factory_id).location().mapLocation())
								.id());
					}
				}
			}
			if (ws.gc.canProduceRobot(factory_id, UnitType.Ranger)
					&& evacuees.size() < loadableRockets.size() * 8) {
				ws.gc.produceRobot(factory_id, UnitType.Ranger);
			}
		}
		// Evacuees behavior
		boolean loaded = false;
		for (int evacuee : evacuees) {
			loaded = false;
			for (int rocket_id : loadableRockets) {
				if (ws.gc.canLoad(rocket_id, evacuee)) {
					ws.gc.load(rocket_id, evacuee);
					evacuees.remove(Integer.valueOf(evacuee));
					loaded = true;
					break;
				}
			}
			if (!loaded) {
				if (ws.gc.isMoveReady(evacuee)
						&& constructionSites.size() > 0) {
					Direction nextDir = ws.pathfinding.getNextDirection(
							ws.gc.unit(evacuee).location().mapLocation(),
							constructionSites);
					if (ws.gc.canMove(evacuee, nextDir)) {
						ws.gc.moveRobot(evacuee, nextDir);
					}
				}
			}
		}

	}

}
