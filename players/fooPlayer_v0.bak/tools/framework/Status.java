package tools.framework;

/**
 * The idea of this framework is to have plans that can be quite complicated and
 * even allow long-running plans over multiple ticks. Therefore it makes sense
 * to introduce for every plan a status variable describing its current abstract
 * state.
 */
public enum Status {
	PENDING, RUNNING, FINAL, FAILED;

	/**
	 * Returns the combination of two statuses following rules:
	 * <ul>
	 * <li>either of a and b failed, the combination failed</li>
	 * <li>one status is final, return the other one</li>
	 * <li>one is running, one pending, return running</li>
	 * <li>they are the same, then it is obvious what to return</li>
	 * </ul>
	 */
	public static Status combine(Status a, Status b) {
		if (a == Status.FAILED || b == Status.FAILED)
			return Status.FAILED;
		if (a == b)
			return a;
		return Status.RUNNING;
	}

	public boolean isTerminal() {
		if (this == Status.FAILED || this == Status.FINAL)
			return true;
		return false;
	}
}