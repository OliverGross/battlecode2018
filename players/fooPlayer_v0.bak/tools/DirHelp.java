package tools;

import java.util.HashMap;
import java.util.Map;

import bc.Direction;
import tools.pathfinding.GeoPoint;
import tools.pathfinding.HashablePoint;

public final class DirHelp {
	private DirHelp() {

	}

	public static final Direction[] adjacentDirections = { Direction.North,
			Direction.Northeast, Direction.East, Direction.Southeast,
			Direction.South, Direction.Southwest, Direction.West,
			Direction.Northwest };

	public static final Direction[] orthogonalDirections = { Direction.North,
			Direction.East, Direction.South, Direction.West };

	public static final Direction[] diagonalDirections = { Direction.Northeast,
			Direction.Southeast, Direction.Southwest, Direction.Northwest };

	public static final Map<Direction, HashablePoint> directionPoint = new HashMap<Direction, HashablePoint>() {
		private static final long serialVersionUID = -2897680512329612630L;

		{
			put(Direction.North, new HashablePoint(new int[] { 0, 1 }));
			put(Direction.Northeast, new HashablePoint(new int[] { 1, 1 }));
			put(Direction.East, new HashablePoint(new int[] { 1, 0 }));
			put(Direction.Southeast, new HashablePoint(new int[] { 1, -1 }));
			put(Direction.South, new HashablePoint(new int[] { 0, -1 }));
			put(Direction.Southwest, new HashablePoint(new int[] { -1, -1 }));
			put(Direction.West, new HashablePoint(new int[] { -1, 0 }));
			put(Direction.Northwest, new HashablePoint(new int[] { -1, 1 }));
			put(Direction.Center, new HashablePoint(new int[] { 0, 0 }));
		}
	};

	public static final Direction[][] directionsByPoints = new Direction[][] {
			{ Direction.Southwest, Direction.West, Direction.Northwest },
			{ Direction.South, Direction.Center, Direction.North },
			{ Direction.Southeast, Direction.East, Direction.Northeast } };

	public static final Map<Direction, Integer> directionIndex = new HashMap<Direction, Integer>() {
		private static final long serialVersionUID = -2897680512329612630L;

		{
			put(Direction.North, 0);
			put(Direction.Northeast, 1);
			put(Direction.East, 2);
			put(Direction.Southeast, 3);
			put(Direction.South, 4);
			put(Direction.Southwest, 5);
			put(Direction.West, 6);
			put(Direction.Northwest, 7);
		}
	};

	public static final Map<Direction, Boolean> isDiagonal = new HashMap<Direction, Boolean>() {
		private static final long serialVersionUID = -2897680512329612630L;

		{
			put(Direction.North, false);
			put(Direction.South, false);
			put(Direction.West, false);
			put(Direction.East, false);
			put(Direction.Northeast, true);
			put(Direction.Northwest, true);
			put(Direction.Southeast, true);
			put(Direction.Southwest, true);
			put(Direction.Center, false);
		}
	};

	public static boolean isDiagonal(Direction dir) {
		return isDiagonal.get(dir);
	}

	public static Direction addAngle(Direction dir, int a) {
		return getDirection(getDirectionIdx(dir) + a);
	}

	public static int getDirectionIdx(Direction dir) {
		return directionIndex.get(dir);
	}

	public static Direction getDirection(int idx) {
		return adjacentDirections[((idx % 8) + 8) % 8];
	}

	public static Direction getDirection(int[] p) {
		return directionsByPoints[p[0] + 1][p[1] + 1];
	}

	public static int[] getPoint(Direction dir) {
		return directionPoint.get(dir).a;
	}

	public static int[] addDirection(int[] p, Direction d) {
		return GeoPoint.add(p, getPoint(d));
	}

	public static int diffAngle(Direction a, Direction b) {
		int diff = (((getDirectionIdx(a) - getDirectionIdx(b)) % 8) + 8) % 8;
		return Math.min(diff, 8 - diff);
	}
}
