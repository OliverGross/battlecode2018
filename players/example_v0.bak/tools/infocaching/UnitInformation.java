package tools.infocaching;

import java.util.HashMap;

import bc.Team;
import bc.Unit;
import bc.UnitType;

public class UnitInformation {

	private Team myTeam;
	private long round;
	UnitInformationByType currentlySeen = new UnitInformationByType(myTeam);
	HashMap<Integer, ExtendedUnitInfo> extendedUnitInfo = new HashMap<>();
	
	public UnitInformation(Team myTeam, long round, UnitInformation oldInfo) {
		this.myTeam = myTeam;
		this.round = round;
		if(oldInfo != null) {
			extendedUnitInfo = oldInfo.extendedUnitInfo;
		}
	}
/**
 * This should only be called from WorldInformation!
 * @param u
 */
	public void addUnit(Unit u) {
		if(!extendedUnitInfo.containsKey(u.id())) {
			extendedUnitInfo.put(u.id(), new ExtendedUnitInfo(u, round));
		}
		switch (u.unitType()) {
		case Factory:
			currentlySeen.addFactory(u);
			break;
		case Rocket:
			currentlySeen.addRocket(u);
			break;
		case Healer:
			currentlySeen.addHealer(u);
			break;
		case Knight:
			currentlySeen.addKnight(u);
			break;
		case Mage:
			currentlySeen.addMage(u);
			break;
		case Ranger:
			currentlySeen.addRanger(u);
			break;
		case Worker:
			currentlySeen.addWorker(u);
			break;
		}
		extendedUnitInfo.get(u.id()).update(u, round);
	}
	
	public UnitInformationByType getCurrentlySeenUnits() {
		return currentlySeen;
	}
	
	public HashMap<Integer, ExtendedUnitInfo> getAllUnitsEver() {
		return extendedUnitInfo;
	}
	
	public int getNumberOfUnitsEnemyBuiltOfType(UnitType type) {
		int ret = 0;
		for(int k : extendedUnitInfo.keySet()) {
			if(extendedUnitInfo.get(k).type == type) {
				ret++;
			}
		}
		return ret;
	}
	
}