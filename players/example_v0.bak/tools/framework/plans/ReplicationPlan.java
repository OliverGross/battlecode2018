package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class ReplicationPlan extends Plan {

	public ReplicationPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		int replicateCost = (int) bc.bcUnitTypeReplicateCost(UnitType.Worker);
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() == UnitType.Worker) {
				for (Direction dir : Direction.values()) {
					if (ws.gc.canReplicate(uid, dir)
							&& ws.availableKarbonite() >= replicateCost) {
						ws.gc.replicate(uid, dir);
						this.status = Status.FINAL;
						return;
					}
				}
			}
		}
	}
}
