package tools.pathfinding;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.GameController;
import bc.MapLocation;
import bc.VecUnit;

public class Pathfinding {

	public GameController gc;
	public JPS jps;

	public Pathfinding(GameController gc) {
		this.gc = gc;
		jps = new JPS(gc);
	}

	public void setObstacles(boolean impassable,
			Set<HashablePoint> leaveUnchanged) {
		VecUnit units = gc.senseNearbyUnits(new MapLocation(gc.planet(), 0, 0),
				5555);
		for (int i = 0; i < units.size(); i++) {
			MapLocation loc = units.get(i).location().mapLocation();
			if (!leaveUnchanged.contains(
					new HashablePoint(new int[] { loc.getX(), loc.getY() })))
				jps.map.set(loc.getX(), loc.getY(), impassable);
		}
	}

	public Set<HashablePoint> getObstacles() {
		VecUnit units = gc.senseNearbyUnits(new MapLocation(gc.planet(), 0, 0),
				5555);
		Set<HashablePoint> result = new HashSet<HashablePoint>();
		for (int i = 0; i < units.size(); i++) {
			MapLocation loc = units.get(i).location().mapLocation();
			result.add(new HashablePoint(new int[] { loc.getX(), loc.getY() }));
		}
		return result;
	}

	public Direction getNextDirection(MapLocation start,
			Set<MapLocation> goals) {
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		for (MapLocation loc : goals) {
			targets.add(new HashablePoint(loc));
		}
		setObstacles(true, targets);
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		starts.add(new HashablePoint(start));
		List<int[]> path = jps.getPath(starts, targets);
		if (path.size() == 1) {
			System.out.println("No path was found!");
			return null;
		}
		int[] dir = GeoPoint.diff(path.get(1), path.get(0));
		int d = GeoPoint.chebyshev_distance(dir);
		dir[0] /= d;
		dir[1] /= d;
		Direction result = start.directionTo(new MapLocation(start.getPlanet(),
				start.getX() + dir[0], start.getY() + dir[1]));
		setObstacles(false, targets);
		return result;
	}

	public Direction getNextDirection(MapLocation start, Set<MapLocation> goals,
			LocalPathFinder lpf) {
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		for (MapLocation loc : goals) {
			targets.add(new HashablePoint(loc));
		}
		Set<HashablePoint> obstacles = getObstacles();
		return lpf.getNextDirection(new HashablePoint(start), targets,
				obstacles);
	}

	public MapLocation getNearestStart(Set<MapLocation> starts,
			Set<MapLocation> goals) {
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		for (MapLocation loc : goals) {
			targets.add(new HashablePoint(loc));
		}
		Set<HashablePoint> startingPoints = new HashSet<HashablePoint>();
		for (MapLocation loc : starts) {
			startingPoints.add(new HashablePoint(loc));
		}
		List<int[]> path = jps.getPath(startingPoints, targets);
		if (path.size() == 1) {
			System.err.println("No path was found!");
			return null;
		} else {
			MapLocation closest = starts.iterator().next();
			closest.setX(path.get(0)[0]);
			closest.setY(path.get(0)[1]);
			return closest;
		}
	}

	public Direction getNextDirection(MapLocation start, MapLocation goal) {
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		targets.add(new HashablePoint(new int[] { goal.getX(), goal.getY() }));
		setObstacles(true, targets);
		Direction result = jps.getNextDirection(start, goal);
		setObstacles(false, targets);
		return result;
	}

	public int getDistance(HashSet<MapLocation> me, MapLocation enem) {
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		for (MapLocation m : me) {
			starts.add(new HashablePoint(new int[] { m.getX(), m.getY() }));
		}
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		targets.add(new HashablePoint(new int[] { enem.getX(), enem.getY() }));
		setObstacles(true, targets);
		List<int[]> path = jps.getPath(starts, targets);
		setObstacles(false, targets);
		return path.size() - 1;
	}
}
