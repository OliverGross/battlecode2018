package tools;

import java.util.List;

import tools.framework.WorldState;

public class QLearning {
	public List<Double> Weights;
	public List<Double> Features;

	public QLearning(WorldState worldstate, double alpha, double gamma) {
		Weights = loadWeights();
	}

	private List<Double> loadWeights() {
		// TODO Initialize each feature to 0.
		return null;
	}

	public List<Float> CalcFeatures(WorldState worldstate) {
		/**
		 * Here, or the in the children class, comes a selection of well thought
		 * of features and their calculation. These features need to measure how
		 * good or bad a given state is. For experimental purposes a lot of
		 * features can be included. The algorithm should give the unimportant
		 * ones a weight close to 0. In the actual training, these should be
		 * excluded.
		 */
		return null;

	}

	public List<Double> update(WorldState worldstate, double reward,
			double learningRate, double discount) {
		/**
		 * Updates the weights and returns them. A reward needs to be specified.
		 * Discount (as well as learning rate) should be between 0 and 1.For
		 * discount, low values focuses on short term goals, and the other way
		 * around. Learning rate indicates how important old knowledge is and
		 * should be higher in more deterministic scenarios. TODO Still needs an
		 * appropriate way to get the new state, depends on the contest.
		 * 
		 */
		List<Double> NewWeights = Weights;
		List<Double> NewFeatures = CalcNewFeatures();

		double difference = learningRate
				* (reward + discount * (getQValue(Weights, NewFeatures)))
				- getQValue(Weights, Features);
		for (int i = 0; i < Weights.size(); i++) {
			NewWeights.set(i,
					(NewWeights.get(i) + difference * Features.get(i)));

		}
		return NewWeights;
	}

	private double getQValue(List<Double> weights, List<Double> features) {
		double sum = 0;
		for (int i = 0; i < weights.size(); i++) {
			sum += weights.get(i) * features.get(i);
		}
		return sum;
	}

	private List<Double> CalcNewFeatures() {
		// TODO Depending on how accessible a future worldstate is, this does
		// the same as CalcFeatures. However it might be easier and more
		// efficient to calculate the new features from the old ones and the
		// chosen action.
		return null;
	}
}
