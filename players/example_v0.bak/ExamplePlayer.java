import java.util.Random;

// import the API.
// See xxx for the javadocs.
import bc.*;

public class ExamplePlayer {
	public static void main(String[] args) {
		Random rand = new Random();
		// MapLocation is a data structure you'll use a lot.
		MapLocation loc = new MapLocation(Planet.Earth, 10, 20);
		System.out.println("loc: " + loc + ", one step to the Northwest: "
				+ loc.add(Direction.Northwest));
		System.out.println("loc x: " + loc.getX());

		// One slightly weird thing: some methods are currently static methods
		// on a static class called bc.
		// This will eventually be fixed :/
		System.out.println("Opposite of " + Direction.North + ": "
				+ bc.bcDirectionOpposite(Direction.North));

		// Connect to the manager, starting the game
		GameController gc = new GameController();

		// Direction is a normal java enum.
		Direction[] directions = Direction.values();

		while (true) {
			if(gc.round() % 25 == 0){
				System.out.flush();
				System.out.println("jround:" + gc.round());
				System.out.println("Number of units: " + gc.myUnits().size());
			}
			// VecUnit is a class that you can think of as similar to
			// ArrayList<Unit>, but immutable.
			VecUnit units = gc.myUnits();
			for (int i = 0; i < units.size(); i++) {
				Unit unit = units.get(i);

				Direction dir = directions[rand.nextInt(9)];
				// Most methods on gc take unit IDs, instead of the unit objects
				// themselves.
				if (gc.isMoveReady(unit.id()) && gc.canMove(unit.id(), dir)) {
					gc.moveRobot(unit.id(), dir);
				}
			}
			// Submit the actions we've done, and wait for our next turn.
			gc.nextTurn();
		}
	}
}