import bc.Planet;
import tools.framework.Agent;
import tools.framework.Plan;
import tools.framework.WorldState;

public class Chicken {
	public static void main(String[] args) {
		WorldState worldstate = new WorldState();
		Plan masterplan = null;
		if (worldstate.gc.planet().equals(Planet.Earth)) {
			masterplan = new ChickenEarthMasterPlan(worldstate);
		} else {
			masterplan = new ChickenMarsMasterPlan(worldstate);
		}
		Agent agent = new Agent(worldstate, masterplan);
		agent.execute();
	}
}