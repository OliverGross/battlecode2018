import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.LandingSiteExplorerPlan;
import tools.framework.plans.RangerAttackPlan;

public class ChickenMarsMasterPlan extends EvaluatorPlan {

	public ChickenMarsMasterPlan(WorldState worldState) {
		super(worldState);
		plans.add(new LandingSiteExplorerPlan(worldState));
		plans.add(new RangerAttackPlan(worldState));

		addEvaluator(new PlanEvaluator(1.0));
	}

	@Override
	public void modifyPlans(WorldState worldState) {

	}
}
