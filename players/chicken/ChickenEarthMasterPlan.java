import java.util.ArrayList;
import java.util.HashMap;

import bc.UnitType;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.buildorder.RangerRushBuildOrder;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.FFactoryBuildPlan;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.KnightsBuildPlan;
import tools.framework.plans.MarsProgramPlan;
import tools.framework.plans.MasterBuildPlan;
import tools.framework.plans.RangerAttackPlan;
import tools.framework.plans.RangerBuildPlan;

public class ChickenEarthMasterPlan extends EvaluatorPlan {

	// check if already in evacuate
	boolean evacuateMode = false;
	// earliest time to think of evacuation
	long magicRoundNumber = 50;
	// when the plan believes he has won
	long maxWonEarthRounds = 50;

	boolean preferKnightsOverRanger = false;

	public ChickenEarthMasterPlan(WorldState worldState) {
		super(worldState);
		ArrayList<Integer> factoryWorkers = new ArrayList<Integer>();
		HashMap<Integer, ArrayList<Integer>> eval = worldState.pga
				.getWorkerEvaluation();
		int minFree = -1;
		// int factoryEnemyDistance = 0;
		int factoryWorker = 0;
		for (HashMap.Entry<Integer, ArrayList<Integer>> entry : eval
				.entrySet()) {
			if (entry.getValue().get(1) > minFree) {
				minFree = entry.getValue().get(1);
				factoryWorker = entry.getKey();
				// factoryEnemyDistance = entry.getValue().get(0);
			}
		}
		preferKnightsOverRanger = worldState.pga.preferKnights;
		// \TODO eventually check factoryEnemyDistance
		factoryWorkers.add(factoryWorker);
		if (this.preferKnightsOverRanger)
			plans.add(new KnightsBuildPlan(worldState));
		else
			plans.add(new RangerBuildPlan(worldState));
		plans.add(new FFactoryBuildPlan(worldState, factoryWorkers, true, 4));

		plans.add(new HarvestPlan(worldState, worldState.pga.karbLocs));
		plans.add(new MarsProgramPlan(worldState));
		if (this.preferKnightsOverRanger) {
			plans.add(new KnightsAttackPlan(worldState));
			worldState.gc.queueResearch(UnitType.Worker);
			worldState.gc.queueResearch(UnitType.Rocket);
			worldState.gc.queueResearch(UnitType.Knight);
			worldState.gc.queueResearch(UnitType.Knight);
			worldState.gc.queueResearch(UnitType.Knight);
		} else {
			plans.add(new RangerAttackPlan(worldState));
			worldState.gc.queueResearch(UnitType.Worker);
			worldState.gc.queueResearch(UnitType.Rocket);
			worldState.gc.queueResearch(UnitType.Ranger);
			worldState.gc.queueResearch(UnitType.Ranger);
			worldState.gc.queueResearch(UnitType.Ranger);

		}

		addEvaluator(new PlanEvaluator(1.0));
	}

	private int wonEarthRounds = 0;

	public boolean wonEarth(WorldState ws) {
		if (ws.gc.round() > magicRoundNumber) {
			if (ws.gc.myUnits().size() >= ws.gc.units().size()) {
				wonEarthRounds++;
				if (wonEarthRounds > 50) {
					return true;
				}
			} else {
				wonEarthRounds = 0;
			}
		}
		return false;
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		if (!evacuateMode) {
			if (wonEarth(worldState)) {
				worldState.gc.resetResearch();
				worldState.gc.queueResearch(UnitType.Rocket);
				plans.clear();
				worldState.resources.resetAllAssignments();
				plans.add(new MarsProgramPlan(worldState));
				plans.add(new MasterBuildPlan(worldState,
						new RangerRushBuildOrder()));
				evacuateMode = true;
			}
		} else {

		}
	}
}
