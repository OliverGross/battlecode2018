package tools.pathfinding;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import tools.DirHelp;

public class LocalPathFinder {
	int diffRegionSize;
	int diffGoalPathOffset;

	// logging
	public static int numberSuccessfullUsedCaches = 0;
	public static int numberStartsTooDifferent = 0;
	public static int numberUnitLeftPath = 0;
	public static int numberGoalsTooDifferent = 0;

	Set<HashablePoint> savedStarts = null;
	Set<HashablePoint> savedGoals = null;
	Set<HashablePoint> savedObstacles = null;
	List<HashablePoint> savedPath = null;

	JPS jps;

	public LocalPathFinder(JPS jps) {
		diffRegionSize = 1;
		diffGoalPathOffset = 2;
		this.jps = jps;
	}

	public Direction getNextDirection(HashablePoint start,
			Set<HashablePoint> goals) {
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		starts.add(start);
		Set<HashablePoint> obstacles = new HashSet<HashablePoint>(
				jps.map.dynamicObstacles);
		if (savedPath == null || savedPath.size() == 1) {
			savedStarts = starts;
			savedGoals = goals;
			savedObstacles = obstacles;
			savedPath = jps.getPath(starts, goals);
			return getNextDirection();
		}
		if (!checkStartPositionValid(start)) {
			savedPath = null;
			numberUnitLeftPath++;
			return getNextDirection(start, goals);
		}
		if (startRegionChangeTooMuch(start, obstacles)) {
			savedPath = null;
			numberStartsTooDifferent++;
			return getNextDirection(start, goals);
		}
		if (goalsChangedTooMuch(goals)) {
			savedPath = null;
			numberGoalsTooDifferent++;
			return getNextDirection(start, goals);
		}
		numberSuccessfullUsedCaches++;
		return getNextDirection();
	}

	public boolean goalsChangedTooMuch(Set<HashablePoint> goals) {
		int threshold = savedPath.size() - 1 - diffGoalPathOffset;
		// check the minimum distance from every goal to one of the saved goals
		for (HashablePoint goal : goals) {
			int minDist = 5555;
			for (HashablePoint savedGoal : savedGoals) {
				minDist = Math.min(minDist,
						Geometry.chebyshev_distance(goal, savedGoal));
				if (minDist <= threshold) {
					break;
				}
			}
			if (minDist > 0 && minDist > threshold) {
				return true;
			}
		}
		// now check the opposite
		// minimum distance from every saved goal to one of the goals
		for (HashablePoint savedGoal : savedGoals) {
			int minDist = 5555;
			for (HashablePoint goal : goals) {
				minDist = Math.min(minDist,
						Geometry.chebyshev_distance(goal, savedGoal));
				if (minDist <= threshold) {
					break;
				}
			}
			if (minDist > 0 && minDist > threshold) {
				return true;
			}
		}
		return false;
	}

	public boolean startRegionChangeTooMuch(HashablePoint start,
			Set<HashablePoint> obstacles) {
		for (int dx = -diffRegionSize; dx <= diffRegionSize; dx++) {
			for (int dy = -diffRegionSize; dy <= diffRegionSize; dy++) {
				HashablePoint p = new HashablePoint(
						new int[] { start.get(0) + dx, start.get(1) + dy });
				if (savedObstacles.contains(p) != obstacles.contains(p)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkStartPositionValid(HashablePoint start) {
		// check if unit just moved along the path
		if (savedPath.get(0).equals(start))
			return true;
		else if (savedPath.get(1).equals(start)) {
			savedPath.remove(0);
			return true;
		}
		// seems the unit did not move along the path, check if it moved in
		// opposite direction for kiting purposes
		Direction dir = DirHelp.getDirection(
				Geometry.diff(savedPath.get(1), savedPath.get(0)));
		if (Geometry.chebyshev_distance(start, savedPath.get(0)) == 1) {
			Direction startDir = DirHelp
					.getDirection(Geometry.diff(start, savedPath.get(0)));
			if (DirHelp.diffAngle(dir, startDir) >= 3) {
				// it seems the unit kited, so we just add the new position to
				// the path
				savedPath.add(0, start);
				return true;
			}
		}
		return false;
	}

	public Direction getNextDirection() {
		if (savedPath.size() == 1) {
			return Direction.Center;
		}
		return DirHelp.getDirection(
				Geometry.diff(savedPath.get(1), savedPath.get(0)));
	}

}
