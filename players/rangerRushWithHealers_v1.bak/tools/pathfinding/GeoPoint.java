package tools.pathfinding;

import bc.Direction;

public final class GeoPoint {
	public static int[] diff(int[] a, int[] b) {
		return new int[] { a[0] - b[0], a[1] - b[1] };
	}

	public static int[] add(int[] a, int[] b) {
		return new int[] { a[0] + b[0], a[1] + b[1] };
	}

	public static int[] abs(int[] a) {
		return new int[] { Math.abs(a[0]), Math.abs(a[1]) };
	}

	public static boolean isDiagonal(int[] a) {
		return a[0] * a[1] != 0;
	}

	public static int chebyshev_distance(int x, int y) {
		return Math.max(Math.abs(x), Math.abs(y));
	}

	public static int chebyshev_distance(int[] a) {
		return chebyshev_distance(a[0], a[1]);
	}

	public static int chebyshev_distance(int[] a, int[] b) {
		return chebyshev_distance(GeoPoint.diff(a, b));
	}

	public static String toString(int[] a) {
		return "[" + a[0] + ", " + a[1] + "]";
	}

	public static Direction getDirection(int[] a) {
		switch (a[0]) {
		case -1:
			switch (a[1]) {
			case -1:
				return Direction.Southwest;
			case 0:
				return Direction.West;
			case 1:
				return Direction.Northwest;
			}
			break;
		case 0:
			switch (a[1]) {
			case -1:
				return Direction.South;
			case 0:
				return Direction.Center;
			case 1:
				return Direction.North;
			}
			break;
		case 1:
			switch (a[1]) {
			case -1:
				return Direction.Southeast;
			case 0:
				return Direction.East;
			case 1:
				return Direction.Northeast;
			}
			break;
		}
		throw new IllegalArgumentException(
				"The hashable point needs to be a valid direction, not " + a[0]
						+ " " + a[1] + "!");
	}
}
