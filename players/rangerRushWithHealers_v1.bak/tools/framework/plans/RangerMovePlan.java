package tools.framework.plans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.WorldState;

public class RangerMovePlan extends Plan {

	class Area {
		Set<Unit> dangerTooClose;
		Set<Unit> enemiesTooFar;
		Set<Unit> harmlessTooClose;
		Set<Unit> unitsInIntervall;
		Unit ranger;

		public Area(Set<Unit> dangerTooClose, Set<Unit> enemiesTooFar,
				Set<Unit> harmlessTooClose, Set<Unit> unitsInIntervall,
				Unit ranger) {
			this.dangerTooClose = dangerTooClose;
			this.enemiesTooFar = enemiesTooFar;
			this.harmlessTooClose = harmlessTooClose;
			this.unitsInIntervall = unitsInIntervall;
			this.ranger = ranger;
		}
	}

	RangerAttackPlan rangerAttackPlan;

	public RangerMovePlan(WorldState worldState, RangerAttackPlan plan) {
		super(worldState);
		this.rangerAttackPlan = plan;
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		for (int rangerId : rangerAttackPlan.getRangersOnMap()) {
			if(!ws.gc.unit(rangerId).location().isOnMap()) continue;
			
			if (ws.gc.isMoveReady(rangerId)) {
				Unit ranger = ws.gc.unit(rangerId);
				if(ranger.health() < 50) {
					VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(ranger.location().mapLocation(), 65, ws.opp);
					Set<Unit> enemiesSet = new HashSet<>();
					if(enemies.size() != 0) {
						for(int i=0; i<enemies.size(); i++) {
							enemiesSet.add(enemies.get(i));
						}
						kite(enemiesSet, ws, ranger);
					}
				} else {
					if (ws.gc.isAttackReady(rangerId)) {
						moveOffensive(rangerId, ws);
					} else {
						moveDefensive(rangerId, ws);
					}
				}
			}
		}
	}

	private void moveDefensive(int rangerId, WorldState ws) {
		Area area = checkSurroundings(ws, rangerId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.ranger);
			return;
		}
		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.ranger);
			}
			// if mages of knights are in intervall kite them
			// if no danger is close, but enemies are in intervall, do nothing
			// and keep your movement
			return;
		}
		if (!area.enemiesTooFar.isEmpty()) {
			Set<MapLocation> enemyLocations = new HashSet<>();
			for (Unit u : area.enemiesTooFar) {
				enemyLocations.add(u.location().mapLocation());
			}
			ws.lll.goTo(area.ranger.id(), enemyLocations);
			// if no enemies are too close or in intervall, go in direction of
			// next enemy
			// TODO check if you get shot rightafter, then maybe keep your
			// movement
			return;
		}
		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.ranger);
			return;
		}

	}

	private void moveOffensive(int rangerId, WorldState ws) {

		Area area = checkSurroundings(ws, rangerId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.ranger);
			return;
		}
		if (!area.enemiesTooFar.isEmpty()) {
			Set<MapLocation> enemyLocations = new HashSet<>();
			for (Unit u : area.enemiesTooFar) {
				enemyLocations.add(u.location().mapLocation());
			}
			ws.lll.goTo(area.ranger.id(), enemyLocations);
			return;
		}
		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.ranger);
			return;
		}
		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.ranger);
			}
			return;
		}

		// TODO nothing on the map, spread out to scout the whole map
	}

	private Area checkSurroundings(WorldState ws, int rangerId) {
		Unit ranger = ws.gc.unit(rangerId);
		MapLocation rangerLocation = ranger.location().mapLocation();

		Set<Unit> dangerTooClose = new HashSet<>();
		Set<Unit> enemiesTooFar = new HashSet<>();
		Set<Unit> harmlessTooClose = new HashSet<>();
		Set<Unit> unitsInIntervall = new HashSet<>();

		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				ws.gc.unit(rangerId).location().mapLocation(), 5000, ws.opp);
		if (enemies.size() == 0) {
			enemies = ws.gc.startingMap(Planet.Earth).getInitial_units();
		}

		for (int i = 0; i < enemies.size(); i++) {
			Unit enemy = enemies.get(i);
			long distance = enemy.location().mapLocation()
					.distanceSquaredTo(rangerLocation);
			// TODO magicnumber
			if (distance < 20) {
				if (enemy.unitType() != UnitType.Factory
						&& enemy.unitType() != UnitType.Rocket
						&& enemy.unitType() != UnitType.Worker) {
					dangerTooClose.add(enemy);
				} else {
					harmlessTooClose.add(enemy);
				}
			} else {
				if (distance >= ranger.attackRange()) {
					enemiesTooFar.add(enemy);
				} else {
					unitsInIntervall.add(enemy);
				}
			}
		}
		return new Area(dangerTooClose, enemiesTooFar, harmlessTooClose,
				unitsInIntervall, ranger);
	}

	private void kite(Set<Unit> enemiesToKite, WorldState ws, Unit ranger) {
		// kite rangers, mages and knights differently! Rangers/Mages via
		// shootingdistance (straight line), knights vs JPS

		Set<MapLocation> knightLocations = new HashSet<>();
		Set<MapLocation> rangeLocations = new HashSet<>();
		Set<MapLocation> harmlessLocations = new HashSet<>();

		for (Unit u : enemiesToKite) {
			if (u.unitType() == UnitType.Knight) {
				knightLocations.add(u.location().mapLocation());
			} else {
				if (u.unitType() == UnitType.Mage
						|| u.unitType() == UnitType.Ranger) {
					rangeLocations.add(u.location().mapLocation());
				} else {
					harmlessLocations.add(u.location().mapLocation());
				}
			}
		}

		Direction kiteD = Direction.Center;
		Direction knightKiteDirection = Direction.Center;
		Direction harmlessKiteDirection = Direction.Center;

		HashMap<Direction, Integer> kitingDesire = new HashMap<>();
		
		// Get direction to kite Knights
		if (!knightLocations.isEmpty()) {
			Direction enemyKnightDir = ws.pathfinding.getNextDirection(
					ranger.location().mapLocation(), knightLocations);
			if (enemyKnightDir != null) {
				knightKiteDirection = ws.lll.kite(ranger.id(), enemyKnightDir);
				int desire = 0;
				if(kitingDesire.containsKey(knightKiteDirection)) {
					desire = kitingDesire.get(knightKiteDirection);
				}
				desire += knightLocations.size();
				kitingDesire.put(knightKiteDirection, desire);
			}
		}

		// Get direction to kite Ranged
		if (!rangeLocations.isEmpty()) {
			for(MapLocation loc : rangeLocations) {
				Direction rDir = ws.lll.kite(ranger.id(), ranger.location().mapLocation().directionTo(loc));
				int prevDesire = 0;
				if(kitingDesire.containsKey(rDir)) {
					prevDesire = kitingDesire.get(rDir);
				}
				kitingDesire.put(rDir, prevDesire+1);
			}
		}
		
		//In case only harmless creatures are found
		if(rangeLocations.isEmpty() && knightLocations.isEmpty()) {
			if(!harmlessLocations.isEmpty()) {
				harmlessKiteDirection = ws.pathfinding.getNextDirection(ranger.location().mapLocation(), harmlessLocations);
				if(harmlessKiteDirection != null) {
					kiteD = ws.lll.kite(ranger.id(), harmlessKiteDirection);
					if (ws.gc.isMoveReady(ranger.id())
							&& ws.gc.canMove(ranger.id(), kiteD)) {
						ws.gc.moveRobot(ranger.id(), kiteD);
					}
					return;
				}
			}
		}
		
		//find out best direction
		int bestval = 0;
		for(Direction d : Direction.values()) {
			if(d == Direction.Center) {
				continue;
			}
			if(kitingDesire.containsKey(d)) {
				int val = kitingDesire.get(d);
				if(val > bestval) {
					kiteD = d;
					bestval = val;
				}
			}
		}

		// Kite
		if (ws.gc.isMoveReady(ranger.id())
				&& ws.gc.canMove(ranger.id(), kiteD)) {
			ws.gc.moveRobot(ranger.id(), kiteD);
		}
	}

}
