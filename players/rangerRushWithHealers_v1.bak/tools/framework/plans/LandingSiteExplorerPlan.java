package tools.framework.plans;

import java.util.ArrayList;
import java.util.Collections;

import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.Veci32;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class LandingSiteExplorerPlan extends Plan {
	boolean[][] map = null;
	int w;
	int h;
	// TODO Check if this is true
	int maxSites = 50;
	ArrayList<MapLocation> landingSites = new ArrayList<MapLocation>();

	public LandingSiteExplorerPlan(WorldState worldstate) {
		super(worldstate);
		initializeMap(worldstate);
		findLandingSites();
		resetTeamArray();
	}

	@Override
	public void tick(WorldState ws) {
		int i = 0;
		Collections.shuffle(landingSites, ws.rand);
		for (MapLocation loc : landingSites) {
			if (i >= maxSites) {
				break;
			}
			appendLocToArrayList(ws, loc);
			i++;
		}
		System.out.println("====LandingSiteExplorerPlan:Debug====");
		System.out.println("Location 3(Write):" + landingSites.get(3));
		System.out.println("Location 3(READ):"
				+ LandingSiteExplorerPlan.getLocFromTeamArray(ws, 3));
		System.out.println("=====================================");
		this.status = Status.FINAL;
	}

	void initializeMap(WorldState ws) {
		PlanetMap pmap = ws.gc.startingMap(ws.gc.planet());
		Planet planet = pmap.getPlanet();
		w = (int) pmap.getWidth();
		h = (int) pmap.getHeight();
		map = new boolean[w][h];
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				map[x][y] = pmap.isPassableTerrainAt(
						new MapLocation(planet, x, y)) == 0;
			}
	}

	public void findLandingSites() {
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				if (!map[x][y]) {
					landingSites.add(new MapLocation(Planet.Mars, x, y));
					addBlastRadius(x, y);
				}
			}
	}

	public void addBlastRadius(int xLandingSite, int yLandingSite) {
		for (int x = Math.max(xLandingSite - 1, 0); x < Math
				.min(xLandingSite + 2, w); x++) {
			for (int y = Math.max(yLandingSite - 1, 0); y < Math
					.min(yLandingSite + 2, h); y++) {
				map[x][y] = true;
			}
		}
	}

	void resetTeamArray() {
		Veci32 teamArray = ws.gc.getTeamArray(Planet.Mars);
		for (int i = 0; i < teamArray.size(); i++) {
			ws.gc.writeTeamArray(i, 0);
		}
	}

	void appendLocToArrayList(WorldState ws, MapLocation loc) {
		int i = ws.gc.getTeamArray(Planet.Mars).get(0) + 1;
		if (i > ws.gc.getTeamArray(Planet.Mars).size() - 2) {
			return;
		} else {
			ws.gc.writeTeamArray(0, i);
			ws.gc.writeTeamArray(i, (loc.getY() & 0xFFFF) + (loc.getX() << 16));
		}
	}

	/**
	 * Get a location to land on mars.Returns null if i > availablePositions.
	 * \NOTE locations are shuffled on beginning of the game
	 * 
	 * @param ws(WorldState)
	 * @param i(int)
	 * @return MapLocation at position i
	 */
	public static MapLocation getLocFromTeamArray(WorldState ws, int i) {
		Veci32 teamArray = ws.gc.getTeamArray(Planet.Mars);
		int maxLocs = teamArray.get(0);
		if (i > maxLocs)
			return null;
		int xy = teamArray.get(i + 1);
		int x = xy >> 16;
		int y = xy & 0xFFFF;
		return new MapLocation(Planet.Mars, x, y);
	}
}
