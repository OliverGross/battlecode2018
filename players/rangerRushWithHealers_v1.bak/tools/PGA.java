package tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.Unit;
import bc.VecUnit;
import tools.framework.WorldState;
import tools.pathfinding.HashablePoint;

public class PGA {

	// magic numbers
	public static final double importanceKarboniteVsSpace = 1.0;

	private static int runningInstances = 0;

	public HashMap<Integer, MapLocation> myWorkers = new HashMap<Integer, MapLocation>();
	public HashMap<Integer, MapLocation> oppWorkers = new HashMap<Integer, MapLocation>();
	WorldState ws;
	PlanetMap startMap;
	public List<List<Long>> karbLocs = new ArrayList<List<Long>>();
	public int[][] closestObstacleDistSqrd;
	public double[][] nearbyKarbonite;
	// public int[][] distanceToInitialWorkers;
	public List<MapLocation> strategicLocations = new ArrayList<MapLocation>();

	public void computeStrategicLocations() {
		final class T {
			double prio;
			MapLocation data;

			public T(double a, MapLocation loc) {
				prio = a;
				data = loc;
			}
		}
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		List<T> orderedByStrategy = new ArrayList<T>();
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				orderedByStrategy.add(new T(closestObstacleDistSqrd[x][y]
						+ importanceKarboniteVsSpace * nearbyKarbonite[x][y],
						new MapLocation(planet, x, y)));
		Collections.sort(orderedByStrategy,
				(T n1, T n2) -> (int) (n1.prio < n2.prio ? 1
						: n1.prio > n2.prio ? -1 : 0));
		strategicLocations.clear();
		for (T t : orderedByStrategy)
			strategicLocations.add(t.data);
	}

	public void computeNearbyKarbonite() {
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		nearbyKarbonite = new double[width][height];
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				int distSqrd = closestObstacleDistSqrd[x][y];
				nearbyKarbonite[x][y] = 0;
				for (int dx = -(int) Math.sqrt(distSqrd); dx <= (int) Math
						.sqrt(distSqrd); dx++) {
					int x_ = x + dx;
					if (x_ < 0 || x_ >= width)
						continue;
					int border = (int) (Math.sqrt(distSqrd - dx * dx) + 1e-3);
					for (int dy = -border; dy <= border; dy++) {
						int y_ = y + dy;
						if (y_ < 0 || y_ >= height)
							continue;
						nearbyKarbonite[x][y] += karbLocs.get(x).get(y)
								/ Math.sqrt(dx * dx + dy * dy);
					}
				}
			}
	}

	public void computeClosestObstacle() {
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		closestObstacleDistSqrd = new int[width][height];
		Set<HashablePoint> obstacles = new HashSet<HashablePoint>();
		int maxDist = 25;
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				closestObstacleDistSqrd[x][y] = maxDist * maxDist;
				MapLocation loc = new MapLocation(planet, x, y);
				if (0 == ws.gc.startingMap(planet).isPassableTerrainAt(loc)) {
					obstacles.add(new HashablePoint(loc));
					closestObstacleDistSqrd[x][y] = 0;
				}
			}
		for (HashablePoint p : obstacles) {
			for (int dx = -maxDist; dx <= maxDist; dx++) {
				int x = p.point[0] + dx;
				if (x < 0 || x >= width)
					continue;
				int border = (int) (Math.sqrt(maxDist * maxDist - dx * dx)
						+ 1e-3);
				for (int dy = -border; dy <= border; dy++) {
					int y = p.point[1] + dy;
					if (y < 0 || y >= height)
						continue;
					int dist = dx * dx + dy * dy;
					closestObstacleDistSqrd[x][y] = Math
							.min(closestObstacleDistSqrd[x][y], dist);
				}
			}
		}
	}

	public PGA(WorldState ws) {
		if (runningInstances >= 1)
			throw new RuntimeException("Let only one PGA instance exist!");
		runningInstances++;

		startMap = ws.gc.startingMap(ws.gc.planet());
		this.ws = ws;
		initWorkers();
		initKarb(ws);
		computeClosestObstacle();
		computeNearbyKarbonite();
		computeStrategicLocations();
	}

	public void initWorkers() {
		VecUnit initial = startMap.getInitial_units();
		for (int i = 0; i < initial.size(); i++) {
			Unit worker = initial.get(i);
			if (worker.team().equals(ws.me)) {
				myWorkers.put(worker.id(), worker.location().mapLocation());
			} else {
				oppWorkers.put(worker.id(), worker.location().mapLocation());
			}
		}

	}

	public void initKarb(WorldState worldstate) {
		long width;
		long height;
		width = startMap.getWidth();
		height = startMap.getHeight();
		MapLocation loc = new MapLocation(startMap.getPlanet(), 0, 0);
		for (int x = 0; x < width; x++) {
			karbLocs.add(new ArrayList<Long>());
			for (int y = 0; y < height; y++) {
				loc.setX(x);
				loc.setY(y);
				karbLocs.get(x).add(startMap.initialKarboniteAt(loc));
				// System.out.println(x + "," + y + " = " +
				// initialmap.initialKarboniteAt(loc));
			}
		}
	}

	public boolean preferKnights(WorldState worldState) {
		int largest_dist = 0;
		ArrayList<MapLocation> enemy = new ArrayList<MapLocation>(
				oppWorkers.values());
		ArrayList<MapLocation> me = new ArrayList<MapLocation>(
				myWorkers.values());
		for (MapLocation enem : enemy) {
			largest_dist = Math.max(largest_dist,
					ws.pathfinding.getDistance(me, enem));
		}
		if (largest_dist <= 5) {
			System.out.println("We are doing KnightRush instead of Rangers!");
			return true;
		}
		return false;
	}

	/**
	 * Evaluates starting map and enemy points The evaluated values are: 0 -
	 * minimal distance to enemy 1 - passableTerrain counter in adjacent squares
	 * 
	 * @return HashMap<id, ArrayList<Integer> values>
	 */
	public HashMap<Integer, ArrayList<Integer>> getWorkerEvaluation() {
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();
		ArrayList<Integer> tempValues;
		for (HashMap.Entry<Integer, MapLocation> pair : myWorkers.entrySet()) {
			tempValues = new ArrayList<Integer>();
			// evaluate closest worker distance
			tempValues.add(ws.pathfinding.getDistance(
					new ArrayList<MapLocation>(oppWorkers.values()),
					pair.getValue()));
			// evaluate passable terrain in adjacent squares
			int passable = 0;
			for (Direction dir : DirHelp.adjacentDirections) {
				MapLocation adjacentLoc = pair.getValue().add(dir);
				if (adjacentLoc.getX() >= 0
						&& adjacentLoc.getX() < startMap.getWidth()
						&& adjacentLoc.getY() >= 0
						&& adjacentLoc.getY() < startMap.getHeight()) {
					{
						passable += startMap.isPassableTerrainAt(adjacentLoc);
					}
				}
			}
			tempValues.add(passable);
			map.put(pair.getKey(), tempValues);
		}
		return map;
	}

	// real PGA stuff starts below this line ##############################
	// ####################################################################
	public enum Strategy {
		Rush, Defense, Evacuate,
	}

	public Strategy getInitialStrategy() {
		return Strategy.Defense;
	}

}
