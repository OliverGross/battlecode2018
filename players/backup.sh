#!/bin/bash

PLAYER="$1"
VERSION="$2"

if [[ $VERSION == "" ]]
then
	echo "You have to provide two arguments, the first one needs to be a valid player name (e.g. fooPlayer), the second one a version number (e.g. 1.3)!"
	echo "An example of such a correct call would look something like this:"
	echo "      $ ./backup.sh fooPlayer 1.3"
	exit
fi
if [ ! -d "$PLAYER" ]
then
	echo "The player you specified does not exist! You specified $PLAYER!"
	exit
fi
if [ ! -d "$PLAYER/tools" ]
then
	echo "The player folder does not contain a tools folder which is probably required!"
	exit
fi
TARGET="$PLAYER"_v"$VERSION.bak"
if [ -d "$TARGET" ]
then
	echo "The folder $TARGET exists already, aborting this backup!"
	exit
fi
echo "Now copying $PLAYER to $TARGET..."
cp -r $PLAYER $TARGET
echo "Finished!"
