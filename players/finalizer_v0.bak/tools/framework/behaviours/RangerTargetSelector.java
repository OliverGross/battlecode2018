package tools.framework.behaviours;

import bc.MapLocation;
import bc.Unit;
import bc.VecUnit;
import tools.framework.WorldState;

public interface RangerTargetSelector {

	public Unit selectBestTarget(Unit ranger, WorldState ws);
	
}
