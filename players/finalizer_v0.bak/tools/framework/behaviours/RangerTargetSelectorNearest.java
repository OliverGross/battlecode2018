package tools.framework.behaviours;

import bc.MapLocation;
import bc.Unit;
import bc.VecUnit;
import tools.framework.WorldState;

public class RangerTargetSelectorNearest implements RangerTargetSelector {

	@Override
	public Unit selectBestTarget(Unit ranger, WorldState ws) {
		MapLocation rangerLocation = ranger.location().mapLocation();

		Unit bestTarget = null;
		long bestDistance = 100;

		VecUnit enemiesInRange = ws.gc.senseNearbyUnitsByTeam(rangerLocation,
				ranger.attackRange(), ws.opp);
		for (int e = 0; e < enemiesInRange.size(); e++) {
			Unit enemy = enemiesInRange.get(e);
			long distance = rangerLocation
					.distanceSquaredTo(enemy.location().mapLocation());
			if (distance > ranger.rangerCannotAttackRange()) {
				if (bestTarget == null) {
					bestTarget = enemy;
					bestDistance = distance;
				} else {
					// Ignore certain enemies
					if (distance < bestDistance && typeValue(enemy) > 2) {
						bestTarget = enemy;
						bestDistance = distance;
					} else if (distance == bestDistance) {
						if (typeValue(bestTarget) < typeValue(enemy)) {
							if (enemy.health() < bestTarget.health()) {
								// if equal in type and distance -> attack
								// lowest
								bestTarget = enemy;
								bestDistance = distance;
							}
						}
					}
				}
			}
		}
		return bestTarget;
	}
	
	// The higher the value, the better to focus
	private int typeValue(Unit u) {
		if (u == null) {
			System.out.println("typeValue Unit was null");
			return 0;
		}

		switch (u.unitType()) {
		case Factory:
			return 1;
		case Worker:
			return 2;
		case Healer:
			return 3;
		case Rocket:
			return 4;
		case Ranger:
			return 5;
		case Knight:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;

		}
	}

}
