package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.KnightsAttackPlan;

public class KnightMoveBehaviour extends Behaviour {

	List<MapLocation> enemyLocations;
	KnightsAttackPlan attackPlan;
	List<MapLocation> harmlessLocations;

	public KnightMoveBehaviour(WorldState worldState,
			KnightsAttackPlan knightsAttackPlan) {
		enemyLocations = new ArrayList<>();
		harmlessLocations = new ArrayList<>();
		attackPlan = knightsAttackPlan;
	}

	@Override
	public void act(WorldState ws) {
		// compute enemyLocations once and not for all units
		enemyLocations.clear();
		Set<Integer> allEnemies = ws.unitLists.allEnemyUnits;

		harmlessLocations.clear();
		for (int uid : allEnemies) {
			if (ws.gc.canSenseUnit(uid)) {
				Unit u = ws.gc.unit(uid);
				if (u.location().isOnMap()) {
					if ((u.unitType() != UnitType.Factory
							|| u.unitType() != UnitType.Worker
							|| u.unitType() != UnitType.Rocket)) {
						enemyLocations.add(u.location().mapLocation());
					} else {
						harmlessLocations.add(u.location().mapLocation());
					}
				}
			}
		}

		// no enemies in sight, get startlocations
		if (enemyLocations.size() == 0 && harmlessLocations.size() == 0) {
			VecUnit startEnemies = ws.gc.startingMap(Planet.Earth)
					.getInitial_units();
			for (int i = 0; i < startEnemies.size(); i++) {
				if (startEnemies.get(i).team() == ws.opp) {
					enemyLocations
							.add(startEnemies.get(i).location().mapLocation());
				}
			}
		}

		if (!enemyLocations.isEmpty()) {
			for (int knightId : attackPlan.getKnights()) {
				Unit knight = ws.gc.unit(knightId);
				if (!knight.location().isOnMap())
					continue;

				if (ws.gc.isMoveReady(knightId)) {
					ws.lll.goTo(knightId, enemyLocations);
					// TODO kite mages if you have rangers
				}
			}
		} else {
			if (!harmlessLocations.isEmpty()) {
				for (int knightId : attackPlan.getKnights()) {
					Unit knight = ws.gc.unit(knightId);
					if (!knight.location().isOnMap())
						continue;

					if (ws.gc.isMoveReady(knightId)) {
						ws.lll.goTo(knightId, harmlessLocations);
						// TODO kite mages if you have rangers
					}
				}
			}
		}
	}

	// for overcharge
	public void actForOne(WorldState ws, Unit knight) {
		int knightId = knight.id();
		if (!knight.location().isOnMap())
			return;

		if (!enemyLocations.isEmpty()) {
			if (ws.gc.isMoveReady(knightId)) {
				ws.lll.goTo(knightId, enemyLocations);
				// TODO kite mages if you have rangers
			}
		} else if (!harmlessLocations.isEmpty()) {
			if (ws.gc.isMoveReady(knightId)) {
				ws.lll.goTo(knightId, harmlessLocations);
				// TODO kite mages if you have rangers
			}
		}

	}

}
