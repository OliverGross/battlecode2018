package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.MageAttackPlan;

public class MageMoveBehaviour extends Behaviour {

	class Area {
		Set<Unit> dangerTooClose;
		// Set<Unit> enemiesTooFar;
		Set<Unit> harmlessTooClose;
		Set<Unit> unitsInIntervall;
		Unit Mage;

		public Area(Set<Unit> dangerTooClose, Set<Unit> harmlessTooClose,
				Set<Unit> unitsInIntervall, Unit Mage) {
			this.dangerTooClose = dangerTooClose;
			// this.enemiesTooFar = enemiesTooFar;
			this.harmlessTooClose = harmlessTooClose;
			this.unitsInIntervall = unitsInIntervall;
			this.Mage = Mage;
		}
	}

	MageAttackPlan MageAttackPlan;
	List<MapLocation> enemyLocations;

	public MageMoveBehaviour(MageAttackPlan plan) {
		this.MageAttackPlan = plan;
		enemyLocations = new ArrayList<>();
	}

	@Override
	public void act(WorldState ws) {
		// compute enemyLocations once and not for all units
		enemyLocations.clear();
		Set<Integer> allEnemies = ws.unitLists.allEnemyUnits;
		// no enemies in sight, get startlocations
		if (enemyLocations.size() == 0) {
			VecUnit startEnemies = ws.gc.startingMap(Planet.Earth)
					.getInitial_units();
			for (int i = 0; i < startEnemies.size(); i++) {
				if (startEnemies.get(i).team() == ws.opp) {
					enemyLocations
							.add(startEnemies.get(i).location().mapLocation());
				}
			}
		}

		enemyLocations = new ArrayList<MapLocation>();
		for (int uid : allEnemies) {
			if (ws.gc.canSenseUnit(uid)) {
				Unit u = ws.gc.unit(uid);
				if (u.location().isOnMap()) {
					enemyLocations.add(u.location().mapLocation());
				}
			}
		}
		;

		// for all units we own
		for (int MageId : MageAttackPlan.getMagesOnMap()) {
			if (!ws.gc.unit(MageId).location().isOnMap())
				continue;

			if (ws.gc.isMoveReady(MageId)) {
				Unit Mage = ws.gc.unit(MageId);
				if (Mage.health() < 50) {
					VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
							Mage.location().mapLocation(), 65, ws.opp);
					Set<Unit> enemiesSet = new HashSet<>();
					if (enemies.size() != 0) {
						for (int i = 0; i < enemies.size(); i++) {
							enemiesSet.add(enemies.get(i));
						}
						kite(enemiesSet, ws, Mage);
					}
				} else {
					if (ws.gc.isAttackReady(MageId)) {
						moveOffensive(MageId, ws);
					} else {
						moveDefensive(MageId, ws);
					}
				}
			}
		}

		// TODO is this necessary?
		ws.updateUnitLists();
	}

	private void moveDefensive(int MageId, WorldState ws) {
		Area area = checkSurroundings(ws, MageId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.Mage);
			return;
		}
		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.Mage);
			}
			// if mages of knights are in intervall kite them
			// if no danger is close, but enemies are in intervall, do nothing
			// and keep your movement
			return;
		}
		if (!enemyLocations.isEmpty()) {
			ws.lll.goTo(area.Mage.id(), enemyLocations);
			// if no enemies are too close or in intervall, go in direction of
			// next enemy
			// TODO check if you get shot rightafter, then maybe keep your
			// movement
			return;
		}

		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.Mage);
			return;
		}

	}

	private void moveOffensive(int MageId, WorldState ws) {

		Area area = checkSurroundings(ws, MageId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.Mage);
			return;
		}

		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.Mage);
			}
			return;
		}

		if (!enemyLocations.isEmpty()) {
			ws.lll.goTo(area.Mage.id(), enemyLocations);
			return;
		}

		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.Mage);
			return;
		}

		// TODO nothing on the map, spread out to scout the whole map
	}

	private Area checkSurroundings(WorldState ws, int MageId) {
		Unit Mage = ws.gc.unit(MageId);
		MapLocation MageLocation = Mage.location().mapLocation();

		Set<Unit> dangerTooClose = new HashSet<>();
		Set<Unit> harmlessTooClose = new HashSet<>();
		Set<Unit> unitsInIntervall = new HashSet<>();

		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				Mage.location().mapLocation(), Mage.attackRange(), ws.opp);

		for (int i = 0; i < enemies.size(); i++) {
			Unit enemy = enemies.get(i);
			if (enemy == null || !enemy.location().isOnMap())
				continue;
			long distance = enemy.location().mapLocation()
					.distanceSquaredTo(MageLocation);
			// TODO magicnumber
			if (distance < 10) {
				if (enemy.unitType() != UnitType.Factory
						&& enemy.unitType() != UnitType.Rocket
						&& enemy.unitType() != UnitType.Worker) {
					dangerTooClose.add(enemy);
				} else {
					harmlessTooClose.add(enemy);
				}
			} else {
				if (distance >= Mage.attackRange()) {
					// enemiesTooFar.add(enemy);
				} else {
					unitsInIntervall.add(enemy);
				}
			}
		}
		return new Area(dangerTooClose, harmlessTooClose, unitsInIntervall,
				Mage);
	}

	private void kite(Set<Unit> enemiesToKite, WorldState ws, Unit Mage) {
		// kite Mages, mages and knights differently! Mages/Mages via
		// shootingdistance (straight line), knights vs JPS

		List<MapLocation> knightLocations = new ArrayList<>();
		Set<MapLocation> rangeLocations = new HashSet<>();
		List<MapLocation> harmlessLocations = new ArrayList<>();

		for (Unit u : enemiesToKite) {
			if (u.unitType() == UnitType.Knight) {
				knightLocations.add(u.location().mapLocation());
			} else {
				if (u.unitType() == UnitType.Mage
						|| u.unitType() == UnitType.Mage) {
					rangeLocations.add(u.location().mapLocation());
				} else {
					harmlessLocations.add(u.location().mapLocation());
				}
			}
		}

		Direction kiteD = Direction.Center;
		Direction knightKiteDirection = Direction.Center;
		Direction harmlessKiteDirection = Direction.Center;

		HashMap<Direction, Integer> kitingDesire = new HashMap<>();

		// Get direction to kite Knights
		if (!knightLocations.isEmpty()) {
			Direction enemyKnightDir = ws.pathfinding
					.getNextDirection(Mage.id(), knightLocations);
			if (enemyKnightDir != null) {
				knightKiteDirection = ws.lll.kite(Mage.id(), enemyKnightDir);
				int desire = 0;
				if (kitingDesire.containsKey(knightKiteDirection)) {
					desire = kitingDesire.get(knightKiteDirection);
				}
				desire += knightLocations.size();
				kitingDesire.put(knightKiteDirection, desire);
			}
		}

		// Get direction to kite Ranged
		if (!rangeLocations.isEmpty()) {
			for (MapLocation loc : rangeLocations) {
				Direction rDir = ws.lll.kite(Mage.id(),
						Mage.location().mapLocation().directionTo(loc));
				int prevDesire = 0;
				if (kitingDesire.containsKey(rDir)) {
					prevDesire = kitingDesire.get(rDir);
				}
				kitingDesire.put(rDir, prevDesire + 1);
			}
		}

		// In case only harmless creatures are found
		if (rangeLocations.isEmpty() && knightLocations.isEmpty()) {
			if (!harmlessLocations.isEmpty()) {
				harmlessKiteDirection = ws.pathfinding
						.getNextDirection(Mage.id(), harmlessLocations);
				if (harmlessKiteDirection != null) {
					kiteD = ws.lll.kite(Mage.id(), harmlessKiteDirection);
					if (ws.gc.isMoveReady(Mage.id())
							&& ws.gc.canMove(Mage.id(), kiteD)) {
						ws.gc.moveRobot(Mage.id(), kiteD);
					}
					return;
				}
			}
		}

		// find out best direction
		int bestval = 0;
		for (Direction d : Direction.values()) {
			if (d == Direction.Center) {
				continue;
			}
			if (kitingDesire.containsKey(d)) {
				int val = kitingDesire.get(d);
				if (val > bestval) {
					kiteD = d;
					bestval = val;
				}
			}
		}

		// Kite
		if (ws.gc.isMoveReady(Mage.id()) && ws.gc.canMove(Mage.id(), kiteD)) {
			ws.gc.moveRobot(Mage.id(), kiteD);
		}
	}

}
