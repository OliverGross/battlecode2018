package tools.framework;

import java.util.HashMap;
import java.util.Map;

import bc.Direction;
import bc.GameController;
import bc.Unit;
import bc.UnitType;
import bc.VecUnitID;

public class OurGameController extends GameController {

	WorldState ws;
	Map<Integer, Unit> unitBackups = new HashMap<Integer, Unit>();

	public OurGameController(WorldState ws) {
		this.ws = ws;
	}

	@Override
	public Unit unit(int unit_id) {
		try {
			Unit u = super.unit(unit_id);
			unitBackups.put(unit_id, u);
			return u;
		} catch (Exception e) {
			System.out.println(
					"Unit " + unit_id + " not found, displaying backupd data:");
			System.out
					.println(WorldState.unitToString(unitBackups.get(unit_id)));
			throw e;
		}
	}

	public Unit backupUnit(int unit_id) {
		if (this.canSenseUnit(unit_id))
			throw new RuntimeException(
					"Why do you want a backup, this unit actually exists!"
							+ WorldState.unitToString(unit(unit_id)));
		return unitBackups.get(unit_id);
	}

	@Override
	public void disintegrateUnit(int unit_id) {
		// System.out.println("disintegrate removes unit:");
		ws.unitLists.addLostUnit(unit(unit_id));
		super.disintegrateUnit(unit_id);
	}

	@Override
	public void blueprint(int worker_id, UnitType structure_type,
			Direction direction) {
		blueprint_return(worker_id, structure_type, direction);
	}

	public Unit blueprint_return(int worker_id, UnitType structure_type,
			Direction direction) {
		super.blueprint(worker_id, structure_type, direction);
		Unit u = senseUnitAtLocation(
				unit(worker_id).location().mapLocation().add(direction));
		// System.out.println("blueprint adds new structure:");
		ws.unitLists.addNewUnit(u);
		return u;
	}

	@Override
	public void replicate(int worker_id, Direction direction) {
		replicate_return(worker_id, direction);
	}

	public Unit replicate_return(int worker_id, Direction direction) {
		super.replicate(worker_id, direction);
		Unit u = senseUnitAtLocation(
				unit(worker_id).location().mapLocation().add(direction));
		// System.out.println("replicate adds new worker:");
		ws.unitLists.addNewUnit(u);
		return u;
	}

	@Override
	public void produceRobot(int factory_id, UnitType robot_type) {
		produceRobot_return(factory_id, robot_type);
	}

	public Unit produceRobot_return(int factory_id, UnitType robot_type) {
		super.produceRobot(factory_id, robot_type);
		VecUnitID inFac = unit(factory_id).structureGarrison();
		for (int i = 0; i < inFac.size(); i++) {
			if (!ws.unitLists.myUnits.get(robot_type).contains(inFac.get(i))) {
				Unit result = unit(inFac.get(i));
				// System.out.println("produceRobot adds new robot:");
				ws.unitLists.addNewUnit(result);
				return result;
			}
		}
		return null;
	}
}
