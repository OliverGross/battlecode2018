package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.BehaviourPlan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.framework.behaviours.RangerAttackBehaviour;
import tools.framework.behaviours.RangerFocusTargetBehaviour;
import tools.framework.behaviours.RangerMoveBehaviour;
import tools.framework.behaviours.UnloadBehaviour;

public class RangerAttackPlan extends BehaviourPlan {
	Set<Integer> rangersOnMap = new HashSet<>();

	public RangerAttackPlan(WorldState worldState) {
		super(worldState);
		//Behaviour attackPatternPlan = new RangerAttackBehaviour(this);
		Behaviour attackPatternPlan = new RangerFocusTargetBehaviour();
		Behaviour unloadPlan = new UnloadBehaviour();
		Behaviour moveRangerPlan = new RangerMoveBehaviour(this);

		// unload all units from structures, we want to ATTACK
		addBehaviour(unloadPlan);
		// attack with all rangers who can
		addBehaviour(attackPatternPlan);
		// move all rangers
		addBehaviour(moveRangerPlan);
		// now that we moved, try to attack once more (nearly no
		// performanceloss, bc only checking units that have attack rdy)
		addBehaviour(attackPatternPlan);
	}

	private void updateRangers(WorldState ws) {
		ws.resources.assignAllFreeByType(pid, UnitType.Ranger);
		rangersOnMap = ws.unitLists.myUnits.get(UnitType.Ranger);
	}

	@Override
	public void tick(WorldState ws) {
		this.updateRangers(ws);
		super.tick(ws);
		this.status = Status.RUNNING;
	}

	public Set<Integer> getRangersOnMap() {
		return rangersOnMap;
	}

}
