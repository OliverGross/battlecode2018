package tools.infocaching;

import java.util.ArrayList;

import bc.GameController;
import bc.MapLocation;
import bc.Planet;
import bc.Team;
import bc.Unit;

public class EarthWorldInformation extends WorldInformation {
	
	TurnInformation lastTurn;
	TurnInformation startTurn;
	
	Team myTeam = null;
	
	public EarthWorldInformation(GameController controller) {
		super(controller);
		myTeam = gc.team();
		UnitInformation startTurnUnitInformation = new UnitInformation(myTeam, gc.round(), null);
		long startTurnKarbonite = gc.karbonite();
		KarboniteInformation startTurnKarboniteInformation = new KarboniteInformation(gc.startingMap(Planet.Earth), Planet.Earth);
		ArrayList<MapLocation> startTurnSeenMapLocations = new ArrayList<>();
		lastTurn = null;
		startTurn = new TurnInformation(gc.round(), startTurnUnitInformation, startTurnKarbonite, startTurnKarboniteInformation, startTurnSeenMapLocations);
	}
	
	/**
	 * should be called on the start of every turn to update information
	 */
	@Override
	public void tick() {
		newTurn();
		fetchNewInformation();
	}
	
	/**
	 * 
	 * @return UnitInformation of the previous Turn
	 */
	public UnitInformation getLastTurnUnitInformation() {
		if(lastTurn != null) {
			return lastTurn.unitInformation;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return UnitInformation of the Start of the turn
	 */
	public UnitInformation getStartTurnUnitInformation() {
		return startTurn.unitInformation;
	}

	/**
	 * 
	 * @return karbonite wallet previous turn
	 */
	public long getLastTurnKarbonite() {
		if(lastTurn != null) {
			return lastTurn.karbonite;
		} else {
			return -1;
		}
	}

	/**
	 * 
	 * @return karbonite wallet at the start of this turn
	 */
	public long getStartTurnKarbonite() {
		return startTurn.karbonite;
	}

	/**
	 * 
	 * @return karbonite deposit information last turn
	 */
	public KarboniteInformation getLastTurnKarboniteInformation() {
		if(lastTurn != null) {
			return lastTurn.karboniteInformation;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return karbonite deposit information at the start of this turn
	 */
	public KarboniteInformation getStartTurnKarboniteInformation() {
		return startTurn.karboniteInformation;
	}

	/**
	 * 
	 * @return seen map locations last turn
	 */
	public ArrayList<MapLocation> getLastTurnSeenMapLocations() {
		if(lastTurn != null) {
			return lastTurn.seenMapLocations;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return seen map locations at the start of this turn
	 */
	public ArrayList<MapLocation> getStartTurnSeenMapLocations() {
		return startTurn.seenMapLocations;
	}
	
	/**
	 * replaces lastTurnInformation
	 * creates some structures for thisTurnInformations
	 */
	private void newTurn() {
		lastTurn = startTurn;
		UnitInformation startTurnUnitInformation = new UnitInformation(myTeam, gc.round(), lastTurn.unitInformation);
		KarboniteInformation startTurnKarboniteInformation = new KarboniteInformation(lastTurn.karboniteInformation);
		startTurn = new TurnInformation(gc.round(), startTurnUnitInformation, gc.karbonite(), startTurnKarboniteInformation, new ArrayList<MapLocation>());
	}

	/**
	 * Scans every MapLocation for Units and Karbonite
	 * updates current Wallet
	 */
	private void fetchNewInformation() {
		long width = gc.startingMap(Planet.Earth).getWidth();
		long height = gc.startingMap(Planet.Earth).getHeight();
		MapLocation currentLocation = new MapLocation(Planet.Earth, 0, 0);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				currentLocation.setX(x);
				currentLocation.setY(y);
				
				if(gc.canSenseLocation(currentLocation)) {
					if(gc.hasUnitAtLocation(currentLocation)) {
						Unit u = gc.senseUnitAtLocation(currentLocation);
						startTurn.unitInformation.addUnit(u);
					}
					startTurn.seenMapLocations.add(currentLocation);
				}
				//Karbonite-Location-Information
				long karboniteAtCurrentLocation = gc.karboniteAt(currentLocation);
				startTurn.karboniteInformation.update(currentLocation, karboniteAtCurrentLocation);
			}
		}
		//Wallet-Karbonite-Information
		startTurn.karbonite = gc.karbonite();
	}

}
