package tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bc.AsteroidStrike;
import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.Unit;
import bc.VecUnit;
import tools.framework.WorldState;
import tools.pathfinding.HashablePoint;

public class PGA {

	// magic numbers
	public static final double importanceKarboniteVsSpace = 100.0;
	public static final double karbonitePerWorker = 400.0;
	public static final double karboniteNearWorker = 80;
	// public static final double karboniteDistanceReduction = 0.5; //Higher
	// numbers mean karbonite should be closer and less workers
	private final int preferKnightsRange = 5;

	private static int runningInstances = 0;

	public HashMap<Integer, MapLocation> myWorkers = new HashMap<Integer, MapLocation>();
	public HashMap<Integer, MapLocation> oppWorkers = new HashMap<Integer, MapLocation>();
	WorldState ws;
	PlanetMap startMap;
	public List<List<Long>> karbLocs = new ArrayList<List<Long>>();
	public long initialTotalKarbonite;
	public int[][] closestObstacleDistSqrd;
	public double[][] nearbyKarbonite;
	public double[][] strategicValues;
	public Map<Integer, int[][]> distanceToInitialWorker = new HashMap<Integer, int[][]>();
	public int replicateWorker;
	public int magicReplicate;
	public HashMap<Integer, Integer> replicateWorkers;
	public List<MapLocation> strategicLocations = new ArrayList<MapLocation>();
	public Set<HashablePoint> reachablePoints = new HashSet<HashablePoint>();
	public boolean enemyIsWalled = false;
	public boolean preferKnights = false;

	private void computeDistancesToInitialWorkers() {
		int w = (int) startMap.getWidth();
		int h = (int) startMap.getHeight();
		Planet planet = startMap.getPlanet();
		for (int wid : myWorkers.keySet()) {
			int[][] result = new int[w][h];
			for (int x = 0; x < w; x++)
				for (int y = 0; y < h; y++)
					result[x][y] = -1;
			int x0 = myWorkers.get(wid).getX();
			int y0 = myWorkers.get(wid).getY();
			result[x0][y0] = 0;
			Set<HashablePoint> openSet = new HashSet<HashablePoint>();
			Set<HashablePoint> nextOpenSet = new HashSet<HashablePoint>();
			openSet.add(new HashablePoint(myWorkers.get(wid)));
			while (!openSet.isEmpty()) {
				HashablePoint p = openSet.iterator().next();
				openSet.remove(p);
				int minimum = 3000;
				for (Direction d : DirHelp.adjacentDirections) {
					HashablePoint p_ = new HashablePoint(
							DirHelp.addDirection(p.point, d));
					if (p_.get(0) < 0 || p_.get(0) >= w || p_.get(1) < 0
							|| p_.get(1) >= h)
						continue;
					if (result[p_.get(0)][p_.get(1)] >= 0) {
						minimum = Math.min(minimum,
								result[p_.get(0)][p_.get(1)]);
					} else if (startMap.isPassableTerrainAt(new MapLocation(
							planet, p_.get(0), p_.get(1))) == 1) {
						nextOpenSet.add(p_);
					}
				}
				if (minimum < 3000)
					result[p.get(0)][p.get(1)] = minimum + 1;

				if (openSet.isEmpty()) {
					openSet = nextOpenSet;
					nextOpenSet = new HashSet<HashablePoint>();
				}
			}
			distanceToInitialWorker.put(wid, result);
			// System.out.println("distanceToInitalWorker:" + wid);
			// for (int y = h - 1; y >= 0; y--) {
			// for (int x = 0; x < w; x++) {
			// System.out.print(
			// String.format("%03d", ((int) result[x][y])) + " ");
			// }
			// System.out.println();
			// }
		}
	}

	public void computeReachablePoints() {
		if (startMap.getPlanet() == Planet.Mars)
			return;
		Set<HashablePoint> openList = new HashSet<HashablePoint>();
		for (int i = 0; i < startMap.getInitial_units().size(); i++) {
			Unit u = startMap.getInitial_units().get(i);
			if (u.team() == ws.me)
				openList.add(new HashablePoint(u.location().mapLocation()));
		}
		while (!openList.isEmpty()) {
			HashablePoint p = openList.iterator().next();
			openList.remove(p);
			reachablePoints.add(p);
			for (Direction d : DirHelp.adjacentDirections) {
				HashablePoint p_ = new HashablePoint(
						DirHelp.addDirection(p.point, d));
				MapLocation loc = new MapLocation(Planet.Earth, p_.get(0),
						p_.get(1));
				if (!startMap.onMap(loc))
					continue;
				if (0 == startMap.isPassableTerrainAt(loc))
					continue;
				if (!reachablePoints.contains(p_))
					openList.add(p_);
			}
		}
		enemyIsWalled = true;
		for (int i = 0; i < startMap.getInitial_units().size(); i++) {
			Unit u = startMap.getInitial_units().get(i);
			if (u.team() == ws.opp) {
				if (reachablePoints.contains(
						new HashablePoint(u.location().mapLocation()))) {
					enemyIsWalled = false;
					break;
				}
			}
		}
		if (enemyIsWalled)
			System.out.println(
					"We cant reach the enemy and he cant reach us! Lets build our marsprogram!");
	}

	public void computeStrategicLocations() {
		final class T {
			double prio;
			MapLocation data;

			public T(double a, MapLocation loc) {
				prio = a;
				data = loc;
			}
		}
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		strategicValues = new double[width][height];
		List<T> orderedByStrategy = new ArrayList<T>();
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				strategicValues[x][y] = closestObstacleDistSqrd[x][y]
						+ importanceKarboniteVsSpace * nearbyKarbonite[x][y];
				orderedByStrategy.add(new T(strategicValues[x][y],
						new MapLocation(planet, x, y)));
			}
		Collections.sort(orderedByStrategy,
				(T n1, T n2) -> (int) (n1.prio < n2.prio ? 1
						: n1.prio > n2.prio ? -1 : 0));
		strategicLocations.clear();
		for (T t : orderedByStrategy)
			strategicLocations.add(t.data);

		// // do print out of computed arrays
		// System.out.println("nearbyKarbonite:");
		// for (int y = height - 1; y >= 0; y--) {
		// for (int x = 0; x < width; x++) {
		// System.out.print(
		// String.format("%04d", ((int) nearbyKarbonite[x][y]))
		// + " ");
		// }
		// System.out.println();
		// }
		// // do print out of computed arrays
		// System.out.println("karbsLocs:");
		// for (int y = height - 1; y >= 0; y--) {
		// for (int x = 0; x < width; x++) {
		// System.out.print(String.format("%03d",
		// ((int) (long) karbLocs.get(x).get(y))) + " ");
		// }
		// System.out.println();
		// }
		// // do print out of computed arrays
		// System.out.println("closestObstacleDistSqrd:");
		// for (int y = height - 1; y >= 0; y--) {
		// for (int x = 0; x < width; x++) {
		// System.out.print(String.format("%04d",
		// ((int) closestObstacleDistSqrd[x][y])) + " ");
		// }
		// System.out.println();
		// }
		// do print out of computed arrays
		//System.out.println("strategicValues:");
//		for (int y = height - 1; y >= 0; y--) {
//			for (int x = 0; x < width; x++) {
//				System.out.print(
//						String.format("%05d", ((int) strategicValues[x][y]))
//								+ " ");
//			}
//			System.out.println();
//		}
	}

	public void computeNearbyKarbonite() {
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		nearbyKarbonite = new double[width][height];
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				int distSqrd = closestObstacleDistSqrd[x][y] - 1;
				if (distSqrd < 0)
					continue;
				nearbyKarbonite[x][y] = 0;
				for (int dx = -(int) Math.sqrt(distSqrd); dx <= (int) Math
						.sqrt(distSqrd); dx++) {
					int x_ = x + dx;
					// if (x_ < 0 || x_ >= width)
					// continue;
					int border = (int) (Math.sqrt(distSqrd - dx * dx) + 1e-3);
					for (int dy = -border; dy <= border; dy++) {
						int y_ = y + dy;
						// if (y_ < 0 || y_ >= height)
						// continue;
						nearbyKarbonite[x][y] += karbLocs.get(x_).get(y_)
								/ (Math.sqrt(dx * dx + dy * dy) + 1.0);
						// System.out.println(nearbyKarbonite[x][y]);
					}
				}
			}
	}

	// public int magicReplicateNumber() {
	// int magicNumber = (int) (initialTotalKarbonite / karbonitePerWorker);
	// if (magicNumber > 10) {
	// return 10;
	// }
	// return magicNumber;
	// }
	//
	// public int workerToReplicate() {
	// long mostKarb = -1;
	// int worker_id = -1;
	// for (int j : myWorkers.keySet()) {
	// int x = myWorkers.get(j).getX();
	// int y = myWorkers.get(j).getY();
	// // System.out.println("The most Karbonite found was" + mostKarb +
	// // "My Location was" + myWorkers.get(j) );
	// if (!(x == 0))
	// x -= 1;
	// else
	// x +=1;
	// if (nearbyKarbonite[x][y] > mostKarb) {
	// mostKarb = (long) nearbyKarbonite[x][y];
	// worker_id = j;
	// }
	// }
	// // System.out.println("The most Karbonite found was" + mostKarb);
	// magicReplicate = (int) (mostKarb / karboniteNearWorker);
	// return worker_id;
	// }

	public void workerReplication() {
		double mostKarb = 0;
		for (int worker : myWorkers.keySet()) {
			double karboniteHelper = 0;
			for (HashablePoint location : reachablePoints) {
				int distanceHelper = distanceToInitialWorker
						.get(worker)[location.get(0)][location.get(1)];
				if (distanceHelper > 0) {
					karboniteHelper += karbLocs.get(location.get(0))
							.get(location.get(1)) / (1.0 + distanceHelper);
				}
			}
			if (karboniteHelper > mostKarb) {
				replicateWorker = worker;
				magicReplicate = (int) (karboniteHelper / karboniteNearWorker);
				mostKarb = karboniteHelper;
			}
		}
	}

	public void multipleWorkerReplication() {
		replicateWorkers = new HashMap<Integer, Integer>();
		Set<Integer> workerHelper = new HashSet<Integer>();
		//workerHelper = myWorkers.keySet();
		HashMap<Integer, Double> workerKarb = new HashMap<Integer, Double>();
		int bestWorker = -1;
		double mostKarb = -1;
		for (int worker : myWorkers.keySet()) {
			double karboniteHelper = 0;
			workerHelper.add(worker);
			for (HashablePoint location : reachablePoints) {
				int distanceHelper = distanceToInitialWorker
						.get(worker)[location.get(0)][location.get(1)];
				if (distanceHelper > 0) {
					karboniteHelper += karbLocs.get(location.get(0))
							.get(location.get(1)) / (1.0 + distanceHelper);
				}
			}
			workerKarb.put(worker, karboniteHelper);
			if (karboniteHelper > mostKarb) {
				bestWorker = worker;
				magicReplicate = (int) (karboniteHelper / karboniteNearWorker);
				mostKarb = karboniteHelper;
			}
		}
		double mostKarbHelper = mostKarb;
		
		while (!workerHelper.isEmpty()) {
			for (int worker : myWorkers.keySet()) {
				if (workerHelper.contains(worker)) {
					if (workerKarb.get(worker) == mostKarbHelper) {
						HashMap<Integer, Double> workerKarbHelper = new HashMap<Integer, Double>();
						workerKarbHelper.put(worker, mostKarbHelper);
						for (int secondaryWorker : myWorkers.keySet()) {
							HashablePoint secondaryLocation = new HashablePoint(myWorkers.get(secondaryWorker));
							if (distanceToInitialWorker
									.get(worker)[secondaryLocation.get(0)][secondaryLocation.get(1)] > -1) {
								workerHelper.remove(secondaryWorker);
								workerKarbHelper.put(secondaryWorker, workerKarb.get(secondaryWorker));
							}
						}
						workerHelper.remove(worker);
						for (int together : workerKarbHelper.keySet()) {
							int replicationNumber = (int) (mostKarbHelper/(karboniteNearWorker*workerKarbHelper.keySet().size()+1.0));
							replicateWorkers.put(together, replicationNumber);
						}
						mostKarbHelper = -1;
						for (int work : myWorkers.keySet()) {
							if (workerHelper.contains(work) && workerKarb.get(work) > mostKarbHelper) {
								mostKarbHelper = workerKarb.get(work);
							}
						}
					}
				}
			}
		}
	}

	public void calculateTotalKarbonite() {
		initialTotalKarbonite = 0;
		for (List<Long> j : karbLocs) {
			for (long k : j) {
				initialTotalKarbonite += k;
			}
		}
	}

	public void computeClosestObstacle() {
		Planet planet = ws.gc.planet();
		int width = (int) ws.gc.startingMap(planet).getWidth();
		int height = (int) ws.gc.startingMap(planet).getHeight();
		closestObstacleDistSqrd = new int[width][height];
		Set<HashablePoint> obstacles = new HashSet<HashablePoint>();
		int maxDist = 25;
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				int minDist = x + 1;
				minDist = Math.min(minDist, width - x);
				minDist = Math.min(minDist, y + 1);
				minDist = Math.min(minDist, height - y);
				closestObstacleDistSqrd[x][y] = minDist * minDist;
				MapLocation loc = new MapLocation(planet, x, y);
				if (0 == ws.gc.startingMap(planet).isPassableTerrainAt(loc)) {
					obstacles.add(new HashablePoint(loc));
					closestObstacleDistSqrd[x][y] = 0;
				}
			}
		for (HashablePoint p : obstacles) {
			for (int dx = -maxDist; dx <= maxDist; dx++) {
				int x = p.point[0] + dx;
				if (x < 0 || x >= width)
					continue;
				int border = (int) (Math.sqrt(maxDist * maxDist - dx * dx)
						+ 1e-3);
				for (int dy = -border; dy <= border; dy++) {
					int y = p.point[1] + dy;
					if (y < 0 || y >= height)
						continue;
					int dist = dx * dx + dy * dy;
					closestObstacleDistSqrd[x][y] = Math
							.min(closestObstacleDistSqrd[x][y], dist);
				}
			}
		}
		// // do print out of computed arrays
		// System.out.println("closestObstacleDistSqrd:");
		// for (int y = height - 1; y >= 0; y--) {
		// for (int x = 0; x < width; x++) {
		// System.out.print(
		// String.format("%04d", ((int) closestObstacleDistSqrd[x][y]))
		// + " ");
		// }
		// System.out.println();
		// }
	}

	public PGA(WorldState ws) {
		if (runningInstances >= 1)
			throw new RuntimeException("Let only one PGA instance exist!");
		runningInstances++;

		startMap = ws.gc.startingMap(ws.gc.planet());
		this.ws = ws;
		initWorkers();
		initKarb(ws);
		computeClosestObstacle();
		computeNearbyKarbonite();
		computeStrategicLocations();
		calculateTotalKarbonite();
		// magicReplicate = magicReplicateNumber();
		// replicateWorker = workerToReplicate();
		computeReachablePoints();
		computeDistancesToInitialWorkers();
		workerReplication();
		multipleWorkerReplication();
		preferKnights = preferKnights(ws);
	}

	public void initWorkers() {
		VecUnit initial = startMap.getInitial_units();
		for (int i = 0; i < initial.size(); i++) {
			Unit worker = initial.get(i);
			if (worker.team().equals(ws.me)) {
				myWorkers.put(worker.id(), worker.location().mapLocation());
			} else {
				oppWorkers.put(worker.id(), worker.location().mapLocation());
			}
		}

	}

	public void initKarb(WorldState worldstate) {
		long width;
		long height;
		width = startMap.getWidth();
		height = startMap.getHeight();
		MapLocation loc = new MapLocation(startMap.getPlanet(), 0, 0);
		for (int x = 0; x < width; x++) {
			karbLocs.add(new ArrayList<Long>());
			for (int y = 0; y < height; y++) {
				loc.setX(x);
				loc.setY(y);
				karbLocs.get(x).add(startMap.initialKarboniteAt(loc));
				// System.out.println(x + "," + y + " = " +
				// initialmap.initialKarboniteAt(loc));
			}
		}
		if (startMap.getPlanet() == Planet.Earth)
			return;
		for (int round = 0; round < 800; round++) {
			if (ws.gc.asteroidPattern().hasAsteroid(round)) {
				AsteroidStrike strike = ws.gc.asteroidPattern().asteroid(round);
				int x = strike.getLocation().getX();
				int y = strike.getLocation().getY();
				long addKarb = strike.getKarbonite();
				karbLocs.get(x).set(y, karbLocs.get(x).get(y) + addKarb);
			}
		}
	}

	private boolean preferKnights(WorldState worldState) {
		int largest_dist = 0;
		ArrayList<MapLocation> enemy = new ArrayList<MapLocation>(
				oppWorkers.values());
		ArrayList<MapLocation> me = new ArrayList<MapLocation>(
				myWorkers.values());
		for (MapLocation enem : enemy) {
			largest_dist = Math.max(largest_dist,
					ws.pathfinding.getDistance(me, enem));
		}
		if (largest_dist <= preferKnightsRange) {
			System.out.println("We are doing KnightRush instead of Rangers!");
			return true;
		}
		return false;
	}

	/**
	 * Evaluates starting map and enemy points The evaluated values are: 0 -
	 * minimal distance to enemy 1 - passableTerrain counter in adjacent squares
	 * 
	 * @return HashMap<id, ArrayList<Integer> values>
	 */
	public HashMap<Integer, ArrayList<Integer>> getWorkerEvaluation() {
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();
		ArrayList<Integer> tempValues;
		for (HashMap.Entry<Integer, MapLocation> pair : myWorkers.entrySet()) {
			tempValues = new ArrayList<Integer>();
			// evaluate closest worker distance
			tempValues.add(ws.pathfinding.getDistance(
					new ArrayList<MapLocation>(oppWorkers.values()),
					pair.getValue()));
			// evaluate passable terrain in adjacent squares
			int passable = 0;
			for (Direction dir : DirHelp.adjacentDirections) {
				MapLocation adjacentLoc = pair.getValue().add(dir);
				if (adjacentLoc.getX() >= 0
						&& adjacentLoc.getX() < startMap.getWidth()
						&& adjacentLoc.getY() >= 0
						&& adjacentLoc.getY() < startMap.getHeight()) {
					{
						passable += startMap.isPassableTerrainAt(adjacentLoc);
					}
				}
			}
			tempValues.add(passable);
			map.put(pair.getKey(), tempValues);
		}
		return map;
	}

	// real PGA stuff starts below this line ##############################
	// ####################################################################
	public enum Strategy {
		Rush, Defense, Evacuate,
	}

	public Strategy getInitialStrategy() {
		return Strategy.Defense;
	}

}
