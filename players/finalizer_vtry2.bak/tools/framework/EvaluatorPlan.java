package tools.framework;

import java.util.ArrayList;
import java.util.List;

public abstract class EvaluatorPlan extends MacroPlan {
	private List<Evaluator> evaluators;

	public EvaluatorPlan(WorldState worldState) {
		super(worldState);
		this.evaluators = new ArrayList<Evaluator>();
	}

	public final void addEvaluator(Evaluator e) {
		evaluators.add(e);
	}

	@Override
	public void computePriorities(WorldState worldState) {
		for (Plan p : plans) {
			p.priority = 0.0;
			for (Evaluator e : evaluators) {
				p.priority += e.evaluate(p, worldState);
			}
		}
	}

}
