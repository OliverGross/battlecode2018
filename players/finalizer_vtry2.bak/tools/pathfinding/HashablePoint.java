package tools.pathfinding;

import java.util.Arrays;

import bc.MapLocation;

public final class HashablePoint {
	public final int[] point;

	public HashablePoint(int[] a) {
		this.point = a;
	}

	public HashablePoint(MapLocation a) {
		this.point = new int[] { a.getX(), a.getY() };
	}

	public int get(int i) {
		return point[i];
	}

	@Override
	public int hashCode() {
		return point[0] + 1000 * point[1];
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HashablePoint other = (HashablePoint) obj;
		if (!Arrays.equals(this.point, other.point))
			return false;
		return true;
	}
}
