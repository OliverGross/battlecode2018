package tools.framework.buildorder;

import java.util.ArrayList;
import java.util.Set;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import tools.framework.WorldState;

public class FinalizerUnitBuildOrder implements UnitBuildOrderEvaluator {

	@Override
	public UnitType getNextUnit(WorldState ws, Unit factory) {
		int healers = ws.unitLists.myUnits.get(UnitType.Healer).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Healer);
		int rangers = ws.unitLists.myUnits.get(UnitType.Ranger).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Ranger);
		int knights = ws.unitLists.myUnits.get(UnitType.Knight).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Knight);
		int mages = ws.unitLists.myUnits.get(UnitType.Mage).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Mage);
		int worker = ws.unitLists.myUnits.get(UnitType.Worker).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Worker);
		int rockets = ws.unitLists.myUnits.get(UnitType.Rocket).size();

		if (worker == 0)
			return UnitType.Worker;

		int totalDamageDealers = rangers + knights + mages;

		if ((totalDamageDealers < 5 && ws.gc.round() < 120
				&& ws.pga.preferKnights) || closestFactory(ws, factory) < 7)
			return UnitType.Knight;

		int prioRanger = 3;
		if (healers == 0)
			healers = 1; // third unit should be a healer
		int prioHealer = totalDamageDealers / healers;

		if (ws.gc.researchInfo().getLevel(UnitType.Healer) == 3) {
			prioHealer *= 2;
		}

		if((totalDamageDealers+healers) > 120 && ws.gc.round() > 300) {
			return null;
		}
		
		if (totalDamageDealers > 30 && ws.gc.round() > 680)
			return null;

		if (prioHealer > prioRanger) {
			return UnitType.Healer;
		} else {
			return UnitType.Ranger;
		}

	}

	ArrayList<MapLocation> enemyFactoryLocations = new ArrayList<>();
	ArrayList<MapLocation> facML = new ArrayList<>();

	private int closestFactory(WorldState ws, Unit factory) {
		enemyFactoryLocations.clear();
		facML.clear();
		if (!factory.location().isOnMap())
			return 1000;

		MapLocation myFactoryLocation = factory.location().mapLocation();
		facML.add(myFactoryLocation);

		Set<Integer> enemyFactories = ws.unitLists.enemyUnits.get(UnitType.Factory);

		int closestDist = 1000;

		for (int enemyId : enemyFactories) {
			if (ws.gc.canSenseUnit(enemyId)) {
				Unit enemy = ws.gc.unit(enemyId);
				if (enemy.location().isOnMap()) {
					enemyFactoryLocations.add(enemy.location().mapLocation());
				}

			}
		}

		for (MapLocation enem : enemyFactoryLocations) {
			int dist = ws.pathfinding.getOpenPathDistance(facML, enem);
			if (dist == -1) {
				continue;
			} else {
				closestDist = Math.min(closestDist, dist);
			}
		}
		return closestDist;
	}

}
