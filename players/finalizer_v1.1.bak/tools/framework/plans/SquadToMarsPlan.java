package tools.framework.plans;

import java.util.ArrayList;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class SquadToMarsPlan extends Plan {
	ArrayList<MapLocation> loadableRocketLocations = new ArrayList<MapLocation>();
	ArrayList<Integer> squad = new ArrayList<Integer>();
	ArrayList<Integer> workers = new ArrayList<Integer>();

	public SquadToMarsPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	protected void updateResources(WorldState ws) {
		loadableRocketLocations.clear();
		squad.clear();
		workers.clear();

		// Rockets
		Set<Integer> myRockets = ws.unitLists.myUnits.get(UnitType.Rocket);
		if (myRockets.size() == 0) {
			// WHY A SQUAD IF WE DONT EVEN HAVE ROCKETS
			return;
		}
		for (int rocketId : myRockets) {
			Unit rocket = ws.gc.unit(rocketId);
			if (!rocket.location().isOnMap() || rocket.rocketIsUsed() == 1
					|| rocket.structureIsBuilt() == 0
					|| rocket.structureGarrison().size() >= rocket
							.structureMaxCapacity())
				continue;

			loadableRocketLocations.add(rocket.location().mapLocation());
		}
		// Squad
		for (int squadId : ws.unitLists.myUnits.get(UnitType.Ranger)) {
			if (ws.gc.unit(squadId).location().isOnMap()) {
				squad.add(squadId);
			}
		}
		for (int squadId : ws.unitLists.myUnits.get(UnitType.Healer)) {
			if (ws.gc.unit(squadId).location().isOnMap()) {
				squad.add(squadId);
			}
		}
		for (int squadId : ws.unitLists.myUnits.get(UnitType.Knight)) {
			if (ws.gc.unit(squadId).location().isOnMap()) {
				squad.add(squadId);
			}
		}
		for (int squadId : ws.unitLists.myUnits.get(UnitType.Mage)) {
			if (ws.gc.unit(squadId).location().isOnMap()) {
				squad.add(squadId);
			}
		}
	}

	@Override
	protected void tick(WorldState ws) {

		if ((ws.gc.round() >= ws.evacutateTime || ws.planetWon
				|| ws.pga.enemyIsWalled)) {
			updateResources(ws);
			if (loadableRocketLocations.size() == 0) {
				return;
			}
			if (ws.gc.round() >= ws.evacutateTime || ws.planetWon) {
				retreatToRocketsAndLoad();
			} else {
				retreatToRockets();
			}
		}

	}

	private void retreatToRockets() {
		for (int squadId : squad) {
			ws.lll.goTo(squadId, loadableRocketLocations);
		}

	}

	private void retreatToRocketsAndLoad() {
		for (int squadId : squad) {
			ws.lll.goTo(squadId, loadableRocketLocations);

			// check if maybe already there and load
			if (ws.gc.isMoveReady(squadId)) {
				MapLocation squadLoc = ws.gc.unit(squadId).location()
						.mapLocation();
				for (MapLocation rocketLoc : loadableRocketLocations) {
					if (squadLoc.isAdjacentTo(rocketLoc)) {
						int rocketId = ws.gc.senseUnitAtLocation(rocketLoc)
								.id();

						if (ws.gc.canLoad(rocketId, squadId)) {
							// Don't let them free if they are already loaded
							ws.resources.setUnitFreeById(squadId);
							ws.resources.assignById(squadId, pid);
							ws.gc.load(rocketId, squadId);
						}
					}
				}
			}
		}
	}

}
