package tools.framework;

/**
 * The idea of this framework is to have plans that can be quite complicated and
 * even allow long-running plans over multiple ticks. Therefore it makes sense
 * to introduce for every plan a status variable describing its current abstract
 * state.
 */
public enum Status {
	PENDING, RUNNING, FINAL, FAILED;

	/**
	 * Returns the combination of two statuses following rules:
	 * <ul>
	 * <li>they are the same, then it is obvious what to return</li>
	 * <li>either of a and b failed, the combination failed</li>
	 * <li>one status is final or running, return running</li>
	 * </ul>
	 */
	public static Status combine(Status a, Status b) {
		if (a == b)
			return a;
		if (a == Status.RUNNING || b == Status.RUNNING)
			return Status.RUNNING;
		if (a.isTerminal() && b.isTerminal())
			return Status.FAILED;
		return Status.PENDING;
	}

	public boolean isTerminal() {
		if (this == Status.FAILED || this == Status.FINAL)
			return true;
		return false;
	}
}