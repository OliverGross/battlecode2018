package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;

public class TargetInfo {
	int x;
	int y;
	long health;
	long defense;
	MapLocation location;
	int targetId;
	UnitType type;
	List<Integer> unitsThatTarget;
	
	public TargetInfo(ArrayList<Integer> unitsThatTarget, Unit target) {
		if(target.location().isOnMap()) {
			type = target.unitType();
			health = target.health();
			location = target.location().mapLocation();
			targetId = target.id();
			this.unitsThatTarget = unitsThatTarget;
		} else {
			location = null;
			System.out.println("targetinfo location was not on map");
		}
		if(target.unitType() == UnitType.Knight) {
			this.defense = target.knightDefense();
		} else {
			this.defense = 0;
		}
	}

	public void deleteAttacker(Unit u) {
		unitsThatTarget.remove(Integer.valueOf(u.id()));
	}

	public boolean isDead() {
		long damage = unitsThatTarget.size()*(30-defense);
		return damage >= health;
	}
}
