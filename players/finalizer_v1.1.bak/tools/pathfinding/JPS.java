package tools.pathfinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import bc.Direction;
import tools.DirHelp;

public class JPS {
	public StaticMapGrid map;
	public static long compTime = 0;
	public static int totalCalls = 0;
	private static int runningInstances = 0;
	public static int numberUnsuccessfullPathfindings = 0;

	private Set<HashablePoint> goals;

	private class Node {
		@Override
		public int hashCode() {
			return point[0] + 1000 * point[1];
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (!Arrays.equals(point, other.point))
				return false;
			return true;
		}

		int[] point;
		Node prev;
		double g;
		double h;
		double f;

		public Node(int[] point) {
			this.point = point;
			prev = null;
			g = 0;
			h = getH(point, goals);
			f = g + h;
		}

		public Node(int[] point, Node previous) {
			this.point = point;
			prev = previous;
			g = previous.g + Geometry.chebyshev_distance(previous.point, point);
			h = getH(point, goals);
			f = g + h;
		}

		@Override
		public String toString() {
			if (prev != null) {
				return "Point:" + Geometry.toString(point) + " g:" + g + " h:"
						+ h + " Previous:" + Geometry.toString(prev.point);
			} else {
				return "Point:" + Geometry.toString(point) + " g:" + g + " h:"
						+ h + " Previous:" + "null";
			}

		}
	}

	private PriorityQueue<Node> openList = new PriorityQueue<Node>(11,
			(Node n1, Node n2) -> ((int) (n1.f - n2.f)));
	private Set<Node> closedList = new HashSet<Node>();

	public JPS() {
		if (runningInstances == 0) {
			runningInstances += 1;
		} else {
			throw new RuntimeException(
					"Make sure to only start up one JPS object!");
		}
	}

	public JPS(StaticMapGrid grid) {
		if (runningInstances == 0) {
			runningInstances += 1;
		} else {
			throw new RuntimeException(
					"Make sure to only start up one JPS object!");
		}
		map = grid;
	}

	private void reset_JPS() {
		openList.clear();
		closedList.clear();
	}

	private static List<int[]> convertJumpPath(List<int[]> jumpPath) {
		List<int[]> result = new ArrayList<int[]>();
		result.add(jumpPath.get(0));
		int[] cur = jumpPath.get(0);
		for (int i = 1; i < jumpPath.size(); i++) {
			Direction dir = getDirection(Geometry.diff(jumpPath.get(i), cur));
			while (!Arrays.equals(cur, jumpPath.get(i))) {
				cur = DirHelp.addDirection(cur, dir);
				result.add(cur);
			}
		}
		return result;
	}

	private static List<HashablePoint> convert2HashablePoint(List<int[]> path) {
		List<HashablePoint> result = new ArrayList<HashablePoint>();
		for (int[] i : path) {
			result.add(new HashablePoint(i));
		}
		return result;
	}

	private void setObstacles(Set<HashablePoint> obstacles,
			boolean impassable) {
		for (HashablePoint p : obstacles) {
			map.setDynamic(p.point, impassable);
		}
	}

	public List<HashablePoint> getPath(Set<HashablePoint> start,
			Set<HashablePoint> goal, Set<HashablePoint> obstacles) {
		setObstacles(obstacles, true);
		List<HashablePoint> path = getPath(start, goal);
		setObstacles(obstacles, false);
		return path;
	}

	public List<HashablePoint> getPath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		return convert2HashablePoint(convertJumpPath(getJumpPath(start, goal)));
	}

	public List<HashablePoint> getPath(HashablePoint start,
			HashablePoint goal) {
		HashSet<HashablePoint> starts = new HashSet<HashablePoint>();
		starts.add(start);
		HashSet<HashablePoint> goals = new HashSet<HashablePoint>();
		goals.add(goal);
		return getPath(starts, goals);
	}

	public List<int[]> getJumpPath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		Set<HashablePoint> removedGoalObstacles = new HashSet<HashablePoint>();
		for (HashablePoint g : goal)
			if (map.get(g.point)) {
				map.setDynamic(g.point, false);
				removedGoalObstacles.add(g);
			}
		long t0 = System.nanoTime();
		List<int[]> result = computePath(start, goal);
		long t1 = System.nanoTime();
		for (HashablePoint g : removedGoalObstacles)
			map.setDynamic(g.point, true);
		compTime += t1 - t0;
		// System.out.println("Single JPS call took: " + (t1 - t0) * 1e-6 +
		// "ms");
		totalCalls += 1;
		if (result.size() == 1) {
			numberUnsuccessfullPathfindings++;
		}
		return result;
	}

	public List<int[]> computePath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		reset_JPS();
		this.goals = goal;
		Node currentNode;
		// add the start to openList
		for (HashablePoint s : start)
			openList.add(new Node(s.point));
		do {
			currentNode = openList.poll();
			if (closedList.contains(currentNode))
				continue;
			// check if goal is reached
			if (goal.contains(new HashablePoint(currentNode.point))) {
				return nodeToPath(currentNode);
			}
			// mark currentNode as already visited
			closedList.add(currentNode);

			// add successors to openList
			addSuccessors(currentNode);
		} while (!openList.isEmpty());// && openList.size() < 500);
		List<Node> orderedClosedList = new ArrayList<Node>(closedList);
		orderedClosedList.sort((Node n1,
				Node n2) -> ((int) ((n1.h - n2.h) * 2500 + (n1.g - n2.g))));
		// System.out.println(
		// "There was no path found, returning the next best thing! Dont let
		// this happen to often, this takes a long time!");
		return nodeToPath(orderedClosedList.get(0));
	}

	/**
	 * Convert a linked list (Node->Previous) to an ArrayList<int[]>
	 * 
	 * @param node
	 * @return Path to n
	 */
	private ArrayList<int[]> nodeToPath(Node node) {
		Node curNode = node;
		if (curNode == null)
			throw new RuntimeException(
					"Something went wrong! nodeToPath should not be called with a null argument!");
		ArrayList<int[]> path = new ArrayList<int[]>();
		do {
			path.add(curNode.point);
			curNode = curNode.prev;
		} while (curNode != null);
		Collections.reverse(path);
		return path;
	}

	/**
	 * Add the successors (forced and natural neighbors) to the openList
	 * 
	 * @param node
	 */
	private void addSuccessors(Node node) {
		List<Direction> neighbours = prunedNeighbors(node);
		for (Direction neighborDir : neighbours) {
			int[] neighbor = jump(neighborDir, node);
			if (neighbor != null) {
				Node newNode = new Node(neighbor, node);
				if (!closedList.contains(newNode)) {
					openList.add(newNode);
				}
			}
		}
	}

	/**
	 * Omit unnecessary neighbors
	 * 
	 * @param node
	 * @param p
	 * @return list of natural and forced neighbors
	 */
	private List<Direction> prunedNeighbors(Node node) {
		if (node.prev == null)
			return Arrays.asList(DirHelp.adjacentDirections);
		ArrayList<Direction> result = new ArrayList<Direction>();

		Direction direction = getDirection(
				Geometry.diff(node.point, node.prev.point));

		if (!DirHelp.isDiagonal(direction)) {
			// add natural neighbor
			result.add(direction);
			// check for forced neighbors
			if (map.get(DirHelp.addDirection(node.point,
					DirHelp.addAngle(direction, -2)))) {
				result.add(DirHelp.addAngle(direction, -1));
			}
			if (map.get(DirHelp.addDirection(node.point,
					DirHelp.addAngle(direction, 2)))) {
				result.add(DirHelp.addAngle(direction, 1));
			}

		} else {
			// add natural neighbors
			result.add(direction);
			result.add(DirHelp.addAngle(direction, -1));
			result.add(DirHelp.addAngle(direction, 1));
			// check for forced neighbors
			if (map.get(DirHelp.addDirection(node.point,
					DirHelp.addAngle(direction, -3)))) {
				result.add(DirHelp.addAngle(direction, -2));
			}
			if (map.get(DirHelp.addDirection(node.point,
					DirHelp.addAngle(direction, 3)))) {
				result.add(DirHelp.addAngle(direction, 2));
			}
		}
		return result;
	}

	private boolean hasForcedNeighbors_diagonal(int[] point,
			Direction direction) {
		return ((map.get(
				DirHelp.addDirection(point, DirHelp.addAngle(direction, -3)))
				&& !map.get(DirHelp.addDirection(point,
						DirHelp.addAngle(direction, -2))))
				|| (map.get(DirHelp.addDirection(point,
						DirHelp.addAngle(direction, 3)))
						&& !map.get(DirHelp.addDirection(point,
								DirHelp.addAngle(direction, 2)))));
	}

	private boolean hasForcedNeighbors_orthogonal(int[] point,
			Direction direction) {
		return ((map.get(
				DirHelp.addDirection(point, DirHelp.addAngle(direction, -2)))
				&& !map.get(DirHelp.addDirection(point,
						DirHelp.addAngle(direction, -1))))
				|| (map.get(DirHelp.addDirection(point,
						DirHelp.addAngle(direction, 2)))
						&& !map.get(DirHelp.addDirection(point,
								DirHelp.addAngle(direction, 1)))));
	}

	private int[] jump_orthogonal(Direction direction, Node node) {
		int[] n = node.point;
		while (true) {
			n = DirHelp.addDirection(n, direction);
			// check for dead end or out of bounds
			if (map.get(n)) {
				Node newNode = new Node(
						DirHelp.addDirection(n, DirHelp.addAngle(direction, 4)),
						node);
				if (!closedList.contains(newNode)
						&& !Arrays.equals(newNode.point, node.point)) {
					closedList.add(newNode);
				}
				return null;
			}

			// check if goal is reached
			if (goals.contains(new HashablePoint(n))) {
				return n;
			}

			// terminate jump in case of forced neighbours
			if (hasForcedNeighbors_orthogonal(n, direction)) {
				return n;
			}
		}
	}

	private int[] jump_diagonal(Direction direction, Node node) {
		int[] n = node.point;
		while (true) {
			n = DirHelp.addDirection(n, direction);
			// check for dead end or out of bounds
			if (map.get(n)) {
				Node newNode = new Node(
						DirHelp.addDirection(n, DirHelp.addAngle(direction, 4)),
						node);
				if (!closedList.contains(newNode)
						&& !Arrays.equals(newNode.point, node.point)) {
					closedList.add(newNode);
				}
				return null;
			}

			// check if goal is reached
			if (goals.contains(new HashablePoint(n))) {
				return n;
			}

			// terminate jump in case of forced neighbours
			if (hasForcedNeighbors_diagonal(n, direction)) {
				return n;
			}

			if (jump_orthogonal(DirHelp.addAngle(direction, 1),
					new Node(n, node)) != null)
				return n;
			if (jump_orthogonal(DirHelp.addAngle(direction, -1),
					new Node(n, node)) != null)
				return n;
			if (map.get(DirHelp.addDirection(n, DirHelp.addAngle(direction, 1)))
					|| map.get(DirHelp.addDirection(n,
							DirHelp.addAngle(direction, -1)))) {
				Node newNode = new Node(n, node);
				if (!closedList.contains(newNode))
					closedList.add(newNode);
			}
		}
	}

	private int[] jump(Direction direction, Node node) {
		if (DirHelp.isDiagonal(direction))
			return jump_diagonal(direction, node);
		else
			return jump_orthogonal(direction, node);
	}

	private double getH(int[] x, int[] y) {
		return Geometry.chebyshev_distance(x, y);
	}

	private double getH(int[] x, Set<HashablePoint> b) {
		double result = 1e5;
		for (HashablePoint y : b) {
			double d = getH(x, y.point);
			if (d < result)
				result = d;
		}
		return result;
	}

	public void set_map(StaticMapGrid map) {
		this.map = map;
	}

	public static Direction getDirection(int[] jump) {
		int d = Geometry.chebyshev_distance(jump);
		return DirHelp.getDirection(new int[] { jump[0] / d, jump[1] / d });
	}
}
