package tools.framework.buildorder;

import java.util.Set;

import bc.UnitType;
import tools.framework.WorldState;

public class StructureBuildOrder implements StructureBuildOrderEvaluator {

	public final long weNeedRocketsRound = 550;
	public final int maxIdleFactories = 2;
	public final int aLotOfKarbonite = 250;

	boolean rocketsBuildable = false;

	@Override
	public UnitType getNextUnit(WorldState ws) {
		int factoryCount = ws.unitLists.myUnits.get(UnitType.Factory).size();
		int rocketCount = ws.unitLists.myUnits.get(UnitType.Rocket).size();
		int robotCount = ws.unitLists.myRobots.size();
		
		int idleFactories = 0;
		Set<Integer> factories = ws.unitLists.myUnits.get(UnitType.Factory);
		for (int id : factories) {
			if (ws.gc.unit(id).isFactoryProducing() == 0
					&& ws.gc.unit(id).structureIsBuilt() == 1) {
				idleFactories++;
			}
		}
		
		rocketsBuildable =  ws.gc.researchInfo().getLevel(UnitType.Rocket) >= 1;

		if (rocketsBuildable) {
			if(ws.planetWon || ws.pga.enemyIsWalled || ws.gc.round() >= weNeedRocketsRound) {
				if(tooManyRobots(rocketCount, robotCount, 1)) {
					return UnitType.Rocket;
				}
				else if(factoryCount == 0){
					return UnitType.Factory;
				}
				else {
					return null;
				}
			}
			else if(ws.gc.karbonite() >= aLotOfKarbonite && idleFactories>maxIdleFactories) {
				return UnitType.Rocket;
			}

		}
		if( factoryCount < 3 || ws.gc.karbonite() >= aLotOfKarbonite) {
			return UnitType.Factory;
		}
		return null;
	}

	boolean tooManyRobots(int rocketCount, int robotCount,
			float overShootFactor) {
		if (rocketCount == 0)
			rocketCount = 1;
		return robotCount / rocketCount >= 8 * overShootFactor;
	}

}
