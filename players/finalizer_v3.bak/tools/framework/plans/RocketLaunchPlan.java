package tools.framework.plans;

import java.util.ArrayList;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.WorldState;

public class RocketLaunchPlan extends Plan {
	long minRocketHealth = 121;

	public RocketLaunchPlan(WorldState worldState) {
		super(worldState);
	}

	@Override
	protected void tick(WorldState worldState) {
		List<Unit> rocketsThatShouldLaunch = new ArrayList<Unit>();
		for (int rocketId : ws.unitLists.myUnits.get(UnitType.Rocket)) {
			Unit rocket = ws.gc.unit(rocketId);
			if (ws.gc.round() >= ws.launchTime
					|| rocket.health() < minRocketHealth) {

				if (rocket != null && rocket.location().isOnMap()
						&& rocket.rocketIsUsed() == 0
						&& rocket.structureIsBuilt() == 1) {
					loadOrRun(rocket);
					rocketsThatShouldLaunch.add(rocket);
				}
			}
		}
		for (Unit rocket : rocketsThatShouldLaunch)
			launch(rocket);
	}

	private void loadOrRun(Unit rocket) {
		VecUnit unitsAdjacent = ws.gc.senseNearbyUnitsByTeam(
				rocket.location().mapLocation(), 2, ws.me);
		for (int i = 0; i < unitsAdjacent.size(); i++) {
			// try to load nearby unists
			Unit unit = unitsAdjacent.get(i);
			if (ws.gc.canLoad(rocket.id(), unit.id())
					&& (unit.unitType() != UnitType.Worker
							|| ws.gc.round() == ws.launchTime)) {
				ws.resources.setUnitFreeById(unit.id());
				ws.resources.assignById(unit.id(), pid);
				ws.gc.load(rocket.id(), unit.id());
			}
			// try to flee from launch site otherwise
			else if (ws.gc.isMoveReady(unit.id())) {
				Direction kiteDir = ws.lll.kite(unit.id(),
						rocket.location().mapLocation()
								.directionTo(unit.location().mapLocation()));
				if (ws.gc.canMove(unit.id(), kiteDir)) {
					ws.gc.moveRobot(unit.id(), kiteDir);
				}
			}
		}
	}

	private void launch(Unit rocket) {
		MapLocation landingLocation = ws.resources.getNextRocketLocation();
		if (landingLocation != null) {
			if (ws.gc.canLaunchRocket(rocket.id(), landingLocation)) {
				ws.gc.launchRocket(rocket.id(), landingLocation);
			}
		}
	}
}
