package tools.framework.plans;

import java.util.ArrayList;

import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.BehaviourPlan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.framework.behaviours.MageAttackBehaviour;
import tools.framework.behaviours.MageMoveBehaviour;
import tools.framework.behaviours.UnloadBehaviour;

public class MageAttackPlan extends BehaviourPlan {
	ArrayList<Integer> MagesOnMap = new ArrayList<>();

	public MageAttackPlan(WorldState worldState) {
		super(worldState);
		Behaviour attackPatternPlan = new MageAttackBehaviour(this);
		Behaviour unloadPlan = new UnloadBehaviour();
		Behaviour moveMagePlan = new MageMoveBehaviour(this);

		// unload all units from structures, we want to ATTACK
		addBehaviour(unloadPlan);
		// attack with all Mages who can
		addBehaviour(attackPatternPlan);
		// move all Mages
		addBehaviour(moveMagePlan);
		// now that we moved, try to attack once more (nearly no
		// performanceloss, bc only checking units that have attack rdy)
		addBehaviour(attackPatternPlan);
	}

	private void updateMages(WorldState ws) {
		ws.resources.assignAllFreeUnitsByTypeToPlan(pid, UnitType.Mage);
		MagesOnMap = ws.resources.getUnitsByPlan(pid);
	}

	@Override
	public void tick(WorldState ws) {
		this.updateMages(ws);
		super.tick(ws);
		this.status = Status.RUNNING;
	}

	public ArrayList<Integer> getMagesOnMap() {
		return MagesOnMap;
	}

}
