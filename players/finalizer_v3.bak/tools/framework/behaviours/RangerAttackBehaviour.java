package tools.framework.behaviours;

import java.util.HashMap;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.RangerAttackPlan;

//Attack the nearest non-worker non-factory enemy. If there are multiple nearest, first decide by type, then by left HP
public class RangerAttackBehaviour extends Behaviour {

	private RangerAttackPlan attackPlan;
	private RangerTargetSelector targetSelector = new RangerTargetSelectorNearest();

	HashMap<Integer, MoveInfo> moveInfos = new HashMap<>();

	public RangerAttackBehaviour(RangerAttackPlan plan) {
		attackPlan = plan;
	}

	@Override
	public void act(WorldState ws) {
		for (int rangerId : attackPlan.getRangersOnMap()) {
			Unit ranger = ws.gc.unit(rangerId);
			actForOne(ranger, ws);
		}
	}

	public void actForOne(Unit unit, WorldState ws) {
		Unit ranger = unit;
		if (!ranger.location().isOnMap())
			return;

		if (ws.gc.isAttackReady(ranger.id())) {
			Unit target = targetSelector.selectBestTarget(ranger, ws);

			if (target != null) {

				// handling standoff
				if (ws.gc.isMoveReady(unit.id())) {
					standOff(ranger, target, ws);
				}

				if (ws.gc.canAttack(ranger.id(), target.id())) {
					ws.gc.attack(ranger.id(), target.id());
				} else {
					System.out.println(
							"this shouldnt really happen, selectBestTarget should only return attackable units");
				}
			}
		}
	}

	private void standOff(Unit ranger, Unit target, WorldState ws) {
		if (ranger == null || target == null || !ranger.location().isOnMap()
				|| !target.location().isOnMap())
			return;

		MoveInfo info = null;
		if (moveInfos.containsKey(ranger.id())) {
			info = moveInfos.get(ranger.id());
		} else {
			info = new MoveInfo(ranger);
			moveInfos.put(ranger.id(), info);
		}
		if (info != null) {
			if (info.standOff(ranger)) {
				Direction d = ws.pathfinding.getNextDirection(ranger.id(),
						target.location().mapLocation());
				MapLocation moveLocation = ranger.location().mapLocation()
						.add(d);
				if (moveLocation.distanceSquaredTo(
						target.location().mapLocation()) > ranger
								.rangerCannotAttackRange()) {
					ws.pathfinding.goTo(ranger.id(),
							target.location().mapLocation());
				} else {
					// distance too low
				}
			}
		}

	}

}
