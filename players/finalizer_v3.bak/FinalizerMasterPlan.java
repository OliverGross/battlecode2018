import java.util.ArrayList;
import java.util.HashSet;

import bc.MapLocation;
import bc.Planet;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.PGA;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.buildorder.FinalizerUnitBuildOrder;
import tools.framework.buildorder.RangerRushBuildOrder;
import tools.framework.buildorder.StructureBuildOrder;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.CMasterStructureBuildPlan;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.HealerPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.LandingSiteExplorerPlan;
import tools.framework.plans.MarsReplicatePlan;
import tools.framework.plans.MasterBuildPlan;
import tools.framework.plans.RangerAttackPlan;
import tools.framework.plans.ReplicatePlan;
import tools.framework.plans.RocketLaunchPlan;
import tools.framework.plans.SquadToMarsPlan;

public class FinalizerMasterPlan extends EvaluatorPlan {

	private int masterBuildPlanRestarts = 0;
	private int masterStructureBuildPlanRestarts = 0;
	public final int maxMasterBuildPlanRestarts = 5;
	public final int maxMasterStructureBuildPlanRestarts = 5;
	public final int minWorkers = 3;

	public FinalizerMasterPlan(WorldState worldState) {
		super(worldState);
		if (worldState.gc.planet().equals(Planet.Earth)) {
			VecUnit initial = worldState.gc.startingMap(Planet.Earth)
					.getInitial_units();
			PGA preGame = worldState.pga;
			Team myTeam = worldState.gc.team();
			HashSet<MapLocation> myWorkers = new HashSet<MapLocation>();
			HashSet<MapLocation> oppWorkers = new HashSet<MapLocation>();
			for (int i = 0; i < initial.size(); i++) {
				Unit worker = initial.get(i);
				if (worker.team().equals(myTeam)) {
					myWorkers.add(worker.location().mapLocation());
				} else {
					oppWorkers.add(worker.location().mapLocation());
				}
			}
			MapLocation closest = worldState.pathfinding
					.getNearestStart(myWorkers, oppWorkers);
			ArrayList<Integer> closestWorker = new ArrayList<Integer>();
			if (closest != null) {
				closestWorker
						.add(worldState.gc.senseUnitAtLocation(closest).id());
			} else {
				closestWorker.add(worldState.gc
						.senseUnitAtLocation(myWorkers.iterator().next()).id());
			}
			// see bellow for an example on how to replicate
//			System.out.println(
//					"I want " + preGame.magicReplicate + "additional workers");
			int replicateNr = 0;
			// if(replicateNr < 2) {
			// replicateNr = 2;
			// }
			// plans.add(new ReplicatePlan(worldState, preGame.replicateWorker,
			// replicateNr));
			for (int worker : preGame.replicateWorkers.keySet()) {
				replicateNr += preGame.replicateWorkers.get(worker);
			}
			replicateNr += preGame.replicateWorkers.keySet().size();
			
			if (replicateNr < minWorkers) {
				int diff = minWorkers - replicateNr;
				preGame.replicateWorkers.put(
						preGame.replicateWorkers.keySet().iterator().next(),
						preGame.replicateWorkers.get(preGame.replicateWorkers
								.keySet().iterator().next()) + diff);
			}
			for (int worker : preGame.replicateWorkers.keySet()) {
				plans.add(new ReplicatePlan(worldState, worker,
						preGame.replicateWorkers.get(worker)));
			}

			plans.add(new MasterBuildPlan(worldState,
					new FinalizerUnitBuildOrder()));
			plans.add(new CMasterStructureBuildPlan(worldState,
					new StructureBuildOrder(), closestWorker));
			plans.add(new SquadToMarsPlan(worldState));
			plans.add(new RocketLaunchPlan(worldState));

			ArrayList<Integer> harvestWorkers = new ArrayList<>();
			for (int i = 0; i < worldState.gc.myUnits().size(); i++) {
				Unit w = worldState.gc.myUnits().get(i);
				if (closestWorker.contains(w.id()))
					continue;
				else
					harvestWorkers.add(w.id());
			}

		} else {
			// Mars-only plans
			plans.add(new LandingSiteExplorerPlan(worldState));
			plans.add(new MarsReplicatePlan(worldState));
		}
		PGA preGame = worldState.pga;
		plans.add(new KnightsAttackPlan(worldState));
		plans.add(new HarvestPlan(worldState, preGame.karbLocs));
		plans.add(new HealerPlan(worldState));
		plans.add(new RangerAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));

		worldState.gc.queueResearch(UnitType.Ranger);
		worldState.gc.queueResearch(UnitType.Healer);
		worldState.gc.queueResearch(UnitType.Healer);
		worldState.gc.queueResearch(UnitType.Healer);
		worldState.gc.queueResearch(UnitType.Rocket);
		worldState.gc.queueResearch(UnitType.Ranger);
		worldState.gc.queueResearch(UnitType.Rocket);
		worldState.gc.queueResearch(UnitType.Rocket);
		worldState.gc.queueResearch(UnitType.Worker);

	}

	@Override
	public void modifyPlans(WorldState worldState) {
		if (masterBuildPlanRestarts < maxMasterBuildPlanRestarts) {
			if (!containsPlanClass(MasterBuildPlan.class)) {
				plans.add(new MasterBuildPlan(ws, new RangerRushBuildOrder()));
				masterBuildPlanRestarts++;
			}
		}
		if (masterStructureBuildPlanRestarts < maxMasterStructureBuildPlanRestarts) {
			if (!containsPlanClass(CMasterStructureBuildPlan.class)) {
				plans.add(new CMasterStructureBuildPlan(ws,
						new StructureBuildOrder(), new ArrayList<Integer>()));
				masterStructureBuildPlanRestarts++;
			}

			if (worldState.unitLists.myUnits.get(UnitType.Worker).size() == 1) {
				if (!containsPlanClass(ReplicatePlan.class)) {
					new ReplicatePlan(worldState, worldState.unitLists.myUnits
							.get(UnitType.Worker).iterator().next(), 2);
				}
			}
		}

	}
}
