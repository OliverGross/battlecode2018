package tools.framework.behaviours;

import java.util.HashMap;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.RangerAttackPlan;

//Attack the nearest non-worker non-factory enemy. If there are multiple nearest, first decide by type, then by left HP
public class RangerAttackNearestBehaviour extends Behaviour {

	class MoveInfo {

		// this is rather conservative, but lower numbers have the danger of
		// kiling us
		private final int roundsStillUntilPush = 10;

		public int lastX;
		public int lastY;
		public int roundsStill;

		public MoveInfo(Unit unit) {
			roundsStill = 0;
			if (!unit.location().isOnMap()) {
				lastY = -1;
				lastX = -1;
				System.out.println("MoveInfo - Unit is not on map!");
			} else {
				lastX = unit.location().mapLocation().getX();
				lastY = unit.location().mapLocation().getY();
			}
		}

		private boolean didMove(Unit unit) {
			if (!unit.location().isOnMap()) {
				return true;
			} else {
				if (unit.location().mapLocation().getX() == lastX
						&& unit.location().mapLocation().getY() == lastY) {
					return false;
				}
			}
			return true;
		}

		public boolean standOff(Unit unit) {
			// System.out.println("checking standoff for unit :" + unit.id());
			if (!didMove(unit)) {
				// System.out.println("did not move");
				roundsStill++;
				// System.out.println("roundsStill " + roundsStill);
				if (roundsStill >= roundsStillUntilPush) {
					// System.out.println("returns true");
					return true;
				}
			} else {
				lastX = unit.location().mapLocation().getX();
				lastY = unit.location().mapLocation().getY();
				roundsStill = 0;
				return false;
			}
			return false;
		}
	}

	private RangerAttackPlan attackPlan;

	HashMap<Integer, MoveInfo> moveInfos = new HashMap<>();

	public RangerAttackNearestBehaviour(RangerAttackPlan plan) {
		attackPlan = plan;
	}

	@Override
	public void act(WorldState ws) {
		for (int rangerId : attackPlan.getRangersOnMap()) {
			Unit ranger = ws.gc.unit(rangerId);
			actForOne(ranger, ws);
		}
	}

	public void actForOne(Unit unit, WorldState ws) {
		Unit ranger = unit;
		if (!ranger.location().isOnMap())
			return;

		if (ws.gc.isAttackReady(ranger.id())) {
			Unit target = selectBestTarget(ranger, ws);

			if (target != null) {

				// handling standoff
				if (ws.gc.isMoveReady(unit.id())) {
					standOff(ranger, target, ws);
				}

				if (ws.gc.canAttack(ranger.id(), target.id())) {
					ws.gc.attack(ranger.id(), target.id());
				} else {
					System.out.println(
							"this shouldnt really happen, selectBestTarget should only return attackable units");
				}
			}
		}
	}

	private void standOff(Unit ranger, Unit target, WorldState ws) {
		if (ranger == null || target == null || !ranger.location().isOnMap()
				|| !target.location().isOnMap())
			return;

		MoveInfo info = null;
		if (moveInfos.containsKey(ranger.id())) {
			// System.out.println("containedKey");
			info = moveInfos.get(ranger.id());
		} else {
			// System.out.println("did not contain key");
			info = new MoveInfo(ranger);
			moveInfos.put(ranger.id(), info);
		}
		if (info != null) {
			// System.out.println("info was not null");
			if (info.standOff(ranger)) {
				Direction d = ws.pathfinding.getNextDirection(ranger.id(),
						target.location().mapLocation());
				MapLocation moveLocation = ranger.location().mapLocation()
						.add(d);
				if (moveLocation.distanceSquaredTo(
						target.location().mapLocation()) > ranger
								.rangerCannotAttackRange()) {
					// System.out.println("moved foreward bc of standoff!");
					ws.lll.goTo(ranger.id(), target.location().mapLocation());
				} else {
					// System.out.println("distance was too low");
				}
			}
		}

	}

	private Unit selectBestTarget(Unit ranger, WorldState ws) {
		MapLocation rangerLocation = ranger.location().mapLocation();

		Unit bestTarget = null;
		long bestDistance = 100;

		VecUnit enemiesInRange = ws.gc.senseNearbyUnitsByTeam(rangerLocation,
				ranger.attackRange(), ws.opp);
		for (int e = 0; e < enemiesInRange.size(); e++) {
			Unit enemy = enemiesInRange.get(e);
			long distance = rangerLocation
					.distanceSquaredTo(enemy.location().mapLocation());
			if (distance > ranger.rangerCannotAttackRange()) {
				if (bestTarget == null) {
					bestTarget = enemy;
					bestDistance = distance;
				} else {
					// Ignore certain enemies
					if (distance < bestDistance && typeValue(enemy) > 2) {
						bestTarget = enemy;
						bestDistance = distance;
					} else if (distance == bestDistance) {
						if (typeValue(bestTarget) < typeValue(enemy)) {
							if (enemy.health() < bestTarget.health()) {
								// if equal in type and distance -> attack
								// lowest
								bestTarget = enemy;
								bestDistance = distance;
							}
						}
					}
				}
			}
		}
		return bestTarget;
	}

	// The higher the value, the better to focus
	private int typeValue(Unit u) {
		if (u == null) {
			System.out.println("typeValue Unit was null");
			return 0;
		}

		switch (u.unitType()) {
		case Factory:
			return 1;
		case Worker:
			return 2;
		case Healer:
			return 3;
		case Rocket:
			return 4;
		case Ranger:
			return 5;
		case Knight:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;

		}
	}

}
