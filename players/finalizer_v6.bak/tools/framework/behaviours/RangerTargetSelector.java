package tools.framework.behaviours;

import bc.Unit;
import tools.framework.WorldState;

public interface RangerTargetSelector {

	public Unit selectBestTarget(Unit ranger, WorldState ws);

}
