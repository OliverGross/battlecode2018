package tools.framework.behaviours;

import bc.Unit;

public class MoveInfo {

	// this is rather conservative, but lower numbers have the danger of kiling
	// us
	private final int roundsStillUntilPush = 10;

	public int lastX;
	public int lastY;
	public int roundsStill;

	public MoveInfo(Unit unit) {
		roundsStill = 0;
		if (!unit.location().isOnMap()) {
			lastY = -1;
			lastX = -1;
			System.out.println("MoveInfo - Unit is not on map!");
		} else {
			lastX = unit.location().mapLocation().getX();
			lastY = unit.location().mapLocation().getY();
		}
	}

	private boolean didMove(Unit unit) {
		if (!unit.location().isOnMap()) {
			return true;
		} else {
			if (unit.location().mapLocation().getX() == lastX
					&& unit.location().mapLocation().getY() == lastY) {
				return false;
			}
		}
		return true;
	}

	public boolean standOff(Unit unit) {
		if (!didMove(unit)) {
			roundsStill++;
			if (roundsStill >= roundsStillUntilPush) {
				// System.out.println("returns true");
				return true;
			}
		} else {
			lastX = unit.location().mapLocation().getX();
			lastY = unit.location().mapLocation().getY();
			roundsStill = 0;
			return false;
		}
		return false;
	}
}
