package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.HealerPlan;

public class HealerMoveBehaviour extends Behaviour {

	HealerPlan healerPlan;

	List<MapLocation> enemyLocations;

	public HealerMoveBehaviour(HealerPlan plan) {
		healerPlan = plan;
		enemyLocations = new ArrayList<>();
	}

	@Override
	public void act(WorldState ws) {
		enemyLocations.clear();

		// compute enemyLocations once and not for all units
		enemyLocations.clear();
		Set<Integer> allEnemies = ws.unitLists.allEnemyUnits;

		for (int uid : allEnemies) {
			if (ws.gc.canSenseUnit(uid)) {
				Unit u = ws.gc.unit(uid);
				if (u.location().isOnMap()) {
					enemyLocations.add(u.location().mapLocation());
				}
			}
		}

		boolean enemiesInSight = true;

		// no enemies in sight, get startlocations
		if (enemyLocations.size() == 0) {
			enemiesInSight = false;
			Planet planet = ws.gc.planet();
			int nbrRandomLocations = (planet == Planet.Earth) ? 1 : 3;
			// add a few random points
			int w = (int) ws.gc.startingMap(planet).getWidth();
			int h = (int) ws.gc.startingMap(planet).getHeight();
			while (enemyLocations.size() < nbrRandomLocations) {
				int x = ws.rand.nextInt(w);
				int y = ws.rand.nextInt(h);
				MapLocation loc = new MapLocation(planet, x, y);
				if (ws.gc.startingMap(planet).isPassableTerrainAt(loc) == 1)
					enemyLocations.add(loc);
			}
			// add enemy starting positions on earth
			if (planet == Planet.Earth) {
				VecUnit startEnemies = ws.gc.startingMap(Planet.Earth)
						.getInitial_units();
				for (int i = 0; i < startEnemies.size(); i++) {
					if (startEnemies.get(i).team() == ws.opp) {
						enemyLocations.add(
								startEnemies.get(i).location().mapLocation());
					}
				}
			}
		}

		if (!enemiesInSight && ws.pga.enemyIsWalled)
			return;

		ArrayList<Integer> healers = healerPlan.getHealersOnMap();
		for (int healerId : healers) {
			if (ws.gc.isMoveReady(healerId)) {
				Unit healer = ws.gc.unit(healerId);
				if (!healer.location().isOnMap())
					continue;

				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), 65, ws.opp);

				if (enemies.size() != 0) {
					kiteEnemies(enemies, healer, ws);
				}

				ws.pathfinding.goTo(healerId, enemyLocations);

			}
		}

	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

}
