package tools.framework.buildorder;

import bc.Unit;
import bc.UnitType;
import tools.framework.WorldState;

public interface UnitBuildOrderEvaluator {

	public UnitType getNextUnit(WorldState ws, Unit producer);
	
}
