package tools.framework.behaviours;

import java.util.List;

import bc.Direction;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.WorldState;

public class UnloadBehaviour extends Behaviour {
	@Override
	public void act(WorldState ws) {
		// Unload all Units from Factories, and from Rockets if on Mars
		
		List<Integer> myFactories = ws.resources.getMyUnitsByType(UnitType.Factory);
		
		for(int facId : myFactories) {
			Unit factory = ws.gc.unit(facId);
			for (Direction d : Direction.values()) {
				if (factory.structureGarrison().size() != 0) {
					if (ws.gc.canUnload(factory.id(), d)) {
						ws.gc.unload(factory.id(), d);
					}
				} else {
					break;
				}
			}
		}
		
		if(ws.gc.planet()==Planet.Mars) {
			List<Integer> myRockets = ws.resources.getMyUnitsByType(UnitType.Rocket);
			for(int rocketId : myRockets) {
				Unit rocket = ws.gc.unit(rocketId);
				for (Direction d : Direction.values()) {
					if (rocket.structureGarrison().size() != 0) {
						if (ws.gc.canUnload(rocket.id(), d)) {
							ws.gc.unload(rocket.id(), d);
						}
					} else {
						break;
					}
				}
			}
		}
	}

}
