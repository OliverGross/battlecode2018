package tools.framework.behaviours;

import java.util.List;

import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.KnightsAttackPlan;

public class KnightAttackBehaviour extends Behaviour {

	public KnightAttackBehaviour(WorldState worldState,
			KnightsAttackPlan knightsAttackPlan) {
	}
	
	@Override
	public void act(WorldState ws) {
		boolean javelin = ws.gc.researchInfo().getLevel(UnitType.Knight)==3;
		List<Integer> knights = ws.resources.getMyUnitsByType(UnitType.Knight);
		for(int kid : knights) {
			Unit knight = ws.gc.unit(kid);
			if(!knight.location().isOnMap()) continue;
			
			//attack
			if(ws.gc.isAttackReady(kid)) {
				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(knight.location().mapLocation(), knight.attackRange(), ws.opp);
				Unit bestEnemy=null;
				for(int i=0; i<enemies.size(); i++) {
					if(bestEnemy==null) {
						bestEnemy = enemies.get(i);
					} else {
						if(typeValue(bestEnemy) < typeValue(enemies.get(i))) {
							bestEnemy = enemies.get(i);
							//TODO wait for gothrough
						}
					}
				}
				if(bestEnemy != null) {
					if(ws.gc.canAttack(kid, bestEnemy.id())) {
						ws.gc.attack(kid, bestEnemy.id());
					}
				}
			}
			
			//javelin
			if(javelin && ws.gc.isJavelinReady(kid)) {
				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(knight.location().mapLocation(), knight.abilityRange(), ws.opp);
				Unit bestEnemy = null;
				for(int i=0; i<enemies.size(); i++) {
					if(bestEnemy==null) {
						bestEnemy = enemies.get(i);
					} else {
						if(typeValue(bestEnemy) < typeValue(enemies.get(i))) {
							bestEnemy = enemies.get(i);
							//TODO wait for gothrough
						}
					}
				}
				if(bestEnemy != null) {
					if(ws.gc.canJavelin(kid, bestEnemy.id())) {
						System.out.println("javelin!");
						ws.gc.javelin(kid, bestEnemy.id());
					}
				}
			}
		}
	}
	
	// The higher the value, the better to focus
	private int typeValue(Unit u) {
		if (u == null) {
			System.out.println("typeValue Unit was null");
			return 0;
		}

		switch (u.unitType()) {
		case Factory:
			return 1;
		case Worker:
			return 2;
		case Rocket:
			return 3;
		case Knight:
			return 4;
		case Ranger:
			return 5;
		case Healer:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;
		}
	}

}
