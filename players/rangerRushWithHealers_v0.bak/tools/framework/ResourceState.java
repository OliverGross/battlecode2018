package tools.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import bc.Unit;
import bc.UnitType;
import bc.VecUnit;

public class ResourceState {
	/**
	 * Assignment of UnitIds to PlanIds
	 */
	HashMap<Integer, Integer> plans = new HashMap<Integer, Integer>();
	/**
	 * Storage of Units Types
	 */
	HashMap<Integer, UnitType> types = new HashMap<Integer, UnitType>();
	/**
	 * Storing a temporary blocking
	 */
	HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>();
	/**
	 * Storing Units by Team and Type
	 */
	HashMap<UnitType, ArrayList<Integer>> myUnits = new HashMap<>();
	HashMap<UnitType, ArrayList<Integer>> enemyUnits = new HashMap<>();
	HashMap<UnitType, Integer> currentlyProducing = new HashMap<>();

	/**
	 * WorldState
	 */
	WorldState ws;

	public ResourceState(WorldState worldState) {
		ws = worldState;
		for (UnitType t : UnitType.values()) {
			myUnits.put(t, new ArrayList<Integer>());
			enemyUnits.put(t, new ArrayList<Integer>());
		}
		update();
	}

	public void resetUnitMaps() {
		for (UnitType t : UnitType.values()) {
			myUnits.get(t).clear();
			enemyUnits.get(t).clear();
		}
	}

	/**
	 * Handles deaths and newly generated units
	 * 
	 * @param ws(WorldState)
	 */
	public void update() {
		resetUnitMaps();
		addUnits(ws.gc.units());
		removeAbsent(ws);
		tickTemporaryBlocks();
	}

	private void addUnits(VecUnit units) {
		for (int i = 0; i < units.size(); i++) {
			addUnit(units.get(i));
		}
	}

	private void addUnit(Unit unit) {
		if (unit.team() == ws.opp)
			addOppUnit(unit);
		else
			addMyUnit(unit);
		addGeneralUnit(unit);
	}

	private void addOppUnit(Unit unit) {
		enemyUnits.get(unit.unitType()).add(unit.id());
	}

	private void addMyUnit(Unit unit) {
		// resource state vectors
		if (plans.putIfAbsent(unit.id(), 0) == null) {
			types.put(unit.id(), unit.unitType());
			temp.put(unit.id(), -1);
		}
		myUnits.get(unit.unitType()).add(unit.id());
	}

	private void addGeneralUnit(Unit unit) {
		if (unit.unitType() == UnitType.Factory && unit.structureIsBuilt() == 1
				&& unit.isFactoryProducing() == 1) {
			int producing = 0;
			if (currentlyProducing.containsKey(unit.factoryUnitType())) {
				producing = currentlyProducing.get(unit.factoryUnitType());
			}
			currentlyProducing.put(unit.factoryUnitType(), producing);
		}
	}

	/**
	 * Try to sense unit, otherwise delete all occurrences.
	 * 
	 * @param ws
	 */
	private void removeAbsent(WorldState ws) {
		for (Iterator<HashMap.Entry<Integer, Integer>> it = plans.entrySet()
				.iterator(); it.hasNext();) {
			int id = it.next().getKey();
			if (!ws.gc.canSenseUnit(id)) {
				it.remove();
				types.remove(Integer.valueOf(id));
				temp.remove(Integer.valueOf(id));
			}
		}

	}

	/**
	 * Update the rounds left for block
	 */
	private void tickTemporaryBlocks() {
		for (HashMap.Entry<Integer, Integer> entry : temp.entrySet()) {
			if (entry.getValue() == 0) {
				plans.put(entry.getKey(), 0);
			}
			entry.setValue(entry.getValue() - 1);
		}
	}

	/**
	 * Return all units associated with a specific plan
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlan(int pid) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			if (entry.getValue() == pid)
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all units associated with a plan and a certain type.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlanAndType(int pid, UnitType type) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			if (entry.getValue() == pid
					&& types.get(entry.getKey()).equals(type))
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all unassigned units.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getAllUnits() {
		return getUnitsByPlan(0);
	}

	/**
	 * Return all unassigned units of certain type.
	 * 
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getFreeUnitsByType(UnitType type) {
		return getUnitsByPlanAndType(0, type);
	}

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeToPlan(int pid) {
		assignFreeToPlan(pid, -1);
	}

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeToPlan(int pid, int rounds) {
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			if (entry.getValue() == 0) {
				entry.setValue(pid);
				temp.put(entry.getValue(), rounds);
			}
		}
	}

	/**
	 * Assign all unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignAllFreeByType(int pid, UnitType type) {
		assignFreeByType(pid, type, -1);
	}

	/**
	 * Assign all unassigned units of specified type to given plan for specified
	 * time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeByType(int planId, UnitType type, int rounds) {
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			if (entry.getValue() == 0
					&& types.get(entry.getKey()).equals(type)) {
				entry.setValue(planId);
				temp.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign maximum of max unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignSomeFreeByType(int pid, UnitType type, int maxAssigned) {
		assignSomeFreeByType(pid, type, -1, maxAssigned);
	}

	/**
	 * Assign maximum of max unassigned units of specified type to given plan
	 * for specified time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignSomeFreeByType(int pid, UnitType type, int rounds,
			int max) {
		int assignments = 0; // check for max
		for (Iterator<HashMap.Entry<Integer, Integer>> it = plans.entrySet()
				.iterator(); it.hasNext() && assignments < max;) {
			HashMap.Entry<Integer, Integer> entry = it.next();
			if (entry.getValue() == 0
					&& types.get(entry.getKey()).equals(type)) {
				assignments++;
				entry.setValue(pid);
				temp.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign all resource_ids to the plan
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByIds(ArrayList<Integer> resource_ids, int pid) {
		return assignByIds(resource_ids, pid, -1);
	}

	/**
	 * Assign all resource_ids to the plan for a given time
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByIds(ArrayList<Integer> resource_ids, int pid,
			int rounds) {
		boolean success = true;
		for (int id : resource_ids) {
			success = success && assignById(id, pid, rounds);
		}
		return success;
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignById(int id, int pid) {
		return assignById(id, pid, -1);
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignById(int id, int pid, int rounds) {
		// is already known?
		if (plans.containsKey(id)) {
			if (plans.get(id) == 0) {
				plans.put(id, pid);
				temp.put(id, rounds);
				return true;
			} else {
				return false;
			}
		}
		// check if newly generated
		else if (ws.gc.canSenseUnit(id)) {
			addUnit(ws.gc.unit(id));
			return assignById(id, pid, rounds);
		} else {
			return false;
		}
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param unit
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByUnit(Unit unit, int pid) {
		return assignByUnit(unit, pid, -1);
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByUnit(Unit unit, int pid, int rounds) {
		// is already known?
		if (plans.containsKey(unit.id())) {
			if (plans.get(unit.id()) == 0) {
				plans.put(unit.id(), pid);
				temp.put(unit.id(), rounds);
				return true;
			} else {
				return false;
			}
		}
		// check if newly generated
		else {
			addUnit(unit);
			return assignById(unit.id(), pid, rounds);
		}
	}

	/**
	 * Deletes all assignment of units to plans.
	 */
	public void resetAllAssignments() {
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			entry.setValue(0);
		}
	}

	/**
	 * Deletes all assignments to specified plans.
	 * 
	 * @param pid
	 */
	public void freeUnitsFromPlan(int pid) {
		for (HashMap.Entry<Integer, Integer> entry : plans.entrySet()) {
			if (entry.getValue() == pid)
				entry.setValue(0);
		}
	}

	/**
	 * Deletes assignment of given unit.
	 * 
	 * @param uid
	 */
	public void freeUnitById(int uid) {
		if (plans.containsKey(uid)) {
			plans.put(uid, 0);
		}
	}

	public List<Integer> getMyUnitsByType(UnitType type) {
		return myUnits.get(type);
	}

	public List<Integer> getEnemyUnitsByType(UnitType type) {
		return enemyUnits.get(type);
	}
	
	public int getCurrentlyProducingByType(UnitType type) {
		if(currentlyProducing.containsKey(type)) {
			return currentlyProducing.get(type);
		}
		else {
			return 0;
		}
	}

}
