package tools.framework;

import java.util.Random;

import bc.*;
import tools.LowLevelLogic;
import tools.pathfinding.Geometry;
import tools.pathfinding.Pathfinding;

/**
 * A class which captures all information in the current game state collected by
 * the single- or multi-master-agent. Here would probably be also the place
 * where one could provide functionality like pathfinding or other general
 * algorithms executed on the given information.
 */
public class WorldState {
	// times for measuring
	private long timeLeftMs;
	private long roundStartNs;
	/**
	 * should be used as the discrete in-game time-stamp
	 */
	public int round;
	public GameController gc;
	public Random rand = new Random(42);
	public Team me;
	public Team opp;
	public Pathfinding pathfinding;
	public LowLevelLogic lll;
	public ResourceState resources;
	public int savedKarbonite;
	public static int numberInstances = 0;
	public UnitLists unitLists;

	public WorldState() {
		if (numberInstances >= 1) {
			throw new RuntimeException(
					"There already exists a world state! Do not construct a second instance!");
		}
		numberInstances++;
		round = 1;
		savedKarbonite = 0;
		gc = new GameController();
		pathfinding = new Pathfinding(gc);
		lll = new LowLevelLogic(gc, pathfinding);
		me = gc.team();
		opp = Team.Blue;
		if (me == Team.Blue)
			opp = Team.Red;
		unitLists = new UnitLists(me);
		resources = new ResourceState(this);
		timeLeftMs = gc.getTimeLeftMs();
		roundStartNs = System.nanoTime();
	}

	public int availableKarbonite() {
		return (int) gc.karbonite() - savedKarbonite;
	}

	public int saveKarbonite(int maxAmount) {
		int available = availableKarbonite();
		if (maxAmount > available) {
			savedKarbonite += available;
			return available;
		}
		savedKarbonite += maxAmount;
		return maxAmount;
	}

	public int spendSavings(int amountSpent) {
		assert savedKarbonite >= amountSpent;
		savedKarbonite -= amountSpent;
		return amountSpent;
	}

	public void nextTurn() {
		gc.nextTurn();
		roundStartNs = System.nanoTime();
		timeLeftMs = gc.getTimeLeftMs();
		update();
	}

	public long getTimeLeftNs() {
		if (timeLeftMs != gc.getTimeLeftMs())
			throw new RuntimeException(timeLeftMs + " " + gc.getTimeLeftMs());
		return 1000000 * timeLeftMs + roundStartNs - System.nanoTime();
	}

	public void updateUnitLists() {
		unitLists.update(gc);
	}

	public void update() {
		// System.out.println("Worldstate ticked!");
		round++;
		if (round != gc.round())
			throw new RuntimeException("Round(" + round + ") and gc.round("
					+ gc.round() + ") are out of sync!");
		resources.update();
		updateUnitLists();
	}

	public int chebyshev_distance(int uid0, int uid1) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(),
				gc.unit(uid1).location().mapLocation());
	}

	public int chebyshev_distance(int uid0, MapLocation b) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(), b);
	}

	public static int chebyshev_distance(MapLocation a, MapLocation b) {
		return Geometry.chebyshev_distance(a.getX() - b.getX(),
				a.getY() - b.getY());
	}
}
