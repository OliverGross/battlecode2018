package tools.framework;

/**
 * An example for a complete agent. In every tick just retrieve all information
 * you can get from the simulation about the current worldState, then execute
 * the next tick of the AgentPlan.
 */
public class Agent {
	Plan masterplan;
	WorldState worldState;

	public Agent(WorldState worldState, Plan masterplan) {
		this.worldState = worldState;
		this.masterplan = masterplan;
	}

	public void execute() {
		while (!masterplan.status.isTerminal()) {
			tick();
			if ((worldState.gc.round() % 50) == 0) {
				System.out.println("================");
				System.out.println("ROUND: " + worldState.gc.round());
				System.out.println(
						"Time left(ms):" + worldState.gc.getTimeLeftMs());
				System.out.println("================");
			}
		}
		System.out.println("Masterpplan finished with status "
				+ masterplan.status + "! Now just playing along...");
		while (true)
			worldState.nextTurn();
	}

	public void tick() {
		masterplan.safeTick(worldState);
		// System.out.println("Total path finding computation time for "
		// + JPS.totalCalls + " calls: " + JPS.compTime * 1e-6 + "ms");
		worldState.nextTurn();
	}
}
