package tools.framework;

import java.util.HashSet;
import java.util.Set;

import bc.GameController;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;

public class UnitLists {
	private int runningInstances = 0;
	private Team me;

	// this is handled in update() to find out which units are new and which
	// were lost (by vision or death)
	public Set<Integer> units = new HashSet<Integer>();

	// the following sets get all handled by addNewUnit and addLostUnit to
	// reduce the operations to a minimum
	public Set<Integer> newUnits = new HashSet<Integer>();
	public Set<Integer> myDiedUnits = new HashSet<Integer>();
	public Set<Integer> enemyLostUnits = new HashSet<Integer>();
	public Set<Integer> myUnits = new HashSet<Integer>();
	public Set<Integer> enemyUnits = new HashSet<Integer>();
	public Set<Integer> myWorkers = new HashSet<Integer>();
	public Set<Integer> enemyMages = new HashSet<Integer>();
	public Set<Integer> structures = new HashSet<Integer>();
	public Set<Integer> robots = new HashSet<Integer>();
	public Set<Integer> myRobots = new HashSet<Integer>();
	public Set<Integer> enemyRobots = new HashSet<Integer>();

	public UnitLists(Team me) {
		if (runningInstances >= 1)
			throw new RuntimeException(
					"There should only be a single UnitLists instance!");
		runningInstances++;
		this.me = me;
	}

	public void update(GameController gc) {
		VecUnit currentUnits = gc.units();
		newUnits.clear();
		Set<Integer> curUnits = new HashSet<Integer>();
		for (int i = 0; i < currentUnits.size(); i++) {
			Unit unit = currentUnits.get(i);
			curUnits.add(unit.id());
			if (!units.contains(unit.id()))
				addNewUnit(unit);
			else
				units.remove(unit.id());
		}
		for (int uid : units)
			addLostUnit(uid);
		units = curUnits;
	}

	public void addNewUnit(Unit unit) {
		newUnits.add(unit.id());
		if (enemyLostUnits.contains(unit.id())) {
			enemyLostUnits.remove(unit.id());
		}
		if (unit.team() == me) {
			myUnits.add(unit.id());
			if (isStructure(unit.unitType())) {
				structures.add(unit.id());
			} else {
				robots.add(unit.id());
				myRobots.add(unit.id());
			}
			switch (unit.unitType()) {
			case Worker:
				myWorkers.add(unit.id());
				break;
			default:
				break;
			}
		} else {
			enemyUnits.add(unit.id());
			if (isStructure(unit.unitType())) {
				structures.add(unit.id());
			} else {
				robots.add(unit.id());
				enemyRobots.add(unit.id());
			}
			switch (unit.unitType()) {
			case Mage:
				enemyMages.add(unit.id());
				break;
			default:
				break;
			}
		}
	}

	public void addLostUnit(int uid) {
		robots.remove(uid);
		structures.remove(uid);
		if (myUnits.contains(uid)) {
			myUnits.remove(uid);
			myDiedUnits.add(uid);
			myWorkers.remove(uid);
			myRobots.remove(uid);
		} else {
			enemyUnits.remove(uid);
			enemyLostUnits.add(uid);
			enemyMages.remove(uid);
			enemyRobots.remove(uid);
		}
	}

	public static boolean isStructure(UnitType ut) {
		if (ut == UnitType.Factory || ut == UnitType.Rocket)
			return true;
		return false;
	}

	public static boolean isRobot(UnitType ut) {
		return !isStructure(ut);
	}
}
