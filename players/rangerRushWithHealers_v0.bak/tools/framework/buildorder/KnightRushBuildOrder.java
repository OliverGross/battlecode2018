package tools.framework.buildorder;

import bc.UnitType;
import tools.framework.WorldState;

public class KnightRushBuildOrder implements BuildOrderEvaluator {

	@Override
	public UnitType getNextUnit(WorldState ws) {
		int healers = ws.resources.getMyUnitsByType(UnitType.Healer).size() + ws.resources.getCurrentlyProducingByType(UnitType.Healer);
		int rangers = ws.resources.getMyUnitsByType(UnitType.Ranger).size() + ws.resources.getCurrentlyProducingByType(UnitType.Ranger);;
		int knights = ws.resources.getMyUnitsByType(UnitType.Knight).size() + ws.resources.getCurrentlyProducingByType(UnitType.Knight);;
		int mages = ws.resources.getMyUnitsByType(UnitType.Mage).size() + ws.resources.getCurrentlyProducingByType(UnitType.Mage);;
		
		int totalother = rangers+knights+mages;
		int prioRanger = 6;
		if(healers==0) healers = 1;
		int prioHealer = totalother/healers;
		
		if(prioHealer > prioRanger) {
			return UnitType.Healer;
		} else {
			return UnitType.Knight;
		}
	}

}
