package tools.framework.plans;

import java.util.List;

import bc.Direction;
import bc.UnitType;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class RocketUnloadPlan extends Plan {

	int savedKarbonite = 0;

	public RocketUnloadPlan(WorldState ws) {
		super(ws);
	}

	@Override
	protected void tick(WorldState ws) {
		List<Integer> myRockets = ws.resources
				.getMyUnitsByType(UnitType.Rocket);

		for (int rocketId : myRockets) {
			for (Direction dir : DirHelp.adjacentDirections) {
				if (ws.gc.canUnload(rocketId, dir)) {
					ws.gc.unload(rocketId, dir);
				}
			}
		}
	}

}
