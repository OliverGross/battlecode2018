package tools.framework.plans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.pathfinding.LocalPathFinder;

public class KnightsAttackPlanSprint extends Plan {
	Map<Integer, LocalPathFinder> lpfs;

	public KnightsAttackPlanSprint(WorldState worldState) {
		super(worldState);
		lpfs = new HashMap<Integer, LocalPathFinder>();
	}

	@Override
	public void tick(WorldState ws) {
		Map<Integer, LocalPathFinder> newLpfs = new HashMap<Integer, LocalPathFinder>();
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Knight)
				continue;
			if (ws.gc.unit(uid).location().isInGarrison())
				continue;
			VecUnit nearbys = ws.gc.senseNearbyUnitsByTeam(
					ws.gc.unit(uid).location().mapLocation(), 5000, ws.opp);
			if (nearbys.size() == 0) {
				nearbys = ws.gc.startingMap(Planet.Earth).getInitial_units();
				System.out.println("Running to initial opponent");
			}
			Set<MapLocation> targetPoints = new HashSet<MapLocation>();
			int target = -1;
			for (int j = 0; j < nearbys.size(); j++) {
				if (nearbys.get(j).team() != ws.opp)
					continue;
				int tid = nearbys.get(j).id();
				MapLocation loc = nearbys.get(j).location().mapLocation();
				targetPoints.add(loc.add(Direction.North));
				targetPoints.add(loc.add(Direction.South));
				targetPoints.add(loc.add(Direction.West));
				targetPoints.add(loc.add(Direction.East));
				if (loc.distanceSquaredTo(
						ws.gc.unit(uid).location().mapLocation()) == 1) {
					target = tid;
					break;
				}
			}
			if (target >= 0) {
				if (ws.gc.canAttack(uid, target)
						&& ws.gc.unit(uid).attackHeat() < 10)
					ws.gc.attack(uid, target);
			} else {
				LocalPathFinder lpf;
				if (lpfs.containsKey(uid))
					lpf = lpfs.get(uid);
				else
					lpf = new LocalPathFinder(ws.pathfinding.jps);
				ws.lll.goTo(uid, targetPoints, lpf);
				newLpfs.put(uid, lpf);
			}
			this.status = Status.RUNNING;
		}
		lpfs = newLpfs;
	}
}
