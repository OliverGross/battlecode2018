package tools.framework.plans;

import bc.UnitType;
import bc.bc;
import tools.framework.Plan;
import tools.framework.WorldState;

public class HealerBuildPlan extends Plan {

	public int savedKarbonite;
	
	public HealerBuildPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		int healers = ws.resources.getMyUnitsByType(UnitType.Healer).size();
		int rangers = ws.resources.getMyUnitsByType(UnitType.Ranger).size();
		int knights = ws.resources.getMyUnitsByType(UnitType.Knight).size();
		int mages = ws.resources.getMyUnitsByType(UnitType.Mage).size();
		
		int totalother = rangers+knights+mages;
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			//integer division here, does not really matter as decimals get cropped which is what we want anyway
			if(totalother/healers <=5) continue;
			
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Factory)
				continue;
			
			//produce healer as every Xth unit
			int healerCost = (int) bc.costOf(UnitType.Healer,
						ws.gc.researchInfo().getLevel(UnitType.Healer));
			if(ws.gc.canProduceRobot(uid, UnitType.Healer) && ws.availableKarbonite() + savedKarbonite >= healerCost) {
				ws.gc.produceRobot(uid, UnitType.Healer);
				System.out.println("Healerproducing");
				savedKarbonite -= ws.spendSavings(Math.max(savedKarbonite, healerCost));
				healers++;
			}
		}
	}

}
