package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class HarvestPlan extends Plan {
	public List<Integer> w_ids;
	public List<List<Long>> karboniteLocations = new ArrayList<List<Long>>();

	public HarvestPlan(WorldState worldstate, List<List<Long>> karbLocs) {
		// ToDo pass "safe" karbonite locations
		super(worldstate);
		// System.out.println("workers for harvest plan:" + worker_ids);

		karboniteLocations = karbLocs;
	}

	@Override
	public void tick(WorldState ws) {
		// temporary block maximum of 5 units for 5 rounds
		// ws.resources.assignSomeFreeByType(pid, UnitType.Worker, 5, 5);
		// get everything i have
		w_ids = ws.resources.getFreeUnitsByType(UnitType.Worker);
		//System.out.println("I got workers" + w_ids.size());
		update(ws);
		for (int i = 0; i < w_ids.size(); i++) {
			Unit worker = ws.gc.unit(w_ids.get(i));
			MapLocation goal = null;
			if (worker.location().isInGarrison()
					|| worker.location().isInSpace())
				continue;
			HashSet<MapLocation> nearestKarbonite = findNearestKarbonite(
					worker.location().mapLocation(), ws);
			// this code is fucking messy.. sorry
			if (nearestKarbonite.isEmpty()) {
				System.out.println("no nearest karbonite found!");
			} else {
				for (MapLocation ml : nearestKarbonite) {
					if (ws.gc.canSenseLocation(ml)) {
						karboniteLocations.get(ml.getX()).set(ml.getY(),
								ws.gc.karboniteAt(ml));
					}
				}
				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
						worker.location().mapLocation(), 65, ws.opp);
				// if not in karbonitelocation, first avoid enemies, if there
				// are none move ahead
				if (nearestKarbonite.iterator().next().distanceSquaredTo(
						worker.location().mapLocation()) > 2) {
					if (enemies.size() != 0) {
						kiteEnemies(enemies, worker, ws);
					}
					// if you did not move to kite (for example vs a worker) you
					// can move further
					ws.lll.goTo(worker.id(), nearestKarbonite);
				}
				// now that you are near (possibly moved further) check if you
				// can harvest
				for (MapLocation l : nearestKarbonite) {
					if (l.distanceSquaredTo(
							worker.location().mapLocation()) <= 2) {
						// you can harvest
						goal = l;
						Direction harvDir = worker.location().mapLocation()
								.directionTo(goal);
						if (worker.workerHasActed() == 0
								&& ws.gc.canHarvest(worker.id(), harvDir)) {
							// System.out.println("harvest! worker: " +
							// worker.id());
							ws.gc.harvest(worker.id(), harvDir);
						}
					}
				}
				// if you stood still before, but there are enemies nearby. now
				// kite!
				if (enemies.size() != 0) {
					kiteEnemies(enemies, worker, ws);
				}
			}
		}
		this.status = Status.RUNNING;
	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

	private void update(WorldState ws) {

	}

	private HashSet<MapLocation> findNearestKarbonite(MapLocation mapLocation,
			WorldState ws) {
		// System.out.println("findNearest: " + mapLocation.getX() + "," +
		// mapLocation.getY());
		int x = mapLocation.getX();
		int y = mapLocation.getY();
		if (karboniteLocations.get(x).get(y) > 0) {
			HashSet<MapLocation> r = new HashSet<>();
			r.add(mapLocation);
			return r;
		}
		HashSet<MapLocation> loc = findNearestKarbonite(mapLocation, 1, ws);
		return loc;
	}

	private boolean onMap(MapLocation loc) {
		int x = loc.getX();
		int y = loc.getY();
		if (x < 0 || x >= karboniteLocations.size() || y < 0
				|| y >= karboniteLocations.get(x).size())
			return false;
		else
			return true;
	}

	// zeichnet ein Quadrat mit laenge und breite level+level+1 und mittelpunkt
	// mapLocation und prueft auf karbonit
	private HashSet<MapLocation> findNearestKarbonite(MapLocation mapLocation,
			int level, WorldState ws) {
		// System.out.println("Level " + level);
		MapLocation currentLoc = mapLocation.addMultiple(Direction.South,
				level);
		HashSet<MapLocation> ret = new HashSet<>();
		if (isAnyKarboniteAt(currentLoc))
			ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
					currentLoc.getY()));
		for (int i = 0; i < level; i++) {
			currentLoc = currentLoc.add(Direction.West);
			// System.out.println("check: " + currentLoc.getX() + "," +
			// currentLoc.getY());
			if (isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
						currentLoc.getY()));
				// System.out.println("founc something on " + currentLoc.getX()
				// + "," + currentLoc.getY());
			}
		}
		for (int i = 0; i < (level + level); i++) {
			currentLoc = currentLoc.add(Direction.North);
			// System.out.println("check: " + currentLoc.getX() + "," +
			// currentLoc.getY());
			if (isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
						currentLoc.getY()));
				// System.out.println("founc something on " + currentLoc.getX()
				// + "," + currentLoc.getY());
			}
		}
		for (int i = 0; i < (level + level); i++) {
			currentLoc = currentLoc.add(Direction.East);
			// System.out.println("check: " + currentLoc.getX() + "," +
			// currentLoc.getY());
			if (isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
						currentLoc.getY()));
				// System.out.println("founc something on " + currentLoc.getX()
				// + "," + currentLoc.getY());
			}
		}
		for (int i = 0; i < (level + level); i++) {
			currentLoc = currentLoc.add(Direction.South);
			// System.out.println("check: " + currentLoc.getX() + "," +
			// currentLoc.getY());
			if (isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
						currentLoc.getY()));
				// System.out.println("founc something on " + currentLoc.getX()
				// + "," + currentLoc.getY());
			}
		}
		for (int i = 0; i < (level - 1); i++) {
			currentLoc = currentLoc.add(Direction.West);
			// System.out.println("check: " + currentLoc.getX() + "," +
			// currentLoc.getY());
			if (isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(),
						currentLoc.getY()));
				// System.out.println("founc something on " + currentLoc.getX()
				// + "," + currentLoc.getY());
			}
		}
		// TODO is a magicnumber
		if (ret.isEmpty() && level < karboniteLocations.size()) {
			// System.out.println("found nothing on level " + level);
			return findNearestKarbonite(mapLocation, level + 1, ws);
		} else {
			return ret;
		}
	}

	private boolean isAnyKarboniteAt(MapLocation currentLoc) {
		if (onMap(currentLoc)) {
			return (karboniteAt(currentLoc) > 0);
		} else {
			return false;
		}
	}

	private long karboniteAt(MapLocation currentLoc) {
		return karboniteLocations.get(currentLoc.getX()).get(currentLoc.getY());
	}

}
