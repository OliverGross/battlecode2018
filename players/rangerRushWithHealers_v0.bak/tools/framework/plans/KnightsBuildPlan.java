package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class KnightsBuildPlan extends Plan {

	public int savedKarbonite;

	public KnightsBuildPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
		savedKarbonite = 0;
	}

	@Override
	public void tick(WorldState ws) {
		int knightCost = (int) bc.costOf(UnitType.Knight,
				ws.gc.researchInfo().getLevel(UnitType.Knight));
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Factory)
				continue;
			for (Direction d : Direction.values()) {
				if (ws.gc.canUnload(uid, d))
					ws.gc.unload(uid, d);
			}
			if (ws.gc.canProduceRobot(uid, UnitType.Knight)
					&& ws.availableKarbonite() + savedKarbonite >= knightCost) {
				ws.gc.produceRobot(uid, UnitType.Knight);
				savedKarbonite -= ws
						.spendSavings(Math.max(savedKarbonite, knightCost));
				this.status = Status.RUNNING;
			} else {
				savedKarbonite += ws.saveKarbonite(knightCost);
			}
		}
		System.out.println(
				"Saved karbonite:" + savedKarbonite + "/" + knightCost);
	}

}
