package tools.framework.plans;

import java.util.ArrayList;

import bc.Unit;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.WorldState;

public class HealerHealPlan extends Plan {

	HealerPlan healerPlan;
	
	public HealerHealPlan(WorldState worldState, HealerPlan healerPlan) {
		super(worldState);
		this.healerPlan = healerPlan;
	}
	
	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		
		ArrayList<Integer> healers = healerPlan.getHealersOnMap();
		for(int healerId : healers) {
			if(ws.gc.isHealReady(healerId)) {
				Unit healer = ws.gc.unit(healerId);
				if(!healer.location().isOnMap()) continue;
				
				VecUnit allysInRange = ws.gc.senseNearbyUnitsByTeam(healer.location().mapLocation(), healer.attackRange() , ws.me);
				
				Unit target = getUnitToHeal(allysInRange);
				System.out.println("targetheal: " + target);
				if(target != null) {
					if(ws.gc.canHeal(healerId, target.id())) {
						System.out.println("Healer healed");
						ws.gc.heal(healerId, target.id());
					}
				}
			}
			
		}
		
	}

	
	
	private Unit getUnitToHeal(VecUnit allysInRange) {
		Unit bestTarget = null;
		for(int i=0; i<allysInRange.size(); i++) {
			Unit target = allysInRange.get(i);
			if(bestTarget == null) {
				if(target.health() != target.maxHealth()) {
					bestTarget = target;
				}
			} else {
				long missingHealth = target.maxHealth() - target.health();
				if(missingHealth > (bestTarget.maxHealth() - bestTarget.health())) {
					bestTarget = target;
				}
			}
		}
		return bestTarget;
	}

}
