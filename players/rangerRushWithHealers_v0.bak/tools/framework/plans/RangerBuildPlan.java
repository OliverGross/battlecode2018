package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class RangerBuildPlan extends Plan {

	public int savedKarbonite;
	public int rangers = 0;

	public RangerBuildPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
		savedKarbonite = 0;
	}

	@Override
	public void tick(WorldState ws) {
		int rangerCost = (int) bc.costOf(UnitType.Ranger,
				ws.gc.researchInfo().getLevel(UnitType.Ranger));
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Factory)
				continue;
			
			//produce healer as every Xth unit
			if(this.rangers == 10) {
				int healerCost = (int) bc.costOf(UnitType.Healer,
						ws.gc.researchInfo().getLevel(UnitType.Healer));
				if(ws.gc.canProduceRobot(uid, UnitType.Healer) && ws.availableKarbonite() + savedKarbonite >= healerCost) {
					ws.gc.produceRobot(uid, UnitType.Healer);
					System.out.println("Healerproducing");
					savedKarbonite -= ws.spendSavings(Math.max(savedKarbonite, healerCost));
					rangers = 0;
				}
			}
			
			
			if (ws.gc.canProduceRobot(uid, UnitType.Ranger)
					&& ws.availableKarbonite() + savedKarbonite >= rangerCost) {
				ws.gc.produceRobot(uid, UnitType.Ranger);
				rangers++;
				savedKarbonite -= ws
						.spendSavings(Math.max(savedKarbonite, rangerCost));
				this.status = Status.RUNNING;
			} else {
				savedKarbonite += ws.saveKarbonite(rangerCost);
			}
		}
	}

}