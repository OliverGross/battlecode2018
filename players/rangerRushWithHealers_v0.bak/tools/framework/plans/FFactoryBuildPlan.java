package tools.framework.plans;

import java.util.ArrayList;

import bc.*;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class FFactoryBuildPlan extends Plan {
	public int savedKarbonite;
	public boolean replicated;
	public int finishedFactories = 0;
	public int maxFactories;

	ArrayList<Integer> workerResources = new ArrayList<Integer>();
	ArrayList<Integer> factoryResources = new ArrayList<Integer>();

	public FFactoryBuildPlan(WorldState worldState, ArrayList<Integer> workers,
			boolean tryReplication, int maxNumberFactories) {
		super(worldState);
		worldState.resources.assignByIds(workers, pid);
		workerResources = worldState.resources.getUnitsByPlanAndType(pid,
				UnitType.Worker);

		savedKarbonite = 0;
		replicated = !tryReplication;
		maxFactories = maxNumberFactories;
	}

	public void updateResources(WorldState ws) {
		workerResources = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Worker);
		factoryResources = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Factory);

		for (int factory_id : factoryResources) {
			if (ws.gc.unit(factory_id).structureIsBuilt() == 1) {
				ws.resources.freeUnitById(factory_id);
				finishedFactories++;
			}
		}
		factoryResources = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Factory);
	}

	public boolean replicate(WorldState ws, int worker_id) {
		for (Direction d : DirHelp.adjacentDirections) {
			if (ws.gc.canReplicate(worker_id, d)) {
				ws.gc.replicate(worker_id, d);
				Unit newWorker = ws.gc.senseUnitAtLocation(
						ws.gc.unit(worker_id).location().mapLocation().add(d));
				if (newWorker != null) {
					ws.resources.assignByUnit(newWorker, pid);
					workerResources = ws.resources.getUnitsByPlanAndType(pid,
							UnitType.Worker);
				} else {
					assert false;
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public void tick(WorldState ws) {
		updateResources(ws);
		if (finishedFactories >= maxFactories || workerResources.size() == 0) {
			status = Status.FINAL;
			return;
		}

		int factorycost = (int) bc.costOf(UnitType.Factory,
				ws.gc.researchInfo().getLevel(UnitType.Factory));

		for (int structure_id : factoryResources) {
			assert savedKarbonite == 0;
			for (int worker_id : workerResources) {
				if (ws.gc.canBuild(worker_id, structure_id))
					ws.gc.build(worker_id, structure_id);
			}
		}
		for (int worker_id : workerResources) {
			if (!replicated) {
				replicated = replicate(ws, worker_id);
				break;
			}
		}

		for (int worker_id : workerResources) {
			Direction dir = Direction.Center;
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canBlueprint(worker_id, UnitType.Factory, d)) {
					dir = d;
					break;
				}
			}

			if (dir == Direction.Center)
				return;

			if (ws.availableKarbonite() + savedKarbonite >= factorycost
					&& (finishedFactories
							+ factoryResources.size()) < maxFactories) {
				ws.gc.blueprint(worker_id, UnitType.Factory, dir);
				Unit prototype = ws.gc.senseUnitAtLocation(ws.gc.unit(worker_id)
						.location().mapLocation().add(dir));
				if (prototype != null) {
					ws.resources.assignByUnit(prototype, pid);
					factoryResources = ws.resources.getUnitsByPlanAndType(pid,
							UnitType.Factory);
				} else {
					assert false;
				}
			}
			savedKarbonite -= ws.spendSavings(savedKarbonite);
			status = Status.RUNNING;
		}
		if (status != Status.RUNNING) {
			savedKarbonite += ws.saveKarbonite(factorycost);
		}
	}

}
