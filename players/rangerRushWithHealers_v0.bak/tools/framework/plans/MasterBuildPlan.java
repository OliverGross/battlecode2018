package tools.framework.plans;

import java.util.List;

import bc.Unit;
import bc.UnitType;
import bc.bc;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.framework.buildorder.BuildOrderEvaluator;

public class MasterBuildPlan extends Plan {

	int savedKarbonite = 0;
	BuildOrderEvaluator evaluator;

	public MasterBuildPlan(WorldState ws, BuildOrderEvaluator evaluator) {
		super(ws);
		this.evaluator = evaluator;
	}

	@Override
	protected void tick(WorldState ws) {
		List<Integer> myFactories = ws.resources
				.getMyUnitsByType(UnitType.Factory);

		for (int factoryId : myFactories) {
			Unit factory = ws.gc.unit(factoryId);
			if (factory.isFactoryProducing() == 1
					|| factory.structureIsBuilt() == 0)
				continue;

			UnitType typeToBuild = evaluator.getNextUnit(ws);

			int cost = (int) bc.costOf(typeToBuild,
					ws.gc.researchInfo().getLevel(typeToBuild));
			if (ws.gc.canProduceRobot(factoryId, typeToBuild)
					&& ws.availableKarbonite() + savedKarbonite >= cost) {
				ws.gc.produceRobot(factoryId, typeToBuild);
				System.out.println("Rangerproducing");
				savedKarbonite -= ws
						.spendSavings(Math.max(savedKarbonite, cost));
				this.status = Status.RUNNING;
			} else {
				savedKarbonite += ws.saveKarbonite(cost);
			}
		}
	}

}
