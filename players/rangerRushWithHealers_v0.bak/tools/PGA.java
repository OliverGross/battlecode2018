package tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Team;
import bc.Unit;
import bc.VecUnit;
import tools.framework.WorldState;

public class PGA {

	public HashMap<Integer, MapLocation> myWorkers = new HashMap<Integer, MapLocation>();
	public HashMap<Integer, MapLocation> oppWorkers = new HashMap<Integer, MapLocation>();
	WorldState ws;
	PlanetMap startMap;
	Team myTeam;
	public List<List<Long>> karbLocs = new ArrayList<List<Long>>();

	public PGA(PlanetMap startingMap, Team team, WorldState worldState) {
		startMap = startingMap;
		myTeam = team;
		ws = worldState;
		initWorkers();
		initKarb(ws);
	}

	public void initWorkers() {
		VecUnit initial = startMap.getInitial_units();
		for (int i = 0; i < initial.size(); i++) {
			Unit worker = initial.get(i);
			if (worker.team().equals(myTeam)) {
				myWorkers.put(worker.id(), worker.location().mapLocation());
			} else {
				oppWorkers.put(worker.id(), worker.location().mapLocation());
			}
		}

	}
	
	public void initKarb(WorldState worldstate) {
		long width;
		long height;
		width = startMap.getWidth();
		height = startMap.getHeight();
		MapLocation loc = new MapLocation(startMap.getPlanet(), 0, 0);
		for (int x = 0; x < width; x++) {
			karbLocs.add(new ArrayList<Long>());
			for (int y = 0; y < height; y++) {
				loc.setX(x);
				loc.setY(y);
				karbLocs.get(x).add(startMap.initialKarboniteAt(loc));
				//System.out.println(x + "," + y + " = " + initialmap.initialKarboniteAt(loc));
			}
		}
	}

	/**
	 * Evaluates starting map and enemy points The evaluated values are: 0 -
	 * minimal distance to enemy 1 - passableTerrain counter in adjacent squares
	 * 
	 * @return HashMap<id, ArrayList<Integer> values>
	 */
	public HashMap<Integer, ArrayList<Integer>> getWorkerEvaluation() {
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();
		ArrayList<Integer> tempValues;
		for (HashMap.Entry<Integer, MapLocation> pair : myWorkers.entrySet()) {
			tempValues = new ArrayList<Integer>();
			// evaluate closest worker distance
			tempValues.add(ws.pathfinding.getDistance(
					new HashSet<MapLocation>(oppWorkers.values()),
					pair.getValue()));
			// evaluate passable terrain in adjacent squares
			int passable = 0;
			for (Direction dir : DirHelp.adjacentDirections) {
				MapLocation adjacentLoc = pair.getValue().add(dir);
				if (adjacentLoc.getX() >= 0
						&& adjacentLoc.getX() < startMap.getWidth()
						&& adjacentLoc.getY() >= 0
						&& adjacentLoc.getY() < startMap.getHeight()) {
					{
						passable += startMap.isPassableTerrainAt(adjacentLoc);
					}
				}
			}
			tempValues.add(passable);
			map.put(pair.getKey(), tempValues);
		}
		return map;
	}

}
