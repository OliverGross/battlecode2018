package tools.infocaching;

import bc.Unit;
import bc.UnitType;

public class ExtendedUnitInfo {

	long lastRoundSeen;
	UnitStatus status;
	Unit unit;
	UnitType type;
	
	public ExtendedUnitInfo(Unit u, long round) {
		type = u.unitType();
		update(u, round);
		
	}
	
		public enum UnitStatus {
			alive, dead, garrison, uncertain
		}
	
	public void update(Unit u, long round) {
		this.unit = u;
		lastRoundSeen = round;
		status = UnitStatus.alive;
	}
	
	public void kill(long round) {
		this.unit = null;
		this.status = UnitStatus.dead;
		lastRoundSeen = round;
	}
	
	
	
}
