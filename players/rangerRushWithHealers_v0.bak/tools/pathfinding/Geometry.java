package tools.pathfinding;

public final class Geometry {
	public static int[] diff(int[] a, int[] b) {
		return new int[] { a[0] - b[0], a[1] - b[1] };
	}

	public static int[] add(int[] a, int[] b) {
		return new int[] { a[0] + b[0], a[1] + b[1] };
	}

	public static int[] abs(int[] a) {
		return new int[] { Math.abs(a[0]), Math.abs(a[1]) };
	}

	public static int chebyshev_distance(int x, int y) {
		return Math.max(Math.abs(x), Math.abs(y));
	}

	public static int chebyshev_distance(int[] a) {
		return chebyshev_distance(a[0], a[1]);
	}

	public static int chebyshev_distance(int[] a, int[] b) {
		return chebyshev_distance(Geometry.diff(a, b));
	}

	public static String toString(int[] a) {
		return "[" + a[0] + ", " + a[1] + "]";
	}
}
