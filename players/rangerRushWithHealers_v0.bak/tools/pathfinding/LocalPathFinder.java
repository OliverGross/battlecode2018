package tools.pathfinding;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import tools.DirHelp;

public class LocalPathFinder {
	int diffRegionSize;
	int diffGoalPathOffset;

	Set<HashablePoint> savedStarts = null;
	Set<HashablePoint> savedGoals = null;
	Set<HashablePoint> savedObstacles = null;
	List<HashablePoint> savedPath = null;

	JPS jps;

	public LocalPathFinder(JPS jps) {
		diffRegionSize = 2;
		diffGoalPathOffset = 3;
		this.jps = jps;
	}

	public Direction getNextDirection(HashablePoint start,
			Set<HashablePoint> goals, Set<HashablePoint> obstacles) {
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		starts.add(start);
		if (savedPath == null) {
			savedStarts = starts;
			savedGoals = goals;
			savedObstacles = obstacles;
			savedPath = jps.getPath(starts, goals, obstacles);
			return getNextDirection();
		} else {
			boolean startInFirstTwo = false;
			if (savedPath.get(0).equals(start))
				startInFirstTwo = true;
			else if (savedPath.get(1).equals(start)) {
				startInFirstTwo = true;
				savedPath.remove(0);
			}
			if (!startInFirstTwo) {
				boolean ranOpposite = false;
				Direction dir = DirHelp.getDirection(
						Geometry.diff(savedPath.get(1).a, savedPath.get(0).a));
				if (Geometry.chebyshev_distance(start.a,
						savedPath.get(0).a) == 1) {
					Direction startDir = DirHelp.getDirection(
							Geometry.diff(start.a, savedPath.get(0).a));
					if (DirHelp.diffAngle(dir, startDir) >= 3) {
						ranOpposite = true;
						savedPath.add(0, start);
					}
				}
				if (!ranOpposite)
					throw new RuntimeException(
							"Somehow path segments were skipped in the LocalPathFinder or unit got teleported or ...!");
			}
			boolean obstaclesChangedInDiffRegion = false;
			for (int dx = -diffRegionSize; dx <= diffRegionSize; dx++) {
				for (int dy = -diffRegionSize; dy <= diffRegionSize; dy++) {
					HashablePoint p = new HashablePoint(
							new int[] { start.a[0] + dx, start.a[1] + dy });
					if (savedObstacles.contains(p) != obstacles.contains(p)) {
						obstaclesChangedInDiffRegion = true;
						break;
					}
				}
			}
			if (obstaclesChangedInDiffRegion) {
				savedPath = null;
				return getNextDirection(start, goals, obstacles);
			}
			boolean goalsChangedToMuch = false;
			for (HashablePoint p : goals) {
				int minDist = 5555;
				for (HashablePoint savedGoal : savedGoals) {
					minDist = Math.min(minDist,
							Geometry.chebyshev_distance(p.a, savedGoal.a));
				}
				if (minDist > 0 && minDist
						+ diffGoalPathOffset > savedPath.size() - 1) {
					goalsChangedToMuch = true;
					break;
				}
			}
			if (!goalsChangedToMuch) {
				for (HashablePoint savedGoal : savedGoals) {
					int minDist = 5555;
					for (HashablePoint p : goals) {
						minDist = Math.min(minDist,
								Geometry.chebyshev_distance(p.a, savedGoal.a));
					}
					if (minDist > 0 && minDist
							+ diffGoalPathOffset > savedPath.size() - 1) {
						goalsChangedToMuch = true;
						break;
					}
				}
			}
			if (goalsChangedToMuch) {
				savedPath = null;
				return getNextDirection(start, goals, obstacles);
			}
			System.out.println("Whooho, using cached path!");
			return getNextDirection();
		}
	}

	public Direction getNextDirection() {
		if (savedPath.size() == 1) {
			return Direction.Center;
		}
		return DirHelp.getDirection(
				Geometry.diff(savedPath.get(1).a, savedPath.get(0).a));
	}

}
