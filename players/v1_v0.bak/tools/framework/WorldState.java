package tools.framework;

import java.util.Random;

import bc.*;
import tools.LowLevelLogic;
import tools.pathfinding.GeoPoint;
import tools.pathfinding.Pathfinding;

/**
 * A class which captures all information in the current game state collected by
 * the single- or multi-master-agent. Here would probably be also the place
 * where one could provide functionality like pathfinding or other general
 * algorithms executed on the given information.
 */
public class WorldState {
	/**
	 * should be used as the discrete in-game time-stamp
	 */
	public int round;
	public GameController gc;
	public Random rand = new Random(42);
	public Team me;
	public Team opp;
	public Pathfinding pathfinding;
	public LowLevelLogic lll;
	public int savedKarbonite;

	public WorldState() {
		savedKarbonite = 0;
		gc = new GameController();
		pathfinding = new Pathfinding(gc);
		lll = new LowLevelLogic(gc, pathfinding);
		me = gc.team();
		opp = Team.Blue;
		if (me == Team.Blue)
			opp = Team.Red;
	}

	public int availableKarbonite() {
		return (int) gc.karbonite() - savedKarbonite;
	}

	public int saveKarbonite(int maxAmount) {
		int available = availableKarbonite();
		if (maxAmount > available) {
			savedKarbonite += available;
			return available;
		}
		savedKarbonite += maxAmount;
		return maxAmount;
	}

	public int spendSavings(int amountSpent) {
		assert savedKarbonite >= amountSpent;
		savedKarbonite -= amountSpent;
		return amountSpent;
	}

	public void update() {
		// TODO maybe use this function
		return;
	}

	public int chebyshev_distance(int uid0, int uid1) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(),
				gc.unit(uid1).location().mapLocation());
	}

	public int chebyshev_distance(int uid0, MapLocation b) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(), b);
	}

	public static int chebyshev_distance(MapLocation a, MapLocation b) {
		return GeoPoint.chebyshev_distance(a.getX() - b.getX(),
				a.getY() - b.getY());
	}
}
