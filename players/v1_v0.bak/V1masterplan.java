import tools.framework.EvaluatorPlan;
import tools.framework.Plan;
import tools.framework.WorldState;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.FactoryBuildPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.KnightsBuildPlan;
import tools.framework.plans.ReplicationPlan;

public class V1masterplan extends EvaluatorPlan {

	public V1masterplan(WorldState worldState) {
		super(worldState);
		//plans.add(new ReplicationPlan(worldState));
		plans.add(new KnightsBuildPlan(worldState));
		plans.add(new FactoryBuildPlan(worldState));
		plans.add(new KnightsAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		// TODO this his highly ineffective but highlevel so i dont care ;)
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < plans.size(); i++) {
				Plan p = plans.get(i);
				if (!p.status.isTerminal())
					continue;
				if (p.getClass() == ReplicationPlan.class) {
					plans.remove(p);
					plans.add(new ReplicationPlan(worldState));
					changed = true;
					break;
				}
				if (p.getClass() == FactoryBuildPlan.class) {
					plans.remove(p);
					plans.add(new FactoryBuildPlan(worldState));
					changed = true;
					break;
				}
			}
		}
	}

}
