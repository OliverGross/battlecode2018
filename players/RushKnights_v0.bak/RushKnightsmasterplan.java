import java.util.ArrayList;
import java.util.HashSet;

import bc.MapLocation;
import bc.Planet;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.FFactoryBuildPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.KnightsBuildPlan;

public class RushKnightsmasterplan extends EvaluatorPlan {

	public RushKnightsmasterplan(WorldState worldState) {
		super(worldState);

		if (worldState.gc.planet().equals(Planet.Earth)) {
			VecUnit initial = worldState.gc.startingMap(Planet.Earth)
					.getInitial_units();
			Team myTeam = worldState.gc.team();
			HashSet<MapLocation> myWorkers = new HashSet<MapLocation>();
			HashSet<MapLocation> oppWorkers = new HashSet<MapLocation>();
			for (int i = 0; i < initial.size(); i++) {
				Unit worker = initial.get(i);
				if (worker.team().equals(myTeam)) {
					myWorkers.add(worker.location().mapLocation());
				} else {
					oppWorkers.add(worker.location().mapLocation());
				}
			}
			MapLocation closest = worldState.pathfinding.getNearestStart(myWorkers, oppWorkers);
			ArrayList<Integer> closestWorker = new ArrayList<Integer>();
			if (closest != null) {
				closestWorker.add(worldState.gc.senseUnitAtLocation(closest).id());
			} else {
				closestWorker.add(worldState.gc.senseUnitAtLocation(myWorkers.iterator().next()).id());
			}
			plans.add(new KnightsBuildPlan(worldState));
			plans.add(new FFactoryBuildPlan(worldState, closestWorker, true, 3));
		}

		plans.add(new KnightsAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		worldState.gc.queueResearch(UnitType.Knight);
		return;
	}

}
