package tools.framework;

public abstract class Evaluator {
	public double priority;

	public Evaluator(double priority) {
		this.priority = priority;
	}

	public abstract double getEvaluation(Plan plan);

	public final double evaluate(Plan plan) {
		return this.getEvaluation(plan) * priority;
	}
}
