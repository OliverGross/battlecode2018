package tools.framework;

import tools.pathfinding.JPS;

/**
 * An example for a complete agent. In every tick just retrieve all information
 * you can get from the simulation about the current worldState, then execute
 * the next tick of the AgentPlan.
 */
public class Agent {
	Plan masterplan;
	WorldState worldState;

	public Agent(WorldState worldState, Plan masterplan) {
		this.worldState = worldState;
		this.masterplan = masterplan;
	}

	public void execute() {
		while (!masterplan.status.isTerminal()) {
			tick();
		}
	}

	public void tick() {
		worldState.update();
		masterplan.tick(worldState);
		worldState.gc.nextTurn();
		System.out.println("Total path finding computation time for "
				+ JPS.totalCalls + " calls: " + JPS.compTime * 1e-6 + "ms");
	}
}
