package tools.framework;

/**
 * A plan can be a fixed execution of actions for one unit, or a dynamic
 * attacking strategy against the enemy front covering movements and actions of
 * an whole army. A plan can be formulated without ever being executed, only
 * partially executed (because something more important came up) or fully
 * executed to the end. A plan can only take one tick to execute or be ongoing
 * for the entire game. The main idea is, that the agent itself is a single plan
 * for the whole game getting executed every round using all the resources
 * optimally.
 * <p>
 * The priorityValue is used to compare its value to other plans and allocated
 * resources optimally. The tick_counter describes the time-stamp for this plan
 * and should be kept up-to-date. The tick() method is the one which gets called
 * every round of the game and executes part of the plan.
 */
public abstract class Plan {
	public Status status;
	public int round;
	public double priority;

	public Plan(WorldState worldState) {
		status = Status.PENDING;
		round = worldState.round;
	}

	public void tick(WorldState worldState) {
		round++;
	}
}
