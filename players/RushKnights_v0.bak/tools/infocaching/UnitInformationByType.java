package tools.infocaching;

import java.util.ArrayList;

import bc.Team;
import bc.Unit;

public class UnitInformationByType {
	
	Team myTeam;
	public UnitInformationByType(Team t) {
		myTeam = t;
	}

	public ArrayList<Unit> enemyRockets = new ArrayList<>();
	public ArrayList<Unit> myRockets = new ArrayList<>();
	public ArrayList<Unit> enemyFactories = new ArrayList<>();
	public ArrayList<Unit> myFactories = new ArrayList<>();
	public ArrayList<Unit> enemyWorkers = new ArrayList<>();
	public ArrayList<Unit> myWorkers = new ArrayList<>();
	public ArrayList<Unit> enemyRanger = new ArrayList<>();
	public ArrayList<Unit> myRanger = new ArrayList<>();
	public ArrayList<Unit> enemyKnights = new ArrayList<>();
	public ArrayList<Unit> myKnights = new ArrayList<>();
	public ArrayList<Unit> enemyMages = new ArrayList<>();
	public ArrayList<Unit> myMages = new ArrayList<>();
	public ArrayList<Unit> enemyHealers = new ArrayList<>();
	public ArrayList<Unit> myHealers = new ArrayList<>();
	
	public ArrayList<Unit> getMyUnits() {
		ArrayList<Unit> ret = new ArrayList<>();
		ret.addAll(myFactories);
		ret.addAll(myRockets);
		ret.addAll(myHealers);
		ret.addAll(myKnights);
		ret.addAll(myMages);
		ret.addAll(myRanger);
		ret.addAll(myWorkers);
		return ret;
	}

	public ArrayList<Unit> getEnemyUnits() {
		ArrayList<Unit> ret = new ArrayList<>();
		ret.addAll(enemyFactories);
		ret.addAll(enemyRockets);
		ret.addAll(enemyHealers);
		ret.addAll(enemyKnights);
		ret.addAll(enemyMages);
		ret.addAll(enemyRanger);
		ret.addAll(enemyWorkers);
		return ret;
	}
	
	public void addFactory(Unit u) {
		if (u.team() == myTeam) {
			myFactories.add(u);
		} else {
			enemyFactories.add(u);
		}
	}

	public void addRocket(Unit u) {
		if (u.team() == myTeam) {
			myRockets.add(u);
		} else {
			enemyRockets.add(u);
		}
	}

	public void addRanger(Unit u) {
		if (u.team() == myTeam) {
			myRanger.add(u);
		} else {
			enemyRanger.add(u);
		}
	}

	public void addKnight(Unit u) {
		if (u.team() == myTeam) {
			myKnights.add(u);
		} else {
			enemyKnights.add(u);
		}
	}

	public void addMage(Unit u) {
		if (u.team() == myTeam) {
			myMages.add(u);
		} else {
			enemyMages.add(u);
		}
	}

	public void addWorker(Unit u) {
		if (u.team() == myTeam) {
			myWorkers.add(u);
		} else {
			enemyWorkers.add(u);
		}
	}

	public void addHealer(Unit u) {
		if (u.team() == myTeam) {
			myHealers.add(u);
		} else {
			enemyHealers.add(u);
		}
	}
	
	public ArrayList<Unit> getEnemyRocketsAll() {
		return enemyRockets;
	}
	
	public ArrayList<Unit> getEnemyRocketsUnbuild() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : enemyRockets) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 0) {
				ret.add(u);
			}
		}
		return ret;
	}
	public ArrayList<Unit> getEnemyRocketsFinished() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : enemyRockets) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 1) {
				ret.add(u);
			}
		}
		return ret;
	}

	public ArrayList<Unit> getMyRocketsAll() {
		return myRockets;
	}
	
	public ArrayList<Unit> getMyRocketsUnbuild() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : myRockets) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 0) {
				ret.add(u);
			}
		}
		return ret;
	}
	public ArrayList<Unit> getMyRocketsFinished() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : myRockets) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 1) {
				ret.add(u);
			}
		}
		return ret;
	}

	public ArrayList<Unit> getEnemyFactoriesAll() {
		return enemyFactories;
	}
	
	public ArrayList<Unit> getEnemyFactoriesUnbuilt() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : enemyFactories) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 0) {
				ret.add(u);
			}
		}
		return ret;
	}
	
	public ArrayList<Unit> getEnemyFactoriesFinished() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : enemyFactories) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 1) {
				ret.add(u);
			}
		}
		return ret;
	}

	public ArrayList<Unit> getMyFactoriesAll() {
		return myFactories;
	}
	
	public ArrayList<Unit> getMyFactoriesUnbuilt() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : myFactories) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 0) {
				ret.add(u);
			}
		}
		return ret;
	}
	
	public ArrayList<Unit> getMyFactoriesFinished() {
		ArrayList<Unit> ret = new ArrayList<>();
		for(Unit u : myFactories) {
			//TODO this is stupid, this should be a bool
			if(u.structureIsBuilt() == 1) {
				ret.add(u);
			}
		}
		return ret;
	}

	public ArrayList<Unit> getEnemyWorkers() {
		return enemyWorkers;
	}

	public ArrayList<Unit> getMyWorkers() {
		return myWorkers;
	}

	public ArrayList<Unit> getEnemyRanger() {
		return enemyRanger;
	}

	public ArrayList<Unit> getMyRanger() {
		return myRanger;
	}

	public ArrayList<Unit> getEnemyKnights() {
		return enemyKnights;
	}

	public ArrayList<Unit> getMyKnights() {
		return myKnights;
	}

	public ArrayList<Unit> getEnemyMages() {
		return enemyMages;
	}

	public ArrayList<Unit> getMyMages() {
		return myMages;
	}

	public ArrayList<Unit> getEnemyHealers() {
		return enemyHealers;
	}

	public ArrayList<Unit> getMyHealers() {
		return myHealers;
	}
	
}
