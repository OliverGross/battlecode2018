import tools.framework.Agent;
import tools.framework.Plan;
import tools.framework.WorldState;

public class Mages {
	public static void main(String[] args) {
		WorldState worldstate = new WorldState();
		Plan masterplan = new MagesMasterPlan(worldstate);
		Agent agent = new Agent(worldstate, masterplan);
		agent.execute();
	}
}