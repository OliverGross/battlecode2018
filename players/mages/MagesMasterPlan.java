import java.util.ArrayList;
import java.util.HashSet;

import bc.MapLocation;
import bc.Planet;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.PGA;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.buildorder.MagesBuildOrder;
import tools.framework.buildorder.StructureBuildOrder;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.HealerPlan;
import tools.framework.plans.MageAttackPlan;
import tools.framework.plans.MasterBuildPlan;
import tools.framework.plans.MasterStructureBuildPlan;

public class MagesMasterPlan extends EvaluatorPlan {

	public MagesMasterPlan(WorldState worldState) {
		super(worldState);
		if (worldState.gc.planet().equals(Planet.Earth)) {
			VecUnit initial = worldState.gc.startingMap(Planet.Earth)
					.getInitial_units();
			PGA preGame = worldState.pga;
			Team myTeam = worldState.gc.team();
			HashSet<MapLocation> myWorkers = new HashSet<MapLocation>();
			HashSet<MapLocation> oppWorkers = new HashSet<MapLocation>();
			for (int i = 0; i < initial.size(); i++) {
				Unit worker = initial.get(i);
				if (worker.team().equals(myTeam)) {
					myWorkers.add(worker.location().mapLocation());
				} else {
					oppWorkers.add(worker.location().mapLocation());
				}
			}
			MapLocation closest = worldState.pathfinding
					.getNearestStart(myWorkers, oppWorkers);
			ArrayList<Integer> closestWorker = new ArrayList<Integer>();
			if (closest != null) {
				closestWorker
						.add(worldState.gc.senseUnitAtLocation(closest).id());
			} else {
				closestWorker.add(worldState.gc
						.senseUnitAtLocation(myWorkers.iterator().next()).id());
			}
			plans.add(new MasterBuildPlan(worldState, new MagesBuildOrder()));
			//plans.add(
			//		new FFactoryBuildPlan(worldState, closestWorker, true, 3));
			//plans.add(new FactoryBuildPlanDynamic(worldState, closestWorker));
			plans.add(new MasterStructureBuildPlan(worldState, new StructureBuildOrder(), closestWorker));
			// see bellow for an example on how to replicate
//			if (!ws.resources.getFreeUnitsByType(UnitType.Worker).isEmpty()) {
//				System.out.println("my free workers:" + ws.resources
//						.getFreeUnitsByType(UnitType.Worker).get(0));
//				plans.add(new ReplicatePlan(worldState,
//						ws.resources.getFreeUnitsByType(UnitType.Worker).get(0),
//						2));
//			}
			ArrayList<Integer> harvestWorkers = new ArrayList<>();
			for (int i = 0; i < worldState.gc.myUnits().size(); i++) {
				Unit w = worldState.gc.myUnits().get(i);
				if (closestWorker.contains(w.id()))
					continue;
				else
					harvestWorkers.add(w.id());
			}
			plans.add(new HarvestPlan(worldState, preGame.karbLocs));
		}
		plans.add(new HealerPlan(worldState));
		plans.add(new MageAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		worldState.gc.queueResearch(UnitType.Mage);
	}

}
