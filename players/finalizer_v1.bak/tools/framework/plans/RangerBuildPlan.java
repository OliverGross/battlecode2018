package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class RangerBuildPlan extends Plan {

	public int rangers = 0;

	public RangerBuildPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		int rangerCost = (int) bc.costOf(UnitType.Ranger,
				ws.gc.researchInfo().getLevel(UnitType.Ranger));
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() != UnitType.Factory)
				continue;
			
			//produce healer as every Xth unit
			if(this.rangers == 10) {
				int healerCost = (int) bc.costOf(UnitType.Healer,
						ws.gc.researchInfo().getLevel(UnitType.Healer));
				if(ws.gc.canProduceRobot(uid, UnitType.Healer) && ws.resources.getAvailableKarbonite(pid) >= healerCost) {
					ws.gc.produceRobot(uid, UnitType.Healer);
					System.out.println("Healerproducing");
					rangers = 0;
				}
			}
			
			
			if (ws.gc.canProduceRobot(uid, UnitType.Ranger)
					&& ws.resources.getAvailableKarbonite(pid) >= rangerCost) {
				ws.gc.produceRobot(uid, UnitType.Ranger);
				rangers++;
				this.status = Status.RUNNING;
			} else {
				
			}
		}
	}

}