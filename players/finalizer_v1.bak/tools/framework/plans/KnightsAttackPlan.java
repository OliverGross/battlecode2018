package tools.framework.plans;

import java.util.ArrayList;

import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.BehaviourPlan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.framework.behaviours.KnightAttackBehaviour;
import tools.framework.behaviours.KnightMoveBehaviour;
import tools.framework.behaviours.UnloadBehaviour;

public class KnightsAttackPlan extends BehaviourPlan{

	ArrayList<Integer> knightsOnMap = new ArrayList<>();
	
	public KnightsAttackPlan(WorldState worldState) {
		super(worldState);

		Behaviour attackBehaviour = new KnightAttackBehaviour(worldState, this);
		Behaviour moveBehaviour = new KnightMoveBehaviour(worldState, this);
		Behaviour unloadBehaviour = new UnloadBehaviour();
		
		addBehaviour(unloadBehaviour);
		addBehaviour(attackBehaviour);
		addBehaviour(moveBehaviour);
		addBehaviour(attackBehaviour);
		
	}

	private void updateKnights(WorldState ws) {
		ws.resources.assignAllFreeByType(pid, UnitType.Knight);
		knightsOnMap = ws.resources.getUnitsByPlan(pid);
	}
	
	@Override
	public void tick(WorldState ws) {
		this.updateKnights(ws);
		super.tick(ws);
		this.status = Status.RUNNING;
	}
	
	public ArrayList<Integer> getKnights() {
		return knightsOnMap;
	}
	
}
