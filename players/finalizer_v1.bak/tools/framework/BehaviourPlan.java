package tools.framework;

import java.util.ArrayList;
import java.util.List;

public class BehaviourPlan extends Plan {
	private List<Behaviour> behaviours;

	public BehaviourPlan(WorldState worldState) {
		super(worldState);
		behaviours = new ArrayList<Behaviour>();
	}

	public void addBehaviour(Behaviour behaviour) {
		behaviours.add(behaviour);
	}

	@Override
	protected void tick(WorldState worldState) {
		for (Behaviour behaviour : behaviours)
			behaviour.act(worldState);
	}

}
