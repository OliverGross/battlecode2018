package tools.framework;

import java.util.Random;

import bc.GameController;
import bc.MapLocation;
import bc.Team;
import tools.LowLevelLogic;
import tools.PGA;
import tools.pathfinding.Geometry;
import tools.pathfinding.Pathfinding;

/**
 * A class which captures all information in the current game state collected by
 * the single- or multi-master-agent. Here would probably be also the place
 * where one could provide functionality like pathfinding or other general
 * algorithms executed on the given information.
 */
public class WorldState {
	// times for measuring
	private long timeLeftMs;
	private long roundStartNs;
	// important game variables
	public int round;
	public GameController gc;
	public Random rand = new Random(42);
	public Team me;
	public Team opp;
	public Pathfinding pathfinding;
	public LowLevelLogic lll;
	public ResourceState resources;
	private static int runningInstances = 0;
	public UnitLists unitLists;
	public PGA pga;

	public WorldState() {
		if (runningInstances >= 1) {
			throw new RuntimeException(
					"There already exists a world state! Do not construct a second instance!");
		}
		runningInstances++;

		round = 0;
		gc = new GameController();
		roundStartNs = System.nanoTime();
		timeLeftMs = gc.getTimeLeftMs();

		pathfinding = new Pathfinding(this);
		lll = new LowLevelLogic(gc, pathfinding);

		me = gc.team();
		opp = Team.Blue;
		if (me == Team.Blue)
			opp = Team.Red;

		unitLists = new UnitLists(me);
		resources = new ResourceState(this);
		update();
		pga = new PGA(this);
	}

	public void nextTurn() {
		gc.nextTurn();
		roundStartNs = System.nanoTime();
		timeLeftMs = gc.getTimeLeftMs();
		update();
	}

	public long getTimeLeftNs() {
		if (timeLeftMs != gc.getTimeLeftMs())
			throw new RuntimeException(timeLeftMs + " " + gc.getTimeLeftMs());
		return 1000000 * timeLeftMs + roundStartNs - System.nanoTime();
	}

	public void updateUnitLists() {
		unitLists.update(gc);
		pathfinding.updateObstacles();
	}

	public void update() {
		// System.out.println("Worldstate ticked!");
		round++;
		if (round != gc.round())
			throw new RuntimeException("Round(" + round + ") and gc.round("
					+ gc.round() + ") are out of sync!");
		resources.update();
		updateUnitLists();
	}

	public int chebyshev_distance(int uid0, int uid1) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(),
				gc.unit(uid1).location().mapLocation());
	}

	public int chebyshev_distance(int uid0, MapLocation b) {
		return chebyshev_distance(gc.unit(uid0).location().mapLocation(), b);
	}

	public static int chebyshev_distance(MapLocation a, MapLocation b) {
		return Geometry.chebyshev_distance(a.getX() - b.getX(),
				a.getY() - b.getY());
	}

	public MapLocation getMLoc(int uid) {
		return gc.unit(uid).location().mapLocation();
	}
}
