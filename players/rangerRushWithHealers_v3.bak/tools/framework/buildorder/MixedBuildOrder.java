package tools.framework.buildorder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import bc.UnitType;
import tools.framework.WorldState;

public class MixedBuildOrder implements BuildOrderEvaluator {

	@Override
	public UnitType getNextUnit(WorldState ws) {
		int healers = ws.unitLists.myUnits.get(UnitType.Healer).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Healer);
		int rangers = ws.unitLists.myUnits.get(UnitType.Ranger).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Ranger);
		int knights = ws.unitLists.myUnits.get(UnitType.Knight).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Knight);
		int mages = ws.unitLists.myUnits.get(UnitType.Mage).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Mage);

		int totalDDUnits = rangers + knights + mages;

		// this is not yet used
		int weightRanger = 6;
		int weightKnight = 0;
		int weightMage = 0;
		int weightHealer = 1;
		BuildPrio rangerPrio = new BuildPrio(UnitType.Ranger,
				weightRanger * rangers);
		BuildPrio knightPrio = new BuildPrio(UnitType.Knight,
				weightKnight * knights);
		BuildPrio magePrio = new BuildPrio(UnitType.Mage, weightMage * mages);
		BuildPrio healerPrio = new BuildPrio(UnitType.Healer,
				weightHealer * healers);
		ArrayList<BuildPrio> sortedPrios = new ArrayList<>();
		sortedPrios.add(rangerPrio);
		sortedPrios.add(knightPrio);
		sortedPrios.add(magePrio);
		sortedPrios.add(healerPrio);
		sortPrio(sortedPrios);

		// this is the currend solution
		if (healers == 0)
			healers = 1;
		int prioHealer = totalDDUnits / healers;

		if (prioHealer > weightRanger) {
			return UnitType.Healer;
		} else {
			return UnitType.Knight;
		}
	}

	public class BuildPrio {

		public int prio;
		public UnitType type;

		public BuildPrio(UnitType type, int prio) {
			this.type = type;
			this.prio = prio;
		}
	}

	public void sortPrio(ArrayList<BuildPrio> prios) {
		Collections.sort(prios, new Comparator<BuildPrio>() {

			@Override
			public int compare(BuildPrio o1, BuildPrio o2) {
				if (o1.prio == o2.prio)
					return 0;
				if (o1.prio > o2.prio) {
					return 1;
				} else
					return -1;
			}
		});
	}

}
