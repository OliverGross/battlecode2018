package tools.framework.behaviours;

import bc.MapLocation;
import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.MageAttackPlan;

//Attack the nearest non-worker non-factory enemy. If there are multiple nearest, first decide by type, then by left HP
public class MageAttackBehaviour extends Behaviour {

	private MageAttackPlan attackPlan;

	public MageAttackBehaviour(MageAttackPlan plan) {
		attackPlan = plan;
	}

	@Override
	public void act(WorldState ws) {
		for (int MageId : attackPlan.getMagesOnMap()) {
			Unit Mage = ws.gc.unit(MageId);
			if (!Mage.location().isOnMap())
				continue;

			if (ws.gc.isAttackReady(MageId)) {
				Unit target = selectBestTarget(Mage, ws);

				if (target != null) {
					if (ws.gc.canAttack(MageId, target.id())) {
						ws.gc.attack(Mage.id(), target.id());
					} else {
						System.out.println(
								"this shouldnt really happen, selectBestTarget should only return attackable units");
					}
				}
			}
		}
	}

	private Unit selectBestTarget(Unit Mage, WorldState ws) {
		MapLocation MageLocation = Mage.location().mapLocation();

		Unit bestTarget = null;
		long bestDistance = 100;

		VecUnit enemiesInRange = ws.gc.senseNearbyUnitsByTeam(MageLocation,
				Mage.attackRange(), ws.opp);
		for (int e = 0; e < enemiesInRange.size(); e++) {
			Unit enemy = enemiesInRange.get(e);
			long distance = MageLocation
					.distanceSquaredTo(enemy.location().mapLocation());
			//TODO Magic Number, do not attack close to you so you don't damage yourself
			if (distance > 2) {
				if (bestTarget == null) {
					bestTarget = enemy;
					bestDistance = distance;
				} else {
					// Ignore certain enemies
					if (distance < bestDistance && typeValue(enemy) > 2) {
						bestTarget = enemy;
						bestDistance = distance;
					} else if (distance == bestDistance) {
						if (typeValue(bestTarget) < typeValue(enemy)) {
							if (enemy.health() < bestTarget.health()) {
								// if equal in type and distance -> attack
								// lowest
								bestTarget = enemy;
								bestDistance = distance;
							}
						}
					}
				}
			}
		}
		return bestTarget;

	}

	// The higher the value, the better to focus
	private int typeValue(Unit u) {
		if (u == null) {
			System.out.println("typeValue Unit was null");
			return 0;
		}

		switch (u.unitType()) {
		case Factory:
			return 1;
		case Worker:
			return 2;
		case Healer:
			return 3;
		case Rocket:
			return 4;
		case Ranger:
			return 5;
		case Knight:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;

		}
	}

}
