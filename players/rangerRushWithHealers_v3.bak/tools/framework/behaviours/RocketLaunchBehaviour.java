package tools.framework.behaviours;

import java.util.ArrayList;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.VecUnit;
import bc.bc;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.MarsProgramPlan;

public class RocketLaunchBehaviour extends Behaviour {
	MarsProgramPlan parent;
	final int rocketBlastRadius = 2;

	public RocketLaunchBehaviour(WorldState worldState,
			MarsProgramPlan marsProgramPlan) {
		parent = marsProgramPlan;
	}

	@Override
	public void act(WorldState ws) {
		ArrayList<Integer> rocketsToLaunch = new ArrayList<Integer>();

		if (parent.launchableRockets.size() >= parent.maxNumLaunchableRockets
				|| ws.gc.round() > parent.latestLaunch) {
			// launch every full rocket
			rocketsToLaunch.addAll(parent.launchableRockets);
		}
		if (ws.gc.round() > parent.latestLaunch) {
			rocketsToLaunch.addAll(parent.loadableRockets);
		}

		for (int rocket_id : rocketsToLaunch) {
			MapLocation landingLoc = ws.resources.getNextRocketLocation();
			Unit rocket = ws.gc.unit(rocket_id);
			VecUnit nearbys = ws.gc.senseNearbyUnits(
					ws.gc.unit(rocket_id).location().mapLocation(),
					rocketBlastRadius);
			for (int i = 0; i < nearbys.size(); i++) {
				Unit nearby = nearbys.get(i);
				if (nearby.team() == ws.me) {
					Direction dir = bc.bcDirectionOpposite(
							nearby.location().mapLocation().directionTo(
									rocket.location().mapLocation()));
					if (ws.gc.canMove(nearby.id(), dir)) {
						ws.gc.moveRobot(nearby.id(), dir);
					} else {
						System.out
								.println("Will damage own unit during launch");
					}
				}

			}
			if (ws.gc.canLaunchRocket(rocket_id, landingLoc)) {
				ws.gc.launchRocket(rocket_id, landingLoc);
				break;
			} else {
				throw new RuntimeException(
						"Cannot launch rocket to " + landingLoc);
			}
		}
	}
}
