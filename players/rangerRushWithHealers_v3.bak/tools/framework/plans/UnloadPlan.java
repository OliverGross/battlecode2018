package tools.framework.plans;

import bc.Direction;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.WorldState;

public class UnloadPlan extends Plan {

	public UnloadPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);

		// Unload all Units from Factories, and from Rockets if on Mars
		VecUnit myUnits = ws.gc.myUnits();
		for (int i = 0; i < myUnits.size(); i++) {
			Unit structure = myUnits.get(i);
			if (structure.unitType() == UnitType.Factory
					|| (structure.unitType() == UnitType.Rocket
							&& ws.gc.planet() == Planet.Mars)) {
				for (Direction d : Direction.values()) {
					if (structure.structureGarrison().size() != 0) {
						if (ws.gc.canUnload(structure.id(), d)) {
							ws.gc.unload(structure.id(), d);
						}
					}
				}
			}
		}
	}

}
