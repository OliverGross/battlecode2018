package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Unit;
import bc.UnitType;
import bc.VecMapLocation;
import bc.VecUnit;
import bc.bc;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;
import tools.framework.buildorder.BuildOrderEvaluator;

public class CMasterStructureBuildPlan extends Plan {

	BuildOrderEvaluator buildOrder = null;

	private Set<Integer> myUnfinishedBuildings = new HashSet<>();
	private Set<Integer> myFinishedFactories = new HashSet<>();
	private Set<Integer> myFinishedRockets = new HashSet<>();
	private List<MapLocation> unfinishedBuildingsLocations = new ArrayList<>();
	private List<MapLocation> finishedStructureLocations = new ArrayList<>();

	public CMasterStructureBuildPlan(WorldState worldState,
			BuildOrderEvaluator evalBuildOrder,
			ArrayList<Integer> closestWorker) {
		super(worldState);
		for (int id : closestWorker) {
			ws.resources.assignById(id, pid);
		}
		buildOrder = evalBuildOrder;

	}

	@Override
	protected void tick(WorldState worldState) {
		update();
		// free all units from this plan (maybe they get added later in the
		// process)
		ws.resources.freeUnitsFromPlan(pid);

		UnitType nextType = buildOrder.getNextUnit(ws);
		if (nextType != null) {
			long cost = bc.costOf(nextType,
					ws.gc.researchInfo().getLevel(nextType));
			Unit worker = getBestWorker(ws);
			if (worker != null) {
				if (ws.resources.getAvailableKarbonite(pid) > cost) {
					buildStructure(worker, nextType);
				}
			}
		}

		for (MapLocation buildingLocation : unfinishedBuildingsLocations) {
			ArrayList<Unit> myWorkers = new ArrayList<>();
			Unit building = null;
			if (ws.gc.hasUnitAtLocation(buildingLocation)) {
				building = ws.gc.senseUnitAtLocation(buildingLocation);
			} else {
				continue;
			}
			if (building == null)
				continue;

			VecUnit workers = ws.gc.senseNearbyUnitsByType(buildingLocation, 4,
					UnitType.Worker);
			for (int i = 0; i < workers.size(); i++) {
				Unit worker = workers.get(i);
				if (!worker.location().isOnMap())
					continue;
				if (worker.team() != ws.me)
					continue;
				myWorkers.add(worker);
			}
			if (myWorkers.size() < 2) {
				VecUnit workersFar = ws.gc.senseNearbyUnitsByType(
						buildingLocation, 20, UnitType.Worker);
				for (int i = 0; i < workersFar.size(); i++) {
					Unit worker = workersFar.get(i);
					if (!worker.location().isOnMap())
						continue;
					if (worker.team() != ws.me)
						continue;
					myWorkers.add(worker);
				}
			}

			for (Unit worker : myWorkers) {
				ws.resources.freeUnitById(worker.id());
				ws.resources.assignById(worker.id(), pid);
				if (worker.location().mapLocation()
						.distanceSquaredTo(buildingLocation) > 2) {
					ws.lll.goTo(worker.id(), buildingLocation);
				}
				if (worker.location().mapLocation()
						.distanceSquaredTo(buildingLocation) <= 2) {
					if (worker.workerHasActed() == 0) {
						if (ws.gc.canBuild(worker.id(), building.id())) {
							ws.gc.build(worker.id(), building.id());
						}
					}
				}
			}
		}
	}

	private Unit getBestWorker(WorldState ws) {
		Unit bestWorker = null;
		boolean bestIsInDanger = true;
		boolean bestIsReady = false;
		int bestFreeLocations = 0;
		int bestSafeLocations = 0;
		for (int workerId : ws.unitLists.myUnits.get(UnitType.Worker)) {
			Unit worker = ws.gc.unit(workerId);
			if (worker == null || !worker.location().isOnMap())
				continue;

			boolean isInDanger = true;
			boolean isReady = false;
			int freeLocations = 0;
			int safeLocations = 0;
			PlanetMap map = ws.gc.startingMap(ws.gc.planet());

			isInDanger = isWorkerInDanger(worker);
			isReady = worker.workerHasActed() == 0;
			for (Direction d : DirHelp.adjacentDirections) {
				MapLocation currLoc = worker.location().mapLocation().add(d);
				if (isFree(currLoc, map))
					freeLocations++;
				if (isSafe(currLoc))
					safeLocations++;
			}

			if (!isInDanger || (isInDanger && bestIsInDanger)) {
				if (isReady || (!isReady && !bestIsReady)) {
					if (bestSafeLocations < safeLocations) {
						bestWorker = worker;
						bestIsInDanger = isInDanger;
						bestIsReady = isReady;
						bestFreeLocations = freeLocations;
						bestSafeLocations = safeLocations;
					}
					if (bestSafeLocations == safeLocations) {
						if (bestFreeLocations < freeLocations) {
							bestWorker = worker;
							bestIsInDanger = isInDanger;
							bestIsReady = isReady;
							bestFreeLocations = freeLocations;
							bestSafeLocations = safeLocations;
						}
					}
				}
			}

		}
		return bestWorker;
	}

	private boolean isWorkerInDanger(Unit worker) {
		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				worker.location().mapLocation(), 65, ws.opp);
		for (int i = 0; i < enemies.size(); i++) {
			if (enemies.get(i).unitType() != UnitType.Worker
					&& enemies.get(i).unitType() != UnitType.Healer
					&& enemies.get(i).unitType() != UnitType.Factory
					&& enemies.get(i).unitType() != UnitType.Rocket) {
				return true;
			}
		}
		return false;
	}

	private void doSomethingUseful(Unit worker, WorldState ws) {
		// run around with unit to the most beneficial areas
		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				worker.location().mapLocation(), 65, ws.opp);
		kiteEnemies(enemies, worker, ws);
		if (ws.gc.isMoveReady(worker.id())) {
			// run to the most "free" point in the area
			VecMapLocation locationsAround = ws.gc
					.allLocationsWithin(worker.location().mapLocation(), 2);
			MapLocation bestLoc = worker.location().mapLocation();
			for (int i = 0; i < locationsAround.size(); i++) {
				if (worker.workerHasActed() == 0
						&& ws.gc.karboniteAt(locationsAround.get(i)) > 0) {
					if (ws.gc.canHarvest(worker.id(),
							worker.location().mapLocation()
									.directionTo(locationsAround.get(i)))) {
						ws.gc.harvest(worker.id(),
								worker.location().mapLocation()
										.directionTo(locationsAround.get(i)));
					}
				}
			}
			// move out of the way
			bestLoc = getBestMoveLocation(worker);

			if (ws.gc.canMove(worker.id(),
					worker.location().mapLocation().directionTo(bestLoc))) {
				ws.gc.moveRobot(worker.id(),
						worker.location().mapLocation().directionTo(bestLoc));
			}
		}
	}

	private MapLocation getBestMoveLocation(Unit worker) {
		VecMapLocation locationsAround = ws.gc
				.allLocationsWithin(worker.location().mapLocation(), 2);
		PlanetMap map = ws.gc.startingMap(ws.gc.planet());
		MapLocation bestLoc = worker.location().mapLocation();
		ArrayList<MapLocation> adjacentFree = new ArrayList<>();
		int bestFree = 0;
		int bestStructureCountAround = 10;

		// get locations directly around and count freetiles and factories
		for (int i = 0; i < locationsAround.size(); i++) {
			if (isFree(locationsAround.get(i), map)) {
				bestFree++;
				adjacentFree.add(locationsAround.get(i));
			} else {
				if (ws.gc.hasUnitAtLocation(locationsAround.get(i))) {
					Unit u = ws.gc.senseUnitAtLocation(locationsAround.get(i));
					if (u.unitType() == UnitType.Factory
							|| u.unitType() == UnitType.Rocket) {
						bestStructureCountAround++;
					}
				}
			}
		}

		// for each adjacent, walkable check their adjacent tiles
		// TODO what happens with the current worker location? at the moment
		// free is counted as 1
		for (MapLocation currLoc : adjacentFree) {
			int free = 1;
			int structureCount = 0;
			for (Direction d : DirHelp.adjacentDirections) {
				MapLocation curr = currLoc.add(d);
				if (isFree(curr, map)) {
					free++;
				} else {
					if (ws.gc.hasUnitAtLocation(currLoc)) {
						Unit u = ws.gc.senseUnitAtLocation(currLoc);
						if (u.unitType() == UnitType.Factory
								|| u.unitType() == UnitType.Rocket) {
							bestStructureCountAround++;
						}
					}
				}
			}
			if (bestFree < free || (bestFree == free
					&& bestStructureCountAround > structureCount)) {
				bestFree = free;
				bestStructureCountAround = structureCount;
				bestLoc = currLoc;
			}
		}

		return bestLoc;
	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

	private void buildStructure(Unit worker, UnitType type) {
		Set<MapLocation> possibleLocations = new HashSet<>();
		MapLocation workerLoc = worker.location().mapLocation();
		PlanetMap map = ws.gc.startingMap(ws.gc.planet());
		VecMapLocation poss = null;
		// if worker can move, consider more tiles
		if (ws.gc.isMoveReady(worker.id())) {
			poss = ws.gc.allLocationsWithin(workerLoc, 2);
		} else {
			poss = ws.gc.allLocationsWithin(workerLoc, 4);
		}
		// check all tiles
		for (int i = 0; i < poss.size(); i++) {
			MapLocation curr = poss.get(i);
			if (isFree(curr, map)) {
				possibleLocations.add(curr);
			}
		}

		// decide for the best location
		MapLocation bestLocation = null;
		int bestFreeTiles = 0;
		for (MapLocation currentLocation : possibleLocations) {
			if (bestLocation == null) {
				int freeTiles = 0;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				if (isSafe(currentLocation)) {
					bestLocation = currentLocation;
					bestFreeTiles = freeTiles;
				}
			} else {
				int freeTiles = 0;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				if (freeTiles > bestFreeTiles && isSafe(currentLocation)) {
					bestLocation = currentLocation;
					bestFreeTiles = freeTiles;
				}
			}
		}
		if (bestLocation != null) {
			long distance = bestLocation.distanceSquaredTo(workerLoc);
			// move should be ready here, so move
			if (distance > 2) {
				ws.lll.goTo(worker.id(), bestLocation);
			}
			// get the direction from the now-new workerlocation
			Direction dir = worker.location().mapLocation()
					.directionTo(bestLocation);
			// int cost = (int) bc.costOf(type,
			// ws.gc.researchInfo().getLevel(type));
			if (ws.gc.canBlueprint(worker.id(), type, dir)) {
				ws.gc.blueprint(worker.id(), type, dir);
				if (ws.gc.hasUnitAtLocation(bestLocation)) {
					Unit fac = ws.gc.senseUnitAtLocation(bestLocation);
					unfinishedBuildingsLocations.add(bestLocation);
					myUnfinishedBuildings.add(fac.id());
					System.out.println(
							"unfinishedBuildings : " + myUnfinishedBuildings);
				}
			}
		}
	}

	private boolean isFree(MapLocation loc, PlanetMap map) {
		if (map.onMap(loc) && map.isPassableTerrainAt(loc) == 1) {
			if (!ws.gc.hasUnitAtLocation(loc)) {
				return true;
			}
		}
		return false;
	}

	private boolean isSafe(MapLocation loc) {
		for (MapLocation construction : unfinishedBuildingsLocations) {
			if (loc.isAdjacentTo(construction)) {
				return false;
			}
		}
		for (MapLocation finished : finishedStructureLocations) {
			if (loc.isAdjacentTo(finished)) {
				return false;
			}
		}
		return true;
	}

	private void update() {
		myUnfinishedBuildings.clear();
		myFinishedFactories.clear();
		unfinishedBuildingsLocations.clear();
		finishedStructureLocations.clear();
		Set<Integer> myFactories = ws.unitLists.myUnits.get(UnitType.Factory);
		for (int facId : myFactories) {
			Unit factory = ws.gc.unit(facId);
			if (factory.structureIsBuilt() == 0) {
				myUnfinishedBuildings.add(facId);
				unfinishedBuildingsLocations
						.add(factory.location().mapLocation());
			} else {
				myFinishedFactories.add(facId);
				finishedStructureLocations
						.add(factory.location().mapLocation());
			}
		}
		Set<Integer> myRockets = ws.unitLists.myUnits.get(UnitType.Rocket);
		for (int rocId : myRockets) {
			Unit rocket = ws.gc.unit(rocId);
			if (rocket.structureIsBuilt() == 0) {
				myUnfinishedBuildings.add(rocId);
				unfinishedBuildingsLocations
						.add(rocket.location().mapLocation());
			} else {
				myFinishedRockets.add(rocId);
				finishedStructureLocations.add(rocket.location().mapLocation());
			}
		}

	}

}
