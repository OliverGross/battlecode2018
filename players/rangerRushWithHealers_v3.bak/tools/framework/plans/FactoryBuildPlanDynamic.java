package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Unit;
import bc.UnitType;
import bc.VecMapLocation;
import bc.VecUnit;
import bc.bc;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class FactoryBuildPlanDynamic extends Plan {

	private Set<Integer> myUnfinishedFactories = new HashSet<>();
	private Set<Integer> myFinishedFactories = new HashSet<>();
	private List<MapLocation> unfinishedFactoryLocations = new ArrayList<>();

	public FactoryBuildPlanDynamic(WorldState worldState,
			ArrayList<Integer> closestWorker) {
		super(worldState);
		for (int id : closestWorker) {
			ws.resources.assignById(id, pid);
		}
	}

	@Override
	protected void tick(WorldState worldState) {
		update();
		ArrayList<Integer> workersAssigned = ws.resources
				.getUnitsByPlanAndType(pid, UnitType.Worker);

		System.out.println(
				"workersAssignedToFactoryBuilding: " + workersAssigned);
		// get workers
		if (workersAssigned.size() < 2) {
			ws.resources.assignSomeFreeByType(pid, UnitType.Worker,
					2 - workersAssigned.size());
			workersAssigned = ws.resources.getUnitsByPlanAndType(pid,
					UnitType.Worker);
			if (workersAssigned.isEmpty()) {
				// get some fucking workers from another plan, we dont care
				// TODO make this smarter
				Set<Integer> workers = ws.unitLists.myWorkers;
				for (int id : workers) {
					ws.resources.freeUnitById(id);
				}
				workersAssigned.addAll(
						ws.resources.getFreeUnitsByType(UnitType.Worker));
				ws.resources.assignAllFreeByType(pid, UnitType.Worker);
			}
		}

		for (int workerId : workersAssigned) {
			Unit worker = ws.gc.unit(workerId);
			if (!worker.location().isOnMap())
				continue;

			if (!myUnfinishedFactories.isEmpty()) {
				// run to next unfinishedFactory
				ws.lll.goTo(workerId, unfinishedFactoryLocations);
				if (worker.workerHasActed() == 0) {
					// get all nearby factories
					VecUnit facsNearby = ws.gc.senseNearbyUnitsByType(
							worker.location().mapLocation(), 2,
							UnitType.Factory);
					Unit bestFactory = null;

					for (int i = 0; i < facsNearby.size(); i++) {
						Unit factory = facsNearby.get(i);
						if (factory.team() != ws.me
								|| factory.structureIsBuilt() == 1)
							continue;

						if (bestFactory == null) {
							bestFactory = factory;
						} else {
							if (bestFactory.health() < factory.health()) {
								bestFactory = factory;
							}
						}
					}
					if (bestFactory != null) {
						if (ws.gc.canBuild(worker.id(), bestFactory.id())) {
							ws.gc.build(worker.id(), bestFactory.id());
							if (bestFactory.structureIsBuilt() == 1) {
								unfinishedFactoryLocations.remove(
										bestFactory.location().mapLocation());
								myUnfinishedFactories.remove(bestFactory.id());
								myFinishedFactories.add(bestFactory.id());
							}
						}
					}
				}

			} else {
				if (((myFinishedFactories.size()
						+ myUnfinishedFactories.size()) < 3)
						|| ws.gc.karbonite() > 150) {
					int cost = (int) bc.costOf(UnitType.Factory,
							ws.gc.researchInfo().getLevel(UnitType.Factory));
					if (ws.resources.getAvailableKarbonite(pid) > cost) {
						buildFactory(worker);
					} else {
						ws.resources.saveKarbonite(cost, pid);
					}
				} else {
					// run around with unit to the most beneficial areas
					VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
							worker.location().mapLocation(), 50, ws.opp);
					kiteEnemies(enemies, worker, ws);
					if (ws.gc.isMoveReady(worker.id())) {
						// run to the most "free" point in the area
						VecMapLocation locationsAround = ws.gc
								.allLocationsWithin(
										worker.location().mapLocation(), 2);
						PlanetMap map = ws.gc.startingMap(ws.gc.planet());
						int bestFree = 0;
						MapLocation bestLoc = worker.location().mapLocation();
						ArrayList<MapLocation> adjacentFree = new ArrayList<>();
						for (int i = 0; i < locationsAround.size(); i++) {
							if (isFree(locationsAround.get(i), map)) {
								bestFree++;
								adjacentFree.add(locationsAround.get(i));
							}
							if (worker.workerHasActed() == 0 && ws.gc
									.karboniteAt(locationsAround.get(i)) > 0) {
								if (ws.gc.canHarvest(worker.id(), worker
										.location().mapLocation()
										.directionTo(locationsAround.get(i)))) {
									ws.gc.harvest(worker.id(), worker.location()
											.mapLocation().directionTo(
													locationsAround.get(i)));
								}
							}
						}
						for (MapLocation currLoc : adjacentFree) {
							int free = numberOfFreeAdjacent(currLoc, map,
									worldState);
							if (free > bestFree) {
								bestLoc = currLoc;
							}
						}
						if (ws.gc.canMove(worker.id(), worker.location()
								.mapLocation().directionTo(bestLoc))) {
							ws.gc.moveRobot(worker.id(), worker.location()
									.mapLocation().directionTo(bestLoc));
						}
					}
				}
			}
		}
	}

	private int numberOfFreeAdjacent(MapLocation loc, PlanetMap map,
			WorldState ws) {
		int ret = 0;
		for (Direction d : DirHelp.adjacentDirections) {
			MapLocation curr = loc.add(d);
			if (isFree(curr, map)) {
				ret++;
			}
		}
		return ret;
	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

	private void buildFactory(Unit worker) {
		Set<MapLocation> possibleLocations = new HashSet<>();
		MapLocation workerLoc = worker.location().mapLocation();
		PlanetMap map = ws.gc.startingMap(ws.gc.planet());
		VecMapLocation poss = null;
		// if worker can move, concider further tiles
		if (ws.gc.isMoveReady(worker.id())) {
			poss = ws.gc.allLocationsWithin(workerLoc, 2);
		} else {
			poss = ws.gc.allLocationsWithin(workerLoc, 4);
		}
		// check all tiles
		for (int i = 0; i < poss.size(); i++) {
			MapLocation curr = poss.get(i);
			if (isFree(curr, map)) {
				possibleLocations.add(curr);
			}
		}

		// decide for the best location
		MapLocation bestLocation = null;
		int bestFreeTiles = 0;
		for (MapLocation currentLocation : possibleLocations) {
			if (bestLocation == null) {
				int freeTiles = 0;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				bestLocation = currentLocation;
				bestFreeTiles = freeTiles;
			} else {
				int freeTiles = 0;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				if (freeTiles > bestFreeTiles) {
					bestLocation = currentLocation;
					bestFreeTiles = freeTiles;
				}
			}
		}
		if (bestLocation != null) {
			long distance = bestLocation.distanceSquaredTo(workerLoc);
			// move should be ready here, so move
			if (distance > 2) {
				ws.lll.goTo(worker.id(), bestLocation);
			}
			// get the direction from the now-new workerlocation
			Direction dir = worker.location().mapLocation()
					.directionTo(bestLocation);
			int cost = (int) bc.costOf(UnitType.Factory,
					ws.gc.researchInfo().getLevel(UnitType.Factory));
			if (ws.gc.canBlueprint(worker.id(), UnitType.Factory, dir)) {
				ws.gc.blueprint(worker.id(), UnitType.Factory, dir);
				Unit fac = ws.gc.senseUnitAtLocation(bestLocation);
				unfinishedFactoryLocations.add(bestLocation);
				myUnfinishedFactories.add(fac.id());
			}
		}
	}

	private boolean isFree(MapLocation loc, PlanetMap map) {
		if (map.onMap(loc) && map.isPassableTerrainAt(loc) == 1) {
			if (!ws.gc.hasUnitAtLocation(loc)) {
				return true;
			}
		}
		return false;
	}

	private void update() {
		myUnfinishedFactories.clear();
		myFinishedFactories.clear();
		unfinishedFactoryLocations.clear();
		Set<Integer> myFactories = ws.unitLists.myUnits.get(UnitType.Factory);
		for (int facId : myFactories) {
			Unit factory = ws.gc.unit(facId);
			if (factory.structureIsBuilt() == 0) {
				myUnfinishedFactories.add(facId);
			} else {
				myFinishedFactories.add(facId);
			}
		}

		for (int unfinishedId : myUnfinishedFactories) {
			Unit fac = ws.gc.unit(unfinishedId);
			unfinishedFactoryLocations.add(fac.location().mapLocation());
		}
	}

}
