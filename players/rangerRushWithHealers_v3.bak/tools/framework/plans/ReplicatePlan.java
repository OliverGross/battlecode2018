package tools.framework.plans;

import bc.Direction;
import bc.Unit;
import bc.UnitType;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class ReplicatePlan extends Plan {
	int workN;
	int wid;
	WorldState ws;
	
	public ReplicatePlan(WorldState worldState, int worker_id, int replicateNumber) {
		super (worldState);
		wid = worker_id;
		workN = replicateNumber;
		ws = worldState;
		//if (ws.gc.unit(wid).unitType() == UnitType.Worker)
		//ws.resources.assignById(wid, pid);
	}
	
	public int replicate(WorldState ws, int worker_id) {
		if(ws.gc.karbonite() >= bc.bc.bcUnitTypeReplicateCost(UnitType.Worker)) {
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canReplicate(worker_id, d)) {
					ws.gc.replicate(worker_id, d);
					Unit newWorker = ws.gc.senseUnitAtLocation(
							ws.gc.unit(worker_id).location().mapLocation().add(d));
					if (newWorker != null) {
						//ws.resources.assignByUnit(newWorker, pid);
					} else {
						return -1;
					}
					return newWorker.id();
				}
			}
		}
		return -1;
	}
	
	@Override
	public void tick(WorldState ws) {
		if (workN > 0) {
			int newWorkerId = replicate(ws, wid);
			//System.out.println("New Worker" + newWorkerId);
			if (newWorkerId > 0) {
				workN -= 1;
				//System.out.print("I replicated, and I still have " + workN + "replications left");
				//ws.resources.freeUnitById(wid);
				wid = newWorkerId;
				if (workN == 0) {
//					ws.resources.freeUnitById(wid);
//					ws.resources.freeUnitById(wid);
					this.status = Status.FINAL;
				}
			}
		} else {
			//ws.resources.freeUnitById(wid);
			this.status = Status.FINAL;
		}
	}
	
	

}
