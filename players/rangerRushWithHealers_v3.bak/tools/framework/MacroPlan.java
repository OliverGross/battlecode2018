package tools.framework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A MacroPlan is consisting of multiple plans, comparing these plans and
 * executing few of them every round according to their priority. The simplest
 * idea is, to have one MacroPlan being effectively the agent for the game,
 * every round creating new plans, modifying existing plans and executing the
 * most profitable ones. Of course one might want to add complexity by having a
 * sub-plan also being a MacroPlan evaluating again multiple plans e.g. the
 * agent decides if farming or attacking is good, but then again attacking is
 * complicated and one has to decide on attack formations and army compositions
 * that work well against the enemy force.
 */
public abstract class MacroPlan extends Plan {
	public List<Plan> plans;

	public MacroPlan(WorldState worldState) {
		super(worldState);
		this.plans = new ArrayList<Plan>();
	}

	/**
	 * This should run over the list of all plans and compute the new/updated
	 * priority for every plan, taking the worldState, the other plans, and the
	 * usage of resources of the plans into account. Ignore failed and final
	 * plans!
	 * 
	 * @param worldState
	 */
	public abstract void computePriorities(WorldState worldState);

	/**
	 * This method should modify the plan list by actually creating new plans
	 * according to the worldState but also maybe slightly adapting the already
	 * running plans (of course their implementation has to allow it). This step
	 * is quite important as it creates the plans in an efficient manner while
	 * making sure only to create and compare realistic plans. It should not
	 * produce much more plans than there will be executed and make sure that
	 * rubbish plans are not taken into account.
	 * 
	 * @param worldState
	 */
	public abstract void modifyPlans(WorldState worldState);

	/**
	 * Implements the execution of a MacroPlan. It simply consists of the
	 * following steps:
	 * <ol>
	 * <li>create new plans/modify existing ones</li>
	 * <li>compute the new/updated priority of all plans</li>
	 * <li>sort the plans according to their priority</li>
	 * <li>go through the sorted list of plans and execute them. They will have
	 * to take care to log the used resources in the worldState object.</li>
	 * <li>update the status of the MacroPlan based on all the sub-plans</li>
	 * </ol>
	 */
	
	/**
	 * Check if a certain planClass is already listed
	 * @param planClass
	 * @return
	 */
	 protected boolean containsPlanClass(Class<?> planClass) {
		for(Plan existingPlans: plans){
			if(existingPlans.getClass() == planClass)
				return true;
		}
		return false;
	}
	@Override
	protected final void tick(WorldState worldState) {
		modifyPlans(worldState);
		computePriorities(worldState);
		plans.sort(
				(Plan arg0, Plan arg1) -> (arg0.priority > arg1.priority) ? -1
						: (arg0.priority < arg1.priority) ? 1 : 0);
		Plan plan = null;
		for (Iterator<Plan> it = plans.iterator(); it.hasNext();) {
			plan = it.next();
			plan.safeTick(worldState);
			status = Status.combine(status, plan.status);
			if (plan.status.isTerminal()) {
				worldState.resources.freeUnitsFromPlan(plan.pid);
				it.remove();
			}
		}
	}
}
