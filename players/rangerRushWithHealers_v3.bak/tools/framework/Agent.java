package tools.framework;

import tools.pathfinding.JPS;
import tools.pathfinding.LocalPathFinder;
import tools.pathfinding.Pathfinding;

/**
 * An example for a complete agent. In every tick just retrieve all information
 * you can get from the simulation about the current worldState, then execute
 * the next tick of the AgentPlan.
 */
public class Agent {
	Plan masterplan;
	WorldState worldState;

	public Agent(WorldState worldState, Plan masterplan) {
		this.worldState = worldState;
		this.masterplan = masterplan;
	}

	public void execute() {
		try {
			while (!masterplan.status.isTerminal()) {
				tick();
				if ((worldState.gc.round() % 50) == 0) {
					System.out.println("================");
					System.out.println("ROUND:\t" + worldState.gc.round());
					System.out.println(
							"Time left(ms):\t" + worldState.gc.getTimeLeftMs());
					System.out.println("Time used(ms):\t"
							+ (10000 + 50 * worldState.gc.round()
									- worldState.gc.getTimeLeftMs()));
					System.out.println("Time pathfinding(ms):\t"
							+ Pathfinding.computationTimeNs * 1e-6);
					System.out.println("Unsuccessfull pathfinding:\t"
							+ JPS.numberUnsuccessfullPathfindings + "/"
							+ JPS.totalCalls);
					System.out.println("Used cached paths:\t"
							+ LocalPathFinder.numberSuccessfullUsedCaches);
					System.out.println("Cachings failed (starts):\t"
							+ LocalPathFinder.numberStartsTooDifferent);
					System.out.println("Cachings failed (goals):\t"
							+ LocalPathFinder.numberGoalsTooDifferent);
					System.out.println("Cachings failed (movement):\t"
							+ LocalPathFinder.numberUnitLeftPath);
					System.out.println("================");
				}
			}
			System.out.println(masterplan.getClass()
					+ ":\tMasterplan finished with status " + masterplan.status
					+ "! Now just playing along...");
		} catch (Exception e) {
			System.out.println(
					"Something threw an exception which was not a plan:");
			e.printStackTrace();
			System.out.println("Now just playing along ...");
			while (true)
				worldState.nextTurn();
		}
	}

	public void tick() {
		masterplan.safeTick(worldState);
		// System.out.println("Total path finding computation time for "
		// + JPS.totalCalls + " calls: " + JPS.compTime * 1e-6 + "ms");
		worldState.nextTurn();
	}
}
