import tools.framework.Agent;
import tools.framework.Plan;
import tools.framework.WorldState;

public class V1 {
	public static void main(String[] args) {
		WorldState worldstate = new WorldState();
		Plan masterplan = new V1masterplan(worldstate);
		Agent agent = new Agent(worldstate, masterplan);
		agent.execute();
	}
}