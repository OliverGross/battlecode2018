import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.evaluators.PlanEvaluator;

public class V1masterplan extends EvaluatorPlan {

	public V1masterplan(WorldState worldState) {
		super(worldState);

		//Oliver wants to put his code here
		
		addEvaluator(new PlanEvaluator(1.0));
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		
	}

}
