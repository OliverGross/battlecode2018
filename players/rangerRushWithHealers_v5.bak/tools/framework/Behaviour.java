package tools.framework;

public abstract class Behaviour {
	public abstract void act(WorldState ws);
}
