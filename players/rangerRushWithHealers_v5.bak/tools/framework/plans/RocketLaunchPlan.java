package tools.framework.plans;

import java.util.ArrayList;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.WorldState;

public class RocketLaunchPlan extends Plan {

	public RocketLaunchPlan(WorldState worldState) {
		super(worldState);
	}

	@Override
	protected void tick(WorldState worldState) {
		ArrayList<Unit> launchList = new ArrayList<>();
		
		if(ws.gc.round() == ws.launchTime) {
			for(int rocketId : ws.unitLists.myUnits.get(UnitType.Rocket)) {
				Unit rocket = ws.gc.unit(rocketId);
				
				if(rocket != null && rocket.location().isOnMap() && rocket.rocketIsUsed()==0) {
					loadInNearUnits(rocket);
					launchList.add(rocket);
				}
			}
		}
		
		for(Unit u : launchList) {
			launch(u);
		}
	}
	
	private void loadInNearUnits(Unit rocket) {
		//currently workers are left on earth until launchday
		long placesLeft = rocket.structureMaxCapacity()
				- rocket.structureGarrison().size();
		if (placesLeft == 0)
			return;
		
		VecUnit unitsAdjacent = ws.gc.senseNearbyUnitsByTeam(rocket.location().mapLocation(), 2, ws.me);
		for(int i=0; i<unitsAdjacent.size(); i++) {
			Unit unit = unitsAdjacent.get(i);
			if(ws.gc.isMoveReady(unit.id()) && (unit.unitType() != UnitType.Worker || ws.gc.round()== ws.launchTime)) {
				ws.resources.freeUnitById(unit.id());
				ws.resources.assignById(unit.id(), pid);
				if(ws.gc.canLoad(rocket.id(), unit.id())) {
					ws.gc.load(rocket.id(), unit.id());
					placesLeft--;
				}
				//if(placesLeft <= 0) return;
			}
		}
	}
	
	private void launch(Unit rocket) {
		MapLocation landingLocation = ws.resources.getNextRocketLocation();
		if(landingLocation != null) {
			if(ws.gc.canLaunchRocket(rocket.id(), landingLocation)) {
				ws.gc.launchRocket(rocket.id(), landingLocation);
			}
		}
	}

	
	
}
