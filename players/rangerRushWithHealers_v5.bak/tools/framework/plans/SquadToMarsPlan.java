package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class SquadToMarsPlan extends Plan {
	
	public SquadToMarsPlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void tick(WorldState ws) {
		Set<Integer> myRockets = ws.unitLists.myUnits.get(UnitType.Rocket);
		
		//EarlyLaunch
		ArrayList<Unit> launchList = new ArrayList<>();
		for(int rocketId : myRockets) {
			Unit rocket = ws.gc.unit(rocketId);
			if(!rocket.location().isOnMap() || rocket.rocketIsUsed()==1 || rocket.structureIsBuilt()==0) continue;
			//rocket dropping low emergency
			if(rocket.health() < 121) {
				readyForTakeOff(rocket);
				launchList.add(rocket);
			}
		}
		for(Unit u : launchList) {
			launch(u);
		}
		
		if(ws.gc.round() < ws.evacutateTime) return;
		
		ArrayList<MapLocation> rocketLocations = new ArrayList<>();
		
		//first load in nearby units bc thats easy, also get all non-full rocketlocations
		for(int rocketId : myRockets) {
			Unit rocket = ws.gc.unit(rocketId);
			if(!rocket.location().isOnMap() || rocket.rocketIsUsed()==1 || rocket.structureIsBuilt()==0) continue;
			
			loadInNearUnits(rocket);
			if(rocket.structureGarrison().size() < rocket.structureMaxCapacity()) {
				rocketLocations.add(rocket.location().mapLocation());
			}
		}
		
		//move every unit to the locations
		loadInUnits(rocketLocations);
		
		
		launchList.clear();
		for(int rocketId : myRockets) {
			Unit rocket = ws.gc.unit(rocketId);
			if(!rocket.location().isOnMap() || rocket.rocketIsUsed()==1 || rocket.structureIsBuilt()==0) continue;
			
			//rocket dropping low emergency
			if(rocket.health() < 121) {
				readyForTakeOff(rocket);
				launchList.add(rocket);
			}
			
			//launchday, start all rockets, bye bye who was too late
			if(ws.gc.round() == ws.launchTime) {
				launchList.add(rocket);
			}
			
			if(ws.gc.round() < ws.launchTime) break;
			if(rocket.structureGarrison().size() == rocket.structureMaxCapacity()) {
				readyForTakeOff(rocket);
				launchList.add(rocket);
			}

		}
		
		for(Unit u : launchList) {
			launch(u);
		}
		
	}

	private void loadInNearUnits(Unit rocket) {
		//currently workers are left on earth until launchday
		long placesLeft = rocket.structureMaxCapacity()
				- rocket.structureGarrison().size();
		if (placesLeft == 0)
			return;
		
		VecUnit unitsAdjacent = ws.gc.senseNearbyUnitsByTeam(rocket.location().mapLocation(), 2, ws.me);
		for(int i=0; i<unitsAdjacent.size(); i++) {
			Unit unit = unitsAdjacent.get(i);
			if(ws.gc.isMoveReady(unit.id()) && (unit.unitType() != UnitType.Worker || ws.gc.round()== ws.launchTime)) {
				ws.resources.freeUnitById(unit.id());
				ws.resources.assignById(unit.id(), pid);
				if(ws.gc.canLoad(rocket.id(), unit.id())) {
					ws.gc.load(rocket.id(), unit.id());
					placesLeft--;
				}
				//if(placesLeft <= 0) return;
			}
		}
	}

	private void launch(Unit rocket) {
		MapLocation landingLocation = ws.resources.getNextRocketLocation();
		if(landingLocation != null) {
			if(ws.gc.canLaunchRocket(rocket.id(), landingLocation)) {
				ws.gc.launchRocket(rocket.id(), landingLocation);
			}
		}
	}

	private void readyForTakeOff(Unit rocket) {
		// this is abit reckless, units will be missed R.I.P
		MapLocation rocketLocation = rocket.location().mapLocation();
		ArrayList<Unit> standInWayUnits = new ArrayList<>();
		for (Direction d : DirHelp.adjacentDirections) {
			MapLocation curr = rocketLocation.add(d);
			if (ws.gc.hasUnitAtLocation(curr)) {
				Unit standInWay = ws.gc.senseUnitAtLocation(curr);
				ws.lll.kite(standInWay.id(), standInWay.location().mapLocation()
						.directionTo(rocketLocation));
			}
		}
	}
	
	private void loadInUnits(ArrayList<MapLocation> rocketLocations) {
		Set<Integer> myRobots = ws.unitLists.myRobots;
		for(int id : myRobots) {
			if(ws.gc.unit(id).location().isOnMap()) {
				ws.resources.freeUnitById(id);
				ws.resources.assignById(id, pid);
				ws.lll.goTo(id, rocketLocations);
			}
		}
	}

	//not currently used
	private void loadInUnits(Unit rocket) {
		//currently workers are left on earth until launchday
		long placesLeft = rocket.structureMaxCapacity()
				- rocket.structureGarrison().size();
		if (placesLeft == 0)
			return;
		
		VecUnit unitsAdjacent = ws.gc.senseNearbyUnitsByTeam(rocket.location().mapLocation(), 2, ws.me);
		for(int i=0; i<unitsAdjacent.size(); i++) {
			Unit unit = unitsAdjacent.get(i);
			if(ws.gc.isMoveReady(unit.id()) && (unit.unitType() != UnitType.Worker || ws.gc.round()== ws.launchTime)) {
				ws.resources.freeUnitById(unit.id());
				ws.resources.assignById(unit.id(), pid);
				if(ws.gc.canLoad(rocket.id(), unit.id())) {
					ws.gc.load(rocket.id(), unit.id());
					placesLeft--;
				}
				//if(placesLeft <= 0) return;
			}
		}
		
		VecUnit unitsNearby = ws.gc.senseNearbyUnitsByTeam(
				rocket.location().mapLocation(), 500, ws.me);
		for (int i = 0; i < unitsNearby.size(); i++) {
			Unit unit = unitsNearby.get(i);
			if (ws.gc.isMoveReady(unit.id()) && (unit.unitType() != UnitType.Worker || ws.gc.round()== ws.launchTime)) {
				//we only move, so we dont assign. We want to shoot while retreating
				//ws.resources.freeUnitById(unit.id());
				//ws.resources.assignById(unit.id(), pid);
				MapLocation unitLocation = unit.location().mapLocation();
				long distance = unitLocation
						.distanceSquaredTo(rocket.location().mapLocation());
				if (distance <= 2) {
					if (ws.gc.canLoad(rocket.id(), unit.id())) {
						ws.gc.load(rocket.id(), unit.id());
					}
				} else {
					ws.lll.goTo(unit.id(), rocket.location().mapLocation());
				}
				placesLeft--;
			}
			//if (placesLeft == 0)
			//	return;
		}
	}

}
