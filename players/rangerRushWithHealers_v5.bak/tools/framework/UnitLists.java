package tools.framework;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import bc.GameController;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;

public class UnitLists {
	private int runningInstances = 0;
	private Team me;

	// this is handled in update() to find out which units are new and which
	// were lost (by vision or death)
	public Set<Integer> units = new HashSet<Integer>();
	public Map<Integer, Unit> lastVisibleUnitState = new HashMap<Integer, Unit>();

	// the following sets get all handled by addNewUnit and addLostUnit to
	// reduce the operations to a minimum
	public Set<Integer> newUnits = new HashSet<Integer>();
	public Set<Integer> lostUnits = new HashSet<Integer>();
	public Map<UnitType, Set<Integer>> myUnits;
	public Set<Integer> myDiedUnits = new HashSet<Integer>();
	public Set<Integer> myWorkers = new HashSet<Integer>();
	public Set<Integer> myRobots = new HashSet<Integer>();
	public Set<Integer> myNewRobots = new HashSet<Integer>();
	public Set<Integer> myDiedRobots = new HashSet<Integer>();
	public Set<Integer> enemyUnits = new HashSet<Integer>();
	public Set<Integer> enemyLostUnits = new HashSet<Integer>();
	public Set<Integer> enemyMages = new HashSet<Integer>();
	public Set<Integer> enemyKnights = new HashSet<Integer>();
	public Set<Integer> enemyHealers = new HashSet<Integer>();
	public Set<Integer> enemyRangers = new HashSet<Integer>();
	public Set<Integer> enemyFactories = new HashSet<Integer>();
	public Set<Integer> enemyRockets = new HashSet<Integer>();
	public Set<Integer> enemyWorkers = new HashSet<Integer>();
	public Set<Integer> enemyRobots = new HashSet<Integer>();
	public Set<Integer> enemyNewRobots = new HashSet<Integer>();
	public Set<Integer> enemyLostRobots = new HashSet<Integer>();
	public Set<Integer> structures = new HashSet<Integer>();
	public Set<Integer> newStructures = new HashSet<Integer>();
	public Set<Integer> lostStructures = new HashSet<Integer>();
	public Set<Integer> robots = new HashSet<Integer>();
	public Map<UnitType, Integer> currentlyProducing = new HashMap<UnitType, Integer>();

	public UnitLists(Team me) {
		if (runningInstances >= 1)
			throw new RuntimeException(
					"There should only be a single UnitLists instance!");
		runningInstances++;
		this.me = me;

		myUnits = new HashMap<UnitType, Set<Integer>>();
		for (UnitType ut : UnitType.values()) {
			myUnits.put(ut, new HashSet<Integer>());
			currentlyProducing.put(ut, 0);
		}
	}

	public void update(GameController gc) {
		VecUnit currentUnits = gc.units();
		newUnits.clear();
		lostUnits.clear();
		myNewRobots.clear();
		myDiedRobots.clear();
		enemyNewRobots.clear();
		enemyLostRobots.clear();
		newStructures.clear();
		lostStructures.clear();
		for (UnitType ut : UnitType.values())
			currentlyProducing.put(ut, 0);
		Set<Integer> curUnits = new HashSet<Integer>();
		for (int i = 0; i < currentUnits.size(); i++) {
			Unit unit = currentUnits.get(i);
			updateCurrentlyProducing(unit);
			lastVisibleUnitState.put(unit.id(), unit);
			curUnits.add(unit.id());
			if (!units.contains(unit.id()))
				addNewUnit(unit);
			else
				units.remove(unit.id());
		}
		for (int uid : units)
			addLostUnit(lastVisibleUnitState.get(uid));
		units = curUnits;
	}

	public void updateCurrentlyProducing(Unit unit) {
		if (unit.team() == me && unit.unitType() == UnitType.Factory
				&& unit.structureIsBuilt() == 1
				&& unit.isFactoryProducing() == 1) {
			int producing = currentlyProducing.get(unit.factoryUnitType());
			currentlyProducing.put(unit.factoryUnitType(), producing + 1);
		}
	}

	public void addNewUnit(Unit unit) {
		addUnit(unit, true);
	}

	public void addLostUnit(Unit unit) {
		addUnit(unit, false);
	}

	// the opposite of isNew is gotLost!
	public void addUnit(Unit unit, boolean isNew) {
		modifySet(newUnits, unit.id(), isNew);
		modifySet(lostUnits, unit.id(), !isNew);
		// filter by team
		if (unit.team() == me) {
			modifyMap(myUnits, unit, isNew);
			modifySet(myDiedUnits, unit.id(), !isNew);
			if (isStructure(unit.unitType())) {
			} else {
				modifySet(myRobots, unit.id(), isNew);
				modifySet(myNewRobots, unit.id(), isNew);
				modifySet(myDiedRobots, unit.id(), !isNew);
			}
			switch (unit.unitType()) {
			case Worker:
				modifySet(myWorkers, unit.id(), isNew);
				break;
			default:
				break;
			}
		} else {
			modifySet(enemyLostUnits, unit.id(), !isNew);
			modifySet(enemyUnits, unit.id(), isNew);
			if (isStructure(unit.unitType())) {
			} else {
				modifySet(enemyRobots, unit.id(), isNew);
				modifySet(enemyNewRobots, unit.id(), isNew);
				modifySet(enemyLostRobots, unit.id(), !isNew);
			}
			switch (unit.unitType()) {
			case Mage:
				modifySet(enemyMages, unit.id(), isNew);
				break;
			case Knight:
				modifySet(enemyKnights, unit.id(), isNew);
				break;
			case Ranger:
				modifySet(enemyRangers, unit.id(), isNew);
				break;
			case Healer:
				modifySet(enemyHealers, unit.id(), isNew);
				break;
			case Worker:
				modifySet(enemyWorkers, unit.id(), isNew);
				break;
			case Factory:
				modifySet(enemyFactories, unit.id(), isNew);
				break;
			case Rocket:
				modifySet(enemyRockets, unit.id(), isNew);
				break;
			default:
				break;
			}
		}

		// filter by type
		if (isStructure(unit.unitType())) {
			modifySet(structures, unit.id(), isNew);
			modifySet(newStructures, unit.id(), isNew);
			modifySet(lostStructures, unit.id(), !isNew);
		} else {
			modifySet(robots, unit.id(), isNew);
		}
	}

	public static boolean modifySet(Set<Integer> set, int id, boolean add) {
		if (add)
			return set.add(id);
		else
			return set.remove(id);
	}

	public static boolean modifyMap(Map<UnitType, Set<Integer>> map, Unit unit,
			boolean add) {
		if (add)
			return map.get(unit.unitType()).add(unit.id());
		else
			return map.get(unit.unitType()).remove(unit.id());
	}

	public static boolean isStructure(UnitType ut) {
		if (ut == UnitType.Factory || ut == UnitType.Rocket)
			return true;
		return false;
	}

	public static boolean isRobot(UnitType ut) {
		return !isStructure(ut);
	}
}
