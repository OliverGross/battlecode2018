package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;

public class RangerFocusAttackBehaviour extends Behaviour {

	HashMap<Integer, Integer> finalTargetting = new HashMap<>();
	HashMap<Integer, Integer> myCurrentBestTargetting = new HashMap<>();
	HashMap<Integer, Integer> myBackupTargets = new HashMap<>();
	HashMap<Integer, TargetInfo> enemyToTargetInfo = new HashMap<>();
	
	HashMap<Integer, MapLocation> standOffTargets = new HashMap<>();
	HashMap<Integer, MoveInfo> moveInfos = new HashMap<>();
	
	@Override
	public void act(WorldState ws) {
		if(ws.unitLists.myUnits.get(UnitType.Ranger).isEmpty()) return;
		
		standOffResolution(ws);

		//clear AFTER standOffResolution, bc we need the points from last round.
		finalTargetting.clear();
		myCurrentBestTargetting.clear();
		myBackupTargets.clear();
		enemyToTargetInfo.clear();
		standOffTargets.clear();
		
		// currently: mages over healers over rangers over knights over rockets
		// over factories over workers
		targetUnit(ws.unitLists.enemyMages, ws);
		targetOnKillOrBackup(ws.unitLists.enemyHealers, ws);
		targetUnit(ws.unitLists.enemyRangers, ws);
		targetUnit(ws.unitLists.enemyKnights, ws);
		targetOnKillOrBackup(ws.unitLists.enemyRockets, ws);
		targetOnKillOrBackup(ws.unitLists.enemyFactories, ws);
		targetOnKillOrBackup(ws.unitLists.enemyWorkers, ws);

		// we do not iterate through finalTargetting at the moment, bc the
		// entries in currentBestTargetting are still there
		// do all the attacks
		for (Entry<Integer, Integer> entry : myCurrentBestTargetting.entrySet()) {
			if (ws.gc.canAttack(entry.getKey(), entry.getValue())) {
				standOffTargets.put(entry.getKey(),
						ws.gc.unit(entry.getValue()).location().mapLocation());
				ws.gc.attack(entry.getKey(), entry.getValue());
			} else {
				System.out.println("could not attack, this should not happen!");
			}
		}
		
		for(Entry<Integer, Integer> entry : myBackupTargets.entrySet()) {
			if(myCurrentBestTargetting.containsKey(entry.getKey())) continue;
			
			if (ws.gc.canAttack(entry.getKey(), entry.getValue())) {
				standOffTargets.put(entry.getKey(),
						ws.gc.unit(entry.getValue()).location().mapLocation());
				ws.gc.attack(entry.getKey(), entry.getValue());
			} else {
				System.out.println("could not attack, this should not happen!");
			}
		}
	}

	private void standOffResolution(WorldState ws) {
		for (int unitId : ws.unitLists.myUnits.get(UnitType.Ranger)) {
			if (standOffTargets.containsKey(unitId)) {
				standOff(ws.gc.unit(unitId), standOffTargets.get(unitId), ws);
			}
		}
	}

	private void standOff(Unit ranger, MapLocation target, WorldState ws) {
		if (ranger == null || target == null || !ranger.location().isOnMap())
			return;

		MoveInfo info = null;
		if (moveInfos.containsKey(ranger.id())) {
			info = moveInfos.get(ranger.id());
		} else {
			info = new MoveInfo(ranger);
			moveInfos.put(ranger.id(), info);
		}
		if (info != null) {
			if (info.standOff(ranger)) {
				Direction d = ws.pathfinding.getNextDirection(ranger.id(),
						target);
				MapLocation moveLocation = ranger.location().mapLocation()
						.add(d);
				if (moveLocation.distanceSquaredTo(target) > ranger
						.rangerCannotAttackRange()) {
					ws.lll.goTo(ranger.id(), target);
				} else {
					// distance too low
				}
			}
		}
	}
	
	private void targetOnKillOrBackup(Set<Integer> enemyUnits, WorldState ws) {
		targetEnemies(enemyUnits, ws, true);
	}
	
	private void targetUnit(Set<Integer> enemyUnits, WorldState ws) {
		targetEnemies(enemyUnits, ws, false);
	}

	private void targetEnemies(Set<Integer> enemyUnits, WorldState ws, boolean onlyBackupTarget) {
		ArrayList<Integer> currentAttackingUnits = new ArrayList<>();
		
		for (int unitId : enemyUnits) {
			currentAttackingUnits.clear();
			// for every enemy enemy:
			if (!ws.gc.canSenseUnit(unitId))
				continue;
			Unit enemy = ws.gc.unit(unitId);
			if (enemy == null || !enemy.location().isOnMap())
				continue;

			VecUnit myUnitsNear = ws.gc.senseNearbyUnitsByTeam(
					enemy.location().mapLocation(), 50, ws.me);
			// check all my units

			int currentDamage = 0;

			//for every ranger in attackrange of enemy
			for (int i = 0; i < myUnitsNear.size(); i++) {
				Unit u = myUnitsNear.get(i);
				if (u.unitType() == UnitType.Ranger) {
					if (u.attackHeat() < 10) {
						if (!finalTargetting.containsKey(u.id())) {
							// unit is a ranger, can attack and is not yet
							// finally assigned
							if (enemy.unitType() == UnitType.Knight) {
								currentDamage += u.damage()
										- enemy.knightDefense();
							} else {
								currentDamage += u.damage();
							}
							currentAttackingUnits.add(u.id());
						}
					}
				}
				if (currentDamage >= enemy.health()) {
					break;
				}
			}

			if (currentDamage >= enemy.health()) {
				for (int id : currentAttackingUnits) {
					finalTargetting.put(id, enemy.id());
					myCurrentBestTargetting.put(id, enemy.id());
				}
			} else {
				for (int id : currentAttackingUnits) {
					if(onlyBackupTarget) {
						if(!myBackupTargets.containsKey(id)) {
							myBackupTargets.put(id, enemy.id());
						}
					} else {
						myCurrentBestTargetting.put(id, enemy.id());
					}
				}
			}
		}
	}

}