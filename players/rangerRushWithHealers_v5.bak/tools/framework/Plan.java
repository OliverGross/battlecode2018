package tools.framework;

/**
 * A plan can be a fixed execution of actions for one unit, or a dynamic
 * attacking strategy against the enemy front covering movements and actions of
 * an whole army. A plan can be formulated without ever being executed, only
 * partially executed (because something more important came up) or fully
 * executed to the end. A plan can only take one tick to execute or be ongoing
 * for the entire game. The main idea is, that the agent itself is a single plan
 * for the whole game getting executed every round using all the resources
 * optimally.
 * <p>
 * The priorityValue is used to compare its value to other plans and allocated
 * resources optimally. The tick_counter describes the time-stamp for this plan
 * and should be kept up-to-date. The tick() method is the one which gets called
 * every round of the game and executes part of the plan.
 */
public abstract class Plan {
	public Status status;
	public WorldState ws;
	public double priority;
	public int pid;
	public int round;
	// static counter for pid
	static int planCounter = 0;
	private long lastTimeConsumptionNs = 0;

	public Plan(WorldState worldState) {
		status = Status.PENDING;
		ws = worldState;
		round = worldState.round - 1;
		pid = ++planCounter;
	}

	public final void safeTick(WorldState worldState) {
		// System.out.println(this.getClass() + " ticked!");
		if (status.isTerminal()) {
			throw new RuntimeException(
					"Don't let a function tick which is terminal! The plan "
							+ this.getClass()
							+ " tried to tick, but has status " + status + "!");
		}
		// TODO it would be nice if the following works, as we can than safely
		// assume, that tick got called every round exactly once
		// round++;
		// if (round != ws.round)
		// throw new RuntimeException("This plan " + this.getClass()
		// + " got out of sync with the world state! The worldState round is "
		// + ws.round + " but the plan round is " + round + "!");
		if (lastTimeConsumptionNs * 1.5 < ws.getTimeLeftNs()) {
			long t0 = System.nanoTime();
			try {
				this.tick(worldState);
			} catch (Exception e) {
				System.out.println(this.getClass()
						+ ":\tThis plan is going into status FAILED, because of the following:");
				e.printStackTrace();
				ws.allCatchedExceptions.add(e);
				status = Status.FAILED;
			}
			long t1 = System.nanoTime();
			lastTimeConsumptionNs = t1 - t0;
			if (ws.round % 50 == 0)
				System.out.println(this.getClass() + ":\tlast runtime[ms]:"
						+ lastTimeConsumptionNs * 1e-6);
		} else {
			System.out.println(this.getClass()
					+ ":\tTime was to low, plan did not tick! Left:"
					+ ws.getTimeLeftNs() * 1e-6 + "ms, maxTimeConsumption:"
					+ lastTimeConsumptionNs * 1e-6 + "ms");
		}
		if (status.isTerminal()) {
			worldState.resources.freeUnitsFromPlan(pid);
		}
	}

	protected abstract void tick(WorldState worldState);
}
