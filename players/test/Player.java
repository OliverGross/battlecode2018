import bc.Direction;
import bc.GameController;
import bc.Planet;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;

public class Player {

	public static GameController gc;
	public static int maxRounds = 101;

	public static void main(String[] args) {
		gc = new GameController();
		System.out.println(
				"Please let this bot play against itself on the 30x30 empty map!");
		if (gc.planet() == Planet.Mars) {
			while (true)
				gc.nextTurn();
		}
		if (gc.team() == Team.Blue)
			blue();
		else
			red();
	}

	public static void red() {
		while (true) {
			System.out.println("Round:" + gc.round());
			VecUnit units = gc.units();
			for (int i = 0; i < units.size(); i++) {
				Unit unit = gc.unit(units.get(i).id());
				System.out.println(toString(unit));
			}
			int worker = gc.myUnits().get(0).id();
			if (gc.round() % 2 == 0)
				if (gc.canMove(worker, Direction.Northeast))
					gc.moveRobot(worker, Direction.Northeast);
			units = gc.units();
			for (int i = 0; i < units.size(); i++) {
				Unit unit = gc.unit(units.get(i).id());
				System.out.println(toString(unit));
			}
			if (gc.round() <= maxRounds)
				gc.nextTurn();
			else
				break;
		}
	}

	public static String toString(Unit u) {
		if (u.location().isOnMap())
			return u.team() + "\t" + u.unitType() + "\t"
					+ u.location().mapLocation().getX() + "\t"
					+ u.location().mapLocation().getY();
		else
			return u.team() + "\t" + u.unitType() + "\t"
					+ gc.unit(u.location().structure()).unitType() + "\t"
					+ gc.unit(u.location().structure()).location().mapLocation()
							.getX()
					+ "\t" + gc.unit(u.location().structure()).location()
							.mapLocation().getY();
	}

	public static void blue() {
		while (true) {
			for (int i = 0; i < gc.units().size(); i++)
				System.out.println(toString(gc.unit(gc.units().get(i).id())));
			System.out.println("Round:" + gc.round());
			if (gc.round() < 50) {
				gc.nextTurn();
				continue;
			}
			int worker = gc.myUnits().get(0).id();
			switch ((int) gc.round()) {
			case 50:
				if (gc.canBlueprint(worker, UnitType.Factory, Direction.North))
					gc.blueprint(worker, UnitType.Factory, Direction.North);
				break;
			case 96:
			case 97:
			case 98:
			case 99:
			case 100:
				int factory = gc.units().get(1).id();
				if (gc.canProduceRobot(factory, UnitType.Mage))
					gc.produceRobot(factory, UnitType.Mage);
			}
			if (gc.round() >= 51 && gc.round() <= 95) {
				int blueprint = gc.units().get(1).id();
				System.out.println(gc.unit(blueprint).health());
				if (gc.canBuild(worker, blueprint))
					gc.build(worker, blueprint);
				System.out.println(gc.unit(blueprint).health());
			}
			for (int i = 0; i < gc.units().size(); i++)
				System.out.println(toString(gc.unit(gc.units().get(i).id())));
			if (gc.round() <= maxRounds)
				gc.nextTurn();
			else
				break;
		}
	}
}