package tools.framework.behaviours;

import java.util.ArrayList;

import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.HealerPlan;

public class HealerHealBehaviour extends Behaviour {

	HealerPlan healerPlan;

	public HealerHealBehaviour(HealerPlan healerPlan) {
		this.healerPlan = healerPlan;
	}

	@Override
	public void act(WorldState ws) {
		ArrayList<Integer> healers = healerPlan.getHealersOnMap();
		for (int healerId : healers) {
			if (ws.gc.isHealReady(healerId)) {
				Unit healer = ws.gc.unit(healerId);
				if (!healer.location().isOnMap())
					continue;

				VecUnit allysInRange = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), healer.attackRange(),
						ws.me);

				Unit target = getUnitToHeal(allysInRange);
				if (target != null) {
					if (ws.gc.canHeal(healerId, target.id())) {
						ws.gc.heal(healerId, target.id());
					}
				}
			}

		}

	}

	private Unit getUnitToHeal(VecUnit allysInRange) {
		Unit bestTarget = null;
		for (int i = 0; i < allysInRange.size(); i++) {
			Unit target = allysInRange.get(i);
			if (bestTarget == null) {
				if (target.health() != target.maxHealth()) {
					bestTarget = target;
				}
			} else {
				long missingHealth = target.maxHealth() - target.health();
				if (missingHealth > (bestTarget.maxHealth()
						- bestTarget.health())) {
					bestTarget = target;
				}
			}
		}
		return bestTarget;
	}

}
