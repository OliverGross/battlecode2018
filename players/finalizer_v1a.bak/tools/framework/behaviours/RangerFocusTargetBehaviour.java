package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;

public class RangerFocusTargetBehaviour extends Behaviour {

	HashMap<Integer, Integer> unitToTargetMapping = new HashMap<>();
	HashMap<Integer, TargetInfo> enemyToTargetInfoMapping = new HashMap<>();

	HashMap<Integer, MapLocation> standOffTargets = new HashMap<>();
	HashMap<Integer, MoveInfo> moveInfos = new HashMap<>();

	@Override
	public void act(WorldState ws) {
		if (ws.unitLists.myUnits.get(UnitType.Ranger).isEmpty())
			return;

		standOffResolution(ws);

		// clear AFTER standOffResolution, bc we need the points from last
		// round.
		unitToTargetMapping.clear();
		enemyToTargetInfoMapping.clear();
		standOffTargets.clear();

		// currently: mages over healers over rangers over knights over rockets
		// over factories over workers
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Mage), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Healer), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Ranger), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Knight), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Rocket), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Factory), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Worker), ws);

		// we do not iterate through finalTargetting at the moment, bc the
		// entries in currentBestTargetting are still there
		// do all the attacks
		for (Entry<Integer, Integer> entry : unitToTargetMapping.entrySet()) {
			if (ws.gc.canAttack(entry.getKey(), entry.getValue())) {
				standOffTargets.put(entry.getKey(),
						ws.gc.unit(entry.getValue()).location().mapLocation());
				ws.gc.attack(entry.getKey(), entry.getValue());
			} else {
				System.out.println("could not attack, this should not happen!");
			}
		}
	}

	private void standOffResolution(WorldState ws) {
		for (int unitId : ws.unitLists.myUnits.get(UnitType.Ranger)) {
			if (standOffTargets.containsKey(unitId)) {
				standOff(ws.gc.unit(unitId), standOffTargets.get(unitId), ws);
			}
		}
	}

	private void standOff(Unit ranger, MapLocation target, WorldState ws) {
		if (ranger == null || target == null || !ranger.location().isOnMap())
			return;

		MoveInfo info = null;
		if (moveInfos.containsKey(ranger.id())) {
			info = moveInfos.get(ranger.id());
		} else {
			info = new MoveInfo(ranger);
			moveInfos.put(ranger.id(), info);
		}
		if (info != null) {
			if (info.standOff(ranger)) {
				Direction d = ws.pathfinding.getNextDirection(ranger.id(),
						target);
				MapLocation moveLocation = ranger.location().mapLocation()
						.add(d);
				if (moveLocation.distanceSquaredTo(target) > ranger
						.rangerCannotAttackRange()) {
					ws.lll.goTo(ranger.id(), target);
				} else {
					// distance too low
				}
			}
		}
	}

	// The higher the value, the better to focus
	private int typeValue(UnitType type) {
		if (type == null) {
			System.out.println("typeValue was null");
			return 0;
		}

		switch (type) {
		case Factory:
			return 1;
		case Worker:
			return 2;
		case Rocket:
			return 3;
		case Knight:
			return 4;
		case Healer:
			return 5;
		case Ranger:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;

		}
	}

	private void targetEnemies(Set<Integer> enemyUnits, WorldState ws) {
		ArrayList<Integer> currentAttackingUnits = new ArrayList<>();

		for (int unitId : enemyUnits) {
			currentAttackingUnits.clear();
			// for every enemy :
			if (!ws.gc.canSenseUnit(unitId))
				continue;
			Unit enemy = ws.gc.unit(unitId);
			if (enemy == null || !enemy.location().isOnMap())
				continue;

			// ArrayList<Unit> myUnitsNearL = getRangersInRange(enemy, ws);
			VecUnit myUnitsNear = ws.gc.senseNearbyUnitsByTeam(
					enemy.location().mapLocation(), 50, ws.me);
			// check all my units

			int currentDamage = 0;

			// for every ranger in attackrange of enemy
			for (int i = 0; i < myUnitsNear.size(); i++) {
				Unit u = myUnitsNear.get(i);
				if (u.unitType() == UnitType.Ranger) {
					if (u.attackHeat() < 10) {
						TargetInfo currentTarget = null;
						if (unitToTargetMapping.containsKey(u.id())) {
							int currentTargetId = unitToTargetMapping
									.get(u.id());
							if (enemyToTargetInfoMapping
									.containsKey(currentTargetId)) {
								currentTarget = enemyToTargetInfoMapping
										.get(currentTargetId);
							} else {
								System.out.println(
										"unitToTargetmapping got something, but no infomapping?!");
							}
						}

						if (currentTarget != null) {
							if (evaluate(u, enemy, currentTarget)) {
								currentTarget.deleteAttacker(u);
								currentAttackingUnits.add(u.id());
								if (enemy.unitType() == UnitType.Knight) {
									currentDamage += u.damage()
											- enemy.knightDefense();
								} else {
									currentDamage += u.damage();
								}
							}
						} else {
							currentAttackingUnits.add(u.id());
							if (enemy.unitType() == UnitType.Knight) {
								currentDamage += u.damage()
										- enemy.knightDefense();
							} else {
								currentDamage += u.damage();
							}
						}
					}
				}
				if (currentDamage >= enemy.health()) {
					break;
				}
			}

			TargetInfo enemyInfo = new TargetInfo(currentAttackingUnits, enemy);
			enemyToTargetInfoMapping.put(enemy.id(), enemyInfo);

			for (int id : currentAttackingUnits) {
				unitToTargetMapping.put(id, enemy.id());
			}
		}
	}

	// this seems to be really slow
	private ArrayList<Unit> getRangersInRange(Unit enemy, WorldState ws) {
		ArrayList<Unit> ret = new ArrayList<>();
		for (int id : ws.unitLists.myUnits.get(UnitType.Ranger)) {
			Unit ranger = ws.gc.unit(id);
			if (ranger == null || !ranger.location().isOnMap())
				continue;

			long distance = enemy.location().mapLocation()
					.distanceSquaredTo(ranger.location().mapLocation());
			if (distance <= ranger.attackRange()) {
				ret.add(ranger);
			}
		}
		return ret;
	}

	private boolean evaluate(Unit u, Unit enemy, TargetInfo currentTarget) {
		if (currentTarget.isDead())
			return false;
		if (typeValue(enemy.unitType()) <= typeValue(currentTarget.type))
			return false;
		if (currentTarget.location != null) {
			long distance = u.location().mapLocation()
					.distanceSquaredTo(enemy.location().mapLocation());
			long distanceCurrentTarget = u.location().mapLocation()
					.distanceSquaredTo(currentTarget.location);
			if (distanceCurrentTarget <= distance)
				return false;
		}
		return true;
	}
}
