package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.KnightsAttackPlan;

public class KnightMoveBehaviour extends Behaviour {

	KnightsAttackPlan attackPlan;

	public KnightMoveBehaviour(WorldState worldState,
			KnightsAttackPlan knightsAttackPlan) {
		attackPlan = knightsAttackPlan;
	}

	ArrayList<MapLocation> enemyFactoriesLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyRangerLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyMagesLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyHealersLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyKnightsLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyWorkerLocation = new ArrayList<>();
	ArrayList<MapLocation> enemyRocketsLocation = new ArrayList<>();

	List<MapLocation> enemyLocations = new ArrayList<>();

	@Override
	public void act(WorldState ws) {

		enemyFactoriesLocation.clear();
		enemyMagesLocation.clear();
		enemyRangerLocation.clear();
		enemyHealersLocation.clear();
		enemyKnightsLocation.clear();
		enemyWorkerLocation.clear();
		enemyRocketsLocation.clear();

		Set<Integer> allEnemies = ws.unitLists.allEnemyUnits;
		System.out.println("allenemyunits " + allEnemies.size());
		for (int uid : allEnemies) {
			if (ws.gc.canSenseUnit(uid)) {
				Unit u = ws.gc.unit(uid);
				if (u.location().isOnMap()) {

					switch (u.unitType()) {
					case Factory:
						enemyFactoriesLocation.add(u.location().mapLocation());
						break;
					case Healer:
						enemyHealersLocation.add(u.location().mapLocation());
						break;
					case Knight:
						enemyKnightsLocation.add(u.location().mapLocation());
						break;
					case Mage:
						enemyMagesLocation.add(u.location().mapLocation());
						break;
					case Ranger:
						enemyRangerLocation.add(u.location().mapLocation());
						break;
					case Rocket:
						enemyRocketsLocation.add(u.location().mapLocation());
						break;
					case Worker:
						enemyWorkerLocation.add(u.location().mapLocation());
						break;
					default:
						break;
					}
					enemyLocations.add(u.location().mapLocation());
				}
			}
		}

		// no enemies in sight, get startlocations
		if (enemyLocations.size() == 0) {
			VecUnit startEnemies = ws.gc.startingMap(Planet.Earth)
					.getInitial_units();
			for (int i = 0; i < startEnemies.size(); i++) {
				if (startEnemies.get(i).team() == ws.opp) {
					enemyLocations
							.add(startEnemies.get(i).location().mapLocation());
				}
			}
			for(int knightId : attackPlan.getKnights()) {
				Unit knight = ws.gc.unit(knightId);
				if(!knight.location().isOnMap()) continue;
				
				if(ws.gc.isMoveReady(knightId)) {
					ws.lll.goTo(knightId, enemyLocations);
				}
			}
		}
		
		for(int knightId : attackPlan.getKnights()) {
			Unit knight = ws.gc.unit(knightId);
			if(!knight.location().isOnMap()) continue;
			
			actForOne(ws, knight);
		}
		
	}

	//For overcharge also
	public void actForOne(WorldState ws, Unit knight) {
		// first try to kill rangers/mages/factoreis taht are in range
		if (!knight.location().isOnMap())
			return;
		if(!ws.gc.isMoveReady(knight.id())) return;
		
		Unit bestTarget = moveToNear(ws, knight);
		
		VecUnit facs = ws.gc.senseNearbyUnitsByType(knight.location().mapLocation(), 2, UnitType.Factory);
		for(int i=0; i<facs.size(); i++) {
			Unit fac = facs.get(i);
			if(fac.team() == ws.opp) {
				return;
			}
		}

		if (bestTarget != null) {
			System.out.println("Knight move to near target");
			ws.lll.goTo(knight.id(), bestTarget.location().mapLocation());
		}
		if(!enemyFactoriesLocation.isEmpty()) {
			System.out.println("Knight move to factory");
			ws.lll.goTo(knight.id(), enemyFactoriesLocation);
		}
		System.out.println("knight, no factories around");
		if(!enemyRangerLocation.isEmpty()) {
			System.out.println("knight, move to ranger");
			ws.lll.goTo(knight.id(), enemyRangerLocation);
		}
		System.out.println("knight, no rangers around");
		
		if(!enemyMagesLocation.isEmpty()) {
			System.out.println("knight, move to mages");
			ws.lll.goTo(knight.id(), enemyMagesLocation);
		}
		System.out.println("knight, no mages around");
		if(!enemyHealersLocation.isEmpty()) {
			System.out.println("knight, move to healers");
			ws.lll.goTo(knight.id(), enemyHealersLocation);
		}
		System.out.println("knight, no healers around");
		if(!enemyLocations.isEmpty()) {
			System.out.println("knight, move to other units / startinglocation");
			ws.lll.goTo(knight.id(), enemyLocations);
		}
	}

	private Unit moveToNear(WorldState ws, Unit knight) {
		VecUnit nearbyUnits = ws.gc.senseNearbyUnitsByTeam(
				knight.location().mapLocation(), 4, ws.opp);
		Unit bestTarget = null;
		for (int i = 0; i < nearbyUnits.size(); i++) {
			Unit enemy = nearbyUnits.get(i);
			if(!enemy.location().isOnMap()) continue;
			switch (enemy.unitType()) {
			case Rocket:
				break;
			case Worker:
				break;
			case Healer:
				break;
			case Knight:
				break;
			case Factory:
			case Mage:
			case Ranger:
				if (bestTarget == null) {
					bestTarget = enemy;
				} else {
					if (typeValue(bestTarget.unitType()) < typeValue(
							enemy.unitType())) {
						bestTarget = enemy;
					}
				}
				break;
			default:
				break;
			}
		}
		return bestTarget;
	}

	// The higher the value, the better to focus
	private int typeValue(UnitType type) {
		if (type == null) {
			System.out.println("typeValue was null");
			return 0;
		}

		switch (type) {

		case Worker:
			return 1;
		case Rocket:
			return 2;
		case Knight:
			return 3;
		case Healer:
			return 4;
		case Ranger:
			return 5;
		case Factory:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;
		}
	}

}
