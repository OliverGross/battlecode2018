package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Unit;
import bc.UnitType;
import bc.VecMapLocation;
import bc.VecUnit;
import bc.bc;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;
import tools.framework.buildorder.UnitBuildOrderEvaluator;
import tools.framework.buildorder.StructureBuildOrderEvaluator;

public class MasterStructureBuildPlan extends Plan {

	StructureBuildOrderEvaluator buildOrder = null;

	private Set<Integer> myUnfinishedBuildings = new HashSet<>();
	private Set<Integer> myFinishedStructures = new HashSet<>();
	private Set<Integer> myFinishedRockets = new HashSet<>();
	private List<MapLocation> unfinishedBuildingsLocations = new ArrayList<>();

	public MasterStructureBuildPlan(WorldState worldState,
			StructureBuildOrderEvaluator evalBuildOrder,
			ArrayList<Integer> closestWorker) {
		super(worldState);
		for (int id : closestWorker) {
			ws.resources.assignById(id, pid);
		}
		buildOrder = evalBuildOrder;

	}

	@Override
	protected void tick(WorldState worldState) {
		System.out.println("round: " + ws.gc.round());
		update();
		ArrayList<Integer> workersAssigned = ws.resources
				.getUnitsByPlanAndType(pid, UnitType.Worker);

		System.out.println(
				"workersAssignedToFactoryBuilding: " + workersAssigned);
		// get workers
		if (workersAssigned.size() < 2) {
			ws.resources.assignSomeFreeByType(pid, UnitType.Worker,
					2 - workersAssigned.size());
			workersAssigned = ws.resources.getUnitsByPlanAndType(pid,
					UnitType.Worker);
			if (workersAssigned.isEmpty()) {
				// get some fucking workers from another plan, we dont care
				// TODO make this smarter
				Set<Integer> workers = ws.unitLists.myUnits
						.get(UnitType.Worker);
				for (int id : workers) {
					ws.resources.freeUnitById(id);
				}
				workersAssigned.addAll(
						ws.resources.getFreeUnitsByType(UnitType.Worker));
				ws.resources.assignAllFreeByType(pid, UnitType.Worker);
			}
		}

		if (ws.gc.round() == 1) {
			ArrayList<Integer> workersAssignedAdd = new ArrayList<>();
			for (int workerId : workersAssigned) {
				for (Direction d : DirHelp.adjacentDirections) {
					if (ws.gc.canReplicate(workerId, d)) {
						ws.gc.replicate(workerId, d);
						if (ws.gc.hasUnitAtLocation(ws.gc.unit(workerId)
								.location().mapLocation().add(d))) {
							Unit u = ws.gc
									.senseUnitAtLocation(ws.gc.unit(workerId)
											.location().mapLocation().add(d));
							if (u != null) {
								ws.resources.assignById(u.id(), pid);
								workersAssignedAdd.add(u.id());
							}
						}
						break;
					}
				}
			}
			workersAssigned.addAll(workersAssignedAdd);
		}

		for (int workerId : workersAssigned) {
			Unit worker = ws.gc.unit(workerId);
			if (!worker.location().isOnMap())
				continue;

			if (!myUnfinishedBuildings.isEmpty()) {
				// run to next unfinishedFactory
				if (worker.location().mapLocation().distanceSquaredTo(
						unfinishedBuildingsLocations.get(0)) < 15) {
					ws.lll.goTo(workerId, unfinishedBuildingsLocations);
				} else {
					if (worker.workerHasActed() == 0) {
						doSomethingUseful(worker, worldState);
						continue;
					}
				}

				// TODO Repair!

				if (worker.workerHasActed() == 0) {
					// get all nearby factories
					VecUnit structureNearby = ws.gc.senseNearbyUnitsByTeam(
							worker.location().mapLocation(), 2, ws.me);
					Unit bestStructure = null;

					for (int i = 0; i < structureNearby.size(); i++) {
						Unit structure = structureNearby.get(i);
						if (structure.team() != ws.me
								|| (structure.unitType() != UnitType.Rocket
										&& structure
												.unitType() != UnitType.Factory)
								|| structure.structureIsBuilt() == 1)
							continue;

						if (bestStructure == null) {
							bestStructure = structure;
						} else {
							if (bestStructure.health() < structure.health()) {
								bestStructure = structure;
							}
						}
					}
					if (bestStructure != null) {
						if (ws.gc.canBuild(worker.id(), bestStructure.id())) {
							ws.gc.build(worker.id(), bestStructure.id());
							if (bestStructure.structureIsBuilt() == 1) {
								unfinishedBuildingsLocations.remove(
										bestStructure.location().mapLocation());
								myUnfinishedBuildings
										.remove(bestStructure.id());
								myFinishedStructures.add(bestStructure.id());
							}
						}
					}
				}

			} else {

				UnitType nextType = buildOrder.getNextUnit(ws);
				if (nextType != null) {
					int cost = (int) bc.costOf(UnitType.Factory,
							ws.gc.researchInfo().getLevel(UnitType.Factory));
					if (ws.resources.getAvailableKarbonite(pid) > cost) {
						buildStructure(worker, nextType);
					}
				} else {
					if (worker.workerHasActed() == 0) {
						doSomethingUseful(worker, ws);
					}
				}
			}
		}
	}

	private void doSomethingUseful(Unit worker, WorldState ws) {
		// run around with unit to the most beneficial areas
		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				worker.location().mapLocation(), 65, ws.opp);
		kiteEnemies(enemies, worker, ws);
		if (ws.gc.isMoveReady(worker.id())) {
			// run to the most "free" point in the area
			VecMapLocation locationsAround = ws.gc
					.allLocationsWithin(worker.location().mapLocation(), 2);
			MapLocation bestLoc = worker.location().mapLocation();
			for (int i = 0; i < locationsAround.size(); i++) {
				if (worker.workerHasActed() == 0
						&& ws.gc.karboniteAt(locationsAround.get(i)) > 0) {
					if (ws.gc.canHarvest(worker.id(),
							worker.location().mapLocation()
									.directionTo(locationsAround.get(i)))) {
						ws.gc.harvest(worker.id(),
								worker.location().mapLocation()
										.directionTo(locationsAround.get(i)));
					}
				}
			}
			// move out of the way
			bestLoc = getBestMoveLocation(worker);

			if (ws.gc.canMove(worker.id(),
					worker.location().mapLocation().directionTo(bestLoc))) {
				ws.gc.moveRobot(worker.id(),
						worker.location().mapLocation().directionTo(bestLoc));
			}
		}
	}

	private MapLocation getBestMoveLocation(Unit worker) {
		VecMapLocation locationsAround = ws.gc
				.allLocationsWithin(worker.location().mapLocation(), 2);
		PlanetMap map = ws.gc.startingMap(ws.gc.planet());
		MapLocation bestLoc = worker.location().mapLocation();
		ArrayList<MapLocation> adjacentFree = new ArrayList<>();
		int bestFree = 0;
		int bestStructureCountAround = 10;

		// get locations directly around and count freetiles and factories
		for (int i = 0; i < locationsAround.size(); i++) {
			if (isFree(locationsAround.get(i), map)) {
				bestFree++;
				adjacentFree.add(locationsAround.get(i));
			} else {
				if (ws.gc.hasUnitAtLocation(locationsAround.get(i))) {
					Unit u = ws.gc.senseUnitAtLocation(locationsAround.get(i));
					if (u.unitType() == UnitType.Factory
							|| u.unitType() == UnitType.Rocket) {
						bestStructureCountAround++;
					}
				}
			}
		}

		// for each adjacent, walkable check their adjacent tiles
		// TODO what happens with the current worker location? at the moment
		// free is counted as 1
		for (MapLocation currLoc : adjacentFree) {
			int free = 1;
			int structureCount = 0;
			for (Direction d : DirHelp.adjacentDirections) {
				MapLocation curr = currLoc.add(d);
				if (isFree(curr, map)) {
					free++;
				} else {
					if (ws.gc.hasUnitAtLocation(currLoc)) {
						Unit u = ws.gc.senseUnitAtLocation(currLoc);
						if (u.unitType() == UnitType.Factory
								|| u.unitType() == UnitType.Rocket) {
							bestStructureCountAround++;
						}
					}
				}
			}
			if (bestFree < free || (bestFree == free
					&& bestStructureCountAround > structureCount)) {
				bestFree = free;
				bestStructureCountAround = structureCount;
				bestLoc = currLoc;
			}
		}

		return bestLoc;
	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

	private void buildStructure(Unit worker, UnitType type) {
		Set<MapLocation> possibleLocations = new HashSet<>();
		MapLocation workerLoc = worker.location().mapLocation();
		PlanetMap map = ws.gc.startingMap(ws.gc.planet());
		VecMapLocation poss = null;
		// if worker can move, concider further tiles
		if (ws.gc.isMoveReady(worker.id())) {
			poss = ws.gc.allLocationsWithin(workerLoc, 2);
		} else {
			poss = ws.gc.allLocationsWithin(workerLoc, 4);
		}
		// check all tiles
		for (int i = 0; i < poss.size(); i++) {
			MapLocation curr = poss.get(i);
			if (isFree(curr, map)) {
				possibleLocations.add(curr);
			}
		}

		// decide for the best location
		MapLocation bestLocation = null;
		int bestFreeTiles = 0;
		boolean structureNearby = true;
		for (MapLocation currentLocation : possibleLocations) {
			if (bestLocation == null) {
				int freeTiles = 0;
				boolean structureNear = false;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (ws.gc.hasUnitAtLocation(curr)) {
						Unit u = ws.gc.senseUnitAtLocation(curr);
						if (u.unitType() == UnitType.Factory
								|| u.unitType() == UnitType.Rocket) {
							structureNear = true;
						}
					}
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				bestLocation = currentLocation;
				bestFreeTiles = freeTiles;
				structureNearby = structureNear;
			} else {
				int freeTiles = 0;
				boolean structureNear = false;
				for (Direction d : Direction.values()) {
					if (d == Direction.Center)
						continue;

					MapLocation curr = currentLocation.add(d);
					if (ws.gc.hasUnitAtLocation(curr)) {
						Unit u = ws.gc.senseUnitAtLocation(curr);
						if (u.unitType() == UnitType.Factory
								|| u.unitType() == UnitType.Rocket) {
							structureNear = true;
						}
					}
					if (isFree(curr, map)) {
						freeTiles++;
					}
				}
				if (freeTiles > bestFreeTiles || (freeTiles == bestFreeTiles
						&& structureNear == false)) {
					bestLocation = currentLocation;
					bestFreeTiles = freeTiles;
					structureNearby = structureNear;
				}
			}
		}
		if (bestLocation != null) {
			long distance = bestLocation.distanceSquaredTo(workerLoc);
			// move should be ready here, so move
			if (distance > 2) {
				ws.lll.goTo(worker.id(), bestLocation);
			}
			// get the direction from the now-new workerlocation
			Direction dir = worker.location().mapLocation()
					.directionTo(bestLocation);
			// int cost = (int) bc.costOf(type,
			// ws.gc.researchInfo().getLevel(type));
			if (ws.gc.canBlueprint(worker.id(), type, dir)) {
				ws.gc.blueprint(worker.id(), type, dir);
				if (ws.gc.hasUnitAtLocation(bestLocation)) {
					Unit fac = ws.gc.senseUnitAtLocation(bestLocation);
					unfinishedBuildingsLocations.add(bestLocation);
					myUnfinishedBuildings.add(fac.id());
					// System.out.println(
					// "unfinishedBuildings : " + myUnfinishedBuildings);
				}
			}
		}
	}

	private boolean isFree(MapLocation loc, PlanetMap map) {
		if (map.onMap(loc) && map.isPassableTerrainAt(loc) == 1) {
			if (!ws.gc.hasUnitAtLocation(loc)) {
				return true;
			}
		}
		return false;
	}

	private void update() {
		myUnfinishedBuildings.clear();
		myFinishedStructures.clear();
		unfinishedBuildingsLocations.clear();
		Set<Integer> myFactories = ws.unitLists.myUnits.get(UnitType.Factory);
		for (int facId : myFactories) {
			Unit factory = ws.gc.unit(facId);
			if (factory.structureIsBuilt() == 0) {
				myUnfinishedBuildings.add(facId);
			} else {
				myFinishedStructures.add(facId);
			}
		}
		Set<Integer> myRockets = ws.unitLists.myUnits.get(UnitType.Rocket);
		for (int rocId : myRockets) {
			Unit rocket = ws.gc.unit(rocId);
			if (rocket.structureIsBuilt() == 0) {
				myUnfinishedBuildings.add(rocId);
			} else {
				myFinishedRockets.add(rocId);
			}
		}

		for (int unfinishedId : myUnfinishedBuildings) {
			Unit fac = ws.gc.unit(unfinishedId);
			unfinishedBuildingsLocations.add(fac.location().mapLocation());
		}
	}

}
