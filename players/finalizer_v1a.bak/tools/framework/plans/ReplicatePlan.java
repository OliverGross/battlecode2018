package tools.framework.plans;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class ReplicatePlan extends Plan {
	int workN;
	int wid;
	WorldState ws;

	public ReplicatePlan(WorldState worldState, int worker_id,
			int replicateNumber) {
		super(worldState);
		wid = worker_id;
		workN = replicateNumber;
		ws = worldState;
		// if (ws.gc.unit(wid).unitType() == UnitType.Worker)
		// ws.resources.assignById(wid, pid);
	}

	public int replicate(WorldState ws, int worker_id) {
		if (ws.gc.karbonite() >= bc.bc
				.bcUnitTypeReplicateCost(UnitType.Worker)) {
			double nearbyKarbHelper = -1;
			Direction dHelper = Direction.Center;
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canReplicate(worker_id, d)) {
					for (Direction k : DirHelp.adjacentCenterDirections) {
						if (ws.gc.canHarvest(worker_id, k)) {
							ws.gc.harvest(worker_id, k);
							break;
						}
					}
					MapLocation locationHelper = ws.gc.unit(worker_id)
							.location().mapLocation();
					locationHelper.add(d);
					if (ws.pga.nearbyKarbonite[locationHelper
							.getX()][locationHelper
									.getY()] > nearbyKarbHelper) {
						dHelper = d;
						nearbyKarbHelper = ws.pga.nearbyKarbonite[locationHelper
								.getX()][locationHelper.getY()];
					}
				}
			}
			if (ws.gc.canReplicate(worker_id, dHelper)) {
				ws.gc.replicate(worker_id, dHelper);
				Unit newWorker = ws.gc.senseUnitAtLocation(ws.gc.unit(worker_id)
						.location().mapLocation().add(dHelper));
				if (newWorker == null) {
					return -1;
				} else {
					return newWorker.id();
				}
			}
			
			
		}
		return -1;

	}

	@Override
	public void tick(WorldState ws) {
		if (workN > 0) {
			int newWorkerId = replicate(ws, wid);
			// System.out.println("New Worker" + newWorkerId);
			if (newWorkerId > 0) {
				workN -= 1;
				// System.out.print("I replicated, and I still have " + workN +
				// "replications left");
				// ws.resources.freeUnitById(wid);
				wid = newWorkerId;
				if (workN == 0) {
					// ws.resources.freeUnitById(wid);
					// ws.resources.freeUnitById(wid);
					this.status = Status.FINAL;
				}
			}
		} else {
			// ws.resources.freeUnitById(wid);
			this.status = Status.FINAL;
		}
	}

}
