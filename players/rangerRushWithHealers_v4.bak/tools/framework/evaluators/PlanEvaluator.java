package tools.framework.evaluators;

import tools.framework.Evaluator;
import tools.framework.Plan;
import tools.framework.WorldState;
import tools.framework.plans.CMasterStructureBuildPlan;
import tools.framework.plans.FFactoryBuildPlan;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.HealerPlan;
import tools.framework.plans.KnightsAttackPlanSprint;
import tools.framework.plans.KnightsBuildPlan;
import tools.framework.plans.MasterBuildPlan;
import tools.framework.plans.MasterStructureBuildPlan;
import tools.framework.plans.RangerAttackPlan;
import tools.framework.plans.ReplicatePlan;
import tools.framework.plans.ReplicationPlan;
import tools.framework.plans.SquadToMarsPlan;

public class PlanEvaluator extends Evaluator {

	public PlanEvaluator(double priority) {
		super(priority);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getEvaluation(Plan plan, WorldState ws) {
		if(plan.getClass() == RangerAttackPlan.class) {
			return 8;
		}
		if(plan.getClass() == HealerPlan.class) {
			return 7;
		}
		if(plan.getClass() == MasterStructureBuildPlan.class) {
			return 4.0;
		}
		if(plan.getClass() == CMasterStructureBuildPlan.class) {
			return 4.0;
		}
		if(plan.getClass() == MasterBuildPlan.class) {
			return 3;
		}
		if(plan.getClass() == HarvestPlan.class) {
			return 2.0;
		}
		if(plan.getClass() == ReplicatePlan.class) {
			return 9;
		}
		if(plan.getClass() == SquadToMarsPlan.class) {
			if(ws.gc.round() >= 650) {
				return 10;
			} else {
				return 1;
			}
		}

		return 0;
	}

}
