package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.HealerPlan;

public class HealerMoveBehaviour extends Behaviour {

	HealerPlan healerPlan;

	public HealerMoveBehaviour(HealerPlan plan) {
		healerPlan = plan;
	}

	@Override
	public void act(WorldState ws) {
		ArrayList<Integer> healers = healerPlan.getHealersOnMap();
		for (int healerId : healers) {
			if (ws.gc.isMoveReady(healerId)) {
				Unit healer = ws.gc.unit(healerId);
				if (!healer.location().isOnMap())
					continue;

				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), 65, ws.opp);

				if (enemies.size() != 0) {
					// System.out.println("Healer Kiting");
					kiteEnemies(enemies, healer, ws);
				}

				VecUnit nearbys = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), 5000, ws.opp);
				if (nearbys.size() == 0) {
					nearbys = ws.gc.startingMap(Planet.Earth)
							.getInitial_units();
				}

				List<MapLocation> targetPoints = new ArrayList<>();
				for (int i = 0; i < nearbys.size(); i++) {
					targetPoints.add(nearbys.get(i).location().mapLocation());
				}

				ws.lll.goTo(healerId, targetPoints);

			}
		}

	}

	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for (int e = 0; e < enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch (enemy.unitType()) {
			case Factory:
				continue;
			case Healer:
				continue;
			case Knight:
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation()
						.directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if (ws.gc.canMove(worker.id(), kite)
						&& ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket:
				continue;
			case Worker:
				continue;
			}
		}
	}

}
