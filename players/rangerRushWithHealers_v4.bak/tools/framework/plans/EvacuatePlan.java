package tools.framework.plans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.Unit;
import bc.UnitType;
import bc.bc;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class EvacuatePlan extends Plan {
	// earliest round to launch any rocket
	long launchDay;

	// only do something if rockets are researched
	long rocketResearchLevel;

	// latest round to launch even
	final long latestLaunch = 745;
	final int maxNumBuildableRockets = 4;
	final int maxNumLoadableRockets = 4;
	final int maxNumLaunchableRockets = 4;
	final int minScientist = 2;

	ArrayList<Integer> rocketScientist = new ArrayList<Integer>();
	ArrayList<Integer> rocketFactories = new ArrayList<Integer>();
	ArrayList<Integer> buildableRockets = new ArrayList<Integer>();
	// Location of unfinished workSites
	List<MapLocation> constructionSites = new ArrayList<MapLocation>();
	ArrayList<Integer> launchableRockets = new ArrayList<Integer>();
	ArrayList<Integer> loadableRockets = new ArrayList<Integer>();
	List<MapLocation> loadableRocketsSites = new ArrayList<MapLocation>();
	ArrayList<Integer> evacuees = new ArrayList<Integer>();

	// launch counter
	int launchedRockets = 0;

	// landing sites
	// \TODO find landingSites
	ArrayList<MapLocation> landingSites = new ArrayList<MapLocation>();

	public EvacuatePlan(WorldState worldState, long launchRound) {
		super(worldState);
		initResources(worldState);
		findLandingSites(worldState);
		launchDay = launchRound;
	}

	public void initResources(WorldState ws) {
		ws.resources.assignFreeToPlan(pid);
	}

	public void findLandingSites(WorldState ws) {
		PlanetMap marsMap = ws.gc.startingMap(Planet.Mars);
		long width = marsMap.getWidth();
		long height = marsMap.getHeight();
		System.out.println(marsMap.getWidth() + "," + marsMap.getHeight());
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height && x < width; y++) {
				System.out.println(x + "," + y);
				MapLocation loc = new MapLocation(Planet.Mars, x, y);
				try {
					if (marsMap.isPassableTerrainAt(loc) == 1) {
						landingSites.add(loc);
						x++;
						y++;
					}
				} catch (Exception e) {
					System.out.println("Got an exception for " + x + "," + y);
					System.out.println(e);
					continue;
				}
			}
		}
		System.out.println(
				"Have found " + landingSites.size() + " landing spots!");
	}

	public void updateResources(WorldState ws) {
		ws.resources.assignFreeToPlan(pid);
		rocketScientist = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Worker);
		rocketFactories = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Factory);
		evacuees = ws.resources.getUnitsByPlan(pid);
		evacuees.removeAll(rocketScientist);
		evacuees.removeAll(rocketFactories);
		evacuees.removeIf(id -> !ws.gc.unit(id).location().isOnMap());
		ArrayList<Integer> allRockets = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Rocket);
		launchableRockets.clear();
		loadableRockets.clear();
		buildableRockets.clear();
		constructionSites.clear();
		loadableRocketsSites.clear();
		for (int rocket_id : allRockets) {
			Unit rocket = ws.gc.unit(rocket_id);
			if (rocket.structureIsBuilt() == 0) {
				buildableRockets.add(rocket_id);
				constructionSites.add(rocket.location().mapLocation());
			} else if (rocket.structureGarrison().size() < 8
					&& rocket.location().isOnMap()) {
				loadableRockets.add(rocket_id);
				loadableRocketsSites.add(rocket.location().mapLocation());
			} else {
				launchableRockets.add(rocket_id);
			}
		}

	}

	@Override
	public void tick(WorldState ws) {
		updateResources(ws);
		rocketResearchLevel = ws.gc.researchInfo().getLevel(UnitType.Rocket);
		// check if launchDay has come or too much rockets
		if (ws.gc.round() >= launchDay
				|| launchableRockets.size() >= maxNumLaunchableRockets) {
			// launch every full rocket
			for (int rocket_id : launchableRockets) {
				for (Iterator<MapLocation> it = landingSites.iterator(); it
						.hasNext();) {
					MapLocation loc = it.next();
					if (ws.gc.canLaunchRocket(rocket_id, loc)) {
						ws.gc.launchRocket(rocket_id, loc);
						it.remove();
						break;
					}
				}
			}
		}
		// emergency evacuation of non-full rockets
		if (ws.gc.round() > latestLaunch) {
			for (int rocket_id : loadableRockets) {
				for (Iterator<MapLocation> it = landingSites.iterator(); it
						.hasNext();) {
					MapLocation loc = it.next();
					if (ws.gc.canLaunchRocket(rocket_id, loc)) {
						ws.gc.launchRocket(rocket_id, loc);
						it.remove();
						break;
					}
				}

			}
		}
		// Worker behavior
		// replicate

		if (rocketScientist.size() <= minScientist) {
			for (int worker_id : rocketScientist) {
				for (Direction d : DirHelp.adjacentDirections) {
					if (ws.gc.canReplicate(worker_id, d)) {
						ws.gc.replicate(worker_id, d);
						ws.resources
								.assignByUnit(
										ws.gc.senseUnitAtLocation(
												ws.gc.unit(worker_id).location()
														.mapLocation().add(d)),
										pid);
						break;
					}
				}
			}
			rocketScientist = ws.resources.getUnitsByPlanAndType(pid,
					UnitType.Worker);
		}
		// construction of rockets
		boolean buildOrBluePrinted = false;
		int rocketCost = (int) bc.costOf(UnitType.Rocket,
				ws.gc.researchInfo().getLevel(UnitType.Rocket));

		for (int worker_id : rocketScientist) {
			buildOrBluePrinted = false;
			for (int rocket_id : buildableRockets) {
				if (ws.gc.canBuild(worker_id, rocket_id)) {
					ws.gc.build(worker_id, rocket_id);
					buildOrBluePrinted = true;
					break;
				}
			}
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canBlueprint(worker_id, UnitType.Rocket, d)
						&& ws.resources.getAvailableKarbonite(pid) >= rocketCost
						&& loadableRockets.size() < maxNumLoadableRockets
						&& buildableRockets.size() < maxNumBuildableRockets) {
					System.out.println("Blueprinting a rocket");
					ws.gc.blueprint(worker_id, UnitType.Rocket, d);
					MapLocation newBluePrint = ws.gc.unit(worker_id).location()
							.mapLocation().add(d);
					constructionSites.add(newBluePrint);
					ws.resources.assignByUnit(
							ws.gc.senseUnitAtLocation(newBluePrint), pid);
					buildOrBluePrinted = true;
					break;
				}

			}
			// Probably move if nothing was done so far
			if (!buildOrBluePrinted) {
				if (ws.gc.isMoveReady(worker_id) && rocketResearchLevel >= 1
						&& constructionSites.size() > 0) {
					Direction nextDir = ws.pathfinding
							.getNextDirection(worker_id, constructionSites);
					if (nextDir != null) {
						if (ws.gc.canMove(worker_id, nextDir)) {
							ws.gc.moveRobot(worker_id, nextDir);
						}
					}
				}
			}

		}
		// Factory add
		for (int factory_id : rocketFactories) {
			if (ws.gc.canProduceRobot(factory_id, UnitType.Ranger)
					&& evacuees.size() < loadableRockets.size() * 8) {
				ws.gc.produceRobot(factory_id, UnitType.Ranger);
			}
			try {
				if (ws.gc.unit(factory_id).structureGarrison().size() > 0) {
					for (Direction d : Direction.values()) {
						if (ws.gc.canUnload(factory_id, d)) {
							ws.gc.unload(factory_id, d);
						}
					}
				}
			} catch (RuntimeException e) {
				System.out.println(e);
			}

		}
		// Evacuees behavior
		boolean loaded = false;
		for (Iterator<Integer> it = evacuees.iterator(); it.hasNext();) {
			int evacuee = it.next();
			loaded = false;
			for (int rocket_id : loadableRockets) {
				if (ws.gc.canLoad(rocket_id, evacuee)) {
					ws.gc.load(rocket_id, evacuee);
					it.remove();
					loaded = true;
					break;
				}
			}
			if (!loaded) {
				if (ws.gc.isMoveReady(evacuee) && loadableRockets.size() > 1) {
					Direction nextDir = ws.pathfinding.getNextDirection(evacuee,
							loadableRocketsSites);
					if (ws.gc.canMove(evacuee, nextDir)) {
						ws.gc.moveRobot(evacuee, nextDir);
					}
				}
			}
		}
		
		if(buildableRockets.size()==0 && launchableRockets.size() < maxNumLaunchableRockets && loadableRockets.size() < maxNumLoadableRockets) {
			ws.resources.saveKarbonite(rocketCost, pid);
		}

	}

}
