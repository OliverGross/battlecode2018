package tools.framework.plans;

import bc.Direction;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.WorldState;

public class MarsReplicatePlan extends Plan{

	public MarsReplicatePlan(WorldState worldState) {
		super(worldState);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void tick(WorldState ws) {
		if(ws.gc.planet() == Planet.Earth) return;
		if(ws.gc.round() < 750) return;
		
		for(int wId : ws.unitLists.myWorkers) {
			Unit worker = ws.gc.unit(wId);
			if(!worker.location().isOnMap()) continue;
			if(worker.unitType() != UnitType.Worker) continue;
			if(worker.abilityCooldown() > 10) continue;
			long cost = bc.bc.bcUnitTypeReplicateCost(UnitType.Worker);
			if(ws.gc.karbonite() < cost) break;
			
			for(Direction d : DirHelp.adjacentDirections) {
				if(ws.gc.canReplicate(wId, d)) {
					ws.gc.replicate(wId, d);
					break;
				}
			}

		}
	}

}