import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.plans.DefenseEconomyPlan;
import tools.framework.plans.EvacuatePlan;
import tools.framework.plans.RushAttackPlan;

public class MasterOfDisasterPlan extends EvaluatorPlan {

	public MasterOfDisasterPlan(WorldState ws) {
		super(ws);
		switch (ws.pga.getInitialStrategy()) {
		case Evacuate:
			this.plans.add(new EvacuatePlan(ws, 0));
			return;
		case Rush:
			this.plans.add(new RushAttackPlan(ws));
			return;
		case Defense:
			this.plans.add(new DefenseEconomyPlan(ws));
			return;
		}
	}

	@Override
	public void modifyPlans(WorldState worldState) {
	}

}
