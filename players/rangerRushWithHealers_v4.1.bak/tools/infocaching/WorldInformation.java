package tools.infocaching;

import bc.GameController;
import bc.Planet;

/**
 * Speichert:
 * Building Placement enemy
 * Building Placement ally
 * Unit enemy seen
 * Units ally
 * All Units ever seen (needs to be updated if destroyed)
 * Updated Start-Mapinformation (Carbonite)
 * Workers seen on Carbonite-Pools (and Carbonite Value)
 * Workers seen on Blueprints (estimate finish time of blueprint)
 * Technologies Researched by enemy
 * Technologies Researched
 * Rockets seen (Garrison?, Units around?)
 * Fields seen
 * @author Oliver
 *
 */
public abstract class WorldInformation {
	
	public static WorldInformation createWorldInformation(GameController controller) {
		if(controller.planet() == Planet.Earth) {
			return new EarthWorldInformation(controller);
		} else {
			return new MarsWorldInformation(controller);
		}
	}
	
	GameController gc;
	
	protected WorldInformation(GameController controller) {
		this.gc = controller;
	}
	
	public abstract void tick();
	
}
