package tools.framework.buildorder;

import java.util.Set;

import bc.UnitType;
import tools.framework.WorldState;

public class StructureBuildOrder implements BuildOrderEvaluator{

	@Override
	public UnitType getNextUnit(WorldState ws) {
		int factoryCount = ws.unitLists.myUnits.get(UnitType.Factory).size();
		int rocketCount = ws.unitLists.myUnits.get(UnitType.Rocket).size();
		
		int idleFactories = 0;
		Set<Integer> factories = ws.unitLists.myUnits.get(UnitType.Factory);
		for(int id : factories) {
			if(ws.gc.unit(id).isFactoryProducing()==0 && ws.gc.unit(id).structureIsBuilt()==1) {
				idleFactories++;
			}
		}
		
		if(ws.gc.researchInfo().getLevel(UnitType.Rocket) >= 1) {
			if(ws.gc.round() > 600) {
				int myrobots = ws.unitLists.myRobots.size();
				if(rocketCount==0) rocketCount = 1;
				int robots_to_rockets = myrobots/rocketCount;
				if(robots_to_rockets >=9 ) {
					return UnitType.Rocket;
				}
			}
			
			if(factoryCount==0) return UnitType.Factory;
			
			if(factoryCount < 3 && ws.gc.round() < 650) {
				return UnitType.Factory;
			}
			
			if(ws.gc.karbonite() > 100) {
				if(ws.gc.round() < 400 && idleFactories < 2) {
					return UnitType.Factory;
				} else {
					return UnitType.Rocket;
				}
			}
		} else {
			if(ws.gc.round() > 650) return null;
			
			if(factoryCount < 3) {
				return UnitType.Factory;
			} else {
				if(ws.gc.karbonite() > 150) {
					return UnitType.Factory;
				}
			}
		}
		
		return null;
	}

}
