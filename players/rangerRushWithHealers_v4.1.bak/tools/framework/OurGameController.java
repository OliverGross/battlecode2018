package tools.framework;

import bc.Direction;
import bc.GameController;
import bc.Unit;
import bc.UnitType;

public class OurGameController extends GameController {

	WorldState ws;
	private static int runningInstances = 0;

	public OurGameController(WorldState ws) {
		if (runningInstances >= 1)
			throw new RuntimeException(
					"There should be only one GameController active!");
		runningInstances++;
		this.ws = ws;
	}

	@Override
	public void moveRobot(int robot_id, Direction direction) {
		super.moveRobot(robot_id, direction);
		ws.pathfinding.updateUnitMoves(robot_id);
	}

	@Override
	public void blueprint(int worker_id, UnitType structure_type,
			Direction direction) {
		super.blueprint(worker_id, structure_type, direction);
		Unit u = super.senseUnitAtLocation(
				super.unit(worker_id).location().mapLocation().add(direction));
		ws.unitLists.addNewUnit(u);
	}

}
