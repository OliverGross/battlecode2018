import tools.framework.Agent;
import tools.framework.Plan;
import tools.framework.WorldState;

public class Sprint {
	public static void main(String[] args) {
		WorldState worldstate = new WorldState();
		Plan masterplan = new SprintMasterPlan(worldstate);
		Agent agent = new Agent(worldstate, masterplan);
		agent.execute();
	}
}