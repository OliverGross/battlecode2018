import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import bc.MapLocation;
import bc.Planet;
import bc.UnitType;
import tools.PGA;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.EvacuatePlan;
import tools.framework.plans.FFactoryBuildPlan;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.KnightsBuildPlan;
import tools.framework.plans.RangerAttackPlan;
import tools.framework.plans.RangerBuildPlan;

public class SprintMasterPlan extends EvaluatorPlan {

	// check if already in evacuate
	boolean evacuateMode = false;
	// earliest time to think of evacuation
	long magicRoundNumber = 250;
	boolean preferKnightsOverRanger = false;

	public SprintMasterPlan(WorldState worldState) {
		super(worldState);
		if (worldState.gc.planet().equals(Planet.Earth)) {
			PGA preGame = new PGA(worldState.gc.startingMap(Planet.Earth),
					worldState.gc.team(), worldState);
			ArrayList<Integer> factoryWorkers = new ArrayList<Integer>();
			ArrayList<Integer> harvestWorkers = new ArrayList<Integer>();
			HashMap<Integer, ArrayList<Integer>> eval = preGame
					.getWorkerEvaluation();
			int minFree = -1;
			int factoryEnemyDistance = 0;
			int factoryWorker = 0;
			for (HashMap.Entry<Integer, ArrayList<Integer>> entry : eval
					.entrySet()) {
				if (entry.getValue().get(1) > minFree) {
					minFree = entry.getValue().get(1);
					factoryWorker = entry.getKey();
					factoryEnemyDistance = entry.getValue().get(0);
				}
			}
			check_knight_preferation(worldState,
					new HashSet<MapLocation>(preGame.myWorkers.values()),
					new HashSet<MapLocation>(preGame.oppWorkers.values()));
			// \TODO eventually chech factoryEnemyDistance
			factoryWorkers.add(factoryWorker);
			harvestWorkers.addAll(preGame.myWorkers.keySet());
			harvestWorkers.remove(Integer.valueOf(factoryWorker));
			if (this.preferKnightsOverRanger)
				plans.add(new KnightsBuildPlan(worldState));
			else
				plans.add(new RangerBuildPlan(worldState));
			plans.add(
					new FFactoryBuildPlan(worldState, factoryWorkers, true, 4));

			plans.add(new HarvestPlan(worldState, harvestWorkers));
		}
		if (this.preferKnightsOverRanger)
			plans.add(new KnightsAttackPlan(worldState));
		else
			plans.add(new RangerAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));
	}

	public void check_knight_preferation(WorldState ws, HashSet<MapLocation> me,
			HashSet<MapLocation> enemy) {
		int largest_dist = 0;
		for (MapLocation enem : enemy) {
			largest_dist = Math.max(largest_dist,
					ws.pathfinding.getDistance(me, enem));
		}
		if (largest_dist <= 5) {
			this.preferKnightsOverRanger = true;
			System.out.println("We are doing KnightRush instead of Rangers!");
		}
	}

	private int wonEarthRounds = 0;

	public boolean wonEarth(WorldState ws) {
		if (ws.gc.round() > magicRoundNumber) {
			if (ws.gc.myUnits().size() >= ws.gc.units().size()) {
				wonEarthRounds++;
				if (wonEarthRounds > 50) {
					return true;
				}
			} else {
				wonEarthRounds = 0;
			}
		}
		return false;
	}

	@Override
	public void modifyPlans(WorldState worldState) {
		if (!evacuateMode) {
			if (!wonEarth(worldState)) {
				if (preferKnightsOverRanger) {
					if (worldState.gc.researchInfo()
							.getLevel(UnitType.Knight) < 3) {
						worldState.gc.queueResearch(UnitType.Knight);
					} else {
						worldState.gc.queueResearch(UnitType.Rocket);
					}
				} else {
					if (worldState.gc.researchInfo()
							.getLevel(UnitType.Ranger) < 3) {
						worldState.gc.queueResearch(UnitType.Ranger);
					} else {
						worldState.gc.queueResearch(UnitType.Rocket);
					}
				}
			} else {
				worldState.gc.resetResearch();
				worldState.gc.queueResearch(UnitType.Rocket);
				plans.clear();
				plans.add(new EvacuatePlan(worldState, 700));
				evacuateMode = true;
			}
		}
	}

}
