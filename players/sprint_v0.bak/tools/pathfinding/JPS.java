package tools.pathfinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import bc.Direction;
import bc.GameController;
import bc.MapLocation;

public class JPS {
	public StaticMapGrid map;
	public static long compTime = 0;
	public static int totalCalls = 0;
	private static int runningInstances = 0;
	public static GameController gc;

	private static final ArrayList<int[]> directions = new ArrayList<int[]>() {
		@Override
		public int indexOf(Object o) {
			if (o instanceof int[]) {
				for (int i = 0; i < this.size(); i++) {
					int[] c = (int[]) o;
					if (Arrays.equals(c, this.get(i))) {
						return i;
					}
				}
			}
			return -1;
		}

		private static final long serialVersionUID = 1L;
		{
			add(new int[] { 1, 0 });
			add(new int[] { 1, 1 });
			add(new int[] { 0, 1 });
			add(new int[] { -1, 1 });
			add(new int[] { -1, 0 });
			add(new int[] { -1, -1 });
			add(new int[] { 0, -1 });
			add(new int[] { 1, -1 });
		}
	};

	private static int getDirIdx(int[] dir) {
		if (dir[0] == 0 && dir[1] == -1)
			return 6;
		return mod((1 - dir[0]) * 2 + dir[0] * dir[1], 8);
	}

	Set<HashablePoint> start;
	Set<HashablePoint> goal;

	private class Node {
		@Override
		public int hashCode() {
			int[] pre = { 100, 100 };
			if (prev != null) {
				pre = prev.point;
			}
			return point[0]
					+ 1000 * (point[1] + 1000 * (pre[0] + 1000 * pre[1]));
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (!Arrays.equals(point, other.point))
				return false;
			if (prev == null) {
				if (other.prev != null)
					return false;
			} else {
				if (!Arrays.equals(prev.point, other.prev.point))
					return false;
			}
			return true;
		}

		int[] point;
		Node prev;
		double g;
		double h;

		double f() {
			return g + h;
		}

		public Node(int[] point, Node previous, double _g, double _h) {
			this.point = point;
			prev = previous;
			g = _g;
			h = _h;
		}

		public Node(int[] point, Node previous) {
			this.point = point;
			prev = previous;
			g = previous.g + GeoPoint.chebyshev_distance(previous.point, point);
			h = getH(point, goal);
		}

		@Override
		public String toString() {
			if (prev != null) {
				return "Point:" + GeoPoint.toString(point) + " g:" + g + " h:"
						+ h + " Previous:" + GeoPoint.toString(prev.point);
			} else {
				return "Point:" + GeoPoint.toString(point) + " g:" + g + " h:"
						+ h + " Previous:" + "null";
			}

		}
	}

	private PriorityQueue<Node> openList = new PriorityQueue<Node>(11,
			(Node n1, Node n2) -> ((int) (n1.f() - n2.f())));
	private Set<Node> closedList = new HashSet<Node>();

	public JPS() {
		if (runningInstances == 0) {
			runningInstances += 1;
		} else {
			throw new RuntimeException(
					"Make sure to only start up one JPS object!");
		}
	}

	public JPS(GameController gc) {
		if (runningInstances == 0) {
			runningInstances += 1;
			this.gc = gc;
		} else {
			throw new RuntimeException(
					"Make sure to only start up one JPS object!");
		}
		map = new StaticMapGrid(gc);
	}

	public JPS(StaticMapGrid grid) {
		if (runningInstances == 0) {
			gc = null;
			runningInstances += 1;
		} else {
			throw new RuntimeException(
					"Make sure to only start up one JPS object!");
		}
		map = grid;
	}

	public Direction getNextDirection(MapLocation start, MapLocation goal) {
		if (start.equals(goal)) {
			System.out.println("when start==goal then path has only length 1");
			return Direction.Center;
		}
		List<int[]> path = getPath(new int[] { start.getX(), start.getY() },
				new int[] { goal.getX(), goal.getY() });
		if (path == null || path.size() == 1) {
			System.out.println("No path was found!");
			// System.out.println(start.getX() + " " + start.getY());
			// System.out.println(goal.getX() + " " + goal.getY());
			// for (int i = 0; i < map.h; i++) {
			// System.out.print((map.h - i - 1) % 10);
			// for (int j = 0; j < map.w; j++) {
			// System.out.print(map.get(j, map.h - i - 1) ? '#' : '.');
			// }
			// System.out.println();
			// }
			// System.out.print(" ");
			// for (int i = 0; i < map.w; i++)
			// System.out.print(i % 10);
			// System.out.println();
			return null;
		}
		int[] dir = GeoPoint.diff(path.get(1), path.get(0));
		int d = GeoPoint.chebyshev_distance(dir);
		if (d == 0) {
			return Direction.Center;
		}
		dir[0] /= d;
		dir[1] /= d;
		return start.directionTo(new MapLocation(start.getPlanet(),
				start.getX() + dir[0], start.getY() + dir[1]));
	}

	public void reset_JPS() {
		openList.clear();
		closedList.clear();
	}

	public static List<int[]> convertJumpPath(List<int[]> jumpPath) {
		List<int[]> result = new ArrayList<int[]>();
		result.add(jumpPath.get(0));
		int[] cur = jumpPath.get(0);
		for (int i = 1; i < jumpPath.size(); i++) {
			while (!Arrays.equals(cur, jumpPath.get(i))) {
				int[] diff = GeoPoint.diff(jumpPath.get(i), cur);
				int d = GeoPoint.chebyshev_distance(diff);
				diff[0] /= d;
				diff[1] /= d;
				cur = GeoPoint.add(cur, diff);
				result.add(cur);
			}
		}
		return result;
	}

	public void setObstacles(Set<HashablePoint> obstacles, boolean impassable) {
		for (HashablePoint p : obstacles) {
			map.set(p.a[0], p.a[1], impassable);
		}
	}

	public List<HashablePoint> getHPath(Set<HashablePoint> start,
			Set<HashablePoint> goal, Set<HashablePoint> obstacles) {
		setObstacles(obstacles, true);
		List<int[]> path = getPath(start, goal);
		setObstacles(obstacles, false);
		List<HashablePoint> result = new ArrayList<HashablePoint>();
		for (int[] i : path) {
			result.add(new HashablePoint(i));
		}
		return result;
	}

	public Direction getNextDirection(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		List<int[]> path = getPath(start, goal);
		if (path.size() == 1)
			return Direction.Center;
		return GeoPoint.getDirection(GeoPoint.diff(path.get(1), path.get(0)));
	}

	public List<int[]> getPath(int[] start, int[] goal) {
		return convertJumpPath(getJumpPath(start, goal));
	}

	public List<int[]> getPath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		return convertJumpPath(getJumpPath(start, goal));
	}

	public List<int[]> getJumpPath(int[] start, int[] goal) {
		Set<HashablePoint> s = new HashSet<HashablePoint>();
		s.add(new HashablePoint(start));
		Set<HashablePoint> g = new HashSet<HashablePoint>();
		g.add(new HashablePoint(goal));
		return getJumpPath(s, g);
	}

	public List<int[]> getJumpPath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		long t0 = System.nanoTime();
		List<int[]> result = computePath(start, goal);
		long t1 = System.nanoTime();
		compTime += t1 - t0;
		//System.out.println("Single JPS call took: " + (t1 - t0) * 1e-6 + "ms");
		totalCalls += 1;
		return result;
	}

	public List<int[]> computePath(Set<HashablePoint> start,
			Set<HashablePoint> goal) {
		reset_JPS();
		this.start = start;
		this.goal = goal;
		Node currentNode;
		// add the start to openList
		for (HashablePoint s : start)
			openList.add(new Node(s.a, null, 0, getH(s.a, goal)));
		do {
			currentNode = openList.poll();

			// check if goal is reached
			if (goal.contains(new HashablePoint(currentNode.point))) {
				return nodeToPath(currentNode);
			}
			// mark currentNode as already visited
			closedList.add(currentNode);

			// add successors to openList
			addSuccessors(currentNode, currentNode.prev);
		} while (!openList.isEmpty());// && openList.size() < 500);
		List<Node> orderedClosedList = new ArrayList<Node>(closedList);
		orderedClosedList.sort((Node n1,
				Node n2) -> ((int) ((n1.h - n2.h) * 2500 + (n1.g - n2.g))));
		System.out.println(
				"There was no path found, dont let this happen to often, this takes a long time!");
		return nodeToPath(orderedClosedList.get(0));
	}

	/**
	 * Convert a linked list (Node->Previous) to an ArrayList<int[]>
	 * 
	 * @param n
	 * @return Path to n
	 */
	private ArrayList<int[]> nodeToPath(Node n) {
		Node x = n;
		ArrayList<int[]> path = new ArrayList<int[]>();
		if (x != null) {
			do {
				path.add(x.point);
				x = x.prev;
			} while (x != null && x.prev != null);
		}
		if (x != null)
			path.add(x.point);
		Collections.reverse(path);
		return path;
	}

	/**
	 * Add the successors (forced and natural neighbors) to the openList
	 * 
	 * @param curNode
	 * @param prevNode
	 */
	private void addSuccessors(Node curNode, Node prevNode) {
		ArrayList<int[]> neighbours = (prevNode != null)
				? prunedNeighbors(curNode.point, prevNode.point)
				: neighbors(curNode.point);
		for (int[] neighbor : neighbours) {
			neighbor = jump(curNode.point,
					GeoPoint.diff(neighbor, curNode.point), curNode);
			if (neighbor != null) {
				Node newNode = new Node(
						neighbor, curNode, curNode.g + GeoPoint
								.chebyshev_distance(curNode.point, neighbor),
						getH(neighbor, goal));
				if (!closedList.contains(newNode))
					openList.add(newNode);
			}
		}
	}

	/**
	 * Mathematical modulo definition
	 */
	private static int mod(int a, int n) {
		return ((a % n) + n) % n;
	}

	/**
	 * Any euclidean neighbors of x
	 * 
	 * @param x
	 * @return ArrayList<int[]> of size eight
	 */
	private ArrayList<int[]> neighbors(int[] x) {
		ArrayList<int[]> n = new ArrayList<int[]>();

		for (int[] dir : directions) {
			n.add(GeoPoint.add(x, dir));
		}
		return n;
	}

	/**
	 * Omit unnecessary neighbors
	 * 
	 * @param x
	 * @param p
	 * @return list of natural and forced neighbors
	 */
	private ArrayList<int[]> prunedNeighbors(int[] x, int[] p) {
		ArrayList<int[]> n = new ArrayList<int[]>();

		int[] dir = GeoPoint.diff(x, p);
		dir[0] = dir[0] > 0 ? 1 : dir[0] < 0 ? -1 : 0;
		dir[1] = dir[1] > 0 ? 1 : dir[1] < 0 ? -1 : 0;

		int i = getDirIdx(dir);
		if (i == -1) {
			System.out.println(x);
			System.out.println(
					"Direction " + GeoPoint.toString(dir) + " not recognized!");
			System.exit(1);
		}
		if (!GeoPoint.isDiagonal(dir)) {
			// add natural neighbor
			n.add(GeoPoint.add(x, dir));
			// check for forced neighbors
			if (map.get(GeoPoint.add(x, directions.get(mod(i - 2, 8))))) {
				n.add(GeoPoint.add(x, directions.get(mod(i - 1, 8))));
			}
			if (map.get(GeoPoint.add(x, directions.get(mod(i + 2, 8))))) {
				n.add(GeoPoint.add(x, directions.get(mod(i + 1, 8))));
			}

		} else {
			// add natural neighbors
			n.add(GeoPoint.add(x, dir));
			n.add(GeoPoint.add(x, directions.get(mod(i - 1, 8))));
			n.add(GeoPoint.add(x, directions.get(mod(i + 1, 8))));
			// check for forced neighbors
			if (map.get(GeoPoint.add(x, directions.get(mod(i - 3, 8))))) {
				n.add(GeoPoint.add(x, directions.get(mod(i - 2, 8))));
			}
			if (map.get(GeoPoint.add(x, directions.get(mod(i + 3, 8))))) {
				n.add(GeoPoint.add(x, directions.get(mod(i + 2, 8))));
			}
		}

		return n;
	}

	private boolean hasForcedNeighbors(int[] x, int[] p) {
		int[] dir = GeoPoint.diff(x, p);
		dir[0] = dir[0] > 0 ? 1 : dir[0] < 0 ? -1 : 0;
		dir[1] = dir[1] > 0 ? 1 : dir[1] < 0 ? -1 : 0;
		int i = getDirIdx(dir);
		if (i == -1) {
			return true;
		}
		if (!GeoPoint.isDiagonal(dir))
			return ((map.get(GeoPoint.add(x, directions.get(mod(i - 2, 8))))
					&& !map.get(GeoPoint.add(x, directions.get(mod(i - 1, 8)))))
					|| (map.get(GeoPoint.add(x, directions.get(mod(i + 2, 8))))
							&& !map.get(GeoPoint.add(x,
									directions.get(mod(i + 1, 8))))));
		else
			return ((map.get(GeoPoint.add(x, directions.get(mod(i - 3, 8))))
					&& !map.get(GeoPoint.add(x, directions.get(mod(i - 2, 8)))))
					|| (map.get(GeoPoint.add(x, directions.get(mod(i + 3, 8))))
							&& !map.get(GeoPoint.add(x,
									directions.get(mod(i + 2, 8))))));
	}

	/**
	 * Jump definition to avoid visits of omissible neighbors
	 * 
	 * @param x
	 * @param dir
	 * @param s
	 * @param g
	 * @return int[] if a promising point is found, else 0
	 */
	private int[] jump(int[] x, int[] dir, Node curNode) {
		int[] n;
		while (true) {
			n = GeoPoint.add(x, dir);
			// check for dead end or out of bounds
			if (map.get(n)) {
				Node newNode = new Node(x, curNode);
				if (!closedList.contains(newNode)) {
					closedList.add(newNode);
				}
				return null;
			}

			// check if goal is reached
			if (goal.contains(new HashablePoint(n))) {
				return n;
			}

			// terminate jump in case of forced neighbours
			if (hasForcedNeighbors(n, x)) {
				return n;
			}

			if (GeoPoint.isDiagonal(dir)) {
				if (jump(n, new int[] { 0, dir[1] },
						new Node(n, curNode)) != null) {
					return n;
				}
				if (jump(n, new int[] { dir[0], 0 },
						new Node(n, curNode)) != null) {
					return n;
				}
			}
			x = GeoPoint.add(x, dir);
		}
	}

	private double getH(int[] x, int[] y) {
		return GeoPoint.chebyshev_distance(x, y);
	}

	private double getH(int[] x, Set<HashablePoint> b) {
		double result = 1e5;
		for (HashablePoint y : b) {
			double d = getH(x, y.a);
			if (d < result)
				result = d;
		}
		return result;
	}

	public void set_map(StaticMapGrid map) {
		this.map = map;
	}

	// private double getH(Set<HashablePoint> a, Set<HashablePoint> b) {
	// double result = 1e5;
	// for (HashablePoint x : a) {
	// double d = getH(x.a, b);
	// if (d < result)
	// result = d;
	// }
	// return result;
	// }
}
