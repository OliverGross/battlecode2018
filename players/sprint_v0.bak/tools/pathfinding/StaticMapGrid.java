package tools.pathfinding;

import java.util.HashSet;
import java.util.Set;

import bc.Direction;
import bc.GameController;
import bc.MapLocation;
import bc.PlanetMap;
import tools.DirHelp;

/**
 * An uniform cost grid stored as BitSet. Encoding is equivalent to:
 * [0->accessible vertex; 1->unaccessible vertex / blocked]
 */
public class StaticMapGrid {
	int w;
	int h;
	boolean[][] map;
	int[][][] precomputedMap;
	Set<HashablePoint> obstacles;

	public StaticMapGrid(GameController gc) {
		PlanetMap pmap = gc.startingMap(gc.planet());
		w = (int) pmap.getWidth();
		h = (int) pmap.getHeight();
		map = new boolean[w][h];
		obstacles = new HashSet<HashablePoint>();
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				this.set(x, y, pmap.isPassableTerrainAt(
						new MapLocation(gc.planet(), x, y)) == 0);
				if (this.get(new int[] { x, y })) {
					obstacles.add(new HashablePoint(new int[] { x, y }));
				}
			}
		doPrecomputation();
	}

	public StaticMapGrid(boolean[][] m) {
		w = (int) m.length;
		h = (int) m[0].length;
		map = new boolean[w][h];
		obstacles = new HashSet<HashablePoint>();
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				this.set(x, y, m[x][y]);
				if (this.get(new int[] { x, y })) {
					obstacles.add(new HashablePoint(new int[] { x, y }));
				}
			}
		doPrecomputation();
	}

	public boolean get(int[] p) {
		if (p[0] < w && p[0] >= 0 && p[1] < h && p[1] >= 0) {
			return map[p[0]][p[1]];
		} else {
			return true;
		}
	}

	public boolean get(int x, int y) {
		if (x < w && x >= 0 && y < h && y >= 0) {
			return map[x][y];

		} else {
			return true;
		}
	}

	public void set(int[] p, boolean v) {
		if (p[0] >= w || p[0] < 0 || p[1] >= h || p[1] < 0)
			return;
		map[p[0]][p[1]] = v;
	}

	public void set(int x, int y, boolean v) {
		map[x][y] = v;
	}

	private void setPre(int[] p, Direction dir, int value) {
		if (p[0] >= w || p[0] < 0 || p[1] >= h || p[1] < 0)
			return;
		if (get(p))
			return;
		precomputedMap[p[0]][p[1]][DirHelp.getDirectionIdx(dir)] = value;
		if (value >= 1 && !DirHelp.isDiagonal(dir)) {
			setPre(DirHelp.addDirection(p, DirHelp.addAngle(dir, 3)),
					DirHelp.addAngle(dir, -1), 1);
			setPre(DirHelp.addDirection(p, DirHelp.addAngle(dir, -3)),
					DirHelp.addAngle(dir, 1), 1);
		}
	}

	public int getPre(int[] p, Direction dir) {
		if (p[0] >= w || p[0] < 0 || p[1] >= h || p[1] < 0)
			return -1;
		return precomputedMap[p[0]][p[1]][DirHelp.getDirectionIdx(dir)];
	}

	private void doPrecomputation() {
		// TODO activate precomputation if needed
		return;
		// long t0 = System.nanoTime();
		// precomputedMap = new int[w][h][8];
		// for (int x = 0; x < w; x++)
		// for (int y = 0; y < h; y++)
		// for (int d = 0; d < 8; d++)
		// precomputedMap[x][y][d] = -1;
		// precomputeInitials();
		// precomputeRestBasedOnInitials();
		// long t1 = System.nanoTime();
		// System.out.println("Precomputation took " + (t1 - t0) * 1e-6 +
		// "ms!");
	}

	private String visualizeMap() {
		String str = "";
		for (int y = h - 1; y >= 0; y--) {
			for (int x = 0; x < w; x++) {
				str += get(x, y) ? '#' : '.';
			}
			str += '\n';
		}
		return str;
	}

	private String visualizePrecomputed() {
		char[][] res = new char[4 * w][4 * h];
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int[] c = new int[] { 4 * x + 1, 4 * y + 1 };
				for (Direction d : DirHelp.adjacentDirections) {
					int[] cur = DirHelp.addDirection(c, d);
					int pre = getPre(new int[] { x, y }, d);
					res[cur[0]][cur[1]] = pre == -1 ? ' '
							: (char) ('0' + pre % 10);
				}
				res[x * 4 + 3][(h - y - 1) * 4 + 3] = '+';
				res[x * 4 + 3][(h - y - 1) * 4 + 2] = '|';
				res[x * 4 + 3][(h - y - 1) * 4 + 1] = '|';
				res[x * 4 + 3][(h - y - 1) * 4 + 0] = '|';
				res[x * 4 + 2][(h - y - 1) * 4 + 3] = '-';
				res[x * 4 + 1][(h - y - 1) * 4 + 3] = '-';
				res[x * 4 + 0][(h - y - 1) * 4 + 3] = '-';
				res[c[0]][c[1]] = get(x, y) ? '#' : '.';
			}
		}
		String result = "";
		for (int y = 0; y < res[0].length - 1; y++) {
			for (int x = 0; x < res.length - 1; x++) {
				result += res[x][y];
			}
			result += '\n';
		}
		return result;
	}

	private void precomputeInitials() {
		for (HashablePoint obstacle : obstacles) {
			for (Direction d : DirHelp.adjacentDirections) {
				setPreInitial(obstacle.a, d);
			}
		}
	}

	private void setPreInitial(int[] p, Direction dir) {
		int passDiff = 1;
		int hiddenDiff = -1;
		if (!DirHelp.isDiagonal(dir)) {
			passDiff = 2;
			hiddenDiff = 0;
		}
		for (int i = -1; i <= 1; i += 2) {
			Direction income = DirHelp.addAngle(dir, 3 * i);
			Direction pass = DirHelp.addAngle(dir, passDiff * i);
			Direction hidden = DirHelp.addAngle(dir, hiddenDiff * i);
			if (!get(DirHelp.addDirection(p, income))
					&& !get(DirHelp.addDirection(p, pass))
					&& !get(DirHelp.addDirection(p, hidden))) {
				setPre(DirHelp.addDirection(p, income), dir, 1);
			}
		}
	}

	private int preJump(int[] p, Direction d) {
		if (getPre(p, d) == 1) {
			return 1;
		}
		if (get(p)) {
			return -1;
		}
		int recResult = preJump(DirHelp.addDirection(p, d), d);
		if (recResult == -1)
			return -1;
		return 1 + recResult;
	}

	private void precomputeRestBasedOnInitials() {
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++)
				for (Direction d : DirHelp.orthogonalDirections) {
					setPre(new int[] { x, y }, d,
							preJump(new int[] { x, y }, d));
				}
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++)
				for (Direction d : DirHelp.diagonalDirections) {
					setPre(new int[] { x, y }, d,
							preJump(new int[] { x, y }, d));
				}
	}
}
