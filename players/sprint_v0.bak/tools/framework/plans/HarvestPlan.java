package tools.framework.plans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.PlanetMap;
import bc.Unit;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class HarvestPlan extends Plan {
	public List<Integer> w_ids;
	long[][] karboniteLocations;
	long width;
	long height;

	public HarvestPlan(WorldState worldstate, List<Integer> worker_ids) {
		// ToDo pass "safe" karbonite locations
		super(worldstate);
		w_ids = worker_ids;
		//System.out.println("workers for harvest plan:" + worker_ids);
		
		init(worldstate);
	}
	
	private void init(WorldState worldstate) {
		width = worldstate.gc.startingMap(worldstate.gc.planet()).getWidth();
		height = worldstate.gc.startingMap(worldstate.gc.planet()).getHeight();
		karboniteLocations = new long[(int) width][(int) height];
		PlanetMap initialmap = worldstate.gc.startingMap(worldstate.gc.planet());
		MapLocation loc = new MapLocation(worldstate.gc.planet(), 0, 0);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				loc.setX(x);
				loc.setY(y);
				karboniteLocations[x][y] = initialmap.initialKarboniteAt(loc);
				//System.out.println(x + "," + y + " = " + initialmap.initialKarboniteAt(loc));
			}
		}
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		if(status == Status.FINAL || status == Status.FAILED) return;
		update(ws);
		for(int i=0; i<w_ids.size(); i++) {
			Unit worker = ws.gc.unit(w_ids.get(i));
			MapLocation goal = null;
			if(worker.location().isInGarrison() || worker.location().isInSpace()) continue;
			HashSet<MapLocation> nearestKarbonite = findNearestKarbonite(worker.location().mapLocation(), ws);
			//this code is fucking messy.. sorry
			if(nearestKarbonite.isEmpty()) {
				//System.out.println("no nearest karbonite found!");
			} else {
				for(MapLocation ml : nearestKarbonite) {
					if(ws.gc.canSenseLocation(ml)) {
						karboniteLocations[ml.getX()][ml.getY()] = ws.gc.karboniteAt(ml);
					}
				}
				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(worker.location().mapLocation(), 65, ws.opp);
				//if not in karbonitelocation, first avoid enemies, if there are none move ahead
				if(nearestKarbonite.iterator().next().distanceSquaredTo(worker.location().mapLocation()) > 2) {
					if(enemies.size() != 0) {
						kiteEnemies(enemies, worker, ws);
					}
					//if you did not move to kite (for example vs a worker) you can move further
					ws.lll.goTo(worker.id(), nearestKarbonite);
				}
				//now that you are near (possibly moved further) check if you can harvest
				for(MapLocation l : nearestKarbonite) {
					if(l.distanceSquaredTo(worker.location().mapLocation()) <= 2) {
						//you can harvest
						goal = l;
						Direction harvDir = worker.location().mapLocation().directionTo(goal);
						if(worker.workerHasActed()==0 && ws.gc.canHarvest(worker.id(), harvDir)) {
							//System.out.println("harvest! worker: " + worker.id());
							ws.gc.harvest(worker.id(), harvDir);
						}
					}
				}
				//if you stood still before, but there are enemies nearby. now kite!
				if(enemies.size() != 0) {
					kiteEnemies(enemies, worker, ws);
				}
			}
		}
		this.status = Status.RUNNING;
	}
	
	private void kiteEnemies(VecUnit enemies, Unit worker, WorldState ws) {
		for(int e=0; e<enemies.size(); e++) {
			Unit enemy = enemies.get(e);
			switch(enemy.unitType()) {
			case Factory: continue;
			case Healer: continue;
			case Knight: 
			case Mage:
			case Ranger:
				Direction dir = worker.location().mapLocation().directionTo(enemy.location().mapLocation());
				Direction kite = ws.lll.kite(worker.id(), dir);
				if(ws.gc.canMove(worker.id(), kite) && ws.gc.isMoveReady(worker.id()))
					ws.gc.moveRobot(worker.id(), kite);
				return;
			case Rocket: continue;
			case Worker: continue;
			}
		}
	}

	private void update(WorldState ws) {
		ArrayList<Integer> newList = new ArrayList<>();
		for(int i=0; i<w_ids.size(); i++) {
			if(ws.gc.canSenseUnit(w_ids.get(i))) {
				newList.add(w_ids.get(i));
			}
		}
		w_ids = newList;
	}

	private HashSet<MapLocation> findNearestKarbonite(MapLocation mapLocation, WorldState ws) {
		//System.out.println("findNearest: " + mapLocation.getX() + "," + mapLocation.getY());
		int x = mapLocation.getX();
		int y = mapLocation.getY();
		if(karboniteLocations[x][y] > 0) {
			HashSet<MapLocation> r = new HashSet<>();
			r.add(mapLocation);
			return r;
		}
		HashSet<MapLocation> loc = findNearestKarbonite(mapLocation, 1, ws);
		return loc;
	}
	
	private boolean onMap(MapLocation loc) {
		int x = loc.getX();
		int y = loc.getY();
		if(x<0 || x>=width || y<0 || y>=height) return false;
		else return true;
	}
	
	//zeichnet ein Quadrat mit laenge und breite level+level+1 und mittelpunkt mapLocation und prueft auf karbonit
	private HashSet<MapLocation> findNearestKarbonite(MapLocation mapLocation, int level, WorldState ws) {
		//System.out.println("Level " + level);
		MapLocation currentLoc = mapLocation.addMultiple(Direction.South, level);
		HashSet<MapLocation> ret = new HashSet<>();
		if(isAnyKarboniteAt(currentLoc)) ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
		for(int i=0; i<level; i++) {
			currentLoc = currentLoc.add(Direction.West);
			//System.out.println("check: " + currentLoc.getX() + "," + currentLoc.getY());
			if(isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
				//System.out.println("founc something on " + currentLoc.getX() + "," + currentLoc.getY());
			}
		}
		for(int i=0; i<(level+level); i++) {
			currentLoc = currentLoc.add(Direction.North);
			//System.out.println("check: " + currentLoc.getX() + "," + currentLoc.getY());
			if(isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
				//System.out.println("founc something on " + currentLoc.getX() + "," + currentLoc.getY());
			}
		}
		for(int i=0; i<(level+level); i++) {
			currentLoc = currentLoc.add(Direction.East);
			//System.out.println("check: " + currentLoc.getX() + "," + currentLoc.getY());
			if(isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
				//System.out.println("founc something on " + currentLoc.getX() + "," + currentLoc.getY());
			}
		}
		for(int i=0; i<(level+level); i++) {
			currentLoc = currentLoc.add(Direction.South);
			//System.out.println("check: " + currentLoc.getX() + "," + currentLoc.getY());
			if(isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
				//System.out.println("founc something on " + currentLoc.getX() + "," + currentLoc.getY());
			}
		}
		for(int i=0; i<(level-1); i++) {
			currentLoc = currentLoc.add(Direction.West);
			//System.out.println("check: " + currentLoc.getX() + "," + currentLoc.getY());
			if(isAnyKarboniteAt(currentLoc)) {
				ret.add(new MapLocation(ws.gc.planet(), currentLoc.getX(), currentLoc.getY()));
				//System.out.println("founc something on " + currentLoc.getX() + "," + currentLoc.getY());
			}
		}
		//TODO is a magicnumber
		if(ret.isEmpty() && level < width/2) {
			//System.out.println("found nothing on level " + level);
			return findNearestKarbonite(mapLocation, level+1, ws);
		} else {
			return ret;
		}
	}

	private boolean isAnyKarboniteAt(MapLocation currentLoc) {
		if(onMap(currentLoc)) {
			return (karboniteAt(currentLoc) > 0);
		} else {
			return false;
		}
	}
	
	private long karboniteAt(MapLocation currentLoc) {
		return karboniteLocations[currentLoc.getX()][currentLoc.getY()];
	}

}
