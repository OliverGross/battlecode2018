package tools.framework.plans;

import java.util.ArrayList;
import java.util.Iterator;

import bc.*;
import tools.DirHelp;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class FFactoryBuildPlan extends Plan {
	public int savedKarbonite;
	public boolean replicated;
	public int finishedFactories = 0;
	public int maxFactories;

	ArrayList<Integer> workerResources = new ArrayList<Integer>();
	ArrayList<Integer> factoryResources = new ArrayList<Integer>();

	MapLocation prototype_loc = null;

	public FFactoryBuildPlan(WorldState worldState, ArrayList<Integer> workers,
			boolean tryReplication, int maxNumberFactories) {
		super(worldState);
		workerResources = workers;
		savedKarbonite = 0;
		replicated = !tryReplication;
		maxFactories = maxNumberFactories;
	}

	public void updateResources(WorldState ws) {
		ArrayList<Integer> removeList = new ArrayList<>();
		for(int i=0; i<workerResources.size(); i++) {
			if (!ws.gc.canSenseUnit(workerResources.get(i))) {
				removeList.add(workerResources.get(i));
			}
		}
		workerResources.removeAll(removeList);
		removeList.clear();
		for(int i=0; i<factoryResources.size(); i++) {
			int id = factoryResources.get(i);
			if(!ws.gc.canSenseUnit(id)) {
				removeList.add(id);
			} else {
				if(ws.gc.unit(id).structureIsBuilt()==1) {
					finishedFactories++;
					removeList.add(id);
				}
			}
		}
		factoryResources.removeAll(removeList);
	}

	public boolean replicate(WorldState ws, int worker_id) {
		MapLocation workerLoc;
		for (Direction d : DirHelp.adjacentDirections) {
			if (ws.gc.canReplicate(worker_id, d)) {
				ws.gc.replicate(worker_id, d);
				workerLoc = ws.gc.unit(worker_id).location().mapLocation()
						.add(d);
				if (ws.gc.senseUnitAtLocation(workerLoc) != null)
					workerResources
							.add(ws.gc.senseUnitAtLocation(workerLoc).id());
				return true;
			}
		}
		return false;
	}

	@Override
	public void tick(WorldState ws) {
		super.tick(ws);
		updateResources(ws);
		if (finishedFactories >= maxFactories || workerResources.size() == 0) {
			status = Status.FINAL;
			return;
		}
		
		int factorycost = (int) bc.costOf(UnitType.Factory,
				ws.gc.researchInfo().getLevel(UnitType.Factory));

		if (prototype_loc != null) {
			factoryResources.add(ws.gc.senseUnitAtLocation(prototype_loc).id());
		}

		for (int structure_id : factoryResources) {
			assert savedKarbonite == 0;
			for (int worker_id : workerResources) {
				if (ws.gc.canBuild(worker_id, structure_id))
					ws.gc.build(worker_id, structure_id);
			}
		}
		for (int worker_id : workerResources) {
			if (!replicated) {
				replicated = replicate(ws, worker_id);
				break;
			}
		}

		for (int worker_id : workerResources) {
			Direction dir = Direction.Center;
			for (Direction d : DirHelp.adjacentDirections) {
				if (ws.gc.canBlueprint(worker_id, UnitType.Factory, d)) {
					dir = d;
					break;
				}
			}
			
			if (dir == Direction.Center)
				return;
			if (ws.availableKarbonite() + savedKarbonite >= factorycost && (finishedFactories+factoryResources.size())<maxFactories ) {
				ws.gc.blueprint(worker_id, UnitType.Factory,
						dir);
				prototype_loc = ws.gc.unit(worker_id).location()
						.mapLocation().add(dir);
				factoryResources.add(ws.gc.senseUnitAtLocation(prototype_loc).id());
				prototype_loc = null;

			}
			savedKarbonite -= ws.spendSavings(savedKarbonite);
			status = Status.RUNNING;
		}
		if (status != Status.RUNNING) {
			savedKarbonite += ws.saveKarbonite(factorycost);
		}
	}

}
