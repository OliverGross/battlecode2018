package tools.framework.evaluators;

import tools.framework.Evaluator;
import tools.framework.Plan;
import tools.framework.plans.FactoryBuildPlan;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.KnightsAttackPlan;
import tools.framework.plans.KnightsBuildPlan;
import tools.framework.plans.ReplicationPlan;

public class PlanEvaluator extends Evaluator {

	public PlanEvaluator(double priority) {
		super(priority);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getEvaluation(Plan plan) {
		// TODO Auto-generated method stub
		if (plan.getClass() == FactoryBuildPlan.class)
			return 4.0;
		if (plan.getClass() == KnightsBuildPlan.class)
			return 8.0;
		if (plan.getClass() == ReplicationPlan.class)
			return 2.0;
		if (plan.getClass() == KnightsAttackPlan.class)
			return 8.0;
		if(plan.getClass() == HarvestPlan.class) {
			return 2.0;
		}
		return 0;
	}

}
