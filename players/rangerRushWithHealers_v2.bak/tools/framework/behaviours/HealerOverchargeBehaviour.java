package tools.framework.behaviours;

import java.util.ArrayList;

import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.HealerPlan;

public class HealerOverchargeBehaviour extends Behaviour {

	HealerPlan plan;

	public HealerOverchargeBehaviour(HealerPlan plan) {
		super();
		this.plan = plan;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(WorldState ws) {
		if (ws.gc.researchInfo().getLevel(UnitType.Healer) < 3)
			return;

		for (int healerId : plan.getHealersOnMap()) {
			Unit healer = ws.gc.unit(healerId);
			if (!healer.location().isOnMap())
				continue;

			if (healer.abilityHeat() < 10) {
				VecUnit nearUnits = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), healer.abilityRange(),
						ws.me);
				ArrayList<Unit> combatUnits = new ArrayList<>();
				for (int i = 0; i < nearUnits.size(); i++) {
					Unit currentUnit = nearUnits.get(i);
					if (currentUnit.unitType() == UnitType.Factory
							|| currentUnit.unitType() == UnitType.Worker
							|| currentUnit.unitType() == UnitType.Healer
							|| currentUnit.unitType() == UnitType.Rocket) continue;
					
					combatUnits.add(currentUnit);
				}
				
				Unit bestTarget = null;
				for(Unit unit : combatUnits) {
					
				}

				// for each nearbyCombatUnit
				// check heats
				// check if enemy is in striking distance

			}
		}
	}

}
