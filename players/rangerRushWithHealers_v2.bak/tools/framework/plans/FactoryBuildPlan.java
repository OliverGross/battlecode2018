package tools.framework.plans;

import bc.*;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class FactoryBuildPlan extends Plan {
	public int savedKarbonite;
	public int blueprint_id = -1;
	public int worker_id = -1;

	public FactoryBuildPlan(WorldState worldState) {
		super(worldState);
		savedKarbonite = 0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		int factorycost = (int) bc.costOf(UnitType.Factory,
				ws.gc.researchInfo().getLevel(UnitType.Factory));
		if (blueprint_id >= 0) {
			assert worker_id >= 0;
			assert savedKarbonite == 0;
			if (ws.gc.canBuild(worker_id, blueprint_id))
				ws.gc.build(worker_id, blueprint_id);
			else
				status = Status.FINAL;
			return;
		}
		worker_id = -1;
		for (int i = 0; i < ws.gc.myUnits().size(); i++) {
			if (ws.gc.myUnits().get(i).unitType() == UnitType.Worker) {
				worker_id = ws.gc.myUnits().get(i).id();
				break;
			}
		}
		if (worker_id == -1)
			return;
		Direction dir = Direction.Center;
		for (Direction d : Direction.values()) {
			if (ws.gc.canBlueprint(worker_id, UnitType.Factory, d)) {
				dir = d;
				break;
			}
		}
		if (dir == Direction.Center)
			return;
		if (ws.availableKarbonite() + savedKarbonite >= factorycost) {
			ws.gc.blueprint(worker_id, UnitType.Factory, dir);
			blueprint_id = ws.gc.senseUnitAtLocation(
					ws.gc.unit(worker_id).location().mapLocation().add(dir))
					.id();
			savedKarbonite -= ws.spendSavings(savedKarbonite);
			status = Status.RUNNING;
		} else {
			savedKarbonite += ws.saveKarbonite(factorycost);
		}
		System.out.println("Saved karbonite(factory):" + savedKarbonite + "/"
				+ factorycost);
	}
}
