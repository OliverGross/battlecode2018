package tools.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.plans.LandingSiteExplorerPlan;

public class ResourceState {
	/**
	 * Assignment of UnitIds to PlanIds
	 */
	HashMap<Integer, Integer> unitPlan = new HashMap<Integer, Integer>();
	/**
	 * Storage of Units Types
	 */
	HashMap<Integer, UnitType> unitType = new HashMap<Integer, UnitType>();
	/**
	 * Storing a temporary blocking
	 */
	HashMap<Integer, Integer> unitRoundsTillFreedom = new HashMap<Integer, Integer>();

	/**
	 * WorldState
	 */
	WorldState ws;

	public ResourceState(WorldState worldState) {
		ws = worldState;
		update();
	}

	/**
	 * Handles deaths and newly generated units
	 * 
	 * @param ws(WorldState)
	 */
	public void update() {
		addUnits(ws.gc.units());
		removeAbsent(ws);
		tickTemporaryBlocks();
		karboniteUpdate();
	}

	//Karbonite
	HashMap<Integer, Long> karboniteSaved = new HashMap<>();
	
	private void karboniteUpdate() {
		karboniteSaved.clear();
	}
	
	public void saveKarbonite(long amount, int planId) {
		long currSaved = 0;
		if(karboniteSaved.containsKey(planId)) {
			currSaved = karboniteSaved.get(planId);
		}
		currSaved += amount;
		karboniteSaved.put(planId, currSaved);
	}
	
	public long getAvailableKarbonite(int planId) {
		long currAvailable = ws.gc.karbonite();
		for(int i : karboniteSaved.keySet()) {
			if(i != planId) {
				currAvailable -= karboniteSaved.get(i);
			}
		}
		return currAvailable;
	}

	private void addUnits(VecUnit units) {
		for (int i = 0; i < units.size(); i++) {
			addUnit(units.get(i));
		}
	}

	private void addUnit(Unit unit) {
		if (unit.team() == ws.me)
			addMyUnit(unit);
	}

	private void addMyUnit(Unit unit) {
		// resource state vectors
		if (unitPlan.putIfAbsent(unit.id(), 0) == null) {
			unitType.put(unit.id(), unit.unitType());
			unitRoundsTillFreedom.put(unit.id(), -1);
		}
	}

	/**
	 * Try to sense unit, otherwise delete all occurrences.
	 * 
	 * @param ws
	 */
	private void removeAbsent(WorldState ws) {
		for (Iterator<HashMap.Entry<Integer, Integer>> it = unitPlan.entrySet()
				.iterator(); it.hasNext();) {
			int id = it.next().getKey();
			if (!ws.gc.canSenseUnit(id)) {
				it.remove();
				unitType.remove(Integer.valueOf(id));
				unitRoundsTillFreedom.remove(Integer.valueOf(id));
			}
		}

	}

	/**
	 * Update the rounds left for block
	 */
	private void tickTemporaryBlocks() {
		for (HashMap.Entry<Integer, Integer> entry : unitRoundsTillFreedom
				.entrySet()) {
			if (entry.getValue() == 0) {
				unitPlan.put(entry.getKey(), 0);
			}
			entry.setValue(entry.getValue() - 1);
		}
	}

	/**
	 * Return all units associated with a specific plan
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlan(int pid) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			if (entry.getValue() == pid)
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all units associated with a plan and a certain type.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlanAndType(int pid, UnitType type) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			if (entry.getValue() == pid
					&& unitType.get(entry.getKey()).equals(type))
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all unassigned units.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getAllUnits() {
		return getUnitsByPlan(0);
	}

	/**
	 * Return all unassigned units of certain type.
	 * 
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getFreeUnitsByType(UnitType type) {
		return getUnitsByPlanAndType(0, type);
	}

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeToPlan(int pid) {
		assignFreeToPlan(pid, -1);
	}

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeToPlan(int pid, int rounds) {
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			if (entry.getValue() == 0) {
				entry.setValue(pid);
				unitRoundsTillFreedom.put(entry.getValue(), rounds);
			}
		}
	}

	/**
	 * Assign all unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignAllFreeByType(int pid, UnitType type) {
		assignFreeByType(pid, type, -1);
	}

	/**
	 * Assign all unassigned units of specified type to given plan for specified
	 * time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignFreeByType(int planId, UnitType type, int rounds) {
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			if (entry.getValue() == 0
					&& unitType.get(entry.getKey()).equals(type)) {
				entry.setValue(planId);
				unitRoundsTillFreedom.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign maximum of max unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignSomeFreeByType(int pid, UnitType type, int maxAssigned) {
		assignSomeFreeByType(pid, type, -1, maxAssigned);
	}

	/**
	 * Assign maximum of max unassigned units of specified type to given plan
	 * for specified time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignSomeFreeByType(int pid, UnitType type, int rounds,
			int max) {
		int assignments = 0; // check for max
		for (Iterator<HashMap.Entry<Integer, Integer>> it = unitPlan.entrySet()
				.iterator(); it.hasNext() && assignments < max;) {
			HashMap.Entry<Integer, Integer> entry = it.next();
			if (entry.getValue() == 0
					&& unitType.get(entry.getKey()).equals(type)) {
				assignments++;
				entry.setValue(pid);
				unitRoundsTillFreedom.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign all resource_ids to the plan
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByIds(ArrayList<Integer> resource_ids, int pid) {
		return assignByIds(resource_ids, pid, -1);
	}

	/**
	 * Assign all resource_ids to the plan for a given time
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByIds(ArrayList<Integer> resource_ids, int pid,
			int rounds) {
		boolean success = true;
		for (int id : resource_ids) {
			success = success && assignById(id, pid, rounds);
		}
		return success;
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignById(int id, int pid) {
		return assignById(id, pid, -1);
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignById(int id, int pid, int rounds) {
		// is already known?
		if (unitPlan.containsKey(id)) {
			if (unitPlan.get(id) == 0) {
				unitPlan.put(id, pid);
				unitRoundsTillFreedom.put(id, rounds);
				return true;
			} else {
				return false;
			}
		}
		// check if newly generated
		else if (ws.gc.canSenseUnit(id)) {
			addUnit(ws.gc.unit(id));
			return assignById(id, pid, rounds);
		} else {
			return false;
		}
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param unit
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByUnit(Unit unit, int pid) {
		return assignByUnit(unit, pid, -1);
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignByUnit(Unit unit, int pid, int rounds) {
		// is already known?
		if (unitPlan.containsKey(unit.id())) {
			if (unitPlan.get(unit.id()) == 0) {
				unitPlan.put(unit.id(), pid);
				unitRoundsTillFreedom.put(unit.id(), rounds);
				return true;
			} else {
				return false;
			}
		}
		// check if newly generated
		else {
			addUnit(unit);
			return assignById(unit.id(), pid, rounds);
		}
	}

	/**
	 * Deletes all assignment of units to plans.
	 */
	public void resetAllAssignments() {
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			entry.setValue(0);
		}
	}

	/**
	 * Deletes all assignments to specified plans.
	 * 
	 * @param pid
	 */
	public void freeUnitsFromPlan(int pid) {
		for (HashMap.Entry<Integer, Integer> entry : unitPlan.entrySet()) {
			if (entry.getValue() == pid)
				entry.setValue(0);
		}
	}

	/**
	 * Deletes assignment of given unit.
	 * 
	 * @param uid
	 */
	public void freeUnitById(int uid) {
		if (unitPlan.containsKey(uid)) {
			unitPlan.put(uid, 0);
		}
	}

	private int rocketLocationCounter = 0;
	
	public MapLocation getNextRocketLocation() {
		MapLocation loc = LandingSiteExplorerPlan.getLocFromTeamArray(ws, rocketLocationCounter);
		rocketLocationCounter++;
		return loc;
	}
}
