package tools.randomstuff;

import bc.Unit;
import bc.UnitType;

public class RandomMathTools {
	
	public static boolean doesKill(Unit unit, int damage, int health) {
		if(unit.unitType() == UnitType.Knight) {
			int realdamage = damage;
			switch((int)unit.researchLevel()) {
			case(0): realdamage -= 5;
				break;
			case(1): realdamage -= 10;
				break;
			case(2): realdamage -= 15;
				break;
			}
			return (realdamage>=health);
		} else {
			return (damage>=health);
		}
	}
	
}
