import java.util.ArrayList;
import java.util.HashSet;

import bc.MapLocation;
import bc.Planet;
import bc.Team;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.PGA;
import tools.framework.EvaluatorPlan;
import tools.framework.WorldState;
import tools.framework.buildorder.RangerRushBuildOrder;
import tools.framework.buildorder.StructureBuildOrder;
import tools.framework.evaluators.PlanEvaluator;
import tools.framework.plans.HarvestPlan;
import tools.framework.plans.HealerPlan;
import tools.framework.plans.LandingSiteExplorerPlan;
import tools.framework.plans.MarsReplicatePlan;
import tools.framework.plans.MasterBuildPlan;
import tools.framework.plans.MasterStructureBuildPlan;
import tools.framework.plans.RangerAttackPlan;
import tools.framework.plans.ReplicatePlan;
import tools.framework.plans.SquadToMarsPlan;

public class RangerRushMasterPlan extends EvaluatorPlan {

	public RangerRushMasterPlan(WorldState worldState) {
		super(worldState);
		if (worldState.gc.planet().equals(Planet.Earth)) {
			VecUnit initial = worldState.gc.startingMap(Planet.Earth)
					.getInitial_units();
			PGA preGame = worldState.pga;
			Team myTeam = worldState.gc.team();
			HashSet<MapLocation> myWorkers = new HashSet<MapLocation>();
			HashSet<MapLocation> oppWorkers = new HashSet<MapLocation>();
			for (int i = 0; i < initial.size(); i++) {
				Unit worker = initial.get(i);
				if (worker.team().equals(myTeam)) {
					myWorkers.add(worker.location().mapLocation());
				} else {
					oppWorkers.add(worker.location().mapLocation());
				}
			}
			MapLocation closest = worldState.pathfinding
					.getNearestStart(myWorkers, oppWorkers);
			ArrayList<Integer> closestWorker = new ArrayList<Integer>();
			if (closest != null) {
				closestWorker
						.add(worldState.gc.senseUnitAtLocation(closest).id());
			} else {
				closestWorker.add(worldState.gc
						.senseUnitAtLocation(myWorkers.iterator().next()).id());
			}
			// see bellow for an example on how to replicate
			//System.out.println("I want " + preGame.magicReplicate + "additional workers");
			plans.add(new ReplicatePlan(worldState, preGame.replicateWorker,
					preGame.magicReplicate));

			plans.add(new MasterBuildPlan(worldState,
					new RangerRushBuildOrder()));
			plans.add(new MasterStructureBuildPlan(worldState,
					new StructureBuildOrder(), closestWorker));
			plans.add(new SquadToMarsPlan(worldState));

			ArrayList<Integer> harvestWorkers = new ArrayList<>();
			for (int i = 0; i < worldState.gc.myUnits().size(); i++) {
				Unit w = worldState.gc.myUnits().get(i);
				if (closestWorker.contains(w.id()))
					continue;
				else
					harvestWorkers.add(w.id());
			}
			plans.add(new HarvestPlan(worldState, preGame.karbLocs));
		} else {
			// Mars-only plans
			plans.add(new LandingSiteExplorerPlan(worldState));
			plans.add(new MarsReplicatePlan(worldState));
		}
		plans.add(new HealerPlan(worldState));
		plans.add(new RangerAttackPlan(worldState));
		addEvaluator(new PlanEvaluator(1.0));

		worldState.gc.queueResearch(UnitType.Ranger);
		worldState.gc.queueResearch(UnitType.Healer);
		worldState.gc.queueResearch(UnitType.Rocket);
		worldState.gc.queueResearch(UnitType.Healer);
		worldState.gc.queueResearch(UnitType.Ranger);
		worldState.gc.queueResearch(UnitType.Rocket);

	}

	@Override
	public void modifyPlans(WorldState worldState) {

	}

}
