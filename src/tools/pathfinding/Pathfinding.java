package tools.pathfinding;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import tools.framework.WorldState;

public class Pathfinding {

	public static long computationTimeNs = 0;

	public WorldState ws;
	public JPS jps;
	public static int runningInstances = 0;

	Map<Integer, LocalPathFinder> savedPaths = new HashMap<Integer, LocalPathFinder>();

	public Pathfinding(WorldState ws) {
		if (runningInstances >= 1)
			throw new RuntimeException(
					"There should only be one Pathfinding instance!");
		runningInstances += 1;
		this.ws = ws;
		jps = new JPS(new StaticMapGrid(ws.gc));
	}

	// public void setObstacles(boolean impassable,
	// Set<HashablePoint> leaveUnchanged) {
	// VecUnit units = ws.gc.units();
	// for (int i = 0; i < units.size(); i++) {
	// HashablePoint loc = new HashablePoint(
	// units.get(i).location().mapLocation());
	// if (!leaveUnchanged.contains(loc))
	// jps.map.setDynamic(loc.point, impassable);
	// }
	// }

	// public Set<HashablePoint> getObstacles() {
	// VecUnit units = ws.gc.units();
	// Set<HashablePoint> result = new HashSet<HashablePoint>();
	// for (int i = 0; i < units.size(); i++) {
	// MapLocation loc = units.get(i).location().mapLocation();
	// result.add(new HashablePoint(new int[] { loc.getX(), loc.getY() }));
	// }
	// return result;
	// }

	// public Direction getNextDirection(MapLocation start,
	// Set<MapLocation> goals) {
	// Set<HashablePoint> targets = new HashSet<HashablePoint>();
	// for (MapLocation loc : goals) {
	// targets.add(new HashablePoint(loc));
	// }
	// setObstacles(true, targets);
	// Set<HashablePoint> starts = new HashSet<HashablePoint>();
	// starts.add(new HashablePoint(start));
	// List<HashablePoint> path = jps.getPath(starts, targets);
	// if (path.size() == 1) {
	// System.err.println("No path was found!");
	// return null;
	// }
	// int[] dir = Geometry.diff(path.get(1), path.get(0));
	// int d = Geometry.chebyshev_distance(dir);
	// dir[0] /= d;
	// dir[1] /= d;
	// Direction result = start.directionTo(new MapLocation(start.getPlanet(),
	// start.getX() + dir[0], start.getY() + dir[1]));
	// setObstacles(false, targets);
	// return result;
	// }

	// public Direction getNextDirection(MapLocation start, Set<MapLocation>
	// goals,
	// LocalPathFinder lpf) {
	// Set<HashablePoint> targets = new HashSet<HashablePoint>();
	// for (MapLocation loc : goals) {
	// targets.add(new HashablePoint(loc));
	// }
	// return lpf.getNextDirection(new HashablePoint(start), targets);
	// }

	public MapLocation getNearestStart(Set<MapLocation> starts,
			Set<MapLocation> goals) {
		long t0 = System.nanoTime();
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		for (MapLocation loc : goals) {
			targets.add(new HashablePoint(loc));
		}
		Set<HashablePoint> startingPoints = new HashSet<HashablePoint>();
		for (MapLocation loc : starts) {
			startingPoints.add(new HashablePoint(loc));
		}
		List<HashablePoint> path = jps.getPath(startingPoints, targets);
		long t1 = System.nanoTime();
		computationTimeNs += t1 - t0;
		if (path.size() == 1) {
			return null;
		} else {
			return new MapLocation(ws.gc.planet(), path.get(0).get(0),
					path.get(0).get(1));
		}
	}

	// public Direction getNextDirection(MapLocation start, MapLocation goal) {
	// Set<HashablePoint> targets = new HashSet<HashablePoint>();
	// targets.add(new HashablePoint(new int[] { goal.getX(), goal.getY() }));
	// setObstacles(true, targets);
	// List<HashablePoint> path = jps.getPath(castMapLoc(start),
	// castMapLoc(goal));
	// setObstacles(false, targets);
	// return getDirection(path);
	// }

	public int getDistance(List<MapLocation> me, MapLocation enem) {
		long t0 = System.nanoTime();
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		for (MapLocation m : me) {
			starts.add(new HashablePoint(m));
		}
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		targets.add(new HashablePoint(enem));
		List<HashablePoint> path = jps.getPath(starts, targets);
		long t1 = System.nanoTime();
		computationTimeNs += t1 - t0;
		return path.size() - 1;
	}

	public int getOpenPathDistance(List<MapLocation> me, MapLocation enem) {
		long t0 = System.nanoTime();
		Set<HashablePoint> starts = new HashSet<HashablePoint>();
		for (MapLocation m : me) {
			starts.add(new HashablePoint(m));
		}
		Set<HashablePoint> targets = new HashSet<HashablePoint>();
		targets.add(new HashablePoint(enem));
		List<HashablePoint> path = jps.getPath(starts, targets);
		long t1 = System.nanoTime();
		computationTimeNs += t1 - t0;
		if (path.get(path.size() - 1).equals(targets.iterator().next()))
			return path.size() - 1;
		return -1;
	}

	public void goTo(int unit_id, List<MapLocation> goalList) {
		Set<HashablePoint> goals = new HashSet<HashablePoint>();
		for (MapLocation ml : goalList)
			goals.add(new HashablePoint(ml));
		goTo(unit_id, goals);
	}

	public void goTo(int unit_id, MapLocation location) {
		goTo(unit_id, Arrays.asList(location));
	}

	public void goTo(int unit_id, Set<HashablePoint> locations) {
		if (!ws.gc.isMoveReady(unit_id))
			return;
		Direction dir = getNextDirection(unit_id, locations);
		if (dir == null) {
			dir = Direction.Center;
		}
		if (ws.gc.canMove(unit_id, dir)) {
			ws.gc.moveRobot(unit_id, dir);
		}
	}
	// public static Direction getDirection(List<HashablePoint> path) {
	// if (path.size() == 1)
	// return Direction.Center;
	// return DirHelp.getDirection(Geometry.diff(path.get(1), path.get(0)));
	// }

	public Direction getNextDirection(int unit_id, List<MapLocation> goalList) {
		Set<HashablePoint> goals = new HashSet<HashablePoint>();
		for (MapLocation ml : goalList)
			goals.add(new HashablePoint(ml));
		return getNextDirection(unit_id, goals);
	}

	public Direction getNextDirection(int unit_id, Set<HashablePoint> goals) {
		long t0 = System.nanoTime();
		LocalPathFinder lpf = savedPaths.get(unit_id);
		if (lpf == null) {
			lpf = new LocalPathFinder(jps);
			savedPaths.put(unit_id, lpf);
		}
		Direction result = lpf.getNextDirection(
				new HashablePoint(ws.getMLoc(unit_id)), goals);
		long t1 = System.nanoTime();
		computationTimeNs += t1 - t0;
		return result;
	}

	public Direction getNextDirection(int unit_id, MapLocation goal) {
		return getNextDirection(unit_id, Arrays.asList(goal));
	}

	public void updateObstacles() {
		long t0 = System.nanoTime();
		Set<Integer> obstacleIds = new HashSet<Integer>();
		// structures first, they are easy, always obstacles
		for (int uid : ws.unitLists.structures)
			if (ws.gc.canSenseUnit(uid))
				obstacleIds.add(uid);
		// for (int uid : ws.unitLists.lostStructures)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, false);
		// for (int uid : ws.unitLists.newStructures)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, true);
		// handle enemy robots as obstacles as well, because you dont know how
		// they move
		for (int uid : ws.unitLists.enemyRobots)
			if (ws.gc.unit(uid).location().isOnMap())
				obstacleIds.add(uid);
		// for (int uid : ws.unitLists.enemyLostRobots)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, false);
		// for (int uid : ws.unitLists.enemyNewRobots)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, true);
		// TODO handle own units correctly
		for (int uid : ws.unitLists.myRobots) {
			if (ws.gc.unit(uid).location().isOnMap())
				obstacleIds.add(uid);
		}
		// for (int uid : ws.unitLists.myDiedRobots)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, false);
		// for (int uid : ws.unitLists.myNewRobots)
		// jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, true);
		jps.map.resetDynamic();
		for (int uid : obstacleIds) {
			jps.map.setDynamic(new HashablePoint(ws.getMLoc(uid)).point, true);
		}
		long t1 = System.nanoTime();
		computationTimeNs += t1 - t0;
	}
}
