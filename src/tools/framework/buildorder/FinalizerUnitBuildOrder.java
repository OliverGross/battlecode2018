package tools.framework.buildorder;

import java.util.ArrayList;
import java.util.Set;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import tools.framework.WorldState;

public class FinalizerUnitBuildOrder implements UnitBuildOrderEvaluator {

	public final int minUnitsToCheckForKnights = 5;
	public final int maxRoundToProduceKnights = 100;

	public final double startRangerHealerRatio = 3;
	public final int maxNumberFightingUnits = 120;
	public final int beginRoundLimitFightingUnits = 300;

	public final int minimumFightingUnitsOnWalledMaps = 16;
	public final int probablyInCombatRound = 400;

	int healers;
	int rangers;
	int knights;
	int mages;
	int workers;
	int rockets;

	int totalDamageDealers;
	int fightingRobots;

	public int winningSpree = 0;
	public final int maxWinningSpreeToPause = 50;
	public final int weReallyNeedARocketRound = 600;
	boolean rocketsBuildable = false;

	@Override
	public UnitType getNextUnit(WorldState ws, Unit factory) {
		countUnits(ws);

		rocketsBuildable = ws.gc.researchInfo().getLevel(UnitType.Rocket) >= 1;
		// EMERGENCY, PRIO ON WORKER IF THEY ARE MISSING
		if (workers == 0)
			return UnitType.Worker;

		if (ws.planetWon && rocketsBuildable) {
			if (enoughRockets(1.0)) {
				return chooseFightingUnit(ws, factory);
			} else {
				return null;
			}
		}

		if (ws.pga.enemyIsWalled && rocketsBuildable) {
			if (enoughRockets(1.0)
					|| totalDamageDealers < minimumFightingUnitsOnWalledMaps) {
				return chooseFightingUnit(ws, factory);
			} else {
				return null;
			}
		}

		if (!enoughRockets(0.75) && ws.gc.round() >= weReallyNeedARocketRound) {
			return null;
		}
		// check for winningSpree
		if (winningSpree > maxWinningSpreeToPause
				&& ws.gc.round() >= probablyInCombatRound) {
			return null;
		}
		// STOP PRODUCTION, TOO MUCH UNITS
		if ((totalDamageDealers + healers) > maxNumberFightingUnits
				&& ws.gc.round() > beginRoundLimitFightingUnits) {
			return null;
		}

		// LATE GAME UNIT CAP
		if (totalDamageDealers > 30 && ws.gc.round() > 680) {
			return null;
		}
		return chooseFightingUnit(ws, factory);

	}

	boolean enoughRockets(double evacuationPercentage) {
		// TODO
		return evacuationPercentage * (fightingRobots + workers) < rockets * 8;
	}

	UnitType chooseFightingUnit(WorldState ws, Unit factory) {

		if (totalDamageDealers < minUnitsToCheckForKnights
				&& ws.gc.round() < maxRoundToProduceKnights) {
			if (ws.pga.preferKnights || closestFactory(ws, factory) < 7)
				return UnitType.Knight;
		}

		// RANGER VS HEALER
		if (preferHealerBuildOverRanger(ws)) {
			return UnitType.Healer;
		} else {
			return UnitType.Ranger;
		}

	}

	private boolean preferHealerBuildOverRanger(WorldState ws) {
		double maxRanger = Math.min(
				ws.gc.startingMap(ws.gc.planet()).getHeight(),
				ws.gc.startingMap(ws.gc.planet()).getWidth())
				* (ws.pga.inverseChokeness < 1.5 ? 1.0 : 2.0);
		// System.out.println("I dont want more than " + maxRanger + "
		// ranger!");
		double rangerAndHealer = rangers + healers;
		double nbrRangersPerUnit = 1.0 / (1.0 + 1.0 / startRangerHealerRatio
				+ rangerAndHealer / maxRanger);
		// System.out.println(
		// "Current ratio of ranger to total number" + nbrRangersPerUnit);
		double nbrRangers = nbrRangersPerUnit * (rangerAndHealer + 1);
		// System.out.println("current target of nbrRangers:" + nbrRangers);
		return nbrRangers <= rangers;
	}

	void countUnits(WorldState ws) {
		workers = ws.unitLists.myUnits.get(UnitType.Worker).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Worker);
		healers = ws.unitLists.myUnits.get(UnitType.Healer).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Healer);
		rangers = ws.unitLists.myUnits.get(UnitType.Ranger).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Ranger);
		knights = ws.unitLists.myUnits.get(UnitType.Knight).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Knight);
		mages = ws.unitLists.myUnits.get(UnitType.Mage).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Mage);
		rockets = ws.unitLists.myUnits.get(UnitType.Rocket).size();

		totalDamageDealers = rangers + knights + mages;

		if (totalDamageDealers + healers >= fightingRobots) {
			winningSpree++;
		} else {
			winningSpree = 0;
		}
		fightingRobots = totalDamageDealers + healers;

	}

	ArrayList<MapLocation> enemyFactoryLocations = new ArrayList<>();
	ArrayList<MapLocation> facML = new ArrayList<>();

	private int closestFactory(WorldState ws, Unit factory) {
		enemyFactoryLocations.clear();
		facML.clear();
		if (!factory.location().isOnMap())
			return 1000;

		MapLocation myFactoryLocation = factory.location().mapLocation();
		facML.add(myFactoryLocation);

		Set<Integer> enemyFactories = ws.unitLists.enemyUnits
				.get(UnitType.Factory);

		int closestDist = 1000;

		for (int enemyId : enemyFactories) {
			if (ws.gc.canSenseUnit(enemyId)) {
				Unit enemy = ws.gc.unit(enemyId);
				if (enemy.location().isOnMap()) {
					enemyFactoryLocations.add(enemy.location().mapLocation());
				}

			}
		}

		for (MapLocation enem : enemyFactoryLocations) {
			int dist = ws.pathfinding.getOpenPathDistance(facML, enem);
			if (dist == -1) {
				continue;
			} else {
				closestDist = Math.min(closestDist, dist);
			}
		}
		return closestDist;
	}

}
