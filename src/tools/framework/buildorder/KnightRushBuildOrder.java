package tools.framework.buildorder;

import bc.Unit;
import bc.UnitType;
import tools.framework.WorldState;

public class KnightRushBuildOrder implements UnitBuildOrderEvaluator {

	@Override
	public UnitType getNextUnit(WorldState ws, Unit factory) {
		int healers = ws.unitLists.myUnits.get(UnitType.Healer).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Healer);
		int rangers = ws.unitLists.myUnits.get(UnitType.Ranger).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Ranger);
		int knights = ws.unitLists.myUnits.get(UnitType.Knight).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Knight);
		int mages = ws.unitLists.myUnits.get(UnitType.Mage).size()
				+ ws.unitLists.currentlyProducing.get(UnitType.Mage);

		int totalother = rangers + knights + mages;
		int prioRanger = 6;
		if (healers == 0)
			healers = 1;
		int prioHealer = totalother / healers;

		if (prioHealer > prioRanger) {
			return UnitType.Healer;
		} else {
			return UnitType.Knight;
		}
	}

}
