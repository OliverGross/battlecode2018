package tools.framework.plans;

import java.util.ArrayList;
import java.util.List;

import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import tools.framework.Behaviour;
import tools.framework.BehaviourPlan;
import tools.framework.WorldState;
import tools.framework.behaviours.RocketBuildBehaviour;
import tools.framework.behaviours.RocketLaunchBehaviour;
import tools.framework.behaviours.RocketLoadBehaviour;

public class MarsProgramPlan extends BehaviourPlan {

	// latest round to launch even
	public final long latestLaunch = 745;
	public final int maxNumBuildableRockets = 4;
	public final int maxNumLoadableRockets = 4;
	public final int maxNumLaunchableRockets = 4;
	public final int minScientist = 2;

	public ArrayList<Integer> rocketScientist = new ArrayList<Integer>();
	public ArrayList<Integer> rocketFactories = new ArrayList<Integer>();
	public ArrayList<Integer> buildableRockets = new ArrayList<Integer>();
	// Location of unfinished workSites
	public List<MapLocation> constructionSites = new ArrayList<MapLocation>();
	public ArrayList<Integer> launchableRockets = new ArrayList<Integer>();
	public ArrayList<Integer> loadableRockets = new ArrayList<Integer>();
	public List<MapLocation> loadableRocketsSites = new ArrayList<MapLocation>();
	public ArrayList<Integer> evacuees = new ArrayList<Integer>();

	
	public long rocketResearchLevel;

	public MarsProgramPlan(WorldState worldState) {
		super(worldState);
		initResources(worldState);
		
		// Behaviours
		Behaviour rocketLaunchBehaviour = new RocketLaunchBehaviour(worldState, this);
		Behaviour rocketLoadBehaviour = new RocketLoadBehaviour(worldState, this);
		Behaviour rocketBuildBehaviour = new RocketBuildBehaviour(worldState, this);
		
		addBehaviour(rocketLaunchBehaviour);
		addBehaviour(rocketBuildBehaviour);
		addBehaviour(rocketLoadBehaviour);

	}

	public void initResources(WorldState ws) {
		ws.resources.assignAllFreeUnitsToPlan(pid);
	}

	public void updateResources(WorldState ws) {
		ws.resources.assignAllFreeUnitsToPlan(pid);
		rocketScientist = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Worker);
		rocketFactories = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Factory);
		evacuees = ws.resources.getUnitsByPlan(pid);
		evacuees.removeAll(rocketScientist);
		evacuees.removeAll(rocketFactories);
		evacuees.removeIf(id -> !ws.gc.unit(id).location().isOnMap());
		ArrayList<Integer> allRockets = ws.resources.getUnitsByPlanAndType(pid,
				UnitType.Rocket);
		launchableRockets.clear();
		loadableRockets.clear();
		buildableRockets.clear();
		constructionSites.clear();
		loadableRocketsSites.clear();
		for (int rocket_id : allRockets) {
			Unit rocket = ws.gc.unit(rocket_id);
			if (rocket.structureIsBuilt() == 0) {
				buildableRockets.add(rocket_id);
				constructionSites.add(rocket.location().mapLocation());
			} else if (rocket.structureGarrison().size() < rocket.structureMaxCapacity()
					&& rocket.location().isOnMap()) {
				loadableRockets.add(rocket_id);
				loadableRocketsSites.add(rocket.location().mapLocation());
			} else {
				launchableRockets.add(rocket_id);
			}
		}

	}

	@Override
	public void tick(WorldState ws) {
		updateResources(ws);
		rocketResearchLevel = ws.gc.researchInfo().getLevel(UnitType.Rocket);


	}

}
