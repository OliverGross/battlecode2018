package tools.framework.plans;

import java.util.ArrayList;
import java.util.List;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;

public class RangerAttackPlanSprint extends Plan {

	public RangerAttackPlanSprint(WorldState worldState) {
		super(worldState);
		// lpfs = new HashMap<Integer, LocalPathFinder>();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tick(WorldState ws) {
		// Map<Integer, LocalPathFinder> newLpfs = new HashMap<Integer,
		// LocalPathFinder>();
		for (int i = 0; i < ws.gc.myUnits().size(); i++) { // Iterate over all
															// units
			int uid = ws.gc.myUnits().get(i).id();
			if (ws.gc.unit(uid).unitType() == UnitType.Factory
					|| (ws.gc.unit(uid).unitType() == UnitType.Rocket
							&& ws.gc.planet() == Planet.Mars)) {
				for (Direction d : Direction.values()) {
					if (ws.gc.canUnload(uid, d))
						ws.gc.unload(uid, d);
				}
			}
			if (ws.gc.unit(uid).unitType() != UnitType.Ranger) // only take
																// ranger (not
																// effective,
																// should be
																// changed, ToDo
				continue;
			if (ws.gc.unit(uid).location().isInGarrison())
				continue;
			VecUnit nearbys = ws.gc.senseNearbyUnitsByTeam(
					ws.gc.unit(uid).location().mapLocation(), 5000, ws.opp);
			if (nearbys.size() == 0) {
				nearbys = ws.gc.startingMap(Planet.Earth).getInitial_units();
			}
			// Kite and attack logic, just read through
			List<MapLocation> targetPoints = new ArrayList<MapLocation>();
			int besttarget = -1;
			long distance = 5555;
			Unit bestTargetUnit = null;
			for (int j = 0; j < nearbys.size(); j++) {
				if (nearbys.get(j).team() != ws.opp)
					continue;
				int tid = nearbys.get(j).id();
				targetPoints.add(nearbys.get(j).location().mapLocation());
				MapLocation loc = nearbys.get(j).location().mapLocation();
				long disHelper = loc.distanceSquaredTo(
						ws.gc.unit(uid).location().mapLocation());
				if (ws.gc.unit(uid).attackHeat() < 10) {
					if (disHelper < distance && disHelper >= 20) {
						if (ws.gc.canSenseUnit(tid)) {
							Unit target = ws.gc.unit(tid);
							if (bestTargetUnit == null
									|| bestTargetUnit
											.unitType() == UnitType.Factory
									|| bestTargetUnit
											.unitType() == UnitType.Worker
									|| bestTargetUnit
											.unitType() == UnitType.Rocket) {
								besttarget = tid;
								distance = disHelper;
								bestTargetUnit = target;
							} else {
								if (target.unitType() != UnitType.Factory
										&& target.unitType() != UnitType.Rocket
										&& target
												.unitType() != UnitType.Worker) {
									besttarget = tid;
									distance = disHelper;
									bestTargetUnit = target;
								}
							}
						}
					}
				} else {
					if (disHelper < distance) {
						besttarget = tid;
						distance = disHelper;

					}
				}
			}

			try {
				ws.gc.unit(besttarget);
			} catch (RuntimeException e) {
				besttarget = -1;
				System.out.println("I lost my target");
			}
			// LocalPathFinder lpf;
			// if (lpfs.containsKey(uid))
			// lpf = lpfs.get(uid);
			// else
			// lpf = new LocalPathFinder(ws.pathfinding.jps);
			if (besttarget >= 0) {
				boolean tooFar = ws.gc.unit(besttarget).location().mapLocation()
						.distanceSquaredTo(
								ws.gc.unit(uid).location().mapLocation()) > 50;
				boolean tooClose = ws.gc.unit(besttarget).location()
						.mapLocation().distanceSquaredTo(
								ws.gc.unit(uid).location().mapLocation()) < 20;
				boolean attackAble = ws.gc.canAttack(uid, besttarget)
						&& ws.gc.unit(uid).attackHeat() < 10;

				if (!tooFar && !tooClose && attackAble) {
					ws.gc.attack(uid, besttarget);
					// if (ws.gc.isMoveReady(uid)) {
					// if (ws.gc.unit(besttarget)
					// .unitType() == UnitType.Knight
					// || ws.gc.unit(besttarget).unitType() == UnitType.Mage) {
					// System.out.println(
					// "I attacked, but now I'm scared");
					// Direction enemyDir = ws.pathfinding.getNextDirection(
					// ws.gc.unit(uid).location().mapLocation(),
					// targetPoints);
					// if (enemyDir == null) {
					//
					// } else {
					// Direction kiteDirection = ws.lll.kite(uid, enemyDir);
					// System.out.println("Im scared of the knight coming from"
					// + enemyDir + "so I move" + kiteDirection);
					// if (ws.gc.isMoveReady(uid)
					// && ws.gc.canMove(uid, kiteDirection)) {
					// ws.gc.moveRobot(uid, kiteDirection);
					// }
					// }
					// }
					// }
				} else if (tooFar) {
					ws.pathfinding.goTo(uid, targetPoints);// , lpf);
					// newLpfs.put(uid, lpf);
				} else if (tooClose) {
					Direction enemyDir = ws.pathfinding.getNextDirection(uid,
							targetPoints);// , lpf);
					// newLpfs.put(uid, lpf);
					if (enemyDir == null) {
						// continue;
					} else {
						Direction kiteDirection = ws.lll.kite(uid, enemyDir);
						if (ws.gc.isMoveReady(uid)
								&& ws.gc.canMove(uid, kiteDirection)) {
							ws.gc.moveRobot(uid, kiteDirection);
							// continue;
						}
					}

					// ws.lll.goTo(uid, bestLoc);
					// } if(ws.gc.isMoveReady(uid)) {

				} else if (ws.gc.unit(besttarget).unitType() == UnitType.Knight
						|| ws.gc.unit(besttarget).unitType() == UnitType.Mage) {
					// System.out.println(
					// "SCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAryy
					// KNIIIIIIIIIIIIIIIIIIIGHTS!!!!!!!!!!!!!");
					Direction enemyDir = ws.pathfinding.getNextDirection(uid,
							targetPoints);// , lpf);
					// newLpfs.put(uid, lpf);
					if (enemyDir == null) {

					} else {
						Direction kiteDirection = ws.lll.kite(uid, enemyDir);
						// System.out.println("Im scared of the knight coming
						// from"
						// + enemyDir + "so I move" + kiteDirection);
						if (ws.gc.isMoveReady(uid)
								&& ws.gc.canMove(uid, kiteDirection)) {
							ws.gc.moveRobot(uid, kiteDirection);
						}
					}

				} else if (ws.gc.unit(besttarget)
						.unitType() == UnitType.Ranger) {
					// should move closer but not to close
					// } else {
					// ws.lll.goTo(uid, targetPoints)
				}

			} else if (besttarget == -1) {
				ws.pathfinding.goTo(uid, targetPoints); // , lpf);
				// newLpfs.put(uid, lpf);
			}
			this.status = Status.RUNNING;

		}
		// lpfs = newLpfs;
	}

}
