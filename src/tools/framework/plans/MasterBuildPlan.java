package tools.framework.plans;

import java.util.Set;
import bc.Unit;
import bc.UnitType;
import bc.bc;
import tools.framework.Plan;
import tools.framework.Status;
import tools.framework.WorldState;
import tools.framework.buildorder.UnitBuildOrderEvaluator;

public class MasterBuildPlan extends Plan {

	UnitBuildOrderEvaluator evaluator;

	public MasterBuildPlan(WorldState ws, UnitBuildOrderEvaluator evaluator) {
		super(ws);
		this.evaluator = evaluator;
	}

	@Override
	protected void tick(WorldState ws) {
		Set<Integer> myFactories = ws.unitLists.myUnits.get(UnitType.Factory);

		for (int factoryId : myFactories) {
			Unit factory = ws.gc.unit(factoryId);
			if (factory.isFactoryProducing() == 1
					|| factory.structureIsBuilt() == 0)
				continue;

			UnitType typeToBuild = evaluator.getNextUnit(ws, factory);

			if(typeToBuild != null) {
				int cost = (int) bc.costOf(typeToBuild,
						ws.gc.researchInfo().getLevel(typeToBuild));
				if (ws.gc.canProduceRobot(factoryId, typeToBuild)
						&& ws.resources.getAvailableKarbonite(pid) >= cost) {
					ws.gc.produceRobot(factoryId, typeToBuild);
					this.status = Status.RUNNING;
				}
			}
		}
	}

}
