package tools.framework;

public abstract class Evaluator {
	public double priority;

	public Evaluator(double priority) {
		this.priority = priority;
	}

	public abstract double getEvaluation(Plan plan, WorldState ws);

	public final double evaluate(Plan plan, WorldState ws) {
		return this.getEvaluation(plan, ws) * priority;
	}
}
