package tools.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import bc.MapLocation;
import bc.UnitType;
import tools.framework.plans.LandingSiteExplorerPlan;

public class ResourceState {
	/**
	 * Assignment of UnitIds to PlanIds
	 */
	HashMap<Integer, Integer> unit2PlanAssignment = new HashMap<Integer, Integer>();
	/**
	 * Storing a temporary blocking
	 */
	// HashMap<Integer, Integer> unitRoundsTillFreedom = new HashMap<Integer,
	// Integer>();

	/**
	 * WorldState
	 */
	WorldState ws;

	public ResourceState(WorldState worldState) {
		ws = worldState;
		// update();
	}

	/**
	 * Handles deaths and newly generated units
	 * 
	 * @param ws(WorldState)
	 */
	// public void update() {
	// addUnits(ws.gc.myUnits());
	// removeAbsent(ws);
	// // tickTemporaryBlocks();
	// karboniteUpdate();
	// }

	// ##################################################################################
	// Karbonite
	HashMap<Integer, Long> karboniteSaved = new HashMap<>();

	public void karboniteUpdate() {
		karboniteSaved.clear();
	}

	public void saveKarbonite(long amount, int planId) {
		long currSaved = 0;
		if (karboniteSaved.containsKey(planId)) {
			currSaved = karboniteSaved.get(planId);
		}
		currSaved += amount;
		karboniteSaved.put(planId, currSaved);
	}

	public long getAvailableKarbonite(int planId) {
		long currAvailable = ws.gc.karbonite();
		for (int i : karboniteSaved.keySet()) {
			if (i != planId) {
				currAvailable -= karboniteSaved.get(i);
			}
		}
		return currAvailable;
	}
	// ##################################################################################

	// private void addUnits(VecUnit units) {
	// for (int i = 0; i < units.size(); i++) {
	// addUnit(units.get(i));
	// }
	// }

	// private void addUnit(Unit unit) {
	// if (unit.team() == ws.me)
	// addMyUnit(unit);
	// }

	public void addMyUnit(int uid) {
		// resource state vectors
		unit2PlanAssignment.putIfAbsent(uid, 0);
		// unitType.put(unit.id(), unit.unitType());
		// unitRoundsTillFreedom.put(unit.id(), -1);
	}

	/**
	 * Try to sense unit, otherwise delete all occurrences.
	 * 
	 * @param ws
	 */
	public void removeAllAbsentUnits() {
		List<Integer> markedForRemoval = new ArrayList<Integer>();
		for (int uid : unit2PlanAssignment.keySet()) {
			if (!ws.unitLists.allMyUnits.contains(uid))
				markedForRemoval.add(uid);
		}
		for (int uid : markedForRemoval)
			removeAbsentUnit(uid);
	}

	public void removeAbsentUnit(int uid) {
		if (unit2PlanAssignment.remove(uid) == null)
			throw new RuntimeException(
					"Try removing an unit, which did not exist in the resource plan!");
	}

	/**
	 * Update the rounds left for block
	 */
	// private void tickTemporaryBlocks() {
	// for (HashMap.Entry<Integer, Integer> entry : unitRoundsTillFreedom
	// .entrySet()) {
	// if (entry.getValue() == 0) {
	// unitPlan.put(entry.getKey(), 0);
	// }
	// entry.setValue(entry.getValue() - 1);
	// }
	// }

	/**
	 * Return all units associated with a specific plan
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlan(int pid) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			if (entry.getValue() == pid)
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all units associated with a plan and a certain type.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getUnitsByPlanAndType(int pid, UnitType type) {
		ArrayList<Integer> unit_ids = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			if (entry.getValue() == pid
					&& ws.gc.unit(entry.getKey()).unitType() == type)
				unit_ids.add(entry.getKey());
		}
		return unit_ids;
	}

	/**
	 * Return all unassigned units.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getAllUnits() {
		return getUnitsByPlan(0);
	}

	/**
	 * Return all unassigned units of certain type.
	 * 
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public ArrayList<Integer> getFreeUnitsByType(UnitType type) {
		return getUnitsByPlanAndType(0, type);
	}

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	// public void assignFreeToPlan(int pid) {
	// assignFreeToPlan(pid, -1);
	// }

	/**
	 * Assign all unassigned units to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignAllFreeUnitsToPlan(int pid) {
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			if (entry.getValue() == 0) {
				entry.setValue(pid);
				// unitRoundsTillFreedom.put(entry.getValue(), rounds);
			}
		}
	}

	/**
	 * Assign all unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	// public void assignAllFreeUnitsByTypeToPlan(int pid, UnitType type) {
	// assignAllFreeUnitsByTypeToPlan(pid, type, -1);
	// }

	/**
	 * Assign all unassigned units of specified type to given plan for specified
	 * time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignAllFreeUnitsByTypeToPlan(int planId, UnitType type) {
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			if (entry.getValue() == 0 && ws.unitLists.myUnits.get(type)
					.contains(entry.getKey())) {
				entry.setValue(planId);
				// unitRoundsTillFreedom.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign maximum of max unassigned units of specified type to given plan.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	// public void assignSomeFreeByType(int pid, UnitType type, int maxAssigned)
	// {
	// assignSomeFreeByType(pid, type, -1, maxAssigned);
	// }

	/**
	 * Assign maximum of max unassigned units of specified type to given plan
	 * for specified time.
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @param max
	 * @return ArrayList<Integer>(Unit_ids)
	 */
	public void assignSomeFreeByType(int pid, UnitType type, int max) {
		int assignments = 0; // check for max
		for (Iterator<HashMap.Entry<Integer, Integer>> it = unit2PlanAssignment
				.entrySet().iterator(); it.hasNext() && assignments < max;) {
			HashMap.Entry<Integer, Integer> entry = it.next();
			if (entry.getValue() == 0 && ws.unitLists.myUnits.get(type)
					.contains(entry.getKey())) {
				assignments++;
				entry.setValue(pid);
				// unitRoundsTillFreedom.put(entry.getKey(), rounds);
			}
		}
	}

	/**
	 * Assign all resource_ids to the plan
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @return (assignment failed?) true: false
	 */
	// public boolean assignByIds(ArrayList<Integer> resource_ids, int pid) {
	// return assignByIds(resource_ids, pid, -1);
	// }

	/**
	 * Assign all resource_ids to the plan for a given time
	 * 
	 * @param pid(Plan.id)
	 * @param type(UnitType)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	// public boolean assignByIds(ArrayList<Integer> resource_ids, int pid,
	// int rounds) {
	// boolean success = true;
	// for (int id : resource_ids) {
	// success = success && assignById(id, pid, rounds);
	// }
	// return success;
	// }

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	// public boolean assignById(int id, int pid) {
	// return assignById(id, pid, -1);
	// }

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	public boolean assignById(int id, int pid) {
		// is already known?
		if (unit2PlanAssignment.containsKey(id)) {
			if (unit2PlanAssignment.get(id) == 0) {
				unit2PlanAssignment.put(id, pid);
				// unitRoundsTillFreedom.put(id, rounds);
				return true;
			} else {
				return false;
			}
		}
		// // check if newly generated
		// else if (ws.gc.canSenseUnit(id)) {
		// if (ws.gc.)
		// addUnit(ws.gc.unit(id));
		// return assignById(id, pid);
		else {
			throw new RuntimeException("You are trying to get the unit with id:"
					+ id + " which does not exist!");
		}
	}

	/**
	 * Assign resource to plan
	 * 
	 * @param unit
	 * @param pid(Plan.id)
	 * @return (assignment failed?) true: false
	 */
	// public boolean assignByUnit(Unit unit, int pid) {
	// return assignByUnit(unit, pid, -1);
	// }

	/**
	 * Assign resource to plan
	 * 
	 * @param pid(Plan.id)
	 * @param rounds
	 * @return (assignment failed?) true: false
	 */
	// public boolean assignByUnit(Unit unit, int pid, int rounds) {
	// // is already known?
	// if (unit2PlanAssignment.containsKey(unit.id())) {
	// if (unit2PlanAssignment.get(unit.id()) == 0) {
	// unit2PlanAssignment.put(unit.id(), pid);
	// unitRoundsTillFreedom.put(unit.id(), rounds);
	// return true;
	// } else {
	// return false;
	// }
	// }
	// // check if newly generated
	// else {
	// addUnit(unit);
	// return assignById(unit.id(), pid, rounds);
	// }
	// }

	/**
	 * Deletes all assignment of units to plans.
	 */
	public void resetAllAssignments() {
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			entry.setValue(0);
		}
	}

	/**
	 * Deletes all assignments to specified plans.
	 * 
	 * @param pid
	 */
	public void freeUnitsFromPlan(int pid) {
		for (HashMap.Entry<Integer, Integer> entry : unit2PlanAssignment
				.entrySet()) {
			if (entry.getValue() == pid)
				entry.setValue(0);
		}
	}

	/**
	 * Deletes assignment of given unit.
	 * 
	 * @param uid
	 */
	public void setUnitFreeById(int uid) {
		if (unit2PlanAssignment.containsKey(uid)) {
			unit2PlanAssignment.put(uid, 0);
		}
	}

	private int rocketLocationCounter = 0;

	public MapLocation getNextRocketLocation() {
		MapLocation loc = LandingSiteExplorerPlan.getLocFromTeamArray(ws,
				rocketLocationCounter);
		rocketLocationCounter++;
		return loc;
	}
}
