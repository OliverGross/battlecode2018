package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;

public class RangerFocusTargetBehaviour extends Behaviour {

	HashMap<Integer, Integer> unitToTargetMapping = new HashMap<>();
	HashMap<Integer, TargetInfo> enemyToTargetInfoMapping = new HashMap<>();

	HashMap<Integer, MapLocation> standOffTargets = new HashMap<>();
	HashMap<Integer, MoveInfo> moveInfos = new HashMap<>();

	@Override
	public void act(WorldState ws) {
		if (!ws.isRangerAttackTurn())
			return;
		if (ws.unitLists.myUnits.get(UnitType.Ranger).isEmpty())
			return;

		standOffResolution(ws);

		// clear AFTER standOffResolution, bc we need the points from last
		// round.
		unitToTargetMapping.clear();
		enemyToTargetInfoMapping.clear();
		standOffTargets.clear();

		// current order for instant kills
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Rocket), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Factory), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Mage), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Healer), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Ranger), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Knight), ws);
		targetEnemies(ws.unitLists.enemyUnits.get(UnitType.Worker), ws);

		// TODO just for checking purposes remove this later
		// int totalAttackingUnits = 0;
		// for (TargetInfo ti : enemyToTargetInfoMapping.values())
		// totalAttackingUnits += ti.unitsThatTarget.size();
		// System.out.println("This round " + ws.round + " there are "
		// + totalAttackingUnits + " rangers attacking!");

		// we do not iterate through finalTargetting at the moment, bc the
		// entries in currentBestTargetting are still there
		// do all the attacks
		for (Entry<Integer, Integer> entry : unitToTargetMapping.entrySet()) {
			if (ws.gc.canAttack(entry.getKey(), entry.getValue())) {
				standOffTargets.put(entry.getKey(),
						ws.gc.unit(entry.getValue()).location().mapLocation());
				ws.gc.attack(entry.getKey(), entry.getValue());
			} else {
				System.out.println("could not attack, this should not happen!");
			}
		}
	}

	private void standOffResolution(WorldState ws) {
		for (int unitId : ws.unitLists.myUnits.get(UnitType.Ranger)) {
			if (standOffTargets.containsKey(unitId)) {
				standOff(ws.gc.unit(unitId), standOffTargets.get(unitId), ws);
			}
		}
	}

	private void standOff(Unit ranger, MapLocation target, WorldState ws) {
		if (ranger == null || target == null || !ranger.location().isOnMap())
			return;

		MoveInfo info = null;
		if (moveInfos.containsKey(ranger.id())) {
			info = moveInfos.get(ranger.id());
		} else {
			info = new MoveInfo(ranger);
			moveInfos.put(ranger.id(), info);
		}
		if (info != null) {
			if (info.standOff(ranger)) {
				Direction d = ws.pathfinding.getNextDirection(ranger.id(),
						target);
				MapLocation moveLocation = ranger.location().mapLocation()
						.add(d);
				if (moveLocation.distanceSquaredTo(target) > ranger
						.rangerCannotAttackRange()) {
					ws.pathfinding.goTo(ranger.id(), target);
				} else {
					// distance too low
				}
			}
		}
	}

	// The higher the value, the better to focus
	private int typeValue(UnitType type) {
		if (type == null) {
			System.out.println("typeValue was null");
			return 0;
		}

		switch (type) {
		case Rocket:
			return 1;
		case Worker:
			return 2;
		case Factory:
			return 3;
		case Knight:
			return 4;
		case Healer:
			return 5;
		case Ranger:
			return 6;
		case Mage:
			return 7;
		default:
			return 0;

		}
	}

	private void targetEnemies(Set<Integer> enemyUnits, WorldState ws) {
		ArrayList<Integer> currentAttackingUnits = new ArrayList<>();

		for (int unitId : enemyUnits) {
			currentAttackingUnits.clear();
			// for every enemy :
			if (!ws.gc.canSenseUnit(unitId))
				continue;
			Unit enemy = ws.gc.unit(unitId);
			if (enemy == null || !enemy.location().isOnMap())
				continue;

			// ArrayList<Unit> myUnitsNearL = getRangersInRange(enemy, ws);
			VecUnit myUnitsNear = ws.gc.senseNearbyUnitsByTeam(
					enemy.location().mapLocation(), 50, ws.me);
			// check all my units

			int currentDamage = 0;

			// System.out.println(
			// "Trying to kill:" + WorldState.unitToString(enemy));

			// for every ranger in attackrange of enemy
			for (int i = 0; i < myUnitsNear.size(); i++) {
				Unit u = myUnitsNear.get(i);
				if (u.unitType() != UnitType.Ranger)
					continue;
				if (!ws.gc.isAttackReady(u.id()))
					continue;
				if (!ws.gc.canAttack(u.id(), unitId))
					continue;

				// System.out.println("This unit might be able to:"
				// + WorldState.unitToString(u));
				TargetInfo currentTarget = null;
				if (unitToTargetMapping.containsKey(u.id())) {
					int currentTargetId = unitToTargetMapping.get(u.id());
					if (enemyToTargetInfoMapping.containsKey(currentTargetId)) {
						currentTarget = enemyToTargetInfoMapping
								.get(currentTargetId);
					} else {
						throw new RuntimeException(
								"unitToTargetmapping got something, but no infomapping?!");
					}
				}

				if (currentTarget != null) {
					// System.out.println("It has already a target:"
					// + WorldState.unitToString(ws.gc
					// .unit(currentTarget.targetId)));
					if (evaluate(u, enemy, currentTarget,
							currentAttackingUnits.size())) {
						// System.out.println("but changed its
						// mind!");
						currentTarget.deleteAttacker(u);
						currentAttackingUnits.add(u.id());
						if (enemy.unitType() == UnitType.Knight) {
							currentDamage += u.damage() - enemy.knightDefense();
						} else {
							currentDamage += u.damage();
						}
					}
				} else {
					currentAttackingUnits.add(u.id());
					if (enemy.unitType() == UnitType.Knight) {
						currentDamage += u.damage() - enemy.knightDefense();
					} else {
						currentDamage += u.damage();
					}
				}
				// System.out.println("curdamage:" + currentDamage);

				if (currentDamage >= enemy.health()) {
					break;
				}
			}

			TargetInfo enemyInfo = new TargetInfo(currentAttackingUnits, enemy);
			enemyToTargetInfoMapping.put(enemy.id(), enemyInfo);

			for (int id : currentAttackingUnits) {
				unitToTargetMapping.put(id, enemy.id());
			}
		}

	}

	// this seems to be really slow
	// private ArrayList<Unit> getRangersInRange(Unit enemy, WorldState ws) {
	// ArrayList<Unit> ret = new ArrayList<>();
	// for (int id : ws.unitLists.myUnits.get(UnitType.Ranger)) {
	// Unit ranger = ws.gc.unit(id);
	// if (ranger == null || !ranger.location().isOnMap())
	// continue;
	//
	// long distance = enemy.location().mapLocation()
	// .distanceSquaredTo(ranger.location().mapLocation());
	// if (distance <= ranger.attackRange()) {
	// ret.add(ranger);
	// }
	// }
	// return ret;
	// }

	private boolean evaluate(Unit u, Unit enemy, TargetInfo currentTarget,
			int nbrUnitsAlreadyAttackingEnemy) {
		// System.out.println("Comparing units:");
		if (currentTarget.isDead()) {
			// System.out.println(
			// "I am helping to kill my current target, so sorry, cant help
			// here!");
			return false;
		}
		if (typeValue(enemy.unitType()) > typeValue(currentTarget.type)) {
			// System.out.println(
			// "I am proud to help against such an overachiever!");
			return true;
		} else if (typeValue(enemy.unitType()) < typeValue(
				currentTarget.type)) {
			// System.out.println(
			// "Well I am targeting an overachiever, I am not trying to help
			// fight a small minion!");
			return false;
		}
		if (currentTarget.location != null) {
			double distance = Math.sqrt(u.location().mapLocation()
					.distanceSquaredTo(enemy.location().mapLocation()));
			double distanceCurrentTarget = Math.sqrt(u.location().mapLocation()
					.distanceSquaredTo(currentTarget.location));
			if (distanceCurrentTarget > 2 * distance) {
				// System.out.println(
				// "Well its closer and looks bigger, so i am gonna help you!");
				return true;
			}
		} else
			throw new RuntimeException(
					"Wow you reached unreachable Code, because the currentTarget had no valid location!");
		int enemyHealthLeft = (int) enemy.health()
				- (30 - (enemy.unitType() == UnitType.Knight
						? (int) enemy.knightDefense()
						: 0)) * nbrUnitsAlreadyAttackingEnemy;
		if (currentTarget.leftHealth() + 30
				- currentTarget.defense < enemyHealthLeft) {
			// System.out.println(
			// "Nah, my current target is almost dead, I'll go for it!");
			return false;
		} else {
			// System.out.println(
			// "Ok your unit is also close to dead, I am gonna help you, maybe
			// someone else is gonna join soon!");
			return true;
		}

	}
}
