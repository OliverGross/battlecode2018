package tools.framework.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import bc.Direction;
import bc.MapLocation;
import bc.Planet;
import bc.Unit;
import bc.UnitType;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.RangerAttackPlan;

public class RangerMoveBehaviour extends Behaviour {

	class Area {
		Set<Unit> dangerTooClose;
		// Set<Unit> enemiesTooFar;
		Set<Unit> harmlessTooClose;
		Set<Unit> unitsInIntervall;
		Unit ranger;

		public Area(Set<Unit> dangerTooClose, Set<Unit> harmlessTooClose,
				Set<Unit> unitsInIntervall, Unit ranger) {
			this.dangerTooClose = dangerTooClose;
			// this.enemiesTooFar = enemiesTooFar;
			this.harmlessTooClose = harmlessTooClose;
			this.unitsInIntervall = unitsInIntervall;
			this.ranger = ranger;
		}
	}

	RangerAttackPlan rangerAttackPlan;
	List<MapLocation> enemyLocations;
	private final int intervallMagicNumber = 16;

	public RangerMoveBehaviour(RangerAttackPlan plan) {
		this.rangerAttackPlan = plan;
		enemyLocations = new ArrayList<>();
	}

	@Override
	public void act(WorldState ws) {
		// compute enemyLocations once and not for all units
		enemyLocations.clear();
		Set<Integer> allEnemies = ws.unitLists.allEnemyUnits;

		for (int uid : allEnemies) {
			if (ws.gc.canSenseUnit(uid)) {
				Unit u = ws.gc.unit(uid);
				if (u.location().isOnMap()) {
					enemyLocations.add(u.location().mapLocation());
				}
			}
		}

		boolean enemiesInSight = true;

		// no enemies in sight, get startlocations
		if (enemyLocations.size() == 0) {
			enemiesInSight = false;
			Planet planet = ws.gc.planet();
			int nbrRandomLocations = (planet == Planet.Earth) ? 0 : 3;
			// add a few random points
			int w = (int) ws.gc.startingMap(planet).getWidth();
			int h = (int) ws.gc.startingMap(planet).getHeight();
			while (enemyLocations.size() < nbrRandomLocations) {
				int x = ws.rand.nextInt(w);
				int y = ws.rand.nextInt(h);
				MapLocation loc = new MapLocation(planet, x, y);
				if (ws.gc.startingMap(planet).isPassableTerrainAt(loc) == 1)
					enemyLocations.add(loc);
			}
			// add enemy starting positions on earth
			if (planet == Planet.Earth) {
				VecUnit startEnemies = ws.gc.startingMap(Planet.Earth)
						.getInitial_units();
				for (int i = 0; i < startEnemies.size(); i++) {
					if (startEnemies.get(i).team() == ws.opp) {
						enemyLocations.add(
								startEnemies.get(i).location().mapLocation());
					}
				}
			}
		}

		// if splitmap and no enemy is in sight dont move
		if (!enemiesInSight && ws.pga.enemyIsWalled)
			return;

		// for all units we own
		for (int rangerId : rangerAttackPlan.getRangersOnMap()) {
			if (!ws.gc.unit(rangerId).location().isOnMap())
				continue;

			if (ws.gc.isMoveReady(rangerId)) {
				Unit ranger = ws.gc.unit(rangerId);
				if ((ws.gc.getLastRoundDamage(rangerId) >= ranger.health())
						|| (ranger.health() <= 60)) {
					VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
							ranger.location().mapLocation(), 65, ws.opp);
					Set<Unit> enemiesSet = new HashSet<>();
					if (enemies.size() != 0) {
						for (int i = 0; i < enemies.size(); i++) {
							enemiesSet.add(enemies.get(i));
						}
						kite(enemiesSet, ws, ranger);
					}
					else {
						ws.pathfinding.goTo(rangerId, ws.getMLocs(ws.unitLists.myUnits.get(UnitType.Ranger)));
					}
				} else {
					if (ws.gc.isAttackReady(rangerId)) {
						moveOffensive(rangerId, ws);
					} else {
						moveDefensive(rangerId, ws);
					}
				}
			}
		}

		// TODO is this necessary? (oliver: yes!)
		ws.updateUnitLists();
	}

	/**
	 * use this with caution, only ment for overcharge
	 * 
	 * @param unit
	 * @param ws
	 */
	public void actForOne(Unit unit, WorldState ws) {
		// at the start of this, enemyLocations should be filled! be careful
		int rangerId = unit.id();
		if (!ws.gc.unit(rangerId).location().isOnMap())
			return;

		if (ws.gc.isMoveReady(rangerId)) {
			Unit ranger = ws.gc.unit(rangerId);
			if (ranger.health() < 50) {
				VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
						ranger.location().mapLocation(), 65, ws.opp);
				Set<Unit> enemiesSet = new HashSet<>();
				if (enemies.size() != 0) {
					for (int i = 0; i < enemies.size(); i++) {
						enemiesSet.add(enemies.get(i));
					}
					kite(enemiesSet, ws, ranger);
				}
			} else {
				if (ws.gc.isAttackReady(rangerId)) {
					moveOffensive(rangerId, ws);
				} else {
					moveDefensive(rangerId, ws);
				}
			}
		}

	}

	private void moveDefensive(int rangerId, WorldState ws) {
		Area area = checkSurroundings(ws, rangerId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.ranger);
			return;
		}

		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.ranger);
			}
			// if mages of knights are in intervall kite them
			// if no danger is close, but enemies are in intervall, do nothing
			// and keep your movement

			/*
			 * MoveDefensive does not move foreward on standoff
			 * if(area.standOff) { ws.pathfinding.goTo(rangerId,
			 * enemyLocations); }
			 */
			return;
		}
		if (!enemyLocations.isEmpty()) {
			ws.pathfinding.goTo(area.ranger.id(), enemyLocations);
			// if no enemies are too close or in intervall, go in direction of
			// next enemy
			// TODO check if you get shot rightafter, then maybe keep your
			// movement
			return;
		}

		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.ranger);
			return;
		}

	}

	private void moveOffensive(int rangerId, WorldState ws) {

		Area area = checkSurroundings(ws, rangerId);

		if (!area.dangerTooClose.isEmpty()) {
			kite(area.dangerTooClose, ws, area.ranger);
			return;
		}

		if (!area.unitsInIntervall.isEmpty()) {
			Set<Unit> dangers = new HashSet<>();
			for (Unit u : area.unitsInIntervall) {
				if (u.unitType() == UnitType.Knight
						|| u.unitType() == UnitType.Mage) {
					// knights and mages are scary RUNAWAAAAAAAAAAAAAAAAAAY
					dangers.add(u);
				}
			}
			if (!dangers.isEmpty()) {
				kite(dangers, ws, area.ranger);
			}
			return;
		}

		if (!enemyLocations.isEmpty()) {
			ws.pathfinding.goTo(area.ranger.id(), enemyLocations);
			return;
		}

		if (!area.harmlessTooClose.isEmpty()) {
			kite(area.harmlessTooClose, ws, area.ranger);
			return;
		}

		// TODO nothing on the map, spread out to scout the whole map
	}

	private Area checkSurroundings(WorldState ws, int rangerId) {
		Unit ranger = ws.gc.unit(rangerId);
		MapLocation rangerLocation = ranger.location().mapLocation();

		Set<Unit> dangerTooClose = new HashSet<>();
		Set<Unit> harmlessTooClose = new HashSet<>();
		Set<Unit> unitsInIntervall = new HashSet<>();

		VecUnit enemies = ws.gc.senseNearbyUnitsByTeam(
				ranger.location().mapLocation(), ranger.attackRange(), ws.opp);

		for (int i = 0; i < enemies.size(); i++) {
			Unit enemy = enemies.get(i);
			if (enemy == null || !enemy.location().isOnMap())
				continue;
			long distance = enemy.location().mapLocation()
					.distanceSquaredTo(rangerLocation);

			int intervallDistance = intervallMagicNumber;

			if (distance < intervallDistance) {
				if (enemy.unitType() != UnitType.Factory
						&& enemy.unitType() != UnitType.Rocket
						&& enemy.unitType() != UnitType.Worker) {
					dangerTooClose.add(enemy);
				} else {
					harmlessTooClose.add(enemy);
				}
			} else {
				if (distance >= ranger.attackRange()) {
					// enemiesTooFar.add(enemy);
				} else {
					unitsInIntervall.add(enemy);
				}
			}
		}
		return new Area(dangerTooClose, harmlessTooClose, unitsInIntervall,
				ranger);
	}

	private void kite(Set<Unit> enemiesToKite, WorldState ws, Unit ranger) {
		// kite rangers, mages and knights differently! Rangers/Mages via
		// shootingdistance (straight line), knights vs JPS

		List<MapLocation> knightLocations = new ArrayList<>();
		Set<MapLocation> rangeLocations = new HashSet<>();
		List<MapLocation> harmlessLocations = new ArrayList<>();

		for (Unit u : enemiesToKite) {
			if (u.unitType() == UnitType.Knight) {
				knightLocations.add(u.location().mapLocation());
			} else {
				if (u.unitType() == UnitType.Mage
						|| u.unitType() == UnitType.Ranger) {
					rangeLocations.add(u.location().mapLocation());
				} else {
					harmlessLocations.add(u.location().mapLocation());
				}
			}
		}

		Direction kiteD = Direction.Center;
		Direction knightKiteDirection = Direction.Center;
		Direction harmlessKiteDirection = Direction.Center;

		HashMap<Direction, Integer> kitingDesire = new HashMap<>();

		// Get direction to kite Knights
		if (!knightLocations.isEmpty()) {
			Direction enemyKnightDir = ws.pathfinding
					.getNextDirection(ranger.id(), knightLocations);
			if (enemyKnightDir != null) {
				knightKiteDirection = ws.lll.kite(ranger.id(), enemyKnightDir);
				int desire = 0;
				if (kitingDesire.containsKey(knightKiteDirection)) {
					desire = kitingDesire.get(knightKiteDirection);
				}
				desire += knightLocations.size();
				kitingDesire.put(knightKiteDirection, desire);
			}
		}

		// Get direction to kite Ranged
		if (!rangeLocations.isEmpty()) {
			for (MapLocation loc : rangeLocations) {
				Direction rDir = ws.lll.kite(ranger.id(),
						ranger.location().mapLocation().directionTo(loc));
				int prevDesire = 0;
				if (kitingDesire.containsKey(rDir)) {
					prevDesire = kitingDesire.get(rDir);
				}
				kitingDesire.put(rDir, prevDesire + 1);
			}
		}

		// In case only harmless creatures are found
		if (rangeLocations.isEmpty() && knightLocations.isEmpty()) {
			if (!harmlessLocations.isEmpty()) {
				harmlessKiteDirection = ws.pathfinding
						.getNextDirection(ranger.id(), harmlessLocations);
				if (harmlessKiteDirection != null) {
					kiteD = ws.lll.kite(ranger.id(), harmlessKiteDirection);
					if (ws.gc.isMoveReady(ranger.id())
							&& ws.gc.canMove(ranger.id(), kiteD)) {
						ws.gc.moveRobot(ranger.id(), kiteD);
					}
					return;
				}
			}
		}

		// find out best direction
		int bestval = 0;
		for (Direction d : Direction.values()) {
			if (d == Direction.Center) {
				continue;
			}
			if (kitingDesire.containsKey(d)) {
				int val = kitingDesire.get(d);
				if (val > bestval) {
					kiteD = d;
					bestval = val;
				}
			}
		}

		// Kite
		if (ws.gc.isMoveReady(ranger.id())
				&& ws.gc.canMove(ranger.id(), kiteD)) {
			ws.gc.moveRobot(ranger.id(), kiteD);
		}
	}

}
