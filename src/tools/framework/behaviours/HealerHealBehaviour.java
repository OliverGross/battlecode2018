package tools.framework.behaviours;

import java.util.ArrayList;

import bc.Unit;
import bc.VecUnit;
import tools.framework.Behaviour;
import tools.framework.WorldState;
import tools.framework.plans.HealerPlan;

public class HealerHealBehaviour extends Behaviour {

	HealerPlan healerPlan;

	public HealerHealBehaviour(HealerPlan healerPlan) {
		this.healerPlan = healerPlan;
	}

	@Override
	public void act(WorldState ws) {
		ArrayList<Integer> healers = healerPlan.getHealersOnMap();
		for (int healerId : healers) {
			if (ws.gc.isHealReady(healerId)) {
				Unit healer = ws.gc.unit(healerId);
				if (!healer.location().isOnMap())
					continue;

				VecUnit allysInRange = ws.gc.senseNearbyUnitsByTeam(
						healer.location().mapLocation(), healer.attackRange(),
						ws.me);

				Unit target = getUnitToHeal(allysInRange, ws);
				if (target != null) {
					if (ws.gc.canHeal(healerId, target.id())) {
						ws.gc.heal(healerId, target.id());
					}
				}
			}

		}

	}

	private Unit getUnitToHeal(VecUnit allysInRange, WorldState ws) {
		Unit bestTarget = null;
		long bestTargetHealthPrio = 0;
		for (int i = 0; i < allysInRange.size(); i++) {
			Unit target = allysInRange.get(i);
			if (bestTarget == null) {
				if (target.health() != target.maxHealth()) {
					bestTarget = target;
					bestTargetHealthPrio = (target.maxHealth()
							- target.health())
							* ws.gc.getLastRoundDamage(target.id());

				}
			} else {
				long missingHealthPrio = (target.maxHealth() - target.health())
						* ws.gc.getLastRoundDamage(target.id());

				if (missingHealthPrio > bestTargetHealthPrio) {
					bestTarget = target;
					bestTargetHealthPrio = missingHealthPrio;
				}
			}
		}
		return bestTarget;
	}

}
