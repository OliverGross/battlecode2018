package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import tools.pathfinding.Geometry;
import tools.pathfinding.HashablePoint;
import tools.pathfinding.JPS;
import tools.pathfinding.StaticMapGrid;

class JPSTest {

	static JPS jps = new JPS();

	public final int n = 1000;

	@Test
	void testGetPath() {
		int w = 11;
		int h = 11;
		boolean[][] m = new boolean[w][h];
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				m[x][y] = false;
			}
		for (int i = 0; i < h; i++) {
			m[5][i] = true;
			m[7][i] = true;
		}
		m[7][0] = false;
		m[5][8] = false;
		StaticMapGrid testGrid = new StaticMapGrid(m);
		jps.set_map(testGrid);
		int[] start = { 0, 0 };
		int[] finish = { 10, 0 };
		long t0 = System.nanoTime();
		for (int i = 0; i < n; i++) {
			jps.getPath(new HashablePoint(start), new HashablePoint(finish));
		}
		long t1 = System.nanoTime();
		List<HashablePoint> path = jps.getPath(new HashablePoint(start),
				new HashablePoint(finish));
		System.out
				.println("The path found in " + (t1 - t0) * 1.e-6 / n + " ms:");
		String s = "";
		for (HashablePoint a : path) {
			s += ", " + Geometry.toString(a);
		}
		System.out.println(s.substring(2));
		assertEquals(s.substring(2),
				"[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [4, 5], [4, 6], [4, 7], [5, 8], [6, 7], [6, 6], [6, 5], [6, 4], [6, 3], [6, 2], [6, 1], [7, 0], [8, 0], [9, 0], [10, 0]");
		System.out.println("Total path finding computation time:"
				+ JPS.compTime * 1e-9 + " s");
	}

	@ParameterizedTest
	@ValueSource(strings = { "bigmap.txt", "testmap.txt", "lowdense.txt",
			"highdense.txt", "notReachable1.txt", "notReachable2.txt",
			"notReachable3.txt", "multipleGoals.txt", "multipleGoals2.txt",
			"multipleStarts.txt", "multipleStartsGoals.txt", "precompTest.txt",
			"nopath.txt", "nopathMultiples.txt", "bugMap.txt" })
	void testComplicatedGetPath(String mapFile) throws FileNotFoundException {
		mapFile = "src/tests/" + mapFile;
		Scanner scanner = new Scanner(new File(mapFile));
		int w = scanner.nextInt();
		int h = scanner.nextInt();
		boolean[][] m = new boolean[w][h];
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++)
				m[x][y] = false;
		Set<HashablePoint> start = new HashSet<HashablePoint>();
		Set<HashablePoint> goal = new HashSet<HashablePoint>();
		scanner.nextLine();
		int y = h;
		while (scanner.hasNextLine()) {
			y--;
			String line = scanner.nextLine();
			int x = 0;
			for (char c : line.toCharArray()) {
				switch (c) {
				case '#':
					m[x][y] = true;
					break;
				case 'S':
					start.add(new HashablePoint(new int[] { x, y }));
					break;
				case 'G':
					goal.add(new HashablePoint(new int[] { x, y }));
				}
				x++;
			}
		}
		scanner.close();
		assertTrue(start.size() >= 1);
		assertTrue(goal.size() >= 1);
		StaticMapGrid testGrid = new StaticMapGrid(m);
		jps.set_map(testGrid);
		long t0 = System.nanoTime();
		for (int i = 0; i < n; i++) {
			jps.getPath(start, goal);
		}
		long t1 = System.nanoTime();
		List<HashablePoint> path = jps.getPath(start, goal);
		assertNotNull(path);
		System.out
				.println("The path found in " + (t1 - t0) * 1.e-6 / n + " ms:");
		char[][] result = new char[w][h];
		for (y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (testGrid.get(x, y))
					result[x][y] = '#';
				else
					result[x][y] = '.';
			}
		}
		int[] cur = path.get(0).point;
		for (HashablePoint p : start) {
			result[p.get(0)][p.get(1)] = 'S';
		}
		for (int i = 1; i < path.size(); i++) {
			while (!Arrays.equals(cur, path.get(i).point)) {
				int[] dir = Geometry.diff(path.get(i).point, cur);
				int d = Geometry.chebyshev_distance(cur,
						path.get(i).point);
				dir[0] /= d;
				dir[1] /= d;
				cur = Geometry.add(cur, dir);
				result[cur[0]][cur[1]] = 'O';
			}
		}
		for (HashablePoint p : goal) {
			result[p.get(0)][p.get(1)] = 'G';
		}
		String s = "";
		scanner = new Scanner(new File(mapFile));
		scanner.nextLine();
		String compare = "";
		for (y = h - 1; y >= 0; y--) {
			String line = scanner.nextLine();
			compare += line + '\n';
			for (int x = 0; x < w; x++) {
				s += result[x][y];
			}
			s += '\n';
		}
		scanner.close();
		System.out.println(s);
		assertEquals(compare, s);
		System.out.println("Total path finding computation time:"
				+ JPS.compTime * 1e-9 + " s");
	}

	@ParameterizedTest
	@ValueSource(strings = { "bigmap.txt", "testmap.txt", "lowdense.txt",
			"highdense.txt", "notReachable1.txt", "notReachable2.txt",
			"notReachable3.txt", "multipleGoals.txt", "multipleGoals2.txt",
			"multipleStarts.txt", "multipleStartsGoals.txt", "precompTest.txt",
			"nopath.txt", "nopathMultiples.txt", "bugMap.txt" })
	void testComplicatedGetPathWithTargetAsObstacle(String mapFile)
			throws FileNotFoundException {
		mapFile = "src/tests/" + mapFile;
		Scanner scanner = new Scanner(new File(mapFile));
		int w = scanner.nextInt();
		int h = scanner.nextInt();
		boolean[][] m = new boolean[w][h];
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++)
				m[x][y] = false;
		Set<HashablePoint> start = new HashSet<HashablePoint>();
		Set<HashablePoint> goal = new HashSet<HashablePoint>();
		scanner.nextLine();
		int y = h;
		while (scanner.hasNextLine()) {
			y--;
			String line = scanner.nextLine();
			int x = 0;
			for (char c : line.toCharArray()) {
				switch (c) {
				case '#':
					m[x][y] = true;
					break;
				case 'S':
					start.add(new HashablePoint(new int[] { x, y }));
					break;
				case 'G':
					goal.add(new HashablePoint(new int[] { x, y }));
					m[x][y] = true;
				}
				x++;
			}
		}
		scanner.close();
		assertTrue(start.size() >= 1);
		assertTrue(goal.size() >= 1);
		StaticMapGrid testGrid = new StaticMapGrid(m);
		jps.set_map(testGrid);
		long t0 = System.nanoTime();
		for (int i = 0; i < n; i++) {
			jps.getPath(start, goal);
		}
		long t1 = System.nanoTime();
		List<HashablePoint> path = jps.getPath(start, goal);
		assertNotNull(path);
		System.out
				.println("The path found in " + (t1 - t0) * 1.e-6 / n + " ms:");
		char[][] result = new char[w][h];
		for (y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (testGrid.get(x, y))
					result[x][y] = '#';
				else
					result[x][y] = '.';
			}
		}
		int[] cur = path.get(0).point;
		for (HashablePoint p : start) {
			result[p.get(0)][p.get(1)] = 'S';
		}
		for (int i = 1; i < path.size(); i++) {
			while (!Arrays.equals(cur, path.get(i).point)) {
				int[] dir = Geometry.diff(path.get(i).point, cur);
				int d = Geometry.chebyshev_distance(cur,
						path.get(i).point);
				dir[0] /= d;
				dir[1] /= d;
				cur = Geometry.add(cur, dir);
				result[cur[0]][cur[1]] = 'O';
			}
		}
		for (HashablePoint p : goal) {
			result[p.get(0)][p.get(1)] = 'G';
		}
		String s = "";
		scanner = new Scanner(new File(mapFile));
		scanner.nextLine();
		String compare = "";
		for (y = h - 1; y >= 0; y--) {
			String line = scanner.nextLine();
			compare += line + '\n';
			for (int x = 0; x < w; x++) {
				s += result[x][y];
			}
			s += '\n';
		}
		scanner.close();
		System.out.println(s);
		assertEquals(compare, s);
		System.out.println("Total path finding computation time:"
				+ JPS.compTime * 1e-9 + " s");
	}
}
