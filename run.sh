#!/bin/sh
docker pull battlecode/battlecode-2018
docker stop $(docker ps -q)
docker container rm $(docker container ls -aq)
docker volume rm $(docker volume ls -q)$
docker volume prune

./update.sh
rm players/replay.bc18
docker run -it --privileged -p 16147:16147 -p 6147:6147 -v $PWD/players:/player battlecode/battlecode-2018
