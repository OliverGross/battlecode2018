#!/bin/sh
python -m SimpleHTTPServer 8080 & 
if which xdg-open > /dev/null
then
  xdg-open http://localhost:8080/tinyview/
elif which gnome-open > /dev/null
then
  gnome-open http://localhost:8080/tinyview/
elif which start > /dev/null
then 
  start http://localhost:8080/tinyview/
elif which open > /dev/null
then 
  open http://localhost:8080/tinyview/
fi
